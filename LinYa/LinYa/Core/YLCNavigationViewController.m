//
//  YLCNavigationViewController.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNavigationViewController.h"

@interface YLCNavigationViewController ()<UINavigationBarDelegate>

@end

@implementation YLCNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateNavigationBar];
    self.navigationBar.translucent = NO;
    self.delegate = self;
}

- (NSDictionary *)navTitleAttributes {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    return attributes;
}
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    if (self = [super initWithRootViewController:rootViewController]) {
        
        //        self.navigationBarHidden = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        WS(weakSelf);
        if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.interactivePopGestureRecognizer.delegate = weakSelf;
            
            self.interactivePopGestureRecognizer.enabled = YES;
        }
        
    }
    return self;
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
    [super pushViewController:viewController animated:animated];
}
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    if (navigationController.viewControllers.count == 1) {
        navigationController.interactivePopGestureRecognizer.enabled = NO;
        navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}
- (UIColor *)navBackgroundColor {
    return YLC_THEME_COLOR;
}

- (UIImage *)navBackgroundImage {
    return [[UIImage imageNamed:@"navagation_background"] stretchableImageWithLeftCapWidth:2 topCapHeight:32];
}

- (void)updateNavigationBar {

    UIColor *navBackgroundColor = [self navBackgroundColor];
    

    [self.navigationBar setBarTintColor:navBackgroundColor];

    NSDictionary *titleAttribute = [self navTitleAttributes];
    if (!JK_IS_DICT_NIL(titleAttribute)) {
        [self.navigationBar setTitleTextAttributes:titleAttribute];
    }
    
    [self.navigationBar setTintColor:[UIColor whiteColor]];
}

- (void)updateNavigationBarBgWithCurrentBackgroundImage {
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navagation_background"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsCompact];
    [self.navigationBar setShadowImage:[UIImage new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
