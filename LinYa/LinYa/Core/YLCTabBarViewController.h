//
//  YLCTabBarViewController.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTabBarViewController : UITabBarController
- (void)hiddenRed;
- (void)getUnreadCount;
@end
