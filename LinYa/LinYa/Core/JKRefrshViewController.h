//
//  JKRefrshViewController.h
//  SHBao
//
//  Created by 初程程 on 16/11/4.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface JKRefrshViewController : YLCBaseViewController
@property (nonatomic, strong) UIScrollView *scrollView;
- (UIScrollView *)customScrollView;//创建刷新视图
- (void)reloadData;//下拉刷新调用的方法
- (void)doneLoadingTableViewData;//在数据加载完成的处理方法
- (void)asyncLoadDataWithPage:(NSInteger)page pageSize:(NSInteger)pageSize;//加载数据方法
- (void)didTriggerLoadNextPageData;//上拉加载执行的方法

- (BOOL)isAllDataLoaded;//判断是否已经加载完成
@end
