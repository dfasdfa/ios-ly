//
//  JKRefrshViewController.m
//  SHBao
//
//  Created by 初程程 on 16/11/4.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import "JKRefrshViewController.h"

@interface JKRefrshViewController ()
{
    UIScrollView *mScrollView;
}
@end

@implementation JKRefrshViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView = [self customScrollView];

    
    [self.view addSubview:self.scrollView];
    
    [self setUpRefreshHeadAndFoot];

}
- (void)setUpRefreshHeadAndFoot{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadData)];
    
    // 设置文字
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"释放刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新 ..." forState:MJRefreshStateRefreshing];
    
    // 设置字体
    header.stateLabel.font = [UIFont systemFontOfSize:15];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:14];
    
    // 设置颜色
    header.stateLabel.textColor = YLC_GRAY_COLOR;
    header.lastUpdatedTimeLabel.textColor = YLC_GRAY_COLOR;
    
    // 马上进入刷新状态
//    [header beginRefreshing];
    
    self.scrollView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(didTriggerLoadNextPageData)];
    
    // 设置文字
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载 ..." forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多" forState:MJRefreshStateNoMoreData];
    
    // 设置字体
    footer.stateLabel.font = [UIFont systemFontOfSize:14];
    
    // 设置颜色
    footer.stateLabel.textColor = YLC_GRAY_COLOR;
    
    self.scrollView.mj_footer = footer;

}
//
- (UIScrollView *)customScrollView {
    if (nil == mScrollView) {
        mScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, self.view.frameHeight)];
        //        mScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    return mScrollView;
}

- (void)reloadData {

    [self asyncLoadDataWithPage:1 pageSize:PAGE_LIMIT];
}
- (void)asyncLoadDataWithPage:(NSInteger)page pageSize:(NSInteger)pageSize{
    
}
- (void)doneLoadingTableViewData{
    
    [self.scrollView.mj_header endRefreshing];
    
    [self.scrollView.mj_footer endRefreshing];
    
    if ([self isAllDataLoaded]==YES) {
        [self.scrollView.mj_footer endRefreshingWithNoMoreData];
    }else{
        [self.scrollView.mj_footer resetNoMoreData];
    }
    
}
- (void)didTriggerLoadNextPageData{

}

- (BOOL)isAllDataLoaded{
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
