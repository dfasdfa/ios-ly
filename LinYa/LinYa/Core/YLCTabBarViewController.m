//
//  YLCTabBarViewController.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTabBarViewController.h"
#import "YLCTabbar.h"
#import "YLCNavigationViewController.h"
#import "AppDelegate.h"
#import "YLCBaseSendViewController.h"
#import "YLCSendGoodsViewController.h"
#import "YLCSendViewController.h"
#import "YLCSendQuestionViewController.h"
#import "YLCSendTrainViewController.h"
#import "HWVideoVC.h"
#import "YLCSendJobViewController.h"
#import "ShootVideoViewController.h"
@interface YLCTabBarViewController ()<UITabBarControllerDelegate,YLCBaseSendViewControllerDelegate>

@end

@implementation YLCTabBarViewController
{
    YLCTabbar *tabbar;
}
- (void)hiddenRed{
    [tabbar hiddenRed];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTabbar];
}
- (void)getUnreadCount{
    [tabbar getUnreadCount];
}
- (void)setTabbar{
    
    tabbar = [[YLCTabbar alloc]init];
    
    [tabbar didselectedIrrBtnWithBlock:^{
        
        if (![YLCLoginManager checkLoginStateWithTarget:self]) {
            return ;
        }
        [self gotoSend];
//        [self setSelectedIndex:2];
    }];
    
    [self setValue:tabbar forKey:@"tabBar"];
    
}

- (void)gotoSend{
    YLCBaseSendViewController *con = [[YLCBaseSendViewController alloc] init];
    
    con.delegate = self;
    
    YLCNavigationViewController *nav = [[YLCNavigationViewController alloc] initWithRootViewController:con];
    
    [self presentViewController:nav animated:NO completion:nil];
}
- (void)gotoSendDetist{
    YLCSendViewController *con  = [[YLCSendViewController alloc] init];
    
    [(UINavigationController *)self.selectedViewController pushViewController:con animated:YES];
}
- (void)didSelectAtIndex:(NSInteger)index{
    if (index==0) {
        [self gotoSendDetist];
    }
    if (index==1) {
        [self gotoSendQuestion];
    }
    if (index==2) {
        [self gotoGoods];
    }
    if (index==3) {
        [self sendJob];
    }
    if (index==4) {
        [self gotoSendTrainWithType:0];
    }
    if (index==5) {
        [self gotoSendTrainWithType:1];
    }
    if (index==6) {
        [self videoRecord];
    }
}
- (void)sendJob{
    YLCSendJobViewController *con = [[YLCSendJobViewController alloc] init];
    
    [(UINavigationController *)self.selectedViewController pushViewController:con animated:YES];

}
- (void)videoRecord{
//    HWVideoVC *con = [[HWVideoVC alloc] init];
//
//    [self presentViewController:con animated:YES completion:^{
//
//    }];
    
    
    ShootVideoViewController *con = [[ShootVideoViewController alloc] init];
    
    YLCNavigationViewController *nav  = [[YLCNavigationViewController alloc] initWithRootViewController:con];
    
    [self presentViewController:nav animated:YES completion:nil];
}
- (void)gotoSendQuestion{
    
    YLCSendQuestionViewController *con = [[YLCSendQuestionViewController alloc] init];
    
    [(UINavigationController *)self.selectedViewController pushViewController:con animated:YES];

}
- (void)gotoGoods{
    YLCSendGoodsViewController *con = [[YLCSendGoodsViewController alloc] init];
    
    [(UINavigationController *)self.selectedViewController pushViewController:con animated:YES];
}
- (void)gotoSendTrainWithType:(NSInteger)type{
    YLCSendTrainViewController *con = [[YLCSendTrainViewController alloc] init];
    
    con.sendType = type;
    
    [(UINavigationController *)self.selectedViewController pushViewController:con animated:YES];
}
- (void)setSelectedIndex:(NSUInteger)selectedIndex{
    [super setSelectedIndex:selectedIndex];
    
    YLCTabBarViewController *tabbar = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
    
    YLCNavigationViewController *nav = tabbar.selectedViewController;
    
    [self tabBarController:tabbar didSelectViewController:nav];
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
    [(YLCNavigationViewController *)viewController popToRootViewControllerAnimated:NO];
//    self.tabBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
