//
//  YLCTabBarConfig.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTabBarConfig.h"
#import "YLCTabBarViewController.h"
#import "YLCNavigationViewController.h"
#import "YLCHomeViewController.h"
#import "YLCDentistBaseViewController.h"
#import "YLCMessageViewController.h"
#import "YLCMineViewController.h"
#import "YLCRYConversationListViewController.h"
@implementation YLCTabBarConfig
+ (YLCTabBarViewController *)configController{
    
    YLCTabBarViewController *tabBar = [[YLCTabBarViewController alloc] init];
    
    YLCHomeViewController *homeVc = [[YLCHomeViewController alloc] init];
    
    homeVc.hidesBottomBarWhenPushed = NO;
    
    YLCNavigationViewController *homeNav = [[YLCNavigationViewController alloc] initWithRootViewController:homeVc];
    
    homeNav.tabBarItem  = [self homeItem];
    
    YLCDentistBaseViewController *dentistVc = [[YLCDentistBaseViewController alloc] init];
    
    dentistVc.hidesBottomBarWhenPushed = NO;
    
    YLCNavigationViewController *dentistNav = [[YLCNavigationViewController alloc] initWithRootViewController:dentistVc];
    
    dentistNav.tabBarItem = [self dentistItem];
    
    YLCBaseViewController *baseVc = [[YLCBaseViewController alloc] init];
    
    baseVc.hidesBottomBarWhenPushed = NO;
    
    YLCNavigationViewController *baseNav = [[YLCNavigationViewController alloc] initWithRootViewController:baseVc];
    
    YLCRYConversationListViewController *messageVc = [[YLCRYConversationListViewController alloc] init];
    
    messageVc.hidesBottomBarWhenPushed = NO;
    
    YLCNavigationViewController *messageNav = [[YLCNavigationViewController alloc] initWithRootViewController:messageVc];
    
    messageNav.tabBarItem = [self messageItem];
    
    YLCMineViewController *mineVc = [[YLCMineViewController alloc] init];
    
    mineVc.hidesBottomBarWhenPushed = NO;
    
    YLCNavigationViewController *mineNav = [[YLCNavigationViewController alloc] initWithRootViewController:mineVc];
    
    mineNav.tabBarItem = [self mineItem];

    tabBar.viewControllers = @[homeNav,dentistNav,baseNav,messageNav,mineNav];
    
    return tabBar;
    
}
+ (UITabBarItem *)homeItem{
    return [self commonTabBarItemWithTitle:@"首页" normalImageName:@"tabbar_normal1" selectedImageName:@"tabbar_select1"];
}
+ (UITabBarItem *)dentistItem{
    return [self commonTabBarItemWithTitle:@"牙医圈" normalImageName:@"tabbar_normal2" selectedImageName:@"tabbar_select2"];
}
+ (UITabBarItem *)mineItem{
    return [self commonTabBarItemWithTitle:@"我的" normalImageName:@"tabbar_normal5" selectedImageName:@"tabbar_select5"];
}
+ (UITabBarItem *)messageItem{
    return [self commonTabBarItemWithTitle:@"消息" normalImageName:@"tabbar_normal4" selectedImageName:@"tabbar_select4"];
}
+ (UITabBarItem *)commonTabBarItemWithTitle:(NSString *)title
                            normalImageName:(NSString *)normalName
                          selectedImageName:(NSString *)selectedName {
    UIImage *normal = [UIImage imageNamed:normalName];
    UIImage *image = [[normal ccc_resizedImage:CGSizeMake(20, 20) interpolationQuality:kCGInterpolationHigh] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *select = [UIImage imageNamed:selectedName];
    UIImage *selectedImage = [[select ccc_resizedImage:CGSizeMake(20, 20) interpolationQuality:kCGInterpolationDefault] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:selectedImage];
    
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10], NSFontAttributeName, YLC_THEME_COLOR, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:10], NSFontAttributeName, RGB(123, 129, 131), NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    return item;
}
+ (UIImage *)commonScaledImageFromImage:(UIImage *)img {
    CGSize newSize = CGSizeMake(20, 20);
    
    UIGraphicsBeginImageContext(newSize);
    
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
