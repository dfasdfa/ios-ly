//
//  YLCRefreshViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/5/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRefreshViewController.h"

@interface YLCRefreshViewController ()

@end

@implementation YLCRefreshViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupRefresh];
}
- (void)setupRefresh{
    self.scrollView = [self customScrollView];
    
    [self.scrollView setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(reloadData) footAction:@selector(didTriggerLoadNextPageData)];
    
    [self.view addSubview:self.scrollView];
}
- (void)setIsOnlyHead:(BOOL)isOnlyHead{
    if (isOnlyHead) {
        self.scrollView.mj_footer = nil;
    }
}
- (void)beganRefresh{
    [self.scrollView.mj_header beginRefreshing];
}
- (void)resetNoMore{
    [self.scrollView.mj_header endRefreshing];
    [self.scrollView.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [self.scrollView.mj_header endRefreshing];
    [self.scrollView.mj_footer endRefreshingWithNoMoreData];
}
- (void)reloadData{
    [self fetchDataWithPage:0];
}
- (void)fetchDataWithPage:(NSInteger)page{
    
}
- (void)didTriggerLoadNextPageData{
    
    NSInteger page = [_dataArr count]/[PAGE_LIMIT integerValue];
    
    [self fetchDataWithPage:page];
}
- (UIScrollView *)customScrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    }
    return _scrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
