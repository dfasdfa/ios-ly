//
//  YLCRefreshViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/5/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCRefreshViewController : YLCBaseViewController
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic ,strong)NSArray *dataArr;
@property (nonatomic ,strong)RACSubject *reloadSignal;
@property (nonatomic ,assign)BOOL isOnlyHead;
- (void)beganRefresh;
- (void)resetNoMore;
- (void)loadedAllData;
- (UIScrollView *)customScrollView;
- (void)fetchDataWithPage:(NSInteger)page;
@end
