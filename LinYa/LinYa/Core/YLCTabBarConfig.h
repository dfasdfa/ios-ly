//
//  YLCTabBarConfig.h
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YLCTabBarViewController;
@interface YLCTabBarConfig : NSObject
+ (YLCTabBarViewController *)configController;
@end
