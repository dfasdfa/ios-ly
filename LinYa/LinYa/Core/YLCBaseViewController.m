//
//  YLCBaseViewController.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCBaseViewController ()

@end

@implementation YLCBaseViewController
- (id)init {
    if (self = [super init]) {
        self.hidesBottomBarWhenPushed =YES;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = YLC_COMMON_BACKCOLOR;
    UIBarButtonItem *backItem = [YLCFactory createImageBarButtonWithImageName:@"item_back" selector:nil target:nil];
    
    backItem.style = UIBarButtonItemStylePlain;
    
    self.navigationItem.backBarButtonItem = backItem;
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = [self customEdgesForExtendedLayout];
    [self customOrUpdateNavigationBar];
    
}
- (NSString *)backButtonTitle {
    return @"返回";
}
- (UIRectEdge)customEdgesForExtendedLayout {
    return UIRectEdgeNone;
}
- (void)customOrUpdateNavigationBar {
    // left item
    UIBarButtonItem *leftItem = [self leftBarButtonItem];
    if (nil != leftItem) {
        self.navigationItem.leftBarButtonItem = leftItem;
    }
    // right item
    UIBarButtonItem *rightItem = [self rightBarButtonItem];
    if (nil != rightItem) {
        self.navigationItem.rightBarButtonItem = rightItem;
    }
    
    if ([self leftBarButtonItems]) {
        self.navigationItem.leftBarButtonItems=[self leftBarButtonItems];
    }
    if ([self rightBarButtonItems]) {
        self.navigationItem.rightBarButtonItems=[self rightBarButtonItems];
    }
}
-(NSArray*)leftBarButtonItems
{
    return nil;
}
-(NSArray*)rightBarButtonItems
{
    return nil;
}
- (UIBarButtonItem *)leftBarButtonItem {
    return nil;
}

- (UIBarButtonItem *)rightBarButtonItem {
    return nil;
}

- (void)leftBarButtonItemClicked {
    
}

- (void)rightBarButtonItemClicked {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
