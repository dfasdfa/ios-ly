//
//  JKWebViewViewController.h
//  JKBao
//
//  Created by 初程程 on 16/9/21.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "NJKWebViewProgress.h"
@interface YLCWebViewViewController : YLCBaseViewController<UIWebViewDelegate>
@property (nonatomic, strong) NSURLRequest *request;
@property (nonatomic, strong) NSString *navTitle;
@property (nonatomic, assign) BOOL isModel;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, copy)NSString *shareUrl;
@property (nonatomic, copy)NSString *shareContent;
@property (nonatomic, copy)NSString *shareImageUrl;
@property (nonatomic, assign)BOOL isShare;
+ (void)showShareWebViewWithRequest:(NSURLRequest *)request
                           navTitle:(NSString *)title
                           shareUrl:(NSString *)shareUrl
                       shareContent:(NSString *)shareContent
                      shareImageUrl:(NSString *)shareImageUrl
                            isModel:(BOOL)isModel
                               from:(UIViewController *)controller;
+ (void)showWebViewWithRequest:(NSURLRequest *)request navTitle:(NSString *)title isModel:(BOOL)isModel from:(UIViewController *)controller;//用webview打开连接

+ (void)showWebViewWithContent:(NSString *)content navTitle:(NSString *)title isModel:(BOOL)isModel from:(UIViewController *)controller;//用webview打开Html文件

@end
