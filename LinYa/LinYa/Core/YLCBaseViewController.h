//
//  YLCBaseViewController.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCBaseViewController : UIViewController
- (void)reloadData;
- (void)customOrUpdateNavigationBar;
- (UIBarButtonItem *)leftBarButtonItem;
- (UIBarButtonItem *)rightBarButtonItem;
- (NSArray *)leftBarButtonItems;
- (NSArray *)rightBarButtonItems;
- (void)leftBarButtonItemClicked;
- (void)rightBarButtonItemClicked;
- (NSString *)backButtonTitle;
@end
