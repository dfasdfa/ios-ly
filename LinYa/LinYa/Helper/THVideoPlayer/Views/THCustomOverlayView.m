//
//  THCustomOverlayView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "THCustomOverlayView.h"

@implementation THCustomOverlayView
{
    UIView *toolBarView;
    UISlider *progressSlider;
    UIButton *starBtn;
    UILabel *loadingTimeLabel;
    UILabel *allTimeLabel;
    UIButton *pushBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.playSubject = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    toolBarView = [[UIView alloc] init];
    
    toolBarView.backgroundColor = RGB(24, 29, 33);
    
    [self addSubview:toolBarView];
    
    progressSlider = [[UISlider alloc] init];
    
    progressSlider.minimumTrackTintColor = YLC_THEME_COLOR;
    
    progressSlider.maximumTrackTintColor = YLC_GRAY_COLOR;
    
    [progressSlider setThumbImage:[YLCFactory imageWithImage:[UIImage imageNamed:@"knob_normal"] scaledToSize:CGSizeMake(15, 15)] forState:UIControlStateNormal];
    
    [progressSlider addTarget:self action:@selector(progressSliderChange:) forControlEvents:UIControlEventValueChanged];
    
    [toolBarView addSubview:progressSlider];
    
    starBtn = [YLCFactory createBtnWithImage:@"video_tool_star"];
    
    [starBtn setImage:[UIImage imageNamed:@"video_tool_pass"] forState:UIControlStateSelected];
    
    [starBtn addTarget:self action:@selector(playVideoWithSender:) forControlEvents:UIControlEventTouchUpInside];
    
    [toolBarView addSubview:starBtn];
    
    loadingTimeLabel = [YLCFactory createLabelWithFont:14 color:[UIColor whiteColor]];
    
    [toolBarView addSubview:loadingTimeLabel];
    
    allTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(164, 171, 174)];
    
    [toolBarView addSubview:allTimeLabel];
    
    pushBtn = [YLCFactory createBtnWithImage:@""];
    
    [toolBarView addSubview:pushBtn];
}
- (void)playVideoWithSender:(UIButton *)sender{
    sender.selected = !sender.selected;
    
    [_playSubject sendNext:sender.selected?@"1":@"0"];
}
- (void)progressSliderChange:(id)sender{
    [self.delegate scrubbedToTime:progressSlider.value];
    [self setScrubbingTime:progressSlider.value];
}
- (void)setTitle:(NSString *)title{
    
}
- (void)setCurrentTime:(NSTimeInterval)time duration:(NSTimeInterval)duration{
    NSInteger currentSeconds = ceilf(time);
    NSInteger durationSeconds = ceilf(duration);
    loadingTimeLabel.text = [self formatSeconds:currentSeconds];
    allTimeLabel.text = [NSString stringWithFormat:@"/%@",[self formatSeconds:durationSeconds]];
    progressSlider.minimumValue = 0.0f;
    
    progressSlider.maximumValue = duration;
    
    progressSlider.value = time;
}
- (void)setScrubbingTime:(NSTimeInterval)time{
    progressSlider.value = time;
}
- (void)playbackComplete{
    starBtn.selected = NO;
}
- (NSString *)formatSeconds:(NSInteger)value {
    NSInteger seconds = value % 60;
    NSInteger minutes = value / 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long) minutes, (long) seconds];
}
- (void)setSubtitles:(NSArray *)subtitles{
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    [toolBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [progressSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(toolBarView.mas_left);
        make.right.equalTo(toolBarView.mas_right);
        make.top.equalTo(toolBarView.mas_top);
        make.height.equalTo(20);
    }];
    
    [starBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(toolBarView.mas_left).offset(20);
        make.top.equalTo(progressSlider.mas_bottom).offset(10);
        make.width.equalTo(13);
        make.height.equalTo(16);
    }];
    
    [loadingTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(starBtn.mas_right).offset(10);
        make.centerY.equalTo(starBtn.mas_centerY);
    }];
    
    [allTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(loadingTimeLabel.mas_right);
        make.centerY.equalTo(starBtn.mas_centerY);
    }];
    
    [pushBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(toolBarView.mas_right).offset(20);
        make.centerY.equalTo(toolBarView.mas_centerY);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
