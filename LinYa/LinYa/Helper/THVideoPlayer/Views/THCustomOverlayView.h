//
//  THCustomOverlayView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THTransport.h"
@interface THCustomOverlayView : UIView<THTransport>
@property (weak, nonatomic) id <THTransportDelegate> delegate;
@property (nonatomic ,strong)RACSubject *playSubject;
@end
