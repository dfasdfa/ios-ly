//
//  ImagePicker.m
//  DWImagePicker
//
//  Created by dwang_sui on 16/6/20.
//  Copyright © 2016年 dwang_sui. All rights reserved.
//

#import "ImagePicker.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
//如果有Debug这个宏的话,就允许log输出...可变参数
#ifdef DEBUG
#define DWLog(...) NSLog(__VA_ARGS__)
#else
#define DWLog(...)
#endif


@implementation ImagePicker

static ImagePicker *sharedManager = nil;

+ (ImagePicker *)sharedManager {
    
    @synchronized (self) {
        
        if (!sharedManager) {
            
            sharedManager = [[[self class] alloc] init];
            
        }
        
    }
    
    return sharedManager;
}

#pragma mark ---设置根控制器 弹框添加视图位置 所需图片样式 默认为UIImagePickerControllerEditedImage
- (void)dwSetPresentDelegateVC:(id)vc SheetShowInView:(UIView *)view InfoDictionaryKeys:(NSInteger)integer {
    if (integer==1) {
        
        AVAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
        if (author == AVAuthorizationStatusRestricted || author ==AVAuthorizationStatusDenied){
//            [JKHUDShow showMessageWithTitle:@"没有相册权限，请前去隐私设置中启用访问" target:self];
            return;
        }
    }
    if (integer==0) {
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
        {
//            [JKHUDShow showMessageWithTitle:@"没有相机权限，请前去隐私设置中启用访问" target:self];
            return;
        }
    }
    
    imagePicker = [[UIImagePickerController alloc]init];
    
    imagePicker.view.backgroundColor = [UIColor clearColor];
    
    imagePicker.delegate = self;
    
    self.integer = integer;
    
    if ([vc isKindOfClass:NSClassFromString(@"JKChangeHeadViewController")]) {
        imagePicker.allowsEditing = NO;
    }else{
        imagePicker.allowsEditing = NO;
    }
    self.allowsEditing = imagePicker.allowsEditing;
    
    self.vc = vc;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        self.typeStr = @"支持相机";
        
    }
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        
        self.typeStr = @"支持图库";
        
    }
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        
        self.typeStr = @"支持相片库";
        
    }
    if (integer == 0) {
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunicode-whitespace"
        
        [self.vc presentViewController:imagePicker animated:YES completion:nil];
        
#pragma clang diagnostic pop
        
        
    }else if (integer == 1) {
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunicode-whitespace"
        
        [self.vc presentViewController:imagePicker animated:YES completion:nil];
        
#pragma clang diagnostic pop
        
    }
}

#pragma mark ---获取设备支持的类型与选中之后的图片
- (void)dwGetpickerTypeStr:(pickerTypeStr)pickerTypeStr pickerImagePic:(pickerImagePic)pickerImagePic {
    
    if (pickerTypeStr) {
        
        pickerTypeStr(self.typeStr);
        
    }
    
    self.pickerImagePic = ^(UIImage *image) {
        
        pickerImagePic(image);
        
    };
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
        UIImage *image = [[UIImage alloc] init];
        
//        NSArray *array = @[@"UIImagePickerControllerMediaType",
//                           @"UIImagePickerControllerOriginalImage",
//                           @"UIImagePickerControllerEditedImage",
//                           @"UIImagePickerControllerCropRect",
//                           @"UIImagePickerControllerMediaURL",
//                           @"UIImagePickerControllerReferenceURL",
//                           @"UIImagePickerControllerMediaMetadata",
//                           @"UIImagePickerControllerLivePhoto"];
    
//        if (self.integer) {
    
            image = [info objectForKey:UIImagePickerControllerOriginalImage];
            
//        }else {
//            
//            image = [info objectForKey:array[2]];
//            
//        }
        
        if (self.pickerImagePic) {
            
            self.pickerImagePic(image);
            
        }

        [self.vc dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    

    
    [self.vc dismissViewControllerAnimated:YES completion:nil];
    
}

@end
