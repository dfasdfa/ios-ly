//
//  DLCollectionTabbarView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "DLCollectionTabbarView.h"
#import "DLCell.h"
#define DL_CELL @"DLCell"
@interface DLCollectionTabbarView()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
@end
@implementation DLCollectionTabbarView
{
    UICollectionView *tabbarTable;
    NSInteger selectIndex;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    tabbarTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    tabbarTable.backgroundColor = [UIColor whiteColor];
    
    [tabbarTable registerClass:[DLCell class] forCellWithReuseIdentifier:DL_CELL];
    
    tabbarTable.delegate = self;
    
    tabbarTable.dataSource = self;
    
    [self addSubview:tabbarTable];
}
- (void)setTabbarItems:(NSArray *)tabbarItems{
    
    _tabbarItems = tabbarItems;
    
    [tabbarTable reloadData];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _tabbarItems.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DLCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DL_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_tabbarItems.count) {
        cell.title = _tabbarItems[indexPath.row];
    }
    if (selectIndex == indexPath.row) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.frameWidth/4, 50);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, DLSlideTabbar:selectAt:)) {
        [self.delegate DLSlideTabbar:self selectAt:indexPath.row];
    }
}
- (void)switchingFrom:(NSInteger)fromIndex to:(NSInteger)toIndex percent:(float)percent{
    if (toIndex<0||toIndex>=_tabbarItems.count) {
        return;
    }
    selectIndex = toIndex;
    
    NSIndexPath *fromIndexPath = [NSIndexPath indexPathForRow:fromIndex inSection:0];;
    
    NSIndexPath *toIndexPath = [NSIndexPath indexPathForRow:toIndex inSection:0];
    
    [tabbarTable reloadItemsAtIndexPaths:@[fromIndexPath,toIndexPath]];
    
    [tabbarTable reloadData];
}
- (void)setSelectedIndex:(NSInteger)selectedIndex{
    selectIndex = selectedIndex;
    [tabbarTable reloadData];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [tabbarTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@synthesize selectedIndex;

@synthesize tabbarCount;

@end
