//
//  DLCollectionTabbarView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLSlideTabbarProtocol.h"

@interface DLCollectionTabbarView : UIView<DLSlideTabbarProtocol>
@property(nonatomic, strong) NSArray *tabbarItems;
@property(nonatomic, weak) id<DLSlideTabbarDelegate> delegate;
- (void)switchingFrom:(NSInteger)fromIndex to:(NSInteger)toIndex percent:(float)percent;
@end
