//
//  DLCell.m
//  YouLe
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "DLCell.h"

@implementation DLCell
{
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        titleLabel = [YLCFactory createLabelWithFont:16];
        
        titleLabel.layer.cornerRadius = 5;
        
        titleLabel.layer.masksToBounds = YES;
        
        titleLabel.textAlignment = NSTextAlignmentCenter;
        
        titleLabel.layer.borderColor = [UIColor clearColor].CGColor;
        
        titleLabel.layer.cornerRadius = 20;
        
        titleLabel.layer.masksToBounds = YES;
        
        titleLabel.layer.borderWidth = 0.5;
        
        [self.contentView addSubview:titleLabel];
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        titleLabel.textColor = YLC_THEME_COLOR;
        titleLabel.layer.borderColor = YLC_THEME_COLOR.CGColor;
    }else{
        
        titleLabel.textColor = RGB(118,129,135);
        titleLabel.layer.borderColor = [UIColor clearColor].CGColor;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(80);
        make.height.equalTo(40);
    }];
}
@end
