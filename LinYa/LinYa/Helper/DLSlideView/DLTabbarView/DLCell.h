//
//  DLCell.h
//  YouLe
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLCell : UICollectionViewCell
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,copy)NSString *title;
@end
