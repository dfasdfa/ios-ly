//
//  YLCTimePickerView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger ,YLCDatePickMode) {
    YLCDatePickYearMonthDay,
    YLCDatePickTime
};
@interface YLCTimePickerView : UIView

@end
