//
//  NSObject+CCModel.h
//  SHBao
//
//  Created by 初程程 on 17/3/9.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CCModel)
+ (id)modelWithDict:(NSDictionary *)dict;
- (NSString *)notNullString;
@end
