//
//  UICollectionViewCell+ShadowAndCorner.m
//  SHBao
//
//  Created by 初程程 on 2017/12/5.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import "UICollectionViewCell+ShadowAndCorner.h"
static const char ShadowViewKey = '\30';
static const char CornerViewKey = '\40';
@implementation UICollectionViewCell (ShadowAndCorner)
- (void)setCcc_shadowView:(UIView *)ccc_shadowView{
    objc_setAssociatedObject(self, &ShadowViewKey, ccc_shadowView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (void)setCcc_backCornerView:(UIView *)ccc_backCornerView{
    objc_setAssociatedObject(self, &CornerViewKey, ccc_backCornerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIView *)ccc_backCornerView{
    return objc_getAssociatedObject(self, &CornerViewKey);
}
- (UIView *)ccc_shadowView{
    return objc_getAssociatedObject(self, &ShadowViewKey);
}
- (void)addShadowAndCorner{
    self.ccc_shadowView = [[UIView alloc] initWithFrame:self.bounds];
    
    self.ccc_backCornerView = [[UIView alloc] initWithFrame:self.bounds];
    
    self.ccc_shadowView.layer.shadowOffset = CGSizeMake(3, 3);
    
    self.ccc_shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    
    self.ccc_shadowView.layer.shadowOpacity = 0.2;
    
    [self.contentView addSubview:self.ccc_shadowView];
    
    self.ccc_backCornerView.layer.cornerRadius = 5;
    
    self.ccc_backCornerView.layer.masksToBounds = YES;
    
    [self.contentView addSubview:self.ccc_backCornerView];
}
@end
