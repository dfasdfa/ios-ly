//
//  NSData+CCAdd.h
//  JKBao
//
//  Created by 初程程 on 16/8/22.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CCAdd)
- (NSString *)md5String ;
- (NSData *)aes256EncryptWithKey:(NSData *)key iv:(NSData *)iv;
- (NSData *)aes256DecryptWithkey:(NSData *)key iv:(NSData *)iv;
- (NSString *)sha1String;
@end
