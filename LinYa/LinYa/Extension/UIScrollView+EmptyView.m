//
//  UIScrollView+EmptyView.m
//  SHBao
//
//  Created by 初程程 on 2017/9/22.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import "UIScrollView+EmptyView.h"
static const char EmptyViewKey = '\3';
static const char EmptyLabelKey = '\4';
@implementation UIScrollView (EmptyView)
- (void)setCcc_emptyView:(YLCEmptyView *)ccc_emptyView{

    objc_setAssociatedObject(self, &EmptyViewKey, ccc_emptyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (void)setCcc_emptyLabel:(UILabel *)ccc_emptyLabel{
    objc_setAssociatedObject(self, &EmptyLabelKey, ccc_emptyLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (YLCEmptyView *)ccc_emptyView{
    return objc_getAssociatedObject(self, &EmptyViewKey);
}
- (UILabel *)ccc_emptyLabel{
    return objc_getAssociatedObject(self, &EmptyLabelKey);
}
- (void)showEmptyViewWithEmptyView:(YLCEmptyView *)emptyView{
    if (!self.ccc_emptyView) {
        [self createEmptyViewWithEmptyView:emptyView];
    }
    [self addSubview:self.ccc_emptyView];
    [self bringSubviewToFront:self.ccc_emptyView];
}
- (void)removeEmptyView{
    [self.ccc_emptyView removeFromSuperview];
}
- (void)createEmptyViewWithEmptyView:(YLCEmptyView *)emptyView{
    self.ccc_emptyView = emptyView;
    self.ccc_emptyView.frame = self.bounds;
}
- (void)setupOnlyHeadRefrshWithTarget:(id)target
                           headAction:(SEL)headAction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:headAction];
    
    // 设置文字
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"释放刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新 ..." forState:MJRefreshStateRefreshing];
    
    // 设置字体
    header.stateLabel.font = [UIFont systemFontOfSize:15];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:14];
    
    // 设置颜色
    header.stateLabel.textColor = YLC_GRAY_COLOR;
    header.lastUpdatedTimeLabel.textColor = YLC_GRAY_COLOR;
    
    // 马上进入刷新状态
    //    [header beginRefreshing];
    
    self.mj_header = header;

}
- (void)setupNormalRefreshHeadAndFootWithTarget:(id)target
                                     headAction:(SEL)headAction
                                     footAction:(SEL)footAction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:target refreshingAction:headAction];
    
    // 设置文字
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"释放刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新 ..." forState:MJRefreshStateRefreshing];
    
    // 设置字体
    header.stateLabel.font = [UIFont systemFontOfSize:15];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:14];
    
    // 设置颜色
    header.stateLabel.textColor = YLC_GRAY_COLOR;
    header.lastUpdatedTimeLabel.textColor = YLC_GRAY_COLOR;
    
    // 马上进入刷新状态
    //    [header beginRefreshing];
    
    self.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:target refreshingAction:footAction];
    
    // 设置文字
    [footer setTitle:@"上拉加载" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载 ..." forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多" forState:MJRefreshStateNoMoreData];
    
    // 设置字体
    footer.stateLabel.font = [UIFont systemFontOfSize:14];
    
    // 设置颜色
    footer.stateLabel.textColor = YLC_GRAY_COLOR;
    
    self.mj_footer = footer;
}

@end
