//
//  NSString+CCAdd.h
//  JKBao
//
//  Created by 初程程 on 16/8/22.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+CCAdd.h"
@interface NSString (CCAdd)
- (NSString *)md5String ;
- (NSString *)subSpace;
- (NSString *)getUUID;
//- (NSString *)notNullString;
@end
