//
//  UIImage+CCAdd.h
//  JKBao
//
//  Created by 初程程 on 16/8/25.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CCAdd)
/**
 *  用颜色转换成图片
 *
 *  @param color 颜色
 *
 *  @return image
 */
+(UIImage*) createImageWithColor:(UIColor*) color;

- (NSString *)base64String;

- (NSString *)allBase64String;

- (NSData *)imageData;

- (UIImage *)createShareImage:(UIImage *)image context:(NSString *)text point:(CGPoint)point;

- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)ccc_thumbnailImage:(NSInteger)thumbnailSize
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)ccc_resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)ccc_resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)ccc_fixOrientation:(UIImage *)aImage;
- (UIImage *)ccc_resizedImage:(CGSize)newSize
                    transform:(CGAffineTransform)transform
               drawTransposed:(BOOL)transpose
         interpolationQuality:(CGInterpolationQuality)quality;
- (CGAffineTransform)ccc_transformForOrientation:(CGSize)newSize;
- (UIImage *)fixOrientation;
- (UIImage*)rotate:(UIImageOrientation)orient;
@end
