//
//  UICollectionViewCell+ShadowAndCorner.h
//  SHBao
//
//  Created by 初程程 on 2017/12/5.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (ShadowAndCorner)
@property (nonatomic ,strong)UIView *ccc_shadowView;
@property (nonatomic ,strong)UIView *ccc_backCornerView;
- (void)addShadowAndCorner;
@end
