//
//  UIScrollView+EmptyView.h
//  SHBao
//
//  Created by 初程程 on 2017/9/22.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (EmptyView)
@property (nonatomic ,strong)YLCEmptyView *ccc_emptyView;
@property (nonatomic ,strong)UILabel *ccc_emptyLabel;

- (void)showEmptyViewWithEmptyView:(YLCEmptyView *)emptyView;
- (void)removeEmptyView;
- (void)setupNormalRefreshHeadAndFootWithTarget:(id)target
                                     headAction:(SEL)headAction
                                     footAction:(SEL)footAction;
- (void)setupOnlyHeadRefrshWithTarget:(id)target
                           headAction:(SEL)headAction;
@end
