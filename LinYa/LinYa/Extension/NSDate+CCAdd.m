//
//  NSDate+CCAdd.m
//  SHBao
//
//  Created by 初程程 on 2017/8/3.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import "NSDate+CCAdd.h"

@implementation NSDate (CCAdd)
- (NSString *)getTimeStringWithFormatter:(NSString *)format{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate:self];
}
@end
