//
//  NSString+CCAdd.m
//  JKBao
//
//  Created by 初程程 on 16/8/22.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import "NSString+CCAdd.h"

@implementation NSString (CCAdd)
- (NSString *)md5String {
    return [[self dataUsingEncoding:NSUTF8StringEncoding] md5String];
}
- (NSString *)subSpace{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
- (NSString *)getUUID{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}
//- (NSString *)notNullString{
//    return [YLCFactory isStringOk:self]?self:@"";
//}
@end
