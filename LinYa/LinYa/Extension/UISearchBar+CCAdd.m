//
//  UISearchBar+CCAdd.m
//  LinYa
//
//  Created by 初程程 on 2018/1/31.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "UISearchBar+CCAdd.h"

@implementation UISearchBar (CCAdd)
- (UITextField *)getField{
    UITextField *searchField;
    NSUInteger numViews = [self.subviews count];
    for(int i = 0;i < numViews; i++) {
        if([[self.subviews objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            searchField = [self.subviews objectAtIndex:i];
        }
    }
    // 如果上述方法找不到searchField,试试下面的方法
    if (searchField == nil) {
        NSArray *arraySub = [self subviews];
        UIView *viewSelf = [arraySub objectAtIndex:0];
        NSArray *arrayView = [viewSelf subviews];
        for(int i = 0;i < arrayView.count; i++) {
            if([[arrayView objectAtIndex:i] isKindOfClass:[UITextField class]]) {
                searchField = [arrayView objectAtIndex:i];
            }
        }
    }
    return searchField;
}
@end
