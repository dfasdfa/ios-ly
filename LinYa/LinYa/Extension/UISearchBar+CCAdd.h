//
//  UISearchBar+CCAdd.h
//  LinYa
//
//  Created by 初程程 on 2018/1/31.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (CCAdd)
- (UITextField *)getField;
@end
