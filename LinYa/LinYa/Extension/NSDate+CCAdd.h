//
//  NSDate+CCAdd.h
//  SHBao
//
//  Created by 初程程 on 2017/8/3.
//  Copyright © 2017年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (CCAdd)
- (NSString *)getTimeStringWithFormatter:(NSString *)format;
@end
