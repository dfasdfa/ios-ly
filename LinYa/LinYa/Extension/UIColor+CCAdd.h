//
//  UIColor+CCAdd.h
//  JKBao
//
//  Created by 初程程 on 16/8/18.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CCAdd)
/**
 *  16进制颜色
 *
 *  @param color 16进制颜色
 *
 *  @return color
 */
+ (UIColor *) colorWithHexString: (NSString *)color;
@end
