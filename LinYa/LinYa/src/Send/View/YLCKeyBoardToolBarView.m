//
//  YLCKeyBoardToolBarView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCKeyBoardToolBarView.h"

@implementation YLCKeyBoardToolBarView
{
    UIButton *faceBtn;
    UIButton *albumBtn;
    UIButton *cameraBtn;
    UIButton *areaBtn;
    UIView *view;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageSigal = [RACSubject subject];
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    
    faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [faceBtn setImage:[UIImage imageNamed:@"send_image"] forState:UIControlStateNormal];
    
    [faceBtn addTarget:self action:@selector(toolDidSelectAtIndex:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:faceBtn];
    
    WS(weakSelf)
    
    [faceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
    
}
- (void)toolDidSelectAtIndex:(UIButton *)sender{
    [_imageSigal sendNext:[NSString stringWithFormat:@"%ld",sender.tag-500]];
}
- (void)showWithDuration:(CGFloat)duration
                    rect:(CGRect)rect{

    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.frame = CGRectMake(0, kScreenHeight-rect.size.height-40, self.frameWidth, 40);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
}
- (void)dissMissWithDuration:(CGFloat)duration {
    
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         self.frame = CGRectMake(0, kScreenHeight, self.frameWidth, 40);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
