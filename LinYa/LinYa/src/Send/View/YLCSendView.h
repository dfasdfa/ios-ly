//
//  YLCSendView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCSendView : UIView
@property (nonatomic ,assign)BOOL hiddenNoti;
- (void)addNoti;
- (void)sendText;
- (void)removeNotification;
@end
