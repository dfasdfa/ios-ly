//
//  YLCKeyBoardToolBarView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCKeyBoardToolBarView : UIView
@property (nonatomic ,strong)RACSubject *imageSigal;
- (void)showWithDuration:(CGFloat)duration
                    rect:(CGRect)rect;
- (void)dissMissWithDuration:(CGFloat)duration;
@end
