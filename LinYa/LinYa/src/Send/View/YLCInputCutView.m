//
//  YLCInputCutView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInputCutView.h"

@implementation YLCInputCutView
{
    UILabel *cutLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    cutLabel = [YLCFactory createLabelWithFont:14];
    
    cutLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:cutLabel];
}
- (void)setInputNumber:(NSInteger )inputNumber{
    
    NSString *inputString = [NSString stringWithFormat:@"%ld",inputNumber];
    
    NSString *text = [NSString stringWithFormat:@"(%ld/50)",inputNumber];
    
    NSAttributedString *attr = [YLCFactory getThreeCountAttributedStringWithString:text FirstCount:1 secondCount:inputString.length thirdCount:4 firstColor:[UIColor blackColor] secondColor:YLC_THEME_COLOR thirdColor:[UIColor blackColor] firstFont:14 secondFont:14 thirdFont:14];
    
    cutLabel.attributedText = attr;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [cutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
