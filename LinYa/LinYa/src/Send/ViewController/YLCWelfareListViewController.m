//
//  YLCWelfareListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWelfareListViewController.h"
#import "YLCShowWelfareListHUDView.h"
@interface YLCWelfareListViewController ()

@end

@implementation YLCWelfareListViewController
{
    YLCShowWelfareListHUDView *walfareView;
    NSArray *dataList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/welfare" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        dataList = responseObject;
        
        walfareView.dataList = dataList;
        
        walfareView.selectList = _selectList.mutableCopy;
    }];
}

- (void)createCustomView{
    walfareView = [[YLCShowWelfareListHUDView alloc] initWithFrame:CGRectZero];
    
    [walfareView.signal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectWalfareWithSelectList:walfareList:)) {
            NSDictionary *dict = x;
            [self.delegate didSelectWalfareWithSelectList:dict[@"select"] walfareList:dict[@"message"]];
        }
    }];
    
    [self.view addSubview:walfareView];
    
    [walfareView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSString *)title{
    return @"福利";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
