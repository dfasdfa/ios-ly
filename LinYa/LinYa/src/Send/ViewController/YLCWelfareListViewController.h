//
//  YLCWelfareListViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCWelfareListViewController;
@protocol YLCWelfareListViewControllerDelegate <NSObject>
- (void)didSelectWalfareWithSelectList:(NSArray *)selectList
                           walfareList:(NSArray *)walfareList;
@end
@interface YLCWelfareListViewController : YLCBaseViewController
@property (nonatomic ,weak)id<YLCWelfareListViewControllerDelegate>delegate;
@property (nonatomic ,strong)NSArray *selectList;
@end
