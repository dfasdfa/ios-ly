//
//  YLCJobDetailViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCJobDetailViewController.h"

@interface YLCJobDetailViewController ()

@end

@implementation YLCJobDetailViewController
{
    UITextView *jobTextView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    jobTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 10, self.view.frameWidth, 230)];
    
    jobTextView.text = _detailMessage;
    
    jobTextView.placeholderAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(183, 183, 183)};
    
    jobTextView.placeholder = @"填写详细，清晰的职位描述，有助于您更准确地展开招聘需求（不能填写QQ，微信，电话等联系方式，以及特殊符号）\n\n例如:\n1.工作内容...\n2.任务要求...\n3.特别说明...";
    
    jobTextView.edgeInsetLeft = 10;
    
    [self.view addSubview:jobTextView];
    
    [jobTextView.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, detailMessageDidChangeWithMessage:)) {
            [self.delegate detailMessageDidChangeWithMessage:x];
        }
    }];
}
- (NSString *)title{
    return @"职位描述";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
