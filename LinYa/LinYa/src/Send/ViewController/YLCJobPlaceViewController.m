//
//  YLCJobPlaceViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCJobPlaceViewController.h"

@interface YLCJobPlaceViewController ()
@property (weak, nonatomic) IBOutlet UITextField *infoPlaceField;
@property (weak, nonatomic) IBOutlet UILabel *areaSubView;
@property (weak, nonatomic) IBOutlet UIView *areaBackView;

@end

@implementation YLCJobPlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = YLC_COMMON_BACKCOLOR;
    WS(weakSelf)
   
    [_infoPlaceField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(weakSelf.delegate, didChangePlaceMessageWithPlace:)) {
            [weakSelf.delegate didChangePlaceMessageWithPlace:x];
        }
    }];
    
    if ([YLCFactory isStringOk:_place]) {
        _infoPlaceField.text = _place;
    }
    
    if (_areaModel) {
        _areaSubView.text = [self getAllAreaNameWithAreaId:_areaModel.id areaModel:_areaModel];
    }
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAreaList)];
    
    [_areaBackView addGestureRecognizer:ges];
}
- (void)showAreaList{
    RACSubject *subject = [RACSubject subject];
    [YLCHUDShow showAreaPickWithSignal:subject];
    [subject subscribeNext:^(id x) {
        YLCAreaModel *model = x;
        _areaSubView.text = [self getAllAreaNameWithAreaId:model.id areaModel:model];
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeAreaMessageWithModel:)) {
            [self.delegate didChangeAreaMessageWithModel:model];
        }
    }];
}
- (NSString *)getAllAreaNameWithAreaId:(NSString *)areaId
                             areaModel:(YLCAreaModel *)model{
    NSString *areaName = model.areaname;
    if ([model.id isEqualToString:@"0"]) {
        areaName = model.areaname;
    }else{
        if (![model.parentid isEqualToString:@"0"]) {
            YLCAreaModel *parentModel = [[YLCAccountManager shareManager] getAreaModelWithAreaId:model.parentid];
            
            areaName = [NSString stringWithFormat:@"%@ %@",parentModel.areaname,areaName];
            
            if (![parentModel.parentid isEqualToString:@"0"]) {
                YLCAreaModel *parentModel2 = [[YLCAccountManager shareManager] getAreaModelWithAreaId:parentModel.parentid];
                
                areaName = [NSString stringWithFormat:@"%@ %@",parentModel2.areaname,areaName];
            }
        }
        
    }
    return areaName;
}
- (NSString *)title{
    return @"工作地点";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
