//
//  YLCSendGoodsViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSendGoodsViewController.h"
#import "ImagePicker.h"
#import "YLCAreaModel.h"
#import "YLCPickModel.h"
#import "IQKeyboardManager.h"
@interface YLCSendGoodsViewController ()<UIActionSheetDelegate>

@end

@implementation YLCSendGoodsViewController
{
    UIScrollView *backgroundView;
    UIView *topBackView;
    UIImageView *addPhotoImageView;;
    UIView *middleView;
    UITextField *titleField;
    YYTextView *goodsIntroField;
    UIView *typeView;
    UILabel *typeTitleLabel;
    UILabel *typeSubLabel;
    UIView *priceView;
    UILabel *priceTitleLabel;
    UILabel *priceSubLabel;
    UIImageView *lineView;
    UIView *locationView;
    UILabel *locationTitleLabel;
    UILabel *locationSubLabel;
    UIImageView *lineView2;
    NSString *imageBase;
    UIButton *sendMessageBtn;
    NSDictionary *priceData;
    YLCAreaModel *areaModel;
    NSArray *typeList;
    NSString *typeString;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/goods/typeLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        typeList = responseObject;
    }];
}
- (void)createCustomView{

    
    backgroundView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:backgroundView];
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    topBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 120)];
    
    topBackView.backgroundColor = [UIColor whiteColor];
    
    [backgroundView addSubview:topBackView];
    
    UITapGestureRecognizer *hiddenGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenKeyBoard)];
    
    [topBackView addGestureRecognizer:hiddenGes];
    
    addPhotoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image_add"]];
    
    addPhotoImageView.userInteractionEnabled = YES;
    
    [backgroundView addSubview:addPhotoImageView];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showImageSheet)];
    
    [addPhotoImageView addGestureRecognizer:ges];
    
    [addPhotoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backgroundView.mas_centerX);
        make.top.equalTo(backgroundView.mas_top).offset(20);
        make.width.equalTo(80);
        make.height.equalTo(80);
    }];
    
    titleField = [[UITextField alloc] init];
    
    titleField.backgroundColor = [UIColor whiteColor];
    
    titleField.leftView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 10, 10)];
    
    titleField.leftViewMode = UITextFieldViewModeAlways;
    
    titleField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"宝贝标题 品牌型号可不少" FirstCount:12 secondCount:0 firstFont:16 secondFont:0 firstColor:RGB(183, 183, 183) secondColor:RGB(183, 183, 183)];
    
    titleField.frame = CGRectMake(0, topBackView.frameMaxY+10, self.view.frameWidth, 50);
    
    [self.view addSubview:titleField];

    middleView = [[UIView alloc] initWithFrame:CGRectMake(0, titleField.frameMaxY+10, self.view.frameWidth, 170)];
    
    middleView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:middleView];
    
    goodsIntroField = [[YYTextView alloc] init];
    
    NSString *goodsIntro = @"在这里详细描述一下你的宝贝吧~";
    
    goodsIntroField.placeholderText = goodsIntro;
    
    goodsIntroField.placeholderTextColor = RGB(183, 183, 183);
    
    [middleView addSubview:goodsIntroField];
    
    goodsIntroField.frame = CGRectMake(10, 10, middleView.frameWidth-20, middleView.frameHeight-20-40);
    
    typeView = [[UIView alloc] initWithFrame:CGRectMake(0, middleView.frameMaxY+10, self.view.frameWidth, 44)];
    
    typeView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:typeView];
    
    UITapGestureRecognizer *typeGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTypeList)];
    
    [typeView addGestureRecognizer:typeGes];
    
    typeTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(44, 44, 44)];
    
    typeTitleLabel.text = @"分类";
    
    [typeView addSubview:typeTitleLabel];
    
    [typeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(typeView.mas_left).offset(10);
        make.centerY.equalTo(typeView.mas_centerY);
    }];
    
    typeSubLabel = [YLCFactory createLabelWithFont:14 color:RGB(153, 153, 153)];
    
    typeSubLabel.text = @"选择分类";
    
    [typeView addSubview:typeSubLabel];
    
    [typeSubLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(typeView.mas_right).offset(-40);
        make.centerY.equalTo(typeView.mas_centerY);
    }];
    
    UITapGestureRecognizer *goodTypeGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTypePickList)];
    
    [typeView addGestureRecognizer:goodTypeGes];
    
    lineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, typeView.frameMaxY, self.view.frameWidth, 0.5)];
    
    lineView.backgroundColor = RGB(235, 235, 235);
    
    [self.view addSubview:lineView];
    
    priceView = [[UIView alloc] initWithFrame:CGRectMake(0, lineView.frameMaxY, self.view.frameWidth, 44)];
    
    priceView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:priceView];
    
    UITapGestureRecognizer *imageGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPriceHUD)];
    
    [priceView addGestureRecognizer:imageGes];
    
    priceTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(44, 44, 44)];
    
    priceTitleLabel.text = @"价格";
    
    [priceView addSubview:priceTitleLabel];
    
    [priceTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(priceView.mas_left).offset(10);
        make.centerY.equalTo(priceView.mas_centerY);
    }];
    
    priceSubLabel = [YLCFactory createLabelWithFont:14 color:RGB(153, 153, 153)];
    
    priceSubLabel.text = @"开个价";
    
    [priceView addSubview:priceSubLabel];
    
    [priceSubLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(priceView.mas_right).offset(-40);
        make.centerY.equalTo(priceView.mas_centerY);
    }];
    
    lineView2 = [[UIImageView alloc] init];
    
    lineView2.backgroundColor = RGB(235, 235, 235);
    
    [self.view addSubview:lineView2];
    
    lineView2.frame = CGRectMake(0, priceView.frameMaxY, self.view.frameWidth, 0.5);
    
    locationView = [[UIView alloc] initWithFrame:CGRectMake(0, lineView2.frameMaxY, self.view.frameWidth, 44)];
    
    locationView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:locationView];
    
    UITapGestureRecognizer *areaGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAreaHUD)];
    
    [locationView addGestureRecognizer:areaGes];
    
    locationTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(44, 44, 44)];
    
    locationTitleLabel.text = @"地址";
    
    [locationView addSubview:locationTitleLabel];
    
    [locationTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(locationView.mas_left).offset(10);
        make.centerY.equalTo(locationView.mas_centerY);
    }];

    locationSubLabel = [YLCFactory createLabelWithFont:14 color:RGB(153, 153, 153)];
    
    locationSubLabel.text = @"请选择地址";
    
    [locationView addSubview:locationSubLabel];
    
    [locationSubLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(locationView.mas_right).offset(-40);
        make.centerY.equalTo(locationView.mas_centerY);
    }];
    
    sendMessageBtn = [YLCFactory createCommondBtnWithTitle:@"发布" backgroundColor:RGB(255, 87, 71) titleColor:[UIColor whiteColor]];
    
    [self.view addSubview:sendMessageBtn];
    
    sendMessageBtn.frame = CGRectMake(10, locationView.frameMaxY+10, self.view.frameWidth-20, 45);
    
    [sendMessageBtn addTarget:self action:@selector(sendGood) forControlEvents:UIControlEventTouchUpInside];
}
- (void)hiddenKeyBoard{
    if ([goodsIntroField isFirstResponder]) {
        [goodsIntroField resignFirstResponder];
    }
    
}
- (void)showTypePickList{
    if (JK_IS_ARRAY_NIL(typeList)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary *dict in typeList) {
        [container addObject:dict[@"classify_name"]];
    }
    YLCPickModel *pickModel = [[YLCPickModel alloc] init];
    pickModel.rowArr = container.copy;
    pickModel.selectIndex = 0;
    RACSubject *signal = [RACSubject subject];
    [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
    [signal subscribeNext:^(id x) {
        YLCPickModel *model = x[0];
        NSDictionary *selectDict = typeList[model.selectIndex];
        typeSubLabel.text = selectDict[@"classify_name"];
        typeString = selectDict[@"id"];
    }];
}
- (void)sendGood{
    
    if (JK_IS_STR_NIL(imageBase)) {
        
        [SVProgressHUD showErrorWithStatus:@"请选择图片"];
        
        return;
        
    }
    
    if (JK_IS_STR_NIL(titleField.text)) {
        
        [SVProgressHUD showErrorWithStatus:@"请输入标题"];
        
        return;
    }
    
    if (JK_IS_STR_NIL(goodsIntroField.text)) {
        
        [SVProgressHUD showErrorWithStatus:@"请输入物品详情"];
        
        return;
    }
    
    if (JK_IS_STR_NIL(_goodsId)) {
        
        _goodsId = @"0";
        
    }
    if (JK_IS_STR_NIL(priceData[@"newPrice"])) {
        
        [SVProgressHUD showErrorWithStatus:@"请输入价格"];
        
        return;
    }
    if (JK_IS_STR_NIL(priceData[@"oldPrice"])) {
        
        [SVProgressHUD showErrorWithStatus:@"请输入原价"];
        
        return;
    }
    if (JK_IS_STR_NIL(goodsIntroField.text)) {
        
        [SVProgressHUD showErrorWithStatus:@"请输入物品描述"];
        
        return;
    }
    if (JK_IS_STR_NIL(typeString)) {
        typeString = @"0";
    }
    NSString *areaId = @"";
    if (!JK_IS_STR_NIL(areaModel.id)) {
        areaId = areaModel.id;
    }
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id,@"title":titleField.text,@"imgData[0]":imageBase,@"goodsId":[_goodsId notNullString],@"price":priceData[@"newPrice"],@"locationId":areaId,@"type":typeString,@"description":goodsIntroField.text,@"oldPrice":priceData[@"oldPrice"]} method:@"POST" urlPath:@"app/goods/doSave" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error) {
            
            return ;
            
        }
        
        [SVProgressHUD showSuccessWithStatus:@"发布成功！"];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}
- (void)showImageSheet{
    
    if ([goodsIntroField isFirstResponder]) {
        
        [goodsIntroField resignFirstResponder];
        
        return;
        
    }
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机",@"相册", nil];
    
    [sheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [[ImagePicker sharedManager] dwSetPresentDelegateVC:self SheetShowInView:self.view InfoDictionaryKeys:buttonIndex];
    
    //回调
    [[ImagePicker sharedManager] dwGetpickerTypeStr:^(NSString *pickerTypeStr) {
        
    } pickerImagePic:^(UIImage *pickerImagePic) {
        
        imageBase = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",[pickerImagePic base64String]];
        
        addPhotoImageView.image = pickerImagePic;
        
    }];
}
- (void)showTypeList{
    
    if ([goodsIntroField isFirstResponder]) {
        
        [goodsIntroField resignFirstResponder];
        
        return;
    }
}
- (void)showPriceHUD{
    
    if ([goodsIntroField isFirstResponder]) {
        
        [goodsIntroField resignFirstResponder];
        
        return;
        
    }
    
    RACSubject *subject = [RACSubject subject];
    
    [YLCHUDShow showPriceWithSignal:subject];
    
    [subject subscribeNext:^(id x) {
        
        priceData = x;
        
        priceSubLabel.text = priceData[@"newPrice"];
        
    }];
    
}
- (void)showAreaHUD{
    
    if ([goodsIntroField isFirstResponder]) {
        
        [goodsIntroField resignFirstResponder];
        
        return;
        
    }
    
    RACSubject *subject = [RACSubject subject];
    
    [YLCHUDShow showAreaPickWithSignal:subject];
    
    [subject subscribeNext:^(id x) {
        
        areaModel = x;
        
        locationSubLabel.text = areaModel.areaname;
        
    }];
    
}
- (NSString *)title{
    
    return @"发布闲置物品";
    
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
