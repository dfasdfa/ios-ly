//
//  YLCSendTrainViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
typedef NS_ENUM(NSInteger, SendActivityState){
    TrainSendType,
    ActivitySendType
};
@interface YLCSendTrainViewController : YLCBaseViewController
@property (nonatomic ,assign)SendActivityState sendType;
@end
