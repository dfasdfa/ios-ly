//
//  YLCJobTypeViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCJobTypeViewController.h"

@interface YLCJobTypeViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation YLCJobTypeViewController
{
    UITableView *jobTable;
    NSArray *jobList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDatSource];
    [self createCustomView];
}
- (void)initDatSource{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobList = responseObject;
        
        [jobTable reloadData];
    }];
}
- (void)createCustomView{
    jobTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    jobTable.backgroundColor = [UIColor whiteColor];
    
    jobTable.delegate = self;
    
    jobTable.dataSource = self;
    
    jobTable.tableFooterView = [[UIView alloc] init];
    
    [self.view addSubview:jobTable];
    
    [jobTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(40, 0, 0, 0));
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return jobList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"job_type_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"job_type_cell"];
    }
    if (indexPath.row<jobList.count) {
        cell.textLabel.text = jobList[indexPath.row];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, jobTypeDidChangeWithType:)) {
        [self.delegate jobTypeDidChangeWithType:jobList[indexPath.row]];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSString *)title{
    return @"职位类型";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
