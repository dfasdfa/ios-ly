//
//  YLCJobPlaceViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCAreaModel.h"
@class YLCJobPlaceViewController;
@protocol YLCJobPlaceViewControllerDelegate <NSObject>
- (void)didChangePlaceMessageWithPlace:(NSString *)place;
- (void)didChangeAreaMessageWithModel:(YLCAreaModel *)model;
@end
@interface YLCJobPlaceViewController : YLCBaseViewController
@property (nonatomic ,strong)YLCAreaModel *areaModel;
@property (nonatomic ,copy)NSString *place;
@property (nonatomic ,weak)id<YLCJobPlaceViewControllerDelegate>delegate;
@end
