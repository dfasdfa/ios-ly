//
//  YLCSendJobViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSendJobViewController.h"
#import "YLCProgramSelectModel.h"
#import "YLCCompanyMessageViewController.h"
#import "YLCJobTypeViewController.h"
#import "YLCJobDetailViewController.h"
#import "YLCPickModel.h"
#import "YLCWelfareListViewController.h"
#import "YLCJobPlaceViewController.h"
#import "YLCAreaModel.h"
#import "ImagePicker.h"
@interface YLCSendJobViewController ()<UITableViewDelegate,UITableViewDataSource,YLCJobPlaceViewControllerDelegate,YLCJobDetailViewControllerDelegate,HXPhotoViewDelegate>
@property (strong, nonatomic) HXPhotoManager *manager;
@end

@implementation YLCSendJobViewController
{
    UITableView *sendTable;
    NSArray *dataList;
    NSString *detailMessage;
    UIImage *iconImage;
    NSString *placeMessage;
    NSArray *jobTypeNameList;
    
    NSArray *welfareNameList;
    NSArray *welfareDataList;
    NSArray *salaryNameList;
    NSArray *salaryDataList;
    NSArray *workYearNameList;
    NSArray *workYearDataList;
    NSArray *eduNameList;
    NSArray *eduDataList;
    HXPhotoView *photoView;
    UIView *footView;
    CGFloat imageHeight;
    NSArray *photoList;
    NSInteger photoNum;
}
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.photoMaxNum = 6;
        
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    imageHeight = 100;
    NSArray *titleList = @[@[@"公司名称",@"公司Logo"],
                           @[@"职位名称",@"工作地点"],
                           @[@"公司福利",@"薪资范围"],
                           @[@"经验要求",@"最高学历"],
                           @[@"职位描述"]];
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    for (NSArray *array in titleList) {
        NSMutableArray *subContainer = [NSMutableArray arrayWithCapacity:0];
        for (NSString *string in array) {
            YLCProgramSelectModel *model = [[YLCProgramSelectModel alloc] init];
            
            model.title = string;
            
            model.subString = @"";
            
            [subContainer addObject:model];
        }
        
        [container addObject:subContainer.copy];
    }
    
    dataList = container.copy;
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/salary" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            [container addObject:dict[@"salary_name"]];
        }
        salaryNameList = [container copy];
        salaryDataList = responseObject;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/welfare" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            [container addObject:dict[@"welfare_name"]];
        }
        welfareNameList = container.copy;
        welfareDataList = responseObject;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/workyear" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            
            [container addObject:dict[@"salary_name"]];
            
        }
        
        workYearNameList = container.copy;
        
        workYearDataList = responseObject;
        
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/education" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            [container addObject:dict[@"salary_name"]];
        }
        eduNameList = container.copy;
        eduDataList = responseObject;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobTypeNameList = responseObject;
    }];

}
- (void)createCustomView{
    sendTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    
    sendTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    sendTable.delegate = self;
    
    sendTable.dataSource = self;
    
    [self.view addSubview:sendTable];
    
    [sendTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataList.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *list = dataList[section];
    return list.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sendJob_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"sendJob_cell"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.detailTextLabel.textColor = [UIColor blackColor];

        cell.textLabel.font = [UIFont systemFontOfSize:14];
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImageView *subImageView = [[UIImageView alloc] init];

        subImageView.tag = 8066;

        subImageView.hidden = YES;

        [cell.contentView addSubview:subImageView];

        [subImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(cell.mas_right).offset(-40);
            make.centerY.equalTo(cell.mas_centerY);
            make.width.height.equalTo(30);
        }];
        
        UITextField *field = [[UITextField alloc] init];
        
        field.font = [UIFont systemFontOfSize:14];
        
        field.textAlignment = NSTextAlignmentRight;
        
        field.tag = 8067;
        
        field.hidden = YES;
        
        [cell.contentView addSubview:field];
        
        [field mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(cell.mas_right).offset(-40);
            make.centerY.equalTo(cell.mas_centerY);
            make.width.height.equalTo(80);
        }];
    }
    NSArray *cellArr = dataList[indexPath.section];
    YLCProgramSelectModel *model = cellArr[indexPath.row];
    cell.textLabel.text = model.title;
    cell.detailTextLabel.text = model.subString;
//    cell.detailTextLabel.numberOfLines = 0;
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            UITextField *field = [cell.contentView viewWithTag:8067];
            field.placeholder = @"公司名称";
            field.hidden = NO;
            field.text = model.subString;
            [field.rac_textSignal subscribeNext:^(id x) {
                [self changeTextWithIndexPath:indexPath text:x subData:x reload:NO];
            }];
            cell.detailTextLabel.text = @"";
        }
        if (indexPath.row==1) {
            UIImageView *imageView = [cell.contentView viewWithTag:8066];
            if (model.subData) {
                imageView.image = model.subData;
            }else{
                imageView.image = [UIImage imageNamed:@"image_placeholder"];
            }
            
            imageView.hidden = NO;
            
            cell.detailTextLabel.text = @"";
        }
    }
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==2) {
        return @"职类信息，职类名称和工作城市发布后不可修改";
    }else{
        return @"";
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section==4) {
        footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 100)];
        photoView = [HXPhotoView photoManager:self.manager];
        photoView.delegate = self;
        photoView.spacing = 10;
        photoView.lineCount = 4;
        [footView addSubview:photoView];
        photoView.frame = CGRectMake(0, 10, self.view.frameWidth, 100);
        YYTextHighlight *highlight = [YYTextHighlight new];
        
        highlight.tapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
            NSLog(@"点击了");
        };
        
//        [highlight setColor:RGB(65, 198, 187)];
        
        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:@"发布职位即表示你已经同意遵守《职业信息发布规则》\n\n所有职位均有专人审核，禁止发布手机兼职，淘宝兼职等职位"];
        
        attri.yy_font = [UIFont systemFontOfSize:12];
        
        attri.yy_color = RGB(183, 183, 183);
        
        [attri yy_setColor:RGB(65, 198, 187) range:NSMakeRange(14, 10)];
        
        [attri yy_setTextHighlight:highlight range:NSMakeRange(14, 10)];
        
        YYLabel *agreementLabel = [YYLabel new];
        
        agreementLabel.numberOfLines = 0;
        
        agreementLabel.attributedText = attri;

        [footView addSubview:agreementLabel];
        
        [agreementLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(footView.mas_left).offset(15);
            make.top.equalTo(photoView.mas_bottom).offset(10);
            make.width.equalTo(self.view.frameWidth-20);
            make.height.equalTo(50);
        }];
        
        UIButton *btn = [YLCFactory createCommondBtnWithTitle:@"发布" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
        
        [btn addTarget:self action:@selector(gotoSend) forControlEvents:UIControlEventTouchUpInside];
        
        [footView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(agreementLabel.mas_left);
            make.right.equalTo(agreementLabel.mas_right);
            make.top.equalTo(agreementLabel.mas_bottom).offset(20);
            make.height.equalTo(45);
        }];
        
        return footView;
    }else{
        return [[UIView alloc] init];
    }
}
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame{

}
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    photoNum = photos.count;
    footView.frameHeight = 100+100*photoNum/4;
    [sendTable reloadData];
    [SVProgressHUD show];
    for (HXPhotoModel *model in photos) {
        [HXPhotoTools getHighQualityFormatPhoto:model.asset size:model.imageSize succeed:^(UIImage *image) {
            [array addObject:image];
            photoList = array.copy;
            [SVProgressHUD dismiss];
        } failed:^{
            [SVProgressHUD dismiss];
        }];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section==4) {
        return 160+(photoNum/4+1)*100;
    }else{
        return 10;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row==0) {
//            [self gotoCompanyMessageVc];
        }
        if (indexPath.row==1) {
            [self showImageSheet];
        }
    }
    if (indexPath.section==1) {
        if (indexPath.row==0) {
            [self showMoneyPicker:jobTypeNameList index:indexPath title:@"工作类型" isMore:NO type:1];
        }
        if (indexPath.row==1) {
            [self gotoWorkPlace];
        }
    }
    if (indexPath.section==2) {
        if (indexPath.row==1) {
            [self showMoneyPicker:salaryNameList index:indexPath title:@"薪资范围" isMore:NO type:1];
        }
        if (indexPath.row==0) {
            [self showMoneyPicker:welfareNameList index:indexPath title:@"福利" isMore:YES type:0];
        }
    }
    if (indexPath.section==3) {
        if (indexPath.row==0) {

            [self showMoneyPicker:workYearNameList index:indexPath title:@"工作年限" isMore:NO type:1];
        }
        if (indexPath.row==1) {
            [self showMoneyPicker:eduNameList index:indexPath title:@"最高学历" isMore:NO type:1];
        }
    }
    if (indexPath.section==4) {
        if (indexPath.row==0) {
            [self jobDetailWrite];
        }
    }
}
#pragma mark - CellSelect
- (void)showImageSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机",@"相册", nil];
    
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[ImagePicker sharedManager] dwSetPresentDelegateVC:[YLCFactory currentNav] SheetShowInView:self.view InfoDictionaryKeys:buttonIndex];
    
    //回调
    [[ImagePicker sharedManager] dwGetpickerTypeStr:^(NSString *pickerTypeStr) {
    } pickerImagePic:^(UIImage *pickerImagePic) {
        //        iconImage = pickerImagePic;
        //        [sendTable reloadData];
        
        [self changeTextWithIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] text:@"" subData:pickerImagePic reload:YES];
    }];
}

- (void)gotoWorkPlace{
    YLCJobPlaceViewController *con = [[YLCJobPlaceViewController alloc] init];
    
    con.delegate = self;
    
    con.place = placeMessage;
    
    YLCProgramSelectModel *workPlaceModel = dataList[1][1];
    
    con.areaModel = workPlaceModel.subData;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)showMoneyPicker:(NSArray *)list
                  index:(NSIndexPath *)indexPath
                  title:(NSString *)title
                 isMore:(BOOL)isMore
                   type:(NSInteger)type{
    if (JK_IS_ARRAY_NIL(list)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    RACSubject *subject = [RACSubject subject];
    [YLCHUDShow showSelectListMessageHUDWithTitleList:list signal:subject isMore:isMore title:title type:type];
    [subject subscribeNext:^(id x) {
//        NSArray *secondList = dataList[indexPath.section];
//        YLCProgramSelectModel *messageModel = secondList[indexPath.row];
        NSString *subString = @"";

        if (isMore) {
//            messageModel.selectList = x;
            if (JK_IS_ARRAY_NIL(x)) {
                [self changeTextWithIndexPath:indexPath text:subString subData:x reload:YES];
            }else{
                for (NSString *string in x) {
                    subString = [NSString stringWithFormat:@"%@ %@",subString,list[[string integerValue]]];
                }
                [self changeTextWithIndexPath:indexPath text:subString subData:x reload:YES];
            }
        }else{
            NSInteger selectIndex = [x integerValue];
            subString = list[selectIndex];
//            messageModel.selectIndex = selectIndex;
            [self changeTextWithIndexPath:indexPath text:subString subData:x reload:YES];
        }
    }];
}
- (void)jobDetailWrite{
    
    YLCJobDetailViewController *con = [[YLCJobDetailViewController alloc] init];
    
    con.delegate = self;
    
    con.detailMessage = detailMessage;
    
    [self.navigationController pushViewController:con animated:YES];
    
}
- (void)changeTextWithIndexPath:(NSIndexPath *)indexPath
                           text:(NSString *)text
                        subData:(id)subData
                         reload:(BOOL)reload{
    NSMutableArray *baseContainer = dataList.mutableCopy;
    NSArray *secondList = baseContainer[indexPath.section];
    NSMutableArray *secondContainer = secondList.mutableCopy;
    YLCProgramSelectModel *model = secondContainer[indexPath.row];
    model.subString = text;
    model.subData = subData;
    [secondContainer setObject:model atIndexedSubscript:indexPath.row];
    [baseContainer setObject:secondContainer.copy atIndexedSubscript:indexPath.section];
    dataList = baseContainer.copy;
    if (reload) {
        [sendTable reloadData];
    }
}
- (void)didChangePlaceMessageWithPlace:(NSString *)place{
    placeMessage = place;
}
- (void)didChangeAreaMessageWithModel:(YLCAreaModel *)model{
    [self changeTextWithIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] text:[self getAllAreaNameWithAreaId:model.id areaModel:model] subData:model reload:YES];
}
- (void)detailMessageDidChangeWithMessage:(NSString *)message{
    detailMessage = message;
    [self changeTextWithIndexPath:[NSIndexPath indexPathForRow:0 inSection:4] text:message subData:message reload:YES];
}
- (void)gotoSend{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    //    NSArray *titleList = @[@[@"公司名称",@"公司Logo"],
    //                           @[@"职位名称",@"工作地点"],
    //                           @[@"公司福利",@"薪资范围"],
    //                           @[@"经验要求",@"最高学历"],
    //                           @[@"职位描述"]];
    if (![self checkData]) {
        return;
    }
    if (JK_IS_STR_NIL(_recruitId)) {
        _recruitId = @"0";
    }
    YLCProgramSelectModel *compNameModel = dataList[0][0];
    YLCProgramSelectModel *logoModel = dataList[0][1];
    YLCProgramSelectModel *workNameModel = dataList[1][0];
    YLCProgramSelectModel *workPlaceModel = dataList[1][1];
    YLCProgramSelectModel *welfModel = dataList[2][0];
    YLCProgramSelectModel *payModel = dataList[2][1];
    YLCProgramSelectModel *expModel = dataList[3][0];
    YLCProgramSelectModel *eduModel = dataList[3][1];
    YLCProgramSelectModel *detailModel = dataList[4][0];
    YLCAreaModel *areaModel = workPlaceModel.subData;
    NSString *locationId = areaModel.id;
    NSArray *welfSelectList = welfModel.subData;
    NSDictionary *firstWelfDict = welfareDataList[[welfSelectList[0] integerValue]];
    NSString *welfIdString = firstWelfDict[@"id"];
    if (welfSelectList.count>1) {
        for (int i = 1; i<welfSelectList.count; i++) {
            NSInteger selectIndex = [welfSelectList[i] integerValue];
            NSDictionary *welfDict = welfareDataList[selectIndex];
            welfIdString = [NSString stringWithFormat:@"%@,%@",welfIdString,welfDict[@"id"]];
        }
    }
    NSDictionary *salaryDict = salaryDataList[[payModel.subData integerValue]];
    
    NSString *salaryId = [NSString stringWithFormat:@"%@",salaryDict[@"id"]];
    
    NSDictionary *expDict = workYearDataList[[expModel.subData integerValue]];
    
    NSString *expId = [NSString stringWithFormat:@"%@",expDict[@"id"]];
    
    NSDictionary *eduDict = eduDataList[[eduModel.subData integerValue]];
    
    NSString *eduID = [NSString stringWithFormat:@"%@",eduDict[@"id"]];
    
    UIImage *logoImage = logoModel.subData;
    NSString *photoString = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",[logoImage base64String]];
    
    if (JK_IS_STR_NIL(photoString)) {
        [SVProgressHUD showErrorWithStatus:@"请选择企业logo图片"];
        return;
    }
    NSDictionary *param = @{@"token":userModel.token,@"userId":userModel.user_id,@"companyName":compNameModel.subString,@"welfare":welfIdString,@"companyLogo":photoString,@"position":workNameModel.subString,@"salary":salaryId,@"content":detailModel.subString,@"locationId":locationId,@"place":placeMessage,@"recruitId":_recruitId,@"workYear":expId,@"education":eduID};
    NSMutableDictionary *containerParam = param.mutableCopy;
    if (!JK_IS_ARRAY_NIL(photoList)) {
        for (int i = 0; i<photoList.count; i++) {
            NSString *photoString = photoList[i];
            
            [containerParam setObject:photoString forKey:[NSString stringWithFormat:@"imgData[%d]",i]];
        }
    }
    [YLCNetWorking loadNetServiceWithParam:containerParam.copy method:@"POST" urlPath:@"app/recruit/doSave" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            [SVProgressHUD dismiss];
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"发布成功！"];
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (BOOL)checkData{
    YLCProgramSelectModel *compNameModel = dataList[0][0];
    YLCProgramSelectModel *logoModel = dataList[0][1];
    YLCProgramSelectModel *workNameModel = dataList[1][0];
    YLCProgramSelectModel *workPlaceModel = dataList[1][1];
    YLCProgramSelectModel *welfModel = dataList[2][0];
    YLCProgramSelectModel *payModel = dataList[2][1];
    YLCProgramSelectModel *expModel = dataList[3][0];
    YLCProgramSelectModel *eduModel = dataList[3][1];
    YLCProgramSelectModel *detailModel = dataList[4][0];
    NSString *workName = workNameModel.subString;
    YLCAreaModel *areaModel = workPlaceModel.subData;
    NSString *locationId = areaModel.id;
    NSArray *welfSelectList = welfModel.subData;
    if (JK_IS_ARRAY_NIL(welfSelectList)) {
        [SVProgressHUD showErrorWithStatus:@"请选择福利信息"];
        return NO;
    }
    NSDictionary *firstWelfDict = welfareDataList[[welfSelectList[0] integerValue]];
    NSString *welfIdString = firstWelfDict[@"id"];
    if (welfSelectList.count>1) {
        for (int i = 1; i<welfSelectList.count; i++) {
            NSInteger selectIndex = [welfSelectList[i] integerValue];
            NSDictionary *welfDict = welfareDataList[selectIndex];
            welfIdString = [NSString stringWithFormat:@"%@,%@",welfIdString,welfDict[@"id"]];
        }
    }
    NSDictionary *salaryDict = salaryDataList[[payModel.subData integerValue]];
    
    NSString *salaryId = [NSString stringWithFormat:@"%@",salaryDict[@"id"]];
    
    NSDictionary *expDict = workYearDataList[[expModel.subData integerValue]];
    
    NSString *expId = [NSString stringWithFormat:@"%@",expDict[@"id"]];
    
    NSDictionary *eduDict = eduDataList[[eduModel.subData integerValue]];
    
    NSString *eduID = [NSString stringWithFormat:@"%@",eduDict[@"id"]];
    
    if (JK_IS_STR_NIL(compNameModel.subData)) {
        [SVProgressHUD showErrorWithStatus:@"请输入公司名称"];
        return NO;
    }
    if (logoModel.subData==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择公司logo"];
        return NO;
    }
    
    if (JK_IS_STR_NIL(workName)) {
        [SVProgressHUD showErrorWithStatus:@"请选择职位名称"];
        return NO;
    }
    
    if (JK_IS_STR_NIL(locationId)) {
        [SVProgressHUD showErrorWithStatus:@"请选择地址信息"];
        return NO;
    }
    if (JK_IS_STR_NIL(placeMessage)) {
        [SVProgressHUD showErrorWithStatus:@"请输入详细地址信息"];
        return NO;
    }
    if (JK_IS_STR_NIL(welfIdString)) {
        [SVProgressHUD showErrorWithStatus:@"请选择福利信息"];
        return NO;
    }
    if (JK_IS_STR_NIL(salaryId)) {
        [SVProgressHUD showErrorWithStatus:@"请选择薪资信息"];
        return NO;
    }
    
    if (JK_IS_STR_NIL(expId)) {
        [SVProgressHUD showErrorWithStatus:@"请选择工作经验信息"];
        return NO;
    }
    if (JK_IS_STR_NIL(eduID)) {
        [SVProgressHUD showErrorWithStatus:@"请选择学历信息"];
        return NO;
    }
    if (JK_IS_STR_NIL(detailModel.subString)) {
        [SVProgressHUD showErrorWithStatus:@"请输入职位描述信息"];
        return NO;
    }
    return YES;
}
- (NSString *)getAllAreaNameWithAreaId:(NSString *)areaId
                             areaModel:(YLCAreaModel *)model{
    NSString *areaName = model.areaname;
    if ([model.id isEqualToString:@"0"]) {
        areaName = model.areaname;
    }else{
        if (![model.parentid isEqualToString:@"0"]) {
            YLCAreaModel *parentModel = [[YLCAccountManager shareManager] getAreaModelWithAreaId:model.parentid];
            
            areaName = [NSString stringWithFormat:@"%@ %@",parentModel.areaname,areaName];
            
            if (![parentModel.parentid isEqualToString:@"0"]) {
                YLCAreaModel *parentModel2 = [[YLCAccountManager shareManager] getAreaModelWithAreaId:parentModel.parentid];
                
                areaName = [NSString stringWithFormat:@"%@ %@",parentModel2.areaname,areaName];
            }
        }
        
    }
    return areaName;
}
- (NSString *)title{
    return @"招聘";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
