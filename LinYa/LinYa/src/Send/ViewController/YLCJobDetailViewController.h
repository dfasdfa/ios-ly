//
//  YLCJobDetailViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCJobDetailViewController;
@protocol YLCJobDetailViewControllerDelegate <NSObject>
- (void)detailMessageDidChangeWithMessage:(NSString *)message;
@end
@interface YLCJobDetailViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *detailMessage;
@property (nonatomic ,weak)id<YLCJobDetailViewControllerDelegate>delegate;
@end
