//
//  YLCSendTieziSuccessViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSendTieziSuccessViewController.h"
#import "YLCTabBarViewController.h"
#import "AppDelegate.h"
@interface YLCSendTieziSuccessViewController ()

@end

@implementation YLCSendTieziSuccessViewController
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"查看" titleColor:[UIColor whiteColor] selector:@selector(gotoCheck) target:self];
}
- (void)gotoCheck{
    [self.navigationController popToRootViewControllerAnimated:YES];
//    YLCTabBarViewController *tabBarVc = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
//    tabBarVc.selectedIndex = 1;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
}
- (void)createCustomView{
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frameWidth, 170)];
    
    backView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backView];
    
    UIImageView *successLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tiezi_success"]];
    
    [backView addSubview:successLogo];
    
    [successLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backView.mas_centerX);
        make.top.equalTo(backView.mas_top).offset(20);
        make.width.equalTo(103);
        make.height.equalTo(93);
    }];
    
    UILabel *successTip = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    successTip.text = @"恭喜您，发布成功！";
    
    [backView addSubview:successTip];
    
    [successTip mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(successLogo.mas_bottom).offset(20);
        make.centerX.equalTo(successLogo.mas_centerX);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
