//
//  YLCBaseSendViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseSendViewController.h"
#import "YLCEasyCommunicateView.h"
#import "YLCTitleAndIconModel.h"
@interface YLCBaseSendViewController ()

@end

@implementation YLCBaseSendViewController
{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSArray *list = @[@"帖子",@"问答",@"闲置物品",@"招聘",@"培训",@"活动",@"小视频",@"",@""];
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (int i = 0; i<list.count; i++) {
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = list[i];
        
        model.iconName = [NSString stringWithFormat:@"send_%d",i+1];
        
        [container addObject:model];
    }
    
    YLCEasyCommunicateView *easyView = [[YLCEasyCommunicateView alloc] initWithFrame:CGRectZero];
    
    easyView.backgroundColor = [UIColor whiteColor];
    
    easyView.imageSize = CGSizeMake(35, 32);
    
    easyView.cellSize = CGSizeMake((self.view.frameWidth-2)/4,(self.view.frameWidth-2)/4);
    
    easyView.listArr = container.copy;
    
    [easyView.touchSignal subscribeNext:^(id x) {
        NSIndexPath *indexPath = x;
        [self dissmissWithIndex:indexPath.row];
    }];

    [self.view addSubview:easyView];
    
    WS(weakSelf)
    
    [easyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.view.mas_bottom).offset(-70);
        make.left.equalTo(weakSelf.view.mas_left);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(200);
    }];
    
    UIButton *closeBtn = [YLCFactory createBtnWithImage:@"login_cancel"];
    
    [closeBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:closeBtn];
    
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(easyView.mas_bottom).offset(10);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
}
- (void)dissmissWithIndex:(NSInteger)index{
    [self dismissViewControllerAnimated:NO completion:^{
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectAtIndex:)) {
            [self.delegate didSelectAtIndex:index];
        }
    }];

}
- (void)dissmiss{
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
