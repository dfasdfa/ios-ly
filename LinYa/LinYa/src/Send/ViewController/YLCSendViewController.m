//
//  YLCSendViewController.m
//  YouLe
//
//  Created by 初程程 on 2018/1/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSendViewController.h"
#import "YLCSendView.h"
@interface YLCSendViewController ()
@property (nonatomic ,strong)YLCSendView *sendView;
@end

@implementation YLCSendViewController
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"下一步" titleColor:[UIColor whiteColor] selector:@selector(gotoNext) target:self];
}
- (void)gotoNext{
    [self.sendView sendText];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomViews];
    [self bindViewModel];
}
- (void)initDataSource{
    
}
- (void)createCustomViews{
    self.sendView = [[YLCSendView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, self.view.frameHeight)];
    [self.view addSubview:self.sendView];
}
- (void)bindViewModel{
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.sendView) {
        if (self.sendView.hiddenNoti) {
            [self.sendView addNoti];
        }
    }
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self.sendView removeNotification];
}
- (NSString *)title{
    return @"发送";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
