//
//  YLCCompanyMessageViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCCompanyMessageViewController;
@protocol YLCCompanyMessageViewControllerDelegate <NSObject>
- (void)companyMessageDidChangeWithName:(NSString *)name
                                   size:(NSString *)size
                                   logo:(UIImage *)logo;
@end
@interface YLCCompanyMessageViewController : YLCBaseViewController
@property (nonatomic ,weak)id<YLCCompanyMessageViewControllerDelegate>delegate;
@property (nonatomic ,strong)UIImage *logoImage;
@property (nonatomic ,copy)NSString *compName;
@property (nonatomic ,copy)NSString *compSize;
@end
