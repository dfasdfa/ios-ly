//
//  YLCSendVideoNextViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/5/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCSendVideoNextViewController : YLCBaseViewController
@property (nonatomic ,strong)NSURL *videoPath;
@property (nonatomic ,assign)BOOL isEasy;
@property (nonatomic ,copy)NSString *classifyId;
@property (nonatomic ,assign)BOOL isAlbum;
@property (nonatomic ,strong)NSData *albumVideoData;
@end
