//
//  YLCBaseSendViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCBaseSendViewController;
@protocol YLCBaseSendViewControllerDelegate <NSObject>
- (void)didSelectAtIndex:(NSInteger)index;
@end
@interface YLCBaseSendViewController : YLCBaseViewController
@property (nonatomic ,weak)id<YLCBaseSendViewControllerDelegate>delegate;
@end
