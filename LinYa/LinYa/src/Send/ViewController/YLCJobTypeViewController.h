//
//  YLCJobTypeViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCJobTypeViewController;
@protocol YLCJobTypeViewControllerDelegate <NSObject>
- (void)jobTypeDidChangeWithType:(NSString *)type;
@end
@interface YLCJobTypeViewController : YLCBaseViewController
@property (nonatomic ,weak)id<YLCJobTypeViewControllerDelegate>delegate;
@end
