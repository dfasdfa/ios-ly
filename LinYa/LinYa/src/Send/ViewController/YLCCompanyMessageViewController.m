//
//  YLCCompanyMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCompanyMessageViewController.h"
#import "YLCSubFieldCell.h"
#import "YLCCustomIconCell.h"
#import "YLCProgramSelectModel.h"
#import "ImagePicker.h"
#define SUB_FIELD  @"YLCSubFieldCell"
#define ICON_CELL  @"YLCCustomIconCell"
@interface YLCCompanyMessageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCSubFieldCellDelegate,UIActionSheetDelegate>

@end

@implementation YLCCompanyMessageViewController
{
    UICollectionView *messageTable;
    NSArray *dataList;
    UIImage *iconImage;
    NSString *name;
    NSString *size;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    NSArray *titleList = @[@"公司全称",@"人员规模",@"公司Logo"];
    
    NSArray *placeholderList = @[@"信息有限公司",@"1000-9999人",@""];
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    iconImage = _logoImage;
    for (int index = 0; index<titleList.count; index++) {
        YLCProgramSelectModel *model = [[YLCProgramSelectModel alloc] init];
        
        model.title = titleList[index];
        
        model.placeholder = placeholderList[index];
        
        if (index==0) {
            model.subString = _compName;
        }else{
            model.subString = _compSize;
        }
        
        [container addObject:model];
    }
    
    dataList = [container copy];
}

- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0.5;
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = RGB(235, 235, 235);
    
    [messageTable registerClass:[YLCCustomIconCell class] forCellWithReuseIdentifier:ICON_CELL];
    
    [messageTable registerClass:[YLCSubFieldCell class] forCellWithReuseIdentifier:SUB_FIELD];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self.view addSubview:messageTable];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(30, 0, 0, 0));
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<dataList.count-1) {
        YLCSubFieldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SUB_FIELD forIndexPath:indexPath];
        
        cell.model = dataList[indexPath.row];
        
        cell.index = indexPath.row;
        
        cell.delegate = self;
        
        return cell;
    }
    YLCCustomIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ICON_CELL forIndexPath:indexPath];
    if (indexPath.row<dataList.count) {
        YLCProgramSelectModel *model = dataList[indexPath.row];
        cell.title = model.title;
        if (iconImage) {
            cell.headImage = iconImage;
        }else{
            cell.headImage = [UIImage imageNamed:@"image_placeholder"];
        }
        cell.backgroundColor = [UIColor whiteColor];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 50);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==2) {
        [self showImageSheet];
    }
}
- (void)showImageSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相机",@"相册", nil];
    
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [[ImagePicker sharedManager] dwSetPresentDelegateVC:[YLCFactory currentNav] SheetShowInView:self.view InfoDictionaryKeys:buttonIndex];
    
    //回调
    [[ImagePicker sharedManager] dwGetpickerTypeStr:^(NSString *pickerTypeStr) {
        
        
        
    } pickerImagePic:^(UIImage *pickerImagePic) {
        iconImage = pickerImagePic;
        [self didChangeMesage];
        [messageTable reloadData];
    }];
    
}
- (void)subDidchangeWithTxt:(NSString *)text
                      index:(NSInteger)index{
    YLCProgramSelectModel *model = dataList[index];
    
    model.subString = text;
    
    if (index==0) {
        name = text;
    }else{
        size = text;
    }
    [self didChangeMesage];
}
- (void)didChangeMesage{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, companyMessageDidChangeWithName:size:logo:)) {
        [self.delegate companyMessageDidChangeWithName:name size:size logo:iconImage];
    }
}
- (NSString *)title{
    return @"发布";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
