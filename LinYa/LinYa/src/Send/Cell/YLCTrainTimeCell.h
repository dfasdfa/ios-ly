//
//  YLCTrainTimeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCTrainTimeCell;
@protocol YLCTrainTimeCellDelegate<NSObject>
- (void)beganTimeDidChangeWithBeganTime:(NSString *)beganTime;
- (void)endTimeDidChangeWithEndTime:(NSString *)endTime;
@end
@interface YLCTrainTimeCell : UICollectionViewCell
@property (nonatomic ,strong)UILabel *beganTitleLabel;
@property (nonatomic ,strong)UILabel *endTitleLabel;
@property (nonatomic ,strong)UILabel *beganTimeLabel;
@property (nonatomic ,strong)UILabel *endTimeLabel;
@property (nonatomic ,assign)id<YLCTrainTimeCellDelegate>delegate;
@end
