//
//  YLCSubFieldCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCProgramSelectModel.h"
@class YLCSubFieldCell;
@protocol YLCSubFieldCellDelegate<NSObject>
- (void)subDidchangeWithTxt:(NSString *)text
                      index:(NSInteger)index;
@end
@interface YLCSubFieldCell : UICollectionViewCell
@property (nonatomic ,strong)UITextField *subField;
@property (nonatomic ,strong)YLCProgramSelectModel *model;
@property (nonatomic ,assign)id<YLCSubFieldCellDelegate>delegate;
@property (nonatomic ,assign)NSInteger index;
@end
