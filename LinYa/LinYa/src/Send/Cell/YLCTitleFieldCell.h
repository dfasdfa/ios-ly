//
//  YLCTitleFieldCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCTitleFieldCell;
@protocol YLCTitleFieldCellDelegate<NSObject>
- (void)titleDidChangeWithTitle:(NSString *)title;
@end
@interface YLCTitleFieldCell : UICollectionViewCell
@property (nonatomic ,strong)UITextField *titleField;
@property (nonatomic ,assign)id<YLCTitleFieldCellDelegate>delegate;
@end
