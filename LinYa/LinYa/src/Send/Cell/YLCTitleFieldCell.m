//
//  YLCTitleFieldCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTitleFieldCell.h"

@implementation YLCTitleFieldCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _titleField = [YLCFactory createTitleFieldWithPlaceholder:@"活动主题" title:@"主题"];
        
        [_titleField.rac_textSignal subscribeNext:^(id x) {
            if (JK_IS_DELEGATE_RSP_SEL(self.delegate, titleDidChangeWithTitle:)) {
                [self.delegate titleDidChangeWithTitle:x];
            }
        }];
        
        [self addSubview:_titleField];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [_titleField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.equalTo(40);
    }];
}
@end
