//
//  YLCTrainTimeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainTimeCell.h"

@implementation YLCTrainTimeCell
{
    BOOL isFirst;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        _beganTitleLabel = [YLCFactory createLabelWithFont:15];
        
        _beganTitleLabel.textColor = RGB(133, 139, 141);
        
        _beganTitleLabel.text = @"开始时间";
        
        [self.contentView addSubview:_beganTitleLabel];
        
//        [_beganTimeField.rac_textSignal subscribeNext:^(id x) {

//        }];
        _beganTimeLabel = [YLCFactory createLabelWithFont:15 color:RGB(60, 60, 60)];
        
        _beganTimeLabel.text = @"请选择";
        
        _beganTimeLabel.userInteractionEnabled = YES;
        
        [self.contentView addSubview:_beganTimeLabel];
        
        _endTitleLabel = [YLCFactory createLabelWithFont:15];
        
        _endTitleLabel.textColor = RGB(133, 139, 141);
        
        _endTitleLabel.text = @"结束时间";
        
        [self.contentView addSubview:_endTitleLabel];
        
        _endTimeLabel = [YLCFactory createLabelWithFont:15 color:RGB(60, 60, 60)];
        
        _endTimeLabel.text = @"请选择";
        
        _endTimeLabel.userInteractionEnabled = YES;
        
        [self.contentView addSubview:_endTimeLabel];
        
//        [_endTimeField.rac_textSignal subscribeNext:^(id x) {

//
//        }];
        UITapGestureRecognizer *firstGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFirst)];
        
        [_beganTimeLabel addGestureRecognizer:firstGes];
        
        UITapGestureRecognizer *secondGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSecond)];
        
        [_endTimeLabel addGestureRecognizer:secondGes];
    }
    return self;
}
- (void)showFirst{
    isFirst = YES;
    [self showTimeList];
}
- (void)showSecond{
    isFirst = NO;
    [self showTimeList];
}
- (void)showTimeList{
    RACSubject *subject = [RACSubject subject];
    [YLCHUDShow showTimePickWithSignal:subject];
    [subject subscribeNext:^(id x) {
        NSLog(@"%@",x);
        if (isFirst) {
            _beganTimeLabel.text = x;
            if (JK_IS_DELEGATE_RSP_SEL(self.delegate, beganTimeDidChangeWithBeganTime:)) {
                [self.delegate beganTimeDidChangeWithBeganTime:x];
            }
        }else{
            _endTimeLabel.text = x;
            if (JK_IS_DELEGATE_RSP_SEL(self.delegate, endTimeDidChangeWithEndTime:)) {
                [self.delegate endTimeDidChangeWithEndTime:x];
            }
        }
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [_beganTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_beganTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_beganTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_endTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_centerX).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_endTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_endTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
//    [_beganTimeField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(weakSelf.mas_left).offset(10);
//        make.centerY.equalTo(weakSelf.mas_centerY);
//        make.width.equalTo(weakSelf.frameWidth/2-20);
//        make.height.equalTo(40);
//    }];
//
//    [_endTimeField mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_beganTimeField.mas_right).offset(20);
//        make.right.equalTo(weakSelf.mas_right).offset(-10);
//        make.centerY.equalTo(weakSelf.mas_centerY);
//        make.height.equalTo(40);
//    }];
}
@end
