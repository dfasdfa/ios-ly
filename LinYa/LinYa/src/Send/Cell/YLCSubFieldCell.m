//
//  YLCSubFieldCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSubFieldCell.h"

@implementation YLCSubFieldCell
{
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(133, 139, 141)];
    
    [self.contentView addSubview:titleLabel];
    
    _subField = [YLCFactory createFieldWithPlaceholder:@"" backgroudColor:[UIColor clearColor]];
    
    _subField.textAlignment = NSTextAlignmentRight;
    
    _subField.font = [UIFont systemFontOfSize:14];
    
    [_subField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, subDidchangeWithTxt:index:)) {
            [self.delegate subDidchangeWithTxt:x index:_index];
        }
    }];
    
    [self.contentView addSubview:_subField];
}
- (void)setModel:(YLCProgramSelectModel *)model{
    titleLabel.text = model.title;
    
    _subField.placeholder = model.placeholder;
    
    _subField.text = [model.subString notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_subField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(200);
        make.height.equalTo(40);
    }];
}
@end
