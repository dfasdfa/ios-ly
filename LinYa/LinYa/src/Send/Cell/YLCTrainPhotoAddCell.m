//
//  YLCTrainPhotoAddCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainPhotoAddCell.h"

@implementation YLCTrainPhotoAddCell
{
    UIImageView *photoView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        photoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image_add"]];
        
        photoView.userInteractionEnabled = YES;
        
        [self.contentView addSubview:photoView];
    }
    return self;
}
- (void)setImage:(UIImage *)image{
    if (image) {
        photoView.image = image;
    }else{
        photoView.image = [UIImage imageNamed:@"image_add"];
    }
    
}
- (void)layoutSubviews{
    WS(weakSelf)
    [photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(100);
        make.height.equalTo(75);
    }];
}
@end
