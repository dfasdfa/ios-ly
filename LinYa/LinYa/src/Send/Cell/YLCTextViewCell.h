//
//  YLCTextViewCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTextViewCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *placeholder;
@property (nonatomic ,copy)NSString *text;
@end
