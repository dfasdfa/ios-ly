//
//  YLCLoginManager.h
//  LinYa
//
//  Created by 初程程 on 2018/3/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCLoginManager : NSObject
+ (void)showLoginViewWithTarget:(id)target;
+ (BOOL)checkLoginStateWithTarget:(id)target;
@end
