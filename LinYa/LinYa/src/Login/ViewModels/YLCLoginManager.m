//
//  YLCLoginManager.m
//  LinYa
//
//  Created by 初程程 on 2018/3/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLoginManager.h"
#import "YLCLoginViewController.h"
#import "YLCNavigationViewController.h"
@implementation YLCLoginManager
+ (void)showLoginViewWithTarget:(id)target{
    
    if (![target isKindOfClass:NSClassFromString(@"UIViewController")]) {
        target = [YLCFactory currentNav];
    }
    if (![target isKindOfClass:NSClassFromString(@"UIViewController")]) {
        target = [YLCFactory getCurrentVC];
    }
    YLCLoginViewController *con = [[YLCLoginViewController alloc] init];
    
    YLCNavigationViewController *loginNav = [[YLCNavigationViewController alloc] initWithRootViewController:con];
    
    [target presentViewController:loginNav animated:YES completion:nil];
}
+ (BOOL)checkLoginStateWithTarget:(id)target{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(userModel.user_id)||JK_IS_STR_NIL(userModel.token)) {
        [self showLoginViewWithTarget:target];
        return NO;
    }
    return YES;
}
@end
