//
//  YLCAccountManager.m
//  YouLe
//
//  Created by 初程程 on 2018/2/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAccountManager.h"

#import "YLCPreference.h"
#import "Lockbox.h"
@implementation YLCAccountManager
{
    YLCAccountModel *mCurrentAccount;
    NSArray *areaList;
}
+ (YLCAccountManager *)shareManager{
    static YLCAccountManager *sharedInstance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance_ = [[YLCAccountManager alloc] init];
    });
    return sharedInstance_;
}
- (YLCAccountModel *)currentAccount{
    
        //在NSUserDefaults里面取出用户数据字典
    NSDictionary *dict = [YLCPreference getGlobalDictKey:ACCOUNT_INFO_DICTIONARY];
//        //通过字典转模型的方式初始化用户数据模型
    YLCAccountModel *info = [YLCAccountModel modelWithDict:dict];
    
    if (JK_IS_STR_NIL(info.user_id)) {
        info.user_id = @"";
    }
    if (JK_IS_STR_NIL(info.token)) {
        info.token = @"";
    }
        
    return info;
}
- (void)saveAccountWithAccount:(YLCAccountModel *)model{
    NSDictionary *dict = [model dictAccountInfo];
    if (JK_IS_DICT_NIL(dict)) {
        [YLCPreference removeGlobalKey:ACCOUNT_INFO_DICTIONARY syncWrite:YES];
    }else{
        [YLCPreference setGlobalDictKey:ACCOUNT_INFO_DICTIONARY value:dict syncWrite:YES];
    }
}
- (void)removeAccount {
    
    YLCAccountModel *account = self.currentAccount;
    
    //    account.userName = nil;
    
    account.user_id = nil;
    
    account.token = nil;
    
    account.is_device = nil;
    
    [self saveAccountWithAccount:account];
}
- (YLCAreaModel *)getAreaModelWithAreaId:(NSString *)areaId{
    for (YLCAreaModel *model in [self getLocationArr]) {
        if ([model.id isEqualToString:areaId]) {
            return model;
        }
    }
    return nil;
}
- (NSArray *)getLocationArr{
    return areaList;
}
- (NSArray *)getProvinceList{
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    YLCAreaModel *baseArae = [[YLCAreaModel alloc] init];
    baseArae.areaname = @"全部";
    baseArae.id = @"0";
    [container addObject:baseArae];
    for (YLCAreaModel *model in [self getLocationArr]) {
        if ([model.level isEqualToString:@"1"]) {
            [container addObject:model];
        }
    }
    return container.copy;
}
- (NSArray *)getCityListWithSuperId:(NSString *)provinceId{
    if ([provinceId isEqualToString:@"0"]) {
        YLCAreaModel *baseArae = [[YLCAreaModel alloc] init];
        baseArae.areaname = @"全部";
        baseArae.id = @"0";
        return @[baseArae];
    }
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (YLCAreaModel *model in [self getLocationArr]) {
        
        if ([model.parentid isEqualToString:provinceId]) {
            
            [container addObject:model];
        }
        
    }
    return container.copy;
}

- (NSArray *)getAreaListWithCityId:(NSString *)cityId
                        provinceId:(NSString *)provinceId{
    
    YLCAreaModel *baseArea = [[YLCAreaModel alloc] init];
    
    baseArea.areaname = @"全部";
    
    baseArea.id = @"0";
    
    if ([cityId isEqualToString:@"0"]) {
        return @[baseArea];
    }
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (YLCAreaModel *model in [self getLocationArr]) {
        
        if ([model.parentid isEqualToString:cityId]) {
            
            [container addObject:model];
        }
        
    }
    
    return container.copy;
    
}
- (void)loadLocationData{
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/area" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSArray *list = responseObject;
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in list) {
            YLCAreaModel *model = [YLCAreaModel modelWithDict:dict];
            
            NSLog(@"%@==%@===%@===%@",model.areaname,model.level,model.id,model.parentid);
            
            [container addObject:model];
        }
        
        areaList = container.copy;
    }];
}
@end
