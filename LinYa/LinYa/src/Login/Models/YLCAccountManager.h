//
//  YLCAccountManager.h
//  YouLe
//
//  Created by 初程程 on 2018/2/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YLCAccountModel.h"
#import "YLCAreaModel.h"
@interface YLCAccountManager : NSObject
@property (nonatomic ,copy)NSString *ryToken;
@property (nonatomic ,assign)BOOL hasLoginRY;
+ (YLCAccountManager *)shareManager;
- (YLCAccountModel *)currentAccount;
- (void)loadLocationData;
- (YLCAreaModel *)getAreaModelWithAreaId:(NSString *)areaId;
- (NSArray *)getLocationArr;
- (NSArray *)getProvinceList;
- (NSArray *)getCityListWithSuperId:(NSString *)provinceId;
- (NSArray *)getAreaListWithCityId:(NSString *)cityId
                        provinceId:(NSString *)provinceId;
- (void)saveAccountWithAccount:(YLCAccountModel *)model;
- (void)removeAccount;
@end
