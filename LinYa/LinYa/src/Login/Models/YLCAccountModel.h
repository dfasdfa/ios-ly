//
//  YLCAccountModel.h
//  YouLe
//
//  Created by 初程程 on 2018/2/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCAccountModel : NSObject
/**
 用户ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 用户token
 */
@property (nonatomic ,copy)NSString *token;
/**
 
 */
@property (nonatomic ,copy)NSString *is_device;
@property (nonatomic ,copy)NSString *account;
@property (nonatomic ,copy)NSString *birthday;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,assign)BOOL hasLogRY;

- (NSDictionary *)dictAccountInfo;
@end
