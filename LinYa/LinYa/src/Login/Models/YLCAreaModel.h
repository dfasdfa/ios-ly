//
//  YLCAreaModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCAreaModel : NSObject
@property (nonatomic ,copy)NSString *areaname;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *is_province;
@property (nonatomic ,copy)NSString *lat;
@property (nonatomic ,copy)NSString *letter;
@property (nonatomic ,copy)NSString *level;
@property (nonatomic ,copy)NSString *lng;
@property (nonatomic ,copy)NSString *parentid;
@property (nonatomic ,copy)NSString *pinyin;
@property (nonatomic ,copy)NSString *shortname;
@end
