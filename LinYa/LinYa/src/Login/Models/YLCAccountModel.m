//
//  YLCAccountModel.m
//  YouLe
//
//  Created by 初程程 on 2018/2/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAccountModel.h"

@implementation YLCAccountModel
- (NSDictionary *)dictAccountInfo{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:0];
    
    if ([YLCFactory isStringOk:self.user_id]) {
        [dict setObject:self.user_id forKey:@"user_id"];
    }
    if ([YLCFactory isStringOk:self.token]) {
        [dict setObject:self.token forKey:@"token"];
    }
    if ([YLCFactory isStringOk:self.is_device]) {
        [dict setObject:self.is_device forKey:@"is_device"];
    }
    return dict;
}
- (NSString *)user_id{
    return [YLCFactory isStringOk:_user_id]?_user_id:@"";
}
- (NSString *)token{
    return [YLCFactory isStringOk:_token]?_token:@"";
}
@end
