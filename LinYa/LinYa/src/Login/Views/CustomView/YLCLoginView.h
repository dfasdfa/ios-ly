//
//  YLCLoginView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCLoginView : UIView
@property (nonatomic ,strong)id target;
@property (nonatomic ,strong)RACSubject *successSignal;
@property (nonatomic ,strong)RACSubject *cancelSignal;
@end
