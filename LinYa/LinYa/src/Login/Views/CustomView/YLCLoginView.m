//
//  YLCLoginView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLoginView.h"
#import "YLCRegisViewController.h"
#import "YLCChangePasswordViewController.h"
#import "WXApi.h"
#import "YLCBindPhoneViewController.h"
#import "RYMessageManager.h"
@implementation YLCLoginView
{
    UIImageView *logoView;
    UIView *fieldBackgroundView;
    UITextField *phoneField;
    UITextField *passwordField;
    UIButton *regisBtn;
    UIButton *forgetWordBtn;
    UIImageView *loginBtn;
    UIImageView *leftLineView;
    UIImageView *rightLineView;
    UILabel *otherTitleLabel;
    UIButton *phoneBtn;
    UIButton *wxBtn;
    UIButton *qqBtn;
    UIImageView *fieldLineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.successSignal = [RACSubject subject];
        self.cancelSignal = [RACSubject subject];
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
//    cancelBtn = [YLCFactory createBtnWithImage:@"login_cancel"];
//
//    [cancelBtn addTarget:self action:@selector(gotoCancel) forControlEvents:UIControlEventTouchUpInside];
//
//    [self addSubview:cancelBtn];
    
    logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_logo"]];
    
    [self addSubview:logoView];
    
    fieldBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    fieldBackgroundView.backgroundColor = [UIColor whiteColor];
    
    fieldBackgroundView.layer.cornerRadius = 5;
    
    fieldBackgroundView.layer.masksToBounds = YES;
    
    [self addSubview:fieldBackgroundView];
    
    phoneField = [[UITextField alloc] initWithFrame:CGRectZero];
    
    UIView *phoneLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_left"]];
    
    leftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [phoneLeftView addSubview:leftImageView];
    
    phoneField.leftView = phoneLeftView;
    
    phoneField.leftViewMode = UITextFieldViewModeAlways;
    
    phoneField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的手机号码" FirstCount:9 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    [self addSubview:phoneField];
    
    passwordField = [[UITextField alloc] init];
    
    passwordField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的密码" FirstCount:7 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *passwordLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *passwordLeftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    
    passwordLeftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [passwordLeftView addSubview:passwordLeftImageView];
    
    UIView *passwordRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 63)];
    
    UIButton *secureBtn = [YLCFactory createBtnWithImage:@"login_secure"];
    
    [secureBtn addTarget:self action:@selector(passTag) forControlEvents:UIControlEventTouchUpInside];
    
    secureBtn.frame = CGRectMake(32, 24, 22, 15);
    
    [passwordRightView addSubview:secureBtn];
    
    UITapGestureRecognizer *passGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passTag)];
    
    [passwordRightView addGestureRecognizer:passGes];
    
    fieldLineView = [[UIImageView alloc] init];
    
    [fieldBackgroundView addSubview:fieldLineView];
    
    fieldLineView.backgroundColor = RGB(183, 183, 183);
    
    passwordField.leftView = passwordLeftView;
    
    passwordField.leftViewMode = UITextFieldViewModeAlways;
    
    passwordField.rightView = passwordRightView;
    
    passwordField.rightViewMode = UITextFieldViewModeAlways;
    
    passwordField.secureTextEntry = YES;
    
    [self addSubview:passwordField];
    
    regisBtn = [YLCFactory createLabelBtnWithTitle:@"我要注册" titleColor:RGB(105, 101, 108)];
    
    regisBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [regisBtn addTarget:self action:@selector(gotoRegis) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:regisBtn];
    
    forgetWordBtn = [YLCFactory createLabelBtnWithTitle:@"忘记密码?" titleColor:RGB(105, 101, 108)];
    
    [forgetWordBtn addTarget:self action:@selector(gotoChangePassword) forControlEvents:UIControlEventTouchUpInside];
    
    forgetWordBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [self addSubview:forgetWordBtn];
    
//    loginBtn = [YLCFactory createBtnWithImage:@""];
    
    loginBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_button"]];
    
    loginBtn.userInteractionEnabled = YES;
    
    loginBtn.contentMode = UIViewContentModeCenter;
    
    [self addSubview:loginBtn];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoLogin)];
    
    [loginBtn addGestureRecognizer:ges];
    
    UILabel *titleLabel = [YLCFactory createLabelWithFont:17 color:[UIColor whiteColor]];
    
    titleLabel.userInteractionEnabled = YES;
    
    titleLabel.text = @"登录";
    
    [loginBtn addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(loginBtn.mas_centerX);
        make.centerY.equalTo(loginBtn.mas_centerY).offset(-10);
    }];
    
    leftLineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_left"]];
    
    [self addSubview:leftLineView];
    
    otherTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(125, 128, 132)];
    
    otherTitleLabel.text = @"其他登录方式";
    
    [self addSubview:otherTitleLabel];
    
    rightLineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_right"]];
    
    [self addSubview:rightLineView];
    
//    phoneBtn = [YLCFactory createBtnWithImage:@"login_phone"];
//
//    [self addSubview:phoneBtn];
    
    wxBtn = [YLCFactory createBtnWithImage:@"login_wx"];
    
    [wxBtn addTarget:self action:@selector(gotoWxLogin) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:wxBtn];
    
    if (![WXApi isWXAppInstalled]) {
        wxBtn.hidden = YES;
    }else{
        wxBtn.hidden = NO;
    }
    
//    qqBtn = [YLCFactory createBtnWithImage:@"login_qq"];
//
//    [self addSubview:qqBtn];
}
- (void)passTag{
    passwordField.secureTextEntry = !passwordField.secureTextEntry;
}
- (void)gotoCancel{
    [self.cancelSignal sendNext:nil];
}
- (void)gotoLogin{
    
    NSString *signString = [NSString stringWithFormat:@"mobile=%@&password=%@",phoneField.text,passwordField.text];
    
    NSString *signMD = [signString md5String];
    
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"password":passwordField.text,
                            @"sign":signMD
                            };
    
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/passport/login" delegate:_target response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }

        YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
        
        model = [YLCAccountModel modelWithDict:responseObject];
        
        [[YLCAccountManager shareManager] saveAccountWithAccount:model];
        
//        [[RYMessageManager shareManager] loginConnectWithToken:model.token];
        [[RYMessageManager shareManager] rongyunLogin];
        [(UINavigationController *)[YLCFactory currentNav] dismissViewControllerAnimated:YES completion:nil];
    }];
}
- (void)gotoRegis{
    YLCRegisViewController *con = [[YLCRegisViewController alloc] init];
    
    UIViewController *viewVc = (UIViewController *)_target;
    
    [viewVc.navigationController pushViewController:con animated:YES];
}
- (void)gotoChangePassword{
    YLCChangePasswordViewController *con = [[YLCChangePasswordViewController alloc] init];
    
    UIViewController *viewVc = (UIViewController *)_target;
    
    con.isPop = YES;
    
    [viewVc.navigationController pushViewController:con animated:YES];
}
- (void)gotoWxLogin{
    if (![WXApi isWXAppInstalled]) {
        [SVProgressHUD showErrorWithStatus:@"您未安装微信，请先安装微信客户端！"];
        return;
    }
    
    [[YLCWxPayManager shareManager] wxLogin];
    
    [[YLCWxPayManager shareManager].loginSignal subscribeNext:^(id x) {
        SendAuthResp *resp = x;
        
        [self uploadWxMessageWithResp:resp];
    }];
}
- (void)uploadWxMessageWithResp:(SendAuthResp *)resp{
    [YLCNetWorking loadNetServiceWithParam:@{@"code":resp.code} method:@"POST" urlPath:@"app/passport/wxLogin" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            
            
            return ;
        }
        NSString *unionId = responseObject[@"unionId"];
        
        if ([YLCFactory isStringOk:unionId]) {
            YLCBindPhoneViewController *con = [[YLCBindPhoneViewController alloc] init];
            
            con.unionId = unionId;
            
            UIViewController *viewVc = (UIViewController *)_target;
            
            [viewVc.navigationController pushViewController:con animated:YES];
        }else{
            NSString *user_id = responseObject[@"user_id"];
            if ([YLCFactory isStringOk:user_id]) {
                YLCAccountModel *userModel = [YLCAccountModel modelWithDict:responseObject];
                
                [[YLCAccountManager shareManager] saveAccountWithAccount:userModel];
                [[RYMessageManager shareManager] rongyunLogin];
                [(UINavigationController *)[YLCFactory currentNav] dismissViewControllerAnimated:YES completion:nil];
            }else{
                [[RYMessageManager shareManager] rongyunLogin];
                [(UINavigationController *)[YLCFactory currentNav] dismissViewControllerAnimated:YES completion:nil];
            }

        }

    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    
    [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top).offset(44);
        make.width.and.height.equalTo(65);
    }];
    
    [fieldBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.top.equalTo(logoView.mas_bottom).offset(64);
        make.height.equalTo(140);
        make.right.equalTo(weakSelf.mas_right).offset(-11);
    }];
   
    [regisBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left).offset(5);
        make.top.equalTo(fieldBackgroundView.mas_bottom).offset(16);
        make.width.equalTo(70);
        make.height.equalTo(15);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_top);
        make.bottom.equalTo(fieldBackgroundView.mas_centerY).offset(-0.25);
    }];
    
    [fieldLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.centerY.equalTo(fieldBackgroundView.mas_centerY);
        make.height.equalTo(0.5);
    }];
    
    
    [passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_centerY).offset(0.25);
        make.bottom.equalTo(fieldBackgroundView.mas_bottom);
    }];
    
    [forgetWordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(fieldBackgroundView.mas_right).offset(-5);
        make.top.equalTo(regisBtn.mas_top);
        make.width.equalTo(75);
        make.height.equalTo(15);
    }];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(regisBtn.mas_bottom).offset(35);
        make.height.equalTo(47);
    }];
    
    [otherTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(loginBtn.mas_bottom).offset(59);
    }];
    
    [leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(otherTitleLabel.mas_left).offset(-10);
        make.width.equalTo(119);
        make.centerY.equalTo(otherTitleLabel.mas_centerY);
        make.height.equalTo(2);
    }];
    
    [rightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(otherTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(otherTitleLabel.mas_centerY);
        make.width.equalTo(119);
        make.height.equalTo(2);
    }];
    
    [wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(otherTitleLabel.mas_bottom).offset(30);
        make.width.and.height.equalTo(39);
    }];
    
//    [phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(wxBtn.mas_left).offset(-33);
//        make.centerY.equalTo(wxBtn.mas_centerY);
//        make.width.and.height.equalTo(39);
//    }];
    
//    [qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wxBtn.mas_right).offset(33);
//        make.centerY.equalTo(wxBtn.mas_centerY);
//        make.width.and.height.equalTo(39);
//    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
