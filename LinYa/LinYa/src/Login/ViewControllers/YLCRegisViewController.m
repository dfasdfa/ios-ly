//
//  YLCRegisViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRegisViewController.h"
#import "YLCTimeButtonView.h"
#import "YLCBindPhoneViewController.h"
@interface YLCRegisViewController ()

@end

@implementation YLCRegisViewController
{
    UIImageView *logoView;
    UIView *fieldBackgroundView;
    UITextField *phoneField;
    UITextField *messageCodeField;
    UITextField *passwordField;
    UIImageView *loginBtn;
    UIImageView *leftLineView;
    UIImageView *rightLineView;
    UILabel *otherTitleLabel;
//    UIButton *phoneBtn;
    UIButton *wxBtn;
//    UIButton *qqBtn;
    UIImageView *fieldLineView;
    UIImageView *fieldLineView2;
    YLCTimeButtonView *buttonView;
}
- (UIBarButtonItem *)leftBarButtonItem{
    return [YLCFactory createImageBarButtonWithImageName:@"item_back" selector:@selector(goback) target:self];
}
- (void)goback{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
    [self layoutSubView];
}
- (void)popToLogin{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)createCustomView{
    
    self.view.backgroundColor = RGB(229, 240, 255);
    
    logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_logo"]];
    
    [self.view addSubview:logoView];
    
    fieldBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    fieldBackgroundView.backgroundColor = [UIColor whiteColor];
    
    fieldBackgroundView.layer.cornerRadius = 5;
    
    fieldBackgroundView.layer.masksToBounds = YES;
    
    [self.view addSubview:fieldBackgroundView];
    
    phoneField = [[UITextField alloc] initWithFrame:CGRectZero];
    
    UIView *phoneLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_left"]];
    
    leftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [phoneLeftView addSubview:leftImageView];
    
    phoneField.leftView = phoneLeftView;
    
    phoneField.leftViewMode = UITextFieldViewModeAlways;
    
    phoneField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的手机号码" FirstCount:9 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    [self.view addSubview:phoneField];
    
    fieldLineView = [[UIImageView alloc] init];
    
    [fieldBackgroundView addSubview:fieldLineView];
    
    fieldLineView.backgroundColor = RGB(183, 183, 183);
    
    buttonView = [[YLCTimeButtonView alloc] initWithFrame:CGRectMake(10, 20, 100, 30) title:@"获取验证码" timeLimit:60];
    
//    @weakify(self);
    
    [buttonView.touchSignal subscribeNext:^(id x) {
        [self getSmsCode];
    }];

    
    messageCodeField = [[UITextField alloc] init];
    
    messageCodeField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入验证码" FirstCount:6 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *messageCodeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *messageCodeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    
    messageCodeImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [messageCodeView addSubview:messageCodeImageView];
    
    messageCodeField.leftView = messageCodeView;
    
    messageCodeField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *messageCodeRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,120, 70)];
    
    messageCodeField.rightView = messageCodeRightView;
    
    messageCodeField.rightViewMode = UITextFieldViewModeAlways;
    
    [messageCodeRightView addSubview:buttonView];
    
    [fieldBackgroundView addSubview:messageCodeField];
    
    fieldLineView2 = [[UIImageView alloc] init];
    
    fieldLineView2.backgroundColor = RGB(183, 183, 183);
    
    [fieldBackgroundView addSubview:fieldLineView2];
    
    passwordField = [[UITextField alloc] init];
    
    passwordField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的密码" FirstCount:7 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *passwordLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *passwordLeftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    
    passwordLeftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [passwordLeftView addSubview:passwordLeftImageView];
    
    UIView *passwordRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 63)];
    
    UIButton *secureBtn = [YLCFactory createBtnWithImage:@"login_secure"];
    
    [secureBtn addTarget:self action:@selector(passTag) forControlEvents:UIControlEventTouchUpInside];
    
    secureBtn.frame = CGRectMake(32, 24, 22, 15);
    
    [passwordRightView addSubview:secureBtn];
    
    passwordField.leftView = passwordLeftView;
    
    passwordField.leftViewMode = UITextFieldViewModeAlways;
    
    passwordField.rightView = passwordRightView;
    
    passwordField.rightViewMode = UITextFieldViewModeAlways;
    
    passwordField.secureTextEntry = YES;
    
    [self.view addSubview:passwordField];
    
    loginBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_button"]];
    
    loginBtn.userInteractionEnabled = YES;
    
    loginBtn.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:loginBtn];
    
    UITapGestureRecognizer *loginGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRegis)];
    
    [loginBtn addGestureRecognizer:loginGes];
    
    UILabel *titleLabel = [YLCFactory createLabelWithFont:17 color:[UIColor whiteColor]];
    
    titleLabel.userInteractionEnabled = YES;
    
    titleLabel.text = @"注册";
    
    [loginBtn addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(loginBtn.mas_centerX);
        make.centerY.equalTo(loginBtn.mas_centerY).offset(-10);
    }];
    
    leftLineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_left"]];
    
    [self.view addSubview:leftLineView];
    
    otherTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(125, 128, 132)];
    
    otherTitleLabel.text = @"其他登录方式";
    
    [self.view addSubview:otherTitleLabel];
    
    rightLineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_right"]];
    
    [self.view addSubview:rightLineView];
    
//    phoneBtn = [YLCFactory createBtnWithImage:@"login_phone"];
//
//    [self.view addSubview:phoneBtn];
    
    wxBtn = [YLCFactory createBtnWithImage:@"login_wx"];
    
    [wxBtn addTarget:self action:@selector(gotoWxLogin) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:wxBtn];
    
    if (![WXApi isWXAppInstalled]) {
        wxBtn.hidden = YES;
    }else{
        wxBtn.hidden = NO;
    }
    
//    qqBtn = [YLCFactory createBtnWithImage:@"login_qq"];
//
//    [self.view addSubview:qqBtn];

}
- (void)gotoWxLogin{
    [[YLCWxPayManager shareManager] wxLogin];
    
    [[YLCWxPayManager shareManager].loginSignal subscribeNext:^(id x) {
        SendAuthResp *resp = x;
        
        [self uploadWxMessageWithResp:resp];
    }];
}
- (void)uploadWxMessageWithResp:(SendAuthResp *)resp{
    [YLCNetWorking loadNetServiceWithParam:@{@"code":resp.code} method:@"POST" urlPath:@"app/passport/wxLogin" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            
            
            return ;
        }
        NSString *unionId = responseObject[@"unionId"];
        
        if ([YLCFactory isStringOk:unionId]) {
            YLCBindPhoneViewController *con = [[YLCBindPhoneViewController alloc] init];
            
            con.unionId = unionId;
            
            [self.navigationController pushViewController:con animated:YES];
        }else{
            NSString *user_id = responseObject[@"user_id"];
            if ([YLCFactory isStringOk:user_id]) {
                YLCAccountModel *userModel = [YLCAccountModel modelWithDict:responseObject];
                
                [[YLCAccountManager shareManager] saveAccountWithAccount:userModel];
                
                [(UINavigationController *)[YLCFactory currentNav] dismissViewControllerAnimated:YES completion:nil];
            }else{
                [(UINavigationController *)[YLCFactory currentNav] dismissViewControllerAnimated:YES completion:nil];
            }
            
        }
        
    }];
}
- (void)getSmsCode{
    if (JK_IS_STR_NIL(phoneField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        [buttonView resetBtn];
        return;
    }
    
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"type":@"0"
                            };
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/public/sendSmsCode" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            [buttonView resetBtn];
            return ;
        }

    }];
}
- (void)passTag{
    passwordField.secureTextEntry = !passwordField.secureTextEntry;
}
- (void)gotoRegis{
    
    if (JK_IS_STR_NIL(phoneField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    if (JK_IS_STR_NIL(messageCodeField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
        return;
    }
    if (JK_IS_STR_NIL(passwordField.text)||passwordField.text.length<6||passwordField.text.length>12) {
        [SVProgressHUD showErrorWithStatus:@"请输入6-12位密码"];
        return;
    }
    
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"code":messageCodeField.text,
                            @"pwd":passwordField.text
                            };
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/passport/reg" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        YLCAccountModel *model = [YLCAccountModel modelWithDict:responseObject];
        
        [[YLCAccountManager shareManager] saveAccountWithAccount:model];
        
        [SVProgressHUD showSuccessWithStatus:@"注册成功！"];
        
        [self popToLogin];
    }];
}
- (void)layoutSubView{
    WS(weakSelf)
    
    [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(weakSelf.view.mas_top).offset(44);
        make.width.and.height.equalTo(65);
    }];
    
    [fieldBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(11);
        make.top.equalTo(logoView.mas_bottom).offset(44);
        make.height.equalTo(211);
        make.right.equalTo(weakSelf.view.mas_right).offset(-11);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_top);
        make.height.equalTo(70);
    }];
    
    [fieldLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(phoneField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [messageCodeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [fieldLineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(messageCodeField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView2.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_bottom).offset(35);
        make.height.equalTo(47);
    }];
    
    [otherTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(loginBtn.mas_bottom).offset(49);
    }];
    
    [leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(otherTitleLabel.mas_left).offset(-10);
        make.width.equalTo(119);
        make.centerY.equalTo(otherTitleLabel.mas_centerY);
        make.height.equalTo(2);
    }];
    
    [rightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(otherTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(otherTitleLabel.mas_centerY);
        make.width.equalTo(119);
        make.height.equalTo(2);
    }];
    
    [wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(otherTitleLabel.mas_bottom).offset(30);
        make.width.and.height.equalTo(39);
    }];
    
//    [phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(wxBtn.mas_left).offset(-33);
//        make.centerY.equalTo(wxBtn.mas_centerY);
//        make.width.and.height.equalTo(39);
//    }];
//    
//    [qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(wxBtn.mas_right).offset(33);
//        make.centerY.equalTo(wxBtn.mas_centerY);
//        make.width.and.height.equalTo(39);
//    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
