//
//  YLCBindPhoneViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBindPhoneViewController.h"
#import "YLCTimeButtonView.h"
#import "RYMessageManager.h"
@interface YLCBindPhoneViewController ()

@end

@implementation YLCBindPhoneViewController
{
    UIView *fieldBackgroundView;
    UITextField *phoneField;
    UITextField *messageCodeField;
    UITextField *passwordField;
    UIImageView *loginBtn;
    UIImageView *fieldLineView;
    UIImageView *fieldLineView2;
    YLCTimeButtonView *buttonView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
    [self layoutUI];
}
- (void)createCustomView{
    fieldBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    fieldBackgroundView.backgroundColor = [UIColor whiteColor];
    
    fieldBackgroundView.layer.cornerRadius = 5;
    
    fieldBackgroundView.layer.masksToBounds = YES;
    
    [self.view addSubview:fieldBackgroundView];
    
    phoneField = [[UITextField alloc] initWithFrame:CGRectZero];
    
    UIView *phoneLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_left"]];
    
    leftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [phoneLeftView addSubview:leftImageView];
    
    phoneField.leftView = phoneLeftView;
    
    phoneField.leftViewMode = UITextFieldViewModeAlways;
    
    phoneField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的手机号码" FirstCount:9 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    [self.view addSubview:phoneField];
    
    fieldLineView = [[UIImageView alloc] init];
    
    [fieldBackgroundView addSubview:fieldLineView];
    
    fieldLineView.backgroundColor = RGB(183, 183, 183);
    
    buttonView = [[YLCTimeButtonView alloc] initWithFrame:CGRectMake(10, 20, 100, 30) title:@"获取验证码" timeLimit:60];
    
    //    @weakify(self);
    
    [buttonView.touchSignal subscribeNext:^(id x) {
        [self getSmsCode];
    }];
    
    
    messageCodeField = [[UITextField alloc] init];
    
    messageCodeField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入验证码" FirstCount:6 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *messageCodeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *messageCodeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_phone"]];
    
    messageCodeImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [messageCodeView addSubview:messageCodeImageView];
    
    messageCodeField.leftView = messageCodeView;
    
    messageCodeField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *messageCodeRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,120, 70)];
    
    messageCodeField.rightView = messageCodeRightView;
    
    messageCodeField.rightViewMode = UITextFieldViewModeAlways;
    
    [messageCodeRightView addSubview:buttonView];
    
    [fieldBackgroundView addSubview:messageCodeField];
    
    fieldLineView2 = [[UIImageView alloc] init];
    
    fieldLineView2.backgroundColor = RGB(183, 183, 183);
    
    [fieldBackgroundView addSubview:fieldLineView2];
    
    passwordField = [[UITextField alloc] init];
    
    passwordField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的密码" FirstCount:7 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *passwordLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *passwordLeftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    
    passwordLeftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [passwordLeftView addSubview:passwordLeftImageView];
    
    UIView *passwordRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 63)];
    
    UIButton *secureBtn = [YLCFactory createBtnWithImage:@"login_secure"];
    
    secureBtn.frame = CGRectMake(32, 24, 22, 15);
    
    [passwordRightView addSubview:secureBtn];
    
    passwordField.leftView = passwordLeftView;
    
    passwordField.leftViewMode = UITextFieldViewModeAlways;
    
    passwordField.rightView = passwordRightView;
    
    passwordField.rightViewMode = UITextFieldViewModeAlways;
    
    passwordField.secureTextEntry = YES;
    
    [self.view addSubview:passwordField];
    
    loginBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_button"]];
    
    loginBtn.userInteractionEnabled = YES;
    
    loginBtn.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:loginBtn];
    
    UITapGestureRecognizer *loginGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRegis)];
    
    [loginBtn addGestureRecognizer:loginGes];
    
    UILabel *titleLabel = [YLCFactory createLabelWithFont:17 color:[UIColor whiteColor]];
    
    titleLabel.userInteractionEnabled = YES;
    
    titleLabel.text = @"提交";
    
    [loginBtn addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(loginBtn.mas_centerX);
        make.centerY.equalTo(loginBtn.mas_centerY).offset(-10);
    }];

}
- (void)getSmsCode{
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"type":@"0"
                            };
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/public/sendSmsCode" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
    }];
}
- (void)gotoRegis{
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"unionId":_unionId,
                            @"code":messageCodeField.text,
                            @"pwd":passwordField.text
                            };
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/passport/reg" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        YLCAccountModel *model = [YLCAccountModel modelWithDict:responseObject];
        
        [[YLCAccountManager shareManager] saveAccountWithAccount:model];
        [[RYMessageManager shareManager] rongyunLogin];
        [SVProgressHUD showSuccessWithStatus:@"注册成功！"];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}
- (void)layoutUI{
    WS(weakSelf)
    [fieldBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(11);
        make.top.equalTo(weakSelf.view.mas_top).offset(44);
        make.height.equalTo(211);
        make.right.equalTo(weakSelf.view.mas_right).offset(-11);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_top);
        make.height.equalTo(70);
    }];
    
    [fieldLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(phoneField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [messageCodeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [fieldLineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(messageCodeField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView2.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_bottom).offset(35);
        make.height.equalTo(47);
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
