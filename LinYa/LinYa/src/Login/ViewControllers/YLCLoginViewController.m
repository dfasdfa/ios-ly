//
//  YLCLoginViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLoginViewController.h"
#import "YLCLoginView.h"
#import "YLCAccountModel.h"
#import "YLCAccountManager.h"
@interface YLCLoginViewController ()
@property (nonatomic ,strong)YLCLoginView *loginView;
@end

@implementation YLCLoginViewController
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.hidden = YES;
//}
- (UIBarButtonItem *)leftBarButtonItem{
    return [YLCFactory createImageBarButtonWithImageName:@"item_back" selector:@selector(goback) target:self];
}
- (void)updateNavigationBar {
    
    UIColor *navBackgroundColor = RGB(235, 235, 235);
    
    [self.navigationController.navigationBar setBarTintColor:navBackgroundColor];
    
    NSDictionary *titleAttribute = [self navTitleAttributes];
    if (!JK_IS_DICT_NIL(titleAttribute)) {
        [self.navigationController.navigationBar setTitleTextAttributes:titleAttribute];
    }
}
- (NSDictionary *)navTitleAttributes {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16], NSFontAttributeName, RGB(60, 63, 64), NSForegroundColorAttributeName, nil];
    
    return attributes;
}
- (void)goback{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
    [self bindViewModel];
    [self updateNavigationBar];
}

- (void)createCustomView{
    self.loginView = [[YLCLoginView alloc] initWithFrame:self.view.bounds];
    
    self.loginView.target = self;
    
    [self.view addSubview:self.loginView];
}
- (void)bindViewModel{
    @weakify(self);
    [self.loginView.successSignal subscribeNext:^(id x) {
        YLCAccountModel *model = [YLCAccountModel modelWithDict:x];
        [[YLCAccountManager shareManager] saveAccountWithAccount:model];
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self.loginView.cancelSignal subscribeNext:^(id x) {
        @strongify(self);
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}
- (NSString *)title{
    return @"登录";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
