//
//  YLCChangePasswordViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCChangePasswordViewController.h"
#import "YLCTimeButtonView.h"
@interface YLCChangePasswordViewController ()

@end

@implementation YLCChangePasswordViewController
{
//    UIButton *cancelBtn;
    UIImageView *logoView;
    UIView *fieldBackgroundView;
    UITextField *phoneField;
    UITextField *messageCodeField;
    UITextField *passwordField;
    UITextField *rePasswordField;
    UIImageView *loginBtn;
    UIImageView *fieldLineView;
    UIImageView *fieldLineView2;
    UIImageView *fieldLineView3;
    YLCTimeButtonView *buttonView;
}
- (void)popToLogin{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
    [self layoutSubView];
}
- (void)createCustomView{
    
    self.view.backgroundColor = RGB(229, 240, 255);
    
//    cancelBtn = [YLCFactory createBtnWithImage:@"login_cancel"];
//
//    [cancelBtn addTarget:self action:@selector(popToLogin) forControlEvents:UIControlEventTouchUpInside];
//
//    [self.view addSubview:cancelBtn];
//
//    logoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_logo"]];
//
//    [self.view addSubview:logoView];
    
    fieldBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    fieldBackgroundView.backgroundColor = [UIColor whiteColor];
    
    fieldBackgroundView.layer.cornerRadius = 5;
    
    fieldBackgroundView.layer.masksToBounds = YES;
    
    [self.view addSubview:fieldBackgroundView];
    
    phoneField = [[UITextField alloc] initWithFrame:CGRectZero];
    
    UIView *phoneLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phone_gray"]];
    
    leftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [phoneLeftView addSubview:leftImageView];
    
    phoneField.leftView = phoneLeftView;
    
    phoneField.leftViewMode = UITextFieldViewModeAlways;
    
    phoneField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的手机号码" FirstCount:9 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    [self.view addSubview:phoneField];
    
    fieldLineView = [[UIImageView alloc] init];
    
    [fieldBackgroundView addSubview:fieldLineView];
    
    fieldLineView.backgroundColor = RGB(183, 183, 183);
    
    buttonView = [[YLCTimeButtonView alloc] initWithFrame:CGRectMake(10, 20, 100, 30) title:@"获取验证码" timeLimit:60];
    
    //    @weakify(self);
    
    [buttonView.touchSignal subscribeNext:^(id x) {
        [self getSmsCode];
    }];
    
    
    messageCodeField = [[UITextField alloc] init];
    
    messageCodeField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入验证码" FirstCount:6 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *messageCodeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *messageCodeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"message_code"]];
    
    messageCodeImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [messageCodeView addSubview:messageCodeImageView];
    
    messageCodeField.leftView = messageCodeView;
    
    messageCodeField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *messageCodeRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,120, 70)];
    
    messageCodeField.rightView = messageCodeRightView;
    
    messageCodeField.rightViewMode = UITextFieldViewModeAlways;
    
    [messageCodeRightView addSubview:buttonView];
    
    [fieldBackgroundView addSubview:messageCodeField];
    
    fieldLineView2 = [[UIImageView alloc] init];
    
    fieldLineView2.backgroundColor = RGB(183, 183, 183);
    
    [fieldBackgroundView addSubview:fieldLineView2];
    
    passwordField = [[UITextField alloc] init];
    
    passwordField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请输入您的密码" FirstCount:7 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *passwordLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *passwordLeftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock_gray"]];
    
    passwordLeftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [passwordLeftView addSubview:passwordLeftImageView];
    
    UIView *passwordRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 63)];
    
    UIButton *secureBtn = [YLCFactory createBtnWithImage:@"login_secure"];
    
    [secureBtn addTarget:self action:@selector(passTag) forControlEvents:UIControlEventTouchUpInside];
    
    secureBtn.frame = CGRectMake(32, 24, 22, 15);
    
    [passwordRightView addSubview:secureBtn];
    
    passwordField.leftView = passwordLeftView;
    
    passwordField.leftViewMode = UITextFieldViewModeAlways;
    
    passwordField.rightView = passwordRightView;
    
    passwordField.rightViewMode = UITextFieldViewModeAlways;
    
    passwordField.secureTextEntry = YES;
    
    [self.view addSubview:passwordField];
    
    fieldLineView3 = [[UIImageView alloc] init];
    
    fieldLineView3.backgroundColor = RGB(183, 183, 183);
    
    [fieldBackgroundView addSubview:fieldLineView3];
    
    rePasswordField = [[UITextField alloc] init];
    
    rePasswordField.attributedPlaceholder = [YLCFactory getAttributedStringWithString:@"请再次输入您的密码" FirstCount:7 secondCount:0 firstFont:16 secondFont:16 firstColor:RGB(229, 209, 189) secondColor:RGB(229, 209, 189)];
    
    UIView *rePasswordLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 89, 65)];
    
    UIImageView *rePasswordLeftImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock_gray"]];
    
    rePasswordLeftImageView.frame = CGRectMake(32, 23, 17, 19);
    
    [rePasswordLeftView addSubview:rePasswordLeftImageView];
    
    UIView *rePasswordRightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 86, 63)];
    
    UIButton *reSecureBtn = [YLCFactory createBtnWithImage:@"login_secure"];
    
    [reSecureBtn addTarget:self action:@selector(passTag) forControlEvents:UIControlEventTouchUpInside];
    
    reSecureBtn.frame = CGRectMake(32, 24, 22, 15);
    
    [rePasswordRightView addSubview:reSecureBtn];
    
    rePasswordField.leftView = rePasswordLeftView;
    
    rePasswordField.leftViewMode = UITextFieldViewModeAlways;
    
    rePasswordField.rightView = rePasswordRightView;
    
    rePasswordField.rightViewMode = UITextFieldViewModeAlways;
    
    rePasswordField.secureTextEntry = YES;
    
    [self.view addSubview:rePasswordField];
    
    loginBtn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login_button"]];
    
    loginBtn.userInteractionEnabled = YES;
    
    loginBtn.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.view addSubview:loginBtn];
    
    UITapGestureRecognizer *loginGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoChangePassword)];
    
    [loginBtn addGestureRecognizer:loginGes];
    
    UILabel *titleLabel = [YLCFactory createLabelWithFont:17 color:[UIColor whiteColor]];
    
    titleLabel.userInteractionEnabled = YES;
    
    titleLabel.text = @"修改密码";
    
    [loginBtn addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(loginBtn.mas_centerX);
        make.centerY.equalTo(loginBtn.mas_centerY).offset(-10);
    }];
}
- (void)passTag{
    passwordField.secureTextEntry = !passwordField.secureTextEntry;
}
- (void)getSmsCode{
    if (JK_IS_STR_NIL(phoneField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        [buttonView resetBtn];
        return;
    }

    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"type":@"1"
                            };
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/public/sendSmsCode" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            [buttonView resetBtn];
            return ;
        }
        
    }];
}
- (void)gotoChangePassword{
    if (JK_IS_STR_NIL(phoneField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    if (JK_IS_STR_NIL(messageCodeField.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
        return;
    }
    if (JK_IS_STR_NIL(passwordField.text)||passwordField.text.length<6||passwordField.text.length>12) {
        [SVProgressHUD showErrorWithStatus:@"请输入6-12位密码"];
        return;
    }
    if (![passwordField.text isEqualToString:rePasswordField.text]) {
        [SVProgressHUD showErrorWithStatus:@"两次密码输入不一致"];
        return;
    }
    NSDictionary *param = @{@"mobile":phoneField.text,
                            @"code":messageCodeField.text,
                            @"password":passwordField.text,
                            @"rePassword":rePasswordField.text
                            };
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/passport/findPwd" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }

        [SVProgressHUD showSuccessWithStatus:@"修改成功！"];
        if (_isPop) {
            [self popToLogin];
        }else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }];
}
- (void)layoutSubView{
    WS(weakSelf)
//    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.view.mas_right).offset(-31);
//        make.top.equalTo(weakSelf.view.mas_top).offset(45);
//        make.width.and.height.equalTo(25);
//    }];
    
//    [logoView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(weakSelf.view.mas_centerX);
//        make.top.equalTo(weakSelf.view.mas_top).offset(74);
//        make.width.and.height.equalTo(65);
//    }];
    
    [fieldBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(11);
        make.top.equalTo(weakSelf.view.mas_top).offset(74);
        make.height.equalTo(281.5);
        make.right.equalTo(weakSelf.view.mas_right).offset(-11);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_top);
        make.height.equalTo(70);
    }];
    
    [fieldLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(phoneField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [messageCodeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [fieldLineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(messageCodeField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [passwordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView2.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [fieldLineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(passwordField.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [rePasswordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldLineView3.mas_bottom);
        make.height.equalTo(70);
    }];
    
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(fieldBackgroundView.mas_left);
        make.right.equalTo(fieldBackgroundView.mas_right);
        make.top.equalTo(fieldBackgroundView.mas_bottom).offset(35);
        make.height.equalTo(47);
    }];
    
}
- (NSString *)title{
    return @"修改密码";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
