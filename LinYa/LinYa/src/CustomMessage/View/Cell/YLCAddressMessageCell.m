//
//  YLCAdressMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAddressMessageCell.h"

@implementation YLCAddressMessageCell
{
    UIImageView *typeView;
    UILabel *customNameLabel;
    UILabel *phoneLabel;
    UILabel *addressLabel;
    UIButton *editBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    typeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goods_selected"]];
    
    [self.contentView addSubview:typeView];
    
    customNameLabel = [YLCFactory createLabelWithFont:18 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:customNameLabel];
    
    phoneLabel = [YLCFactory createLabelWithFont:18 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:phoneLabel];
    
    addressLabel = [YLCFactory createLabelWithFont:15 color:RGB(153, 153, 153)];
    
    [self.contentView addSubview:addressLabel];
    
    editBtn = [YLCFactory createBtnWithImage:@"goods_edit"];
    
    [self.contentView addSubview:editBtn];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [typeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.height.equalTo(25);
    }];
    
    [customNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(typeView.mas_right).offset(2);
        make.top.equalTo(typeView.mas_bottom);
    }];
    
    [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customNameLabel.mas_right).offset(10);
        make.centerY.equalTo(customNameLabel.mas_centerY);
    }];
    
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customNameLabel.mas_left);
        make.top.equalTo(customNameLabel.mas_bottom).offset(10);
    }];
    
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-21);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(19);
    }];
}
@end
