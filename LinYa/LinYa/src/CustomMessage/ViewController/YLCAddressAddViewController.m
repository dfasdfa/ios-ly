//
//  YLCAddressAddViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAddressAddViewController.h"

@interface YLCAddressAddViewController ()

@end

@implementation YLCAddressAddViewController
{
    UILabel *tipLabel;
    UIView *backgroundView;
    UITextField *nameField;
    UITextField *phoneField;
    UIView *areaBackgroundView;
    UIImageView *areaIconView;
    UILabel *cityLabel;
    UITextField *addressField;
    UIView *normalBackView;
    UILabel *normalTitleLabel;
    UISwitch *setSwitchView;
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"保存" titleColor:[UIColor whiteColor] selector:@selector(saveAddress) target:self];
    
}
- (void)saveAddress{
    [SVProgressHUD showSuccessWithStatus:@"保存成功"];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    tipLabel = [YLCFactory createLabelWithFont:14 color:RGB(113, 125, 133)];
    
    tipLabel.text = @"为了更好的为您配送，请填写服务地址和联系方式";
    
    [self.view addSubview:tipLabel];
    
    backgroundView = [[UIView alloc] init];
    
    backgroundView.backgroundColor = RGB(235, 235, 235);
    
    [self.view addSubview:backgroundView];
    
    nameField = [YLCFactory  createFieldWithLeftImage:@"goods_name" placeholder:@"请输入姓名"];
    
    [backgroundView addSubview:nameField];
    
    phoneField = [YLCFactory createFieldWithLeftImage:@"goods_phone" placeholder:@"请输入手机号"];
    
    [backgroundView addSubview:phoneField];
    
    areaBackgroundView = [[UIView alloc] init];
    
    areaBackgroundView.backgroundColor = [UIColor whiteColor];
    
    [backgroundView addSubview:areaBackgroundView];
    
    areaIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goods_area"]];
    
    [backgroundView addSubview:areaIconView];
    
    cityLabel = [YLCFactory createLabelWithFont:15 color:RGB(178,188,196)];
    
    cityLabel.text = @"请选择您的省市区";
    
    [backgroundView addSubview:cityLabel];
    
    addressField = [YLCFactory createFieldWithLeftImage:@"job_area" placeholder:@"请填写您的详细地址（门牌号等）"];
    
    [backgroundView addSubview:addressField];
    
    normalBackView = [[UIView alloc] init];
    
    normalBackView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:normalBackView];
    
    normalTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(56, 60, 63)];
    
    normalTitleLabel.text = @"设为默认地址";
    
    [normalBackView addSubview:normalTitleLabel];
    
    setSwitchView = [[UISwitch alloc] init];
    
    setSwitchView.on = YES;
    
    [setSwitchView addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:setSwitchView];
    WS(weakSelf)
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(13);
        make.top.equalTo(weakSelf.view.mas_top).offset(14);
    }];
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.top.equalTo(tipLabel.mas_bottom).offset(14);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(240+1.5);
    }];
    
    [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left);
        make.top.equalTo(backgroundView.mas_top);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(60);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameField.mas_left);
        make.top.equalTo(nameField.mas_bottom).offset(0.5);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(60);
    }];
    
    [areaBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneField.mas_left);
        make.top.equalTo(phoneField.mas_bottom).offset(0.5);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(60);
    }];
    
    [areaIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(7);
        make.centerY.equalTo(areaBackgroundView.mas_centerY);
        make.width.height.equalTo(20);
    }];
    
    [cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaIconView.mas_right).offset(10);
        make.centerY.equalTo(areaIconView.mas_centerY);
    }];
    
    [addressField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left);
        make.top.equalTo(areaBackgroundView.mas_bottom).offset(0.5);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(60);
    }];
    
    [normalBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left);
        make.top.equalTo(backgroundView.mas_bottom).offset(20);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(60);
    }];
    
    [normalTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(normalBackView.mas_left).offset(10);
        make.centerY.equalTo(normalBackView.mas_centerY);
    }];
    
    [setSwitchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(normalBackView.mas_right).offset(-10);
        make.centerY.equalTo(normalBackView.mas_centerY);
        make.width.equalTo(60);
        make.height.equalTo(28);
    }];
}
- (void)switchAction:(UISwitch *)sender{
    if (sender.isOn) {
        
    }else{
        
    }
}
- (NSString *)title{
    return @"新增地址";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
