//
//  YLCAdressManagerViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAddressManagerViewController.h"
#import "YLCAddressMessageCell.h"
#import "YLCAddressEmptyView.h"
#import "YLCAddressAddViewController.h"
#define ADDRESS_CELL  @"YLCAddressMessageCell"
@interface YLCAddressManagerViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong)YLCAddressEmptyView *emptyView;
@end

@implementation YLCAddressManagerViewController
{
    UICollectionView *addressTable;
    NSArray *addessList;
}
- (YLCAddressEmptyView *)emptyView{
    if (!_emptyView) {
        _emptyView = [[YLCAddressEmptyView alloc] initWithFrame:self.view.bounds];
        
        [_emptyView.insertSubject subscribeNext:^(id x) {
            [self gotoAdd];
        }];
    }
    return _emptyView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{

}
- (void)reloadData{
    if (JK_IS_ARRAY_NIL(addessList)) {
        
        [addressTable showEmptyViewWithEmptyView:self.emptyView];
    }else{
        [addressTable removeEmptyView];
    }
}
- (void)createCustomView{
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    addressTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    addressTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [addressTable registerClass:[YLCAddressMessageCell class] forCellWithReuseIdentifier:ADDRESS_CELL];
    
    addressTable.delegate = self;
    
    addressTable.dataSource = self;
    
    [self.view addSubview:addressTable];
    
    [addressTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [self reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return addessList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCAddressMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ADDRESS_CELL forIndexPath:indexPath];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frameWidth, 60);
    
}
- (void)gotoAdd{
    
    YLCAddressAddViewController *con = [[YLCAddressAddViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
