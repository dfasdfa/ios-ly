//
//  YLCChatBaseViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCChatBaseViewController.h"
#import "IQKeyboardManager.h"
#import "RYMessageManager.h"
#import "YLCTabBarViewController.h"
#import "AppDelegate.h"
@interface YLCChatBaseViewController ()

@end

@implementation YLCChatBaseViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    YLCTabBarViewController *con = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
    
    [con hiddenRed];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    self.enableNewComingMessageIcon = YES;
    
    self.hidesBottomBarWhenPushed = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
