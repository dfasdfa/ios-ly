//
//  YLCRYConversationListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/17.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRYConversationListViewController.h"
#import "YLCChatBaseViewController.h"
#import "YLCMessageView.h"
#import "YLCSystemMessageViewController.h"
#import "YLCReplyMessageViewController.h"
#import <RongIMLib/RongIMLib.h>
#import "RYMessageManager.h"
#import "YLCTabBarViewController.h"
@interface YLCRYConversationListViewController ()
@property (nonatomic ,strong)YLCMessageView *messageView;
@end

@implementation YLCRYConversationListViewController
{
    NSArray *commentList;
    NSArray *replyList;
    NSArray *systemList;
    BOOL hasLoad;
}
- (YLCMessageView *)messageView{
    if (!_messageView) {
        _messageView = [[YLCMessageView alloc] initWithFrame:CGRectZero];
    }
    return _messageView;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if (([YLCFactory isStringOk:userModel.token]&&[YLCFactory isStringOk:userModel.user_id])||!hasLoad) {
        [self initDataSource];
    }
    [self getUnreadCount];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
}
- (void)observeMessage{
    [[RYMessageManager shareManager].messageSignal subscribeNext:^(id x) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getUnreadCount];
        });
    }];
}
- (void)getUnreadCount{
    
    int count = [[RCIMClient sharedRCIMClient] getUnreadCount:@[@(ConversationType_PRIVATE)]];
    self.messageView.unreadCount = [NSString stringWithFormat:@"%d",    count];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
    NSArray *conversationList = [[RCIMClient sharedRCIMClient]
                                 getConversationList:@[@(ConversationType_PRIVATE),
                                                       @(ConversationType_DISCUSSION),
                                                       @(ConversationType_GROUP),
                                                       @(ConversationType_SYSTEM),
                                                       @(ConversationType_APPSERVICE),
                                                       @(ConversationType_PUBLICSERVICE)]];
    if (conversationList.count>0) {
        RCConversation *conver = conversationList[0];
        
        if ([conver.objectName isEqualToString:@"RC:ImgMsg"]) {
            self.messageView.lastMessage = @"[图片]";
        }else if ([conver.objectName isEqualToString:@"RC:TxtMsg"]){
            self.messageView.lastMessage = [conver.lastestMessage conversationDigest];
        }else{
            self.messageView.lastMessage = @"[音视频]";
        }
    }
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE),
                                            @(ConversationType_DISCUSSION),
                                            @(ConversationType_CHATROOM),
                                            @(ConversationType_GROUP),
                                            @(ConversationType_APPSERVICE),
                                            @(ConversationType_SYSTEM)]];
        
        [self setCollectionConversationType:@[@(ConversationType_DISCUSSION),
                                              @(ConversationType_GROUP)]];

    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.conversationListTableView.tableFooterView = [[UIView alloc] init];
    self.messageView.frame = CGRectMake(0, 0, self.view.frameWidth, 300);
    [self.view addSubview:self.messageView];
    self.conversationListTableView.frame  = CGRectMake(0, 300, self.view.frameWidth, self.view.frameHeight-300);
    [self bindVM];
}
- (void)bindVM{
    [self.messageView.selectSignal subscribeNext:^(id x) {
        NSIndexPath *indexPath = x;
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
        
        if (JK_IS_STR_NIL(userModel.token)||JK_IS_STR_NIL(userModel.user_id)) {
            [YLCLoginManager showLoginViewWithTarget:self];
            return ;
        }
//        if (indexPath.row==0) {
//            [self gotoChat];
//        }
        if (indexPath.row == 0) {
            [self gotoReplyMeWithIndex:indexPath.row];
        }
        if (indexPath.row==1) {
            [self gotoReplyMeWithIndex:2];
        }
        if (indexPath.row==2) {
            [self gotoSystemMessageVc];
        }
    }];
}
- (void)gotoReplyMeWithIndex:(NSInteger)index{
    YLCReplyMessageViewController *con = [[YLCReplyMessageViewController alloc] init];
    
    con.type = index-1;
    
    if (index==1) {
        con.messageList = commentList;
    }else{
        con.messageList = replyList;
    }
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)gotoSystemMessageVc{
    YLCSystemMessageViewController *con = [[YLCSystemMessageViewController alloc] init];
    
    con.systemList = systemList;
    
    [self.navigationController pushViewController:con animated:YES];
}

- (void)initDataSource{
    [self observeMessage];
    hasLoad = YES;
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/comment" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.commentList = @[];
            return ;
        }
        commentList = responseObject;
        self.messageView.commentList = commentList;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/commentReply" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.replyList = @[];
            return ;
        }
        replyList = responseObject;
        self.messageView.replyList = replyList;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/system" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.systemList = @[];
            return ;
        }
        systemList = responseObject[@"list"];
        self.messageView.systemList = systemList;
    }];

}
- (void)onSelectedTableRow:(RCConversationModelType)conversationModelType conversationModel:(RCConversationModel *)model atIndexPath:(NSIndexPath *)indexPath{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"myId":userModel.user_id,@"token":userModel.token,@"userId":model.targetId} method:@"POST" urlPath:@"app/user/baseDetail" delegate:self response:^(id responseObject, NSError *error) {
        YLCChatBaseViewController *conversationVC = [[YLCChatBaseViewController alloc] init];
        
        conversationVC.conversationType = model.conversationType;
        
        conversationVC.targetId = model.targetId;
        
        conversationVC.displayUserNameInCell = YES;
        //    conversationVC.conversation = model;
        if (!JK_IS_DICT_NIL(responseObject)) {
            conversationVC.title = responseObject[@"nick_name"];
        }
        
        conversationVC.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:conversationVC animated:YES];

        
    }];

}
- (NSString *)title{
    return @"消息列表";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
