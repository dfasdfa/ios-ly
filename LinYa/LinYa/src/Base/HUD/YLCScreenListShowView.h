//
//  YLCScreenListShowView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCScreenListShowView : UIView
@property (nonatomic ,strong)NSArray *list;
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,strong)RACSubject *dissSignal;
@property (nonatomic ,assign)BOOL hasShow;
@property (nonatomic ,assign)NSInteger showIndex;
- (void)show;
- (void)dissMiss;
@end
