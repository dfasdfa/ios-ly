//
//  YLCSignInHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCSignInHUDView : UIView
- (instancetype)initWithFrame:(CGRect)frame
                     integral:(NSString *)integral;
- (void)show;
@end
