//
//  YLCBorderLabelCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBorderLabelCell.h"

@implementation YLCBorderLabelCell
{
    UILabel *tipLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    self.backgroundColor = [UIColor whiteColor];
    
    tipLabel = [YLCFactory createLabelWithFont:14];
    
    tipLabel.layer.borderWidth = 0.5;
    
    tipLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView addSubview:tipLabel];
}
- (void)setType:(NSInteger)type{
    tipLabel.layer.borderWidth = type==1?0:0.5;
    if (type==1) {
        self.backgroundColor = [UIColor clearColor];
    }
}
- (void)setTipString:(NSString *)tipString{
    tipLabel.text = tipString;
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        tipLabel.layer.borderColor = YLC_THEME_COLOR.CGColor;
        tipLabel.textColor = YLC_THEME_COLOR;
    }else{
        tipLabel.layer.borderColor = RGB(128,133,135).CGColor;
        tipLabel.textColor = RGB(128,133,135);
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
}
@end
