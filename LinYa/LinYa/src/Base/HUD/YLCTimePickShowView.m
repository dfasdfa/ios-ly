//
//  YLCTimePickShowView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTimePickShowView.h"
#import "GQYearMonthDayDatePicker.h"
@interface YLCTimePickShowView()<GQYearMonthDayDatePickerDelegate>

@end

@implementation YLCTimePickShowView
{
    UIButton *cancelBtn;
    UIButton *confirmBtn;
    UIImageView *verLineView;
    GQYearMonthDayDatePicker *pickView;
    UIView *view;
    NSString *dateString;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    self.backgroundColor = [UIColor whiteColor];
    
    cancelBtn = [self commonBtnWithTitle:@"取消" selector:@selector(cancelPick)];
    
    [self addSubview:cancelBtn];
    
    confirmBtn = [self commonBtnWithTitle:@"确定" selector:@selector(confirmPick)];
    
    [self addSubview:confirmBtn];
    
    verLineView = [[UIImageView alloc] init];
    
    verLineView.backgroundColor = YLC_GRAY_COLOR;
    
    [self addSubview:verLineView];
    
    pickView = [[GQYearMonthDayDatePicker alloc] init];
    
    pickView.columnNumber = 3;
    
    pickView.rowHeight = 40;
    
    pickView.yearTextColor = YLC_THEME_COLOR;
    
    pickView.monthTextColor = YLC_THEME_COLOR;
    
    pickView.dayTextColor = YLC_THEME_COLOR;
    
    [pickView setupMinYear:1900 maxYear:2030];
    
    pickView.gqdelegate = self;
    
    [pickView selectToday];
    
    [self addSubview:pickView];

}

-(void)yearMonthDayDatePicker:(GQYearMonthDayDatePicker *)yearMonthDatePicker didSectedDate:(NSDate *)date{
    
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                        selector:(SEL)selector{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}
- (void)confirmPick{
    [self.confirmSignal sendNext:[pickView.currentDate getTimeStringWithFormatter:@"yyyy-MM-dd"]];
    [self dissMiss];
}
- (void)cancelPick{
    [self dissMiss];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(40);
        make.height.equalTo(30);
    }];
    
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.centerY.equalTo(cancelBtn.mas_centerY);
        make.width.equalTo(40);
        make.height.equalTo(30);
    }];
    
    [verLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(cancelBtn.mas_bottom).offset(10);
        make.height.equalTo(0.5);
    }];
    
    [pickView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(verLineView.mas_bottom).offset(5);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
}
- (void)tapDissMissSelf{
    [self dissMiss];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDissMissSelf)];
    
    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.y -= 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y += 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
