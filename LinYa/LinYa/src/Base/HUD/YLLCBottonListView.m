//
//  YLLCBottonListView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLLCBottonListView.h"
#import "YLCScreenTrainCell.h"
#import "YLDoubleBtnView.h"
#define TRAIN_CELL  @"YLCScreenTrainCell"
#define DOUBLE_VIEW @"YLDoubleBtnView"
@interface YLLCBottonListView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCScreenTrainCellDelegate,YLDoubleBtnViewDelegate>
@end

@implementation YLLCBottonListView
{
    UICollectionView *tableView;
    UIView *view;
    NSInteger typeIndex;
    NSInteger feeIndex;
    NSInteger timeIndex;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        typeIndex = 0;
        feeIndex = 0;
        timeIndex = 0;
        self.backgroundColor = YLC_THEME_COLOR;
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    tableView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    tableView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [tableView registerClass:[YLCScreenTrainCell class] forCellWithReuseIdentifier:TRAIN_CELL];
    
    [tableView registerClass:[YLDoubleBtnView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:DOUBLE_VIEW];
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
    
    [self addSubview:tableView];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(64, 0, 0, 0));
    }];
}
- (void)setSelectList:(NSArray *)selectList{
    _selectList = selectList;
    
    [tableView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _selectList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCScreenTrainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TRAIN_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_titleList.count) {
        cell.title = _titleList[indexPath.row];
    }
    if (indexPath.row<_selectList.count) {
        cell.dataList = _selectList[indexPath.row];
    }
    cell.index = indexPath.row;
    
    cell.delegate = self;
    
    if (indexPath.row==0) {
        cell.selectIndex = typeIndex;
    }
    if (indexPath.row==1) {
        cell.selectIndex = feeIndex;
    }
    if (indexPath.row==2) {
        cell.selectIndex = timeIndex;
    }
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionFooter) {
        YLDoubleBtnView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:DOUBLE_VIEW forIndexPath:indexPath];
        
        footView.delegate = self;
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    NSArray *dataArr = _selectList[indexPath.row];
    return CGSizeMake(self.frameWidth, 120);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth, 50);
}
- (void)didSelectWithSelectIndex:(NSInteger)selectIndex indexRow:(NSInteger)indexRow{
    if (indexRow==0) {
        typeIndex = selectIndex;
    }
    if (indexRow==1) {
        feeIndex = selectIndex;
    }
    if (indexRow==2) {
        timeIndex = selectIndex;
    }
}
- (void)didTouchWithType:(BOOL)isLeft{
    if (isLeft) {
        typeIndex = 0;
        feeIndex = 0;
        timeIndex = 0;
        [tableView reloadData];
    }else{
        [self.confirmSignal sendNext:@[[NSString stringWithFormat:@"%ld",typeIndex],[NSString stringWithFormat:@"%ld",feeIndex],[NSString stringWithFormat:@"%ld",timeIndex]]];
        [self dissMiss];
    }
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissMiss)];
    
    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.x = frame.origin.x-250;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         //                         [nowPayField becomeFirstResponder];
                         
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.x = kScreenWidth;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
