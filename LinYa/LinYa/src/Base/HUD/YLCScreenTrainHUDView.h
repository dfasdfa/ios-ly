//
//  YLCScreenTrainHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCScreenTrainHUDView : UIView
@property (nonatomic ,strong)NSArray *titleList;
@property (nonatomic ,strong)NSArray *selectList;
@property (nonatomic ,strong)RACSubject *confirmSignal;
@property (nonatomic ,assign)NSInteger type;//0 横列布局 1是数列布局
- (void)show;
@end
