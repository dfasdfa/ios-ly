//
//  YLCBuySuccessHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBuySuccessHUDView.h"

@implementation YLCBuySuccessHUDView
{
    UIView *view;
    UILabel *successTipLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hud_background"]];
        
        backgroundView.userInteractionEnabled = YES;
        
        [self addSubview:backgroundView];
        
        [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
        }];
        
        UIImageView *headIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"buy_success"]];
        
        [backgroundView addSubview:headIconView];
        
        [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backgroundView.mas_top).offset(-10);
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.width.equalTo(100);
            make.height.equalTo(98);
        }];
        
        successTipLabel = [YLCFactory createLabelWithFont:13 color:RGB(51, 51, 51)];
        
        successTipLabel.text = @"恭喜您！购买成功！";
        
        [backgroundView addSubview:successTipLabel];
        
        [successTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headIconView.mas_bottom).offset(25);
            make.centerX.equalTo(backgroundView.mas_centerX);
        }];
        
        UIButton *cancelBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:20 title:@"知道了" backgroundColor:[UIColor whiteColor] textColor:YLC_THEME_COLOR borderColor:YLC_THEME_COLOR borderWidth:0.5];
        
        [backgroundView addSubview:cancelBtn];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.top.equalTo(successTipLabel.mas_bottom).offset(30);
            make.width.equalTo(100);
            make.height.equalTo(40);
        }];
        
        [cancelBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)setTipString:(NSString *)tipString{
    successTipLabel.text = tipString;
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmiss)];
    
    [view addGestureRecognizer:ges];
    //
    self.alpha = 1;
    [keyWindow addSubview:self];
    [keyWindow bringSubviewToFront:self];
    
    __block CGPoint center = view.center;
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.alpha = 0.3f;
                         
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         
                         
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:0
                                          animations:^{
                                              center.y -= 20;
                                              self.center = center;
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (void)dissmiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:0
                     animations:^{
                         CGPoint center = self.center;
                         center.y += 20;
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.4
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              CGRect frame = view.frame;
                                              frame.origin.y = -frame.size.height;
                                              self.frame = CGRectMake(kScreenWidth/2-130, -160, 260, 237);
                                          }
                                          completion:^(BOOL finished) {
                                              [self removeFromSuperview];
                                              [view removeFromSuperview];
                                          }];
                     }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
