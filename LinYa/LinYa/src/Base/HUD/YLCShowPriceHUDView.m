//
//  YLCShowPriceHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCShowPriceHUDView.h"

@implementation YLCShowPriceHUDView
{
    
    UILabel *titleLabel;
    UITextField *nowPayField;
    UITextField *oldPayField;
    UITextField *freightField;
    UILabel *freightTitleLabel;
    UIButton *freightBtn;
    UILabel *freightTipLabel;
    UIView *view;
    BOOL isAnimation;
    UIButton *confirmBtn;
    CGRect selfFrame;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        selfFrame = frame;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    titleLabel.text = @"想卖多少钱?";
    
    [self addSubview:titleLabel];
    
    nowPayField = [YLCFactory createFieldWithPlaceholder:@"￥"];
    
    nowPayField.keyboardType = UIKeyboardTypeNumberPad;
    
    [self addSubview:nowPayField];
    
    oldPayField = [YLCFactory createTitleFieldWithPlaceholder:@"￥" title:@"原价"];
    
    oldPayField.keyboardType = UIKeyboardTypeNumberPad;
    
    [self addSubview:oldPayField];
    
    freightField = [YLCFactory createTitleFieldWithPlaceholder:@"待议" title:@"运费"];
    
    freightField.keyboardType = UIKeyboardTypeNumberPad;
    
    [self addSubview:freightField];
    
    freightTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    freightTitleLabel.text = @"包邮";
    
    [self addSubview:freightTitleLabel];
    
    freightBtn = [YLCFactory createBtnWithImage:@""];
    
    [self addSubview:freightBtn];
    
//    freightTipLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
//
//    freightTipLabel.text = @"运费提示";
//
//    [self addSubview:freightTipLabel];
    
    confirmBtn = [YLCFactory createLabelBtnWithTitle:@"确定" titleColor:YLC_THEME_COLOR];
    
    [confirmBtn addTarget:self action:@selector(confirmPrice) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:confirmBtn];
    
    titleLabel.frame = CGRectMake(10, 10, 100, 20);
    
    nowPayField.frame = CGRectMake(10, titleLabel.frameMaxY+10, self.frameWidth-20, 40);
    
    oldPayField.frame = CGRectMake(10, nowPayField.frameMaxY+10, self.frameWidth-20, 30);
    
    freightField.frame = CGRectMake(10, oldPayField.frameMaxY+10, self.frameWidth-100, 30);
    
//    freightTitleLabel.frame = CGRectMake(freightField.frameMaxX+10, freightField.frameY, 30, 30);
    
//    freightBtn.frame = CGRectMake(freightTitleLabel.frameMaxX+10, freightTitleLabel.frameY, 30, 30);
    
//    freightTipLabel.frame = CGRectMake(freightField.frameMaxX+10, freightField.frameY, 70, 30);
    
    confirmBtn.frame = CGRectMake(self.frameWidth-70, 10, 50, 30);
}
- (void)confirmPrice{
    [self.confirmSignal sendNext:@{@"newPrice":nowPayField.text,@"oldPrice":oldPayField.text,@"freight":freightField.text}];
    
    [self dissMiss];
}
- (void)keyboardWillHide:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    if (!isAnimation) {
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            isAnimation = YES;

            self.frame = CGRectMake(0, kScreenHeight-160, kScreenWidth, 160);
        } completion:^(BOOL finished) {
            if (isAnimation) {
                isAnimation = NO;
            }
            
        }];
    }

}
- (void)keyboardDidHide:(NSNotification *)notification{
    
}
- (void)keyboardDidShow:(NSNotification *)notification{
    
}
- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardF = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (!isAnimation) {
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            isAnimation = YES;
            CGRect frame = selfFrame;
            frame.origin.y =frame.origin.y- keyboardF.size.height-160;
            self.frame = frame;
        } completion:^(BOOL finished) {
            if (isAnimation) {
                isAnimation = NO;
            }
            
        }];
    }
}

- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissMiss)];
    
    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.y -= 160;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
//                         [nowPayField becomeFirstResponder];
    
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         isAnimation = YES;
                         CGRect frame = self.frame;
                         frame.origin.y = kScreenHeight;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         isAnimation = NO;
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
