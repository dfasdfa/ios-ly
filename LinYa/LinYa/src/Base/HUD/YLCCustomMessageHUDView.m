//
//  YLCCustomMessageHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomMessageHUDView.h"
#import "YLCBorderLabelCell.h"
#define BORDER_CELL  @"YLCBorderLabelCell"
@interface YLCCustomMessageHUDView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCCustomMessageHUDView
{
    UIView *view;
    NSArray *dataList;
    UICollectionView *labelTable;
    NSInteger selectIndex;
    UIImageView *line1;
    UIImageView *line2;
    UILabel *titleLabel;
    NSMutableArray *selectList;
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type==1) {
        labelTable.backgroundColor = [UIColor clearColor];
    }
    [labelTable reloadData];
}
- (id)initWithFrame:(CGRect)frame
          titleList:(NSArray *)titleList
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        
        titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(139,136,136)];
        
        titleLabel.text = @"最高学历";
        
        [self addSubview:titleLabel];
        WS(weakSelf)
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(33);
            make.top.equalTo(weakSelf.mas_top).offset(18);
        }];
        
        line1 = [[UIImageView alloc] init];
        
        line1.backgroundColor = YLC_THEME_COLOR;
        
        [self addSubview:line1];
        
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(titleLabel.mas_left).offset(-5);
            make.centerY.equalTo(titleLabel.mas_centerY);
            make.width.equalTo(23);
            make.height.equalTo(0.5);
        }];
        
        line2 = [[UIImageView alloc] init];
        
        line2.backgroundColor = YLC_THEME_COLOR;
        
        [self addSubview:line2];
        
        [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(titleLabel.mas_right).offset(5);
            make.right.equalTo(weakSelf.mas_right).offset(-5);
            make.centerY.equalTo(titleLabel.mas_centerY);
            make.height.equalTo(0.5);
        }];
        selectList = [NSMutableArray arrayWithCapacity:0];
        
        selectIndex = 0;
        
        dataList = titleList;
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 20;
        
        flow.minimumInteritemSpacing = 10;
        
        labelTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        labelTable.backgroundColor = [UIColor clearColor];
        
        [labelTable registerClass:[YLCBorderLabelCell class] forCellWithReuseIdentifier:BORDER_CELL];
        
        labelTable.delegate = self;
        
        labelTable.dataSource = self;
        
        [self addSubview:labelTable];
        
        [labelTable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(15);
            make.top.equalTo(titleLabel.mas_bottom).offset(27);
            make.width.equalTo(frame.size.width-30);
            make.height.equalTo(weakSelf.frameHeight-55-50-30);
        }];
        
        UIButton *cancelBtn = [self commonBtnWithTitle:@"取消" textColor:RGB(133, 133, 133) backgroundColor:[UIColor whiteColor]];
        
        [cancelBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:cancelBtn];
        
        UIButton *confirmBtn = [self commonBtnWithTitle:@"确定" textColor:[UIColor whiteColor] backgroundColor:YLC_THEME_COLOR];
        
        [confirmBtn addTarget:self action:@selector(confirmEdu) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:confirmBtn];
        
        CGFloat bottomHeight = kDevice_Is_iPhoneX?34:0;
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.mas_bottom).offset(-bottomHeight);
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(weakSelf.mas_centerX);
            make.height.equalTo(50);
        }];
        
        [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.mas_bottom).offset(-bottomHeight);
            make.right.equalTo(weakSelf.mas_right);
            make.left.equalTo(weakSelf.mas_centerX);
            make.height.equalTo(50);
        }];
    }
    return self;
}
- (void)confirmEdu{
    if (_type==1) {
        if (_canSelectMore) {
            [_confirmSignal sendNext:selectList];
        }else{
            [_confirmSignal sendNext:[NSString stringWithFormat:@"%ld",selectIndex]];
        }
    }else{
        if (_canSelectMore) {
            [_confirmSignal sendNext:selectList];
        }else{
            [_confirmSignal sendNext:dataList[selectIndex]];
        }
    }

    
    [self dissmiss];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCBorderLabelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BORDER_CELL forIndexPath:indexPath];
    
    if (_canSelectMore) {
        BOOL hasSelect = NO;
        
        for (NSString *selectIndex in selectList) {
            if ([selectIndex integerValue]==indexPath.row) {
                hasSelect = YES;
            }
        }
        cell.isSelect = hasSelect;
    }else{
        if (indexPath.row==selectIndex) {
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
    }

    if (indexPath.row<dataList.count) {
        cell.tipString = dataList[indexPath.row];
    }
    cell.type = _type;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_type==1) {
        return CGSizeMake(self.frameWidth, 50);
    }
    return CGSizeMake(70, 30);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (_type==1) {
        return 0;
    }
    return 20;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (_type==1) {
        return 0;
    }
    return 10;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_canSelectMore) {
        [self fetchMoreSelectWithIndexPath:indexPath];
    }else{
        [self fetchSignalSelectWithIndexPath:indexPath];
    }

}
- (void)fetchSignalSelectWithIndexPath:(NSIndexPath *)indexPath{
    selectIndex = indexPath.row;
    
    [labelTable reloadData];
}
- (void)fetchMoreSelectWithIndexPath:(NSIndexPath *)indexPath{
    NSString *selectString = [NSString stringWithFormat:@"%ld",indexPath.row];
    
    if ([selectList containsObject:selectString]) {
        [selectList removeObject:selectString];
    }else{
        [selectList addObject:selectString];
    }
    
    [labelTable reloadData];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                       textColor:(UIColor *)textColor
                 backgroundColor:(UIColor *)backgroundColor{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:textColor forState:UIControlStateNormal];
    
    button.backgroundColor = backgroundColor;
    
    return button;
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmiss)];

    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.y -= 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}
- (void)dissmiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y += 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
