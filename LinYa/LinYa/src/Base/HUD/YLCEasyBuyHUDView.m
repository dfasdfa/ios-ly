//
//  YLCEasyBuyHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyBuyHUDView.h"

@implementation YLCEasyBuyHUDView
{
    UIView *view;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.buttonSignal = [RACSubject subject];
        
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hud_background"]];
        
        backgroundView.userInteractionEnabled = YES;
        
        [self addSubview:backgroundView];
        
        [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
        }];
        
        UIImageView *headIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"buy_head"]];
        
        [backgroundView addSubview:headIconView];
        
        [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backgroundView.mas_top).offset(-10);
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.width.equalTo(100);
            make.height.equalTo(98);
        }];
        
        UILabel *titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
        
        titleLabel.text = @"^o^试用期间只能使用根管治疗";
        
        [backgroundView addSubview:titleLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.top.equalTo(headIconView.mas_bottom).offset(20);
        }];
        
        UIView *buyBackView = [[UIView alloc] init];
        
        buyBackView.layer.shadowOffset = CGSizeMake(0, 3);
        
        buyBackView.layer.shadowOpacity = 0.2;
        
        buyBackView.layer.shadowColor = [UIColor blackColor].CGColor;
        
        [backgroundView addSubview:buyBackView];
        
        UIButton *buyBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:20 title:@"立即购买" backgroundColor:[UIColor whiteColor] textColor:YLC_THEME_COLOR borderColor:YLC_THEME_COLOR borderWidth:1];

        buyBtn.imageEdgeInsets = UIEdgeInsetsMake(14, 30, 14, 150-32-10);
        
        buyBtn.titleEdgeInsets = UIEdgeInsetsMake(10, 30, 10, 20);
        
        [buyBtn setImage:[UIImage imageNamed:@"buy_icon"] forState:UIControlStateNormal];
        
        buyBtn.tag = 0+300;
        
        [buyBtn addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
        
        [buyBackView addSubview:buyBtn];
        
        [buyBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.top.equalTo(titleLabel.mas_bottom).offset(60);
            make.width.equalTo(150);
            make.height.equalTo(40);
        }];
        
        [buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];
        
        UIView *trialView = [[UIView alloc] init];

        trialView.userInteractionEnabled = YES;

        trialView.layer.shadowOffset = CGSizeMake(0, 3);

        trialView.layer.shadowOpacity = 0.2;

        trialView.layer.shadowColor = [UIColor blackColor].CGColor;

        [backgroundView addSubview:trialView];

        UIButton *trialBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:20 title:@"免费试用" backgroundColor:[UIColor whiteColor] textColor:RGB(183, 183, 183) borderColor:[UIColor clearColor] borderWidth:0];

        trialBtn.tag = 300+1;

        [trialView addSubview:trialBtn];

        [trialView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.top.equalTo(buyBtn.mas_bottom).offset(10);
            make.width.equalTo(150);
            make.height.equalTo(40);
        }];
        [trialBtn addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
        [trialBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];

        UILabel *trialTipLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];

        trialTipLabel.backgroundColor = YLC_COMMON_BACKCOLOR;

        trialTipLabel.textAlignment = NSTextAlignmentCenter;

        trialTipLabel.text = @"试用期为一个月";

        trialTipLabel.numberOfLines = 0;

        [backgroundView addSubview:trialTipLabel];

        [trialTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.top.equalTo(trialBtn.mas_bottom).offset(10);
            make.width.equalTo(190);
            make.height.equalTo(50);
        }];
        
        UIButton *cancelBtn = [YLCFactory createBtnWithImage:@"hud_cancel"];
        
        [self addSubview:cancelBtn];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_right).offset(-5);
            make.centerY.equalTo(backgroundView.mas_top).offset(35);
            make.width.equalTo(26);
            make.height.equalTo(26);
        }];
        
        [cancelBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)buttonTouch:(UIButton *)sender{
    [self dissmiss];
    
    NSString *tag = [NSString stringWithFormat:@"%ld",sender.tag-300];
    
    [self.buttonSignal sendNext:tag];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmiss)];
    
    [view addGestureRecognizer:ges];
    //
    self.alpha = 1;
    [keyWindow addSubview:self];
    [keyWindow bringSubviewToFront:self];
    
    __block CGPoint center = view.center;
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.alpha = 0.3f;
                         
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         
                         
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:0
                                          animations:^{
                                              center.y -= 20;
                                              self.center = center;
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (void)dissmiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:0
                     animations:^{
                         CGPoint center = self.center;
                         center.y += 20;
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.4
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              CGRect frame = view.frame;
                                              frame.origin.y = -frame.size.height;
                                              self.frame = CGRectMake(kScreenWidth/2-130, -160, 260, 360);
                                          }
                                          completion:^(BOOL finished) {
                                              [self removeFromSuperview];
                                              [view removeFromSuperview];
                                          }];
                     }];
}

@end
