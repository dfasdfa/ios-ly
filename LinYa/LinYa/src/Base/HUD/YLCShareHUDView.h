//
//  YLCShareHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/5/3.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCShareHUDView : UIView
@property (nonatomic ,copy)NSString *shareContent;
@property (nonatomic ,copy)NSString *shareLink;
- (void)show;
@end
