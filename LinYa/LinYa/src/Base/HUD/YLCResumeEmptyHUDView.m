//
//  YLCResumeEmptyHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeEmptyHUDView.h"

@implementation YLCResumeEmptyHUDView
{
    UIView *view;
    UILabel *successTipLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hud_background"]];
        
        backgroundView.userInteractionEnabled = YES;
        
        [self addSubview:backgroundView];
        
        [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
        }];
        
        UIImageView *headIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_hud_head"]];
        
        [backgroundView addSubview:headIconView];
        
        [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(backgroundView.mas_top).offset(20);
            make.centerX.equalTo(backgroundView.mas_centerX);
            make.width.equalTo(65);
            make.height.equalTo(80);
        }];
        
        successTipLabel = [YLCFactory createLabelWithFont:13 color:RGB(51, 51, 51)];
        
        successTipLabel.text = @"您还未创建你的简历";
        
        [backgroundView addSubview:successTipLabel];
        
        [successTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headIconView.mas_bottom).offset(25);
            make.centerX.equalTo(backgroundView.mas_centerX);
        }];
        
        UIButton *cancelBtn = [self commonBtnWithTitle:@"下次再说" color:[UIColor whiteColor] titleColor:RGB(88, 88, 88)];
        
        [backgroundView addSubview:cancelBtn];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(backgroundView.mas_centerX);
            make.bottom.equalTo(backgroundView.mas_bottom);
            make.left.equalTo(backgroundView.mas_left);
            make.height.equalTo(40);
        }];
        
        [cancelBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *createBtn = [self commonBtnWithTitle:@"立即创建" color:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
        
        [backgroundView addSubview:createBtn];
        
        [createBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(cancelBtn.mas_right);
            make.right.equalTo(backgroundView.mas_right);
            make.bottom.equalTo(backgroundView.mas_bottom);
            make.height.equalTo(40);
        }];
        
        [createBtn addTarget:self action:@selector(createResume) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)createResume{
    [_subject sendNext:nil];
    [self dissmiss];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    
    return button;
}

- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmiss)];
    
    [view addGestureRecognizer:ges];
    //
    self.alpha = 1;
    [keyWindow addSubview:self];
    [keyWindow bringSubviewToFront:self];
    
    __block CGPoint center = view.center;
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.alpha = 0.3f;
                         
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         
                         
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:0
                                          animations:^{
                                              center.y -= 20;
                                              self.center = center;
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (void)dissmiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:0
                     animations:^{
                         CGPoint center = self.center;
                         center.y += 20;
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.4
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              CGRect frame = view.frame;
                                              frame.origin.y = -frame.size.height;
                                              self.frame = CGRectMake(kScreenWidth/2-130, -160, 260, 237);
                                          }
                                          completion:^(BOOL finished) {
                                              [self removeFromSuperview];
                                              [view removeFromSuperview];
                                          }];
                     }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
