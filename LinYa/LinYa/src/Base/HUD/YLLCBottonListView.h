//
//  YLLCBottonListView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLLCBottonListView : UIView
@property (nonatomic ,strong)NSArray *titleList;
@property (nonatomic ,strong)NSArray *selectList;
@property (nonatomic ,strong)RACSubject *confirmSignal;
- (void)show;
@end
