//
//  YLCCustomMessageHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCCustomMessageHUDView : UIView
@property (nonatomic ,strong)RACSubject *confirmSignal;
@property (nonatomic ,assign)BOOL canSelectMore;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,assign)NSInteger type;//0为横列布局 1为数列布局
- (id)initWithFrame:(CGRect)frame
          titleList:(NSArray *)titleList;
- (void)show;
- (void)dissmiss;
@end
