//
//  YLCShowWelfareListHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCShowWelfareListHUDView : UIView
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,strong)NSMutableArray *selectList;
@property (nonatomic ,strong)RACSubject *signal;
@end
