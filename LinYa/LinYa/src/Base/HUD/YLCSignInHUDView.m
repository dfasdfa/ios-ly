//
//  YLCSignInHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSignInHUDView.h"

@implementation YLCSignInHUDView
{
    UIView *view;
}
- (instancetype)initWithFrame:(CGRect)frame
                     integral:(NSString *)integral
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        
        backgroundView.backgroundColor = [UIColor whiteColor];
        
        backgroundView.layer.cornerRadius = 10;
        
        backgroundView.layer.masksToBounds = YES;
        
        [self addSubview:backgroundView];
        
        [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
        }];
        
        UIImageView *leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        
        [backgroundView addSubview:leftView];
        
        UILabel *integralLabel = [[UILabel alloc] init];
        
        NSString *integralString = [NSString stringWithFormat:@"%@",integral];
        
        [backgroundView addSubview:integralLabel];
        
        NSAttributedString *attr = [YLCFactory getAttributedStringWithString:integralString FirstCount:integral.length+1 secondCount:integralString.length-integral.length-1 firstFont:17 secondFont:14 firstColor:[UIColor redColor] secondColor:[UIColor blackColor]];
        
        integralLabel.attributedText = attr;
        
        [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(backgroundView.mas_left);
            make.top.equalTo(backgroundView.mas_top);
            make.bottom.equalTo(backgroundView.mas_bottom);
            make.width.equalTo(120);
        }];
        
        [integralLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftView.mas_right).offset(20);
            make.centerY.equalTo(leftView.mas_centerY);
        }];
        
        UIButton *cancelBtn = [YLCFactory createBtnWithImage:@"hud_cancel"];
        
        [self addSubview:cancelBtn];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(backgroundView.mas_right).offset(-5);
            make.centerY.equalTo(backgroundView.mas_top).offset(5);
            make.width.equalTo(26);
            make.height.equalTo(26);
        }];
        
        [cancelBtn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissmiss)];
    
    [view addGestureRecognizer:ges];
    //
    self.alpha = 1;
    [keyWindow addSubview:self];
    [keyWindow bringSubviewToFront:self];
    
    __block CGPoint center = view.center;
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         view.alpha = 0.3f;
                         
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         
                         
                         [UIView animateWithDuration:0.1
                                               delay:0.0
                                             options:0
                                          animations:^{
                                              center.y -= 20;
                                              self.center = center;
                                          }
                                          completion:^(BOOL finished) {
                                              
                                          }];
                     }];
}
- (void)dissmiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:0
                     animations:^{
                         CGPoint center = self.center;
                         center.y += 20;
                         self.center = center;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.4
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              CGRect frame = view.frame;
                                              frame.origin.y = -frame.size.height;
                                              self.frame = CGRectMake(view.frame.size.width/2-130, frame.origin.y, 260, 120);
                                          }
                                          completion:^(BOOL finished) {
                                              [self removeFromSuperview];
                                              [view removeFromSuperview];
                                          }];
                     }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
