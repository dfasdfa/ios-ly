//
//  YLCTimePickShowView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTimePickShowView : UIView
@property (nonatomic ,strong)RACSubject *confirmSignal;
- (void)show;
- (void)dissMiss;
@end
