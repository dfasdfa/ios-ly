//
//  YLCAreaPickerView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAreaPickerView.h"
#import "YLCAreaModel.h"
@interface YLCAreaPickerView()<UIPickerViewDelegate,UIPickerViewDataSource>
@end
@implementation YLCAreaPickerView
{
    UIButton *cancelBtn;
    UIButton *confirmBtn;
    UIImageView *verLineView;
    UIView *view;
    UIPickerView *areaPicker;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}

- (void)createCustomView{
    cancelBtn = [self commonBtnWithTitle:@"取消" selector:@selector(cancelPick)];
    
    [self addSubview:cancelBtn];
    
    confirmBtn = [self commonBtnWithTitle:@"确定" selector:@selector(confirmPick)];
    
    [self addSubview:confirmBtn];
    
    verLineView = [[UIImageView alloc] init];
    
    verLineView.backgroundColor = YLC_GRAY_COLOR;
    
    [self addSubview:verLineView];
    
    areaPicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    
    areaPicker.backgroundColor = [UIColor whiteColor];
    
    areaPicker.delegate = self;
    
    areaPicker.dataSource = self;
    
    [self addSubview:areaPicker];
}
- (NSArray *)provinceList{
    return [[YLCAccountManager shareManager] getProvinceList];
}
- (NSArray *)cityList{
    YLCAreaModel *areaModel = [self provinceList][[self provinceIndex]];
    return [[YLCAccountManager shareManager] getCityListWithSuperId:areaModel.id];
}
- (NSArray *)areaList{
    NSInteger provinceIndex =[areaPicker selectedRowInComponent:0];
    NSArray *cityList = [self cityList];
    if (JK_IS_ARRAY_NIL(cityList)) {
        return @[];
    }
    NSInteger cityIndex = [areaPicker selectedRowInComponent:1];
    YLCAreaModel *provinceModel = [self provinceList][provinceIndex];
    YLCAreaModel *cityModel = cityList[cityIndex];
    return [[YLCAccountManager shareManager] getAreaListWithCityId:cityModel.id provinceId:provinceModel.id];
}
- (NSInteger)provinceIndex{
    return [areaPicker selectedRowInComponent:0];
}
- (NSInteger)cityIndex{
    return [areaPicker selectedRowInComponent:1];
}
- (NSInteger)areaIndex{
    return [areaPicker selectedRowInComponent:2];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component==0) {
        return [self provinceList].count;
    }
    if (component==1) {
        return [self cityList].count;
    }
    if (component==2) {
        
        return [self areaList].count;
    }
    return 0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return self.frameWidth/3;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/3, [pickerView rowSizeForComponent:component].height)];
    
    rowLabel.font = [UIFont systemFontOfSize:17];
    
    rowLabel.textColor = YLC_THEME_COLOR;
    
    rowLabel.textAlignment = NSTextAlignmentCenter;
    
    if (component==0) {
        YLCAreaModel *areaModel = [self provinceList][row];
        rowLabel.text = areaModel.areaname;
    }
    if (component==1) {
        YLCAreaModel *areaModel = [self cityList][row];
        rowLabel.text = areaModel.areaname;
    }
    if (component==2) {
        YLCAreaModel *areaModel = [self areaList][row];
        rowLabel.text = areaModel.areaname;
    }
    return rowLabel;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component==0) {
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    if (component==1) {
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(40);
        make.height.equalTo(30);
    }];
    
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.centerY.equalTo(cancelBtn.mas_centerY);
        make.width.equalTo(40);
        make.height.equalTo(30);
    }];
    
    [verLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(cancelBtn.mas_bottom).offset(10);
        make.height.equalTo(0.5);
    }];
    
    [areaPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(verLineView.mas_bottom).offset(5);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];

}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                        selector:(SEL)selector{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}
- (void)confirmPick{
    if (JK_IS_ARRAY_NIL([self areaList])) {
        YLCAreaModel *model = [self cityList][[self cityIndex]];
        
        [self.confirmSignal sendNext:model];
    }else{
        YLCAreaModel *model = [self areaList][[self areaIndex]];
        
        [self.confirmSignal sendNext:model];
    }
    [self dissMiss];
}
- (void)cancelPick{
    [self dissMiss];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissMiss)];
    
    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.y -= 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y += 260;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
