//
//  YLCShareHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/3.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCShareHUDView.h"

@implementation YLCShareHUDView
{
    UIImageView *wxIconView;
    UIImageView *wxCircleIconView;
    UIView *wxView;
    UILabel *wxTitleView;
    UILabel *wxCircleTitleView;
    UIView *circleView;
    UIView *view;
    UIButton *cancelBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    wxView = [[UIView alloc] init];
    
    [self addSubview:wxView];
    
    circleView = [[UIView alloc] init];
    
    [self addSubview:circleView];
    
    wxIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wx_share"]];
    
    [wxView addSubview:wxIconView];
    
    wxTitleView = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
    
    wxTitleView.text = @"微信";
    
    [wxView addSubview:wxTitleView];
    
    wxCircleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wx_circle"]];
    
    [circleView addSubview:wxCircleIconView];
    
    wxCircleTitleView = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
    
    wxCircleTitleView.text = @"朋友圈";
    
    [circleView addSubview:wxCircleTitleView];
    
    cancelBtn = [YLCFactory createCommondBtnWithTitle:@"取消" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [cancelBtn addTarget:self action:@selector(dissMiss) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:cancelBtn];
    
    UITapGestureRecognizer *wxGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wxShare)];
    
    [wxView addGestureRecognizer:wxGes];
    
    UITapGestureRecognizer *circleGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wxCircleShare)];
    
    [circleView addGestureRecognizer:circleGes];
    
    WS(weakSelf)
    
    [wxView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(20);
        make.width.height.equalTo(80);
    }];
    
    [wxIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wxView.mas_centerX);
        make.top.equalTo(wxView.mas_top).offset(5);
        make.width.height.equalTo(50);
    }];
    
    [wxTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(wxIconView.mas_centerX);
        make.top.equalTo(wxIconView.mas_bottom).offset(10);
    }];
    
    [circleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wxView.mas_right).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(20);
        make.width.height.equalTo(80);
    }];
    
    [wxCircleIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(circleView.mas_centerX);
        make.top.equalTo(circleView.mas_top).offset(5);
        make.width.height.equalTo(50);
    }];
    
    [wxCircleTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(circleView.mas_centerX);
        make.top.equalTo(wxCircleIconView.mas_bottom).offset(10);
    }];
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        make.width.equalTo(weakSelf.frameWidth-100);
        make.height.equalTo(40);
        make.centerX.equalTo(weakSelf.mas_centerX);
    }];
}
- (void)wxShare{
    
        [[YLCWxPayManager shareManager] shareWithContent:_shareContent title:@"邻牙" shareLink:_shareLink scene:WXSceneSession];
    
    [self dissMiss];
}
- (void)wxCircleShare{
    [[YLCWxPayManager shareManager] shareWithContent:_shareContent title:@"邻牙" shareLink:_shareLink scene:WXSceneTimeline];
    
    [self dissMiss];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.3;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissMiss)];
    
    [view addGestureRecognizer:ges];
    
    self.alpha = 1;
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.alpha = 0.3f;
                         
                         CGRect frame = self.frame;
                         frame.origin.y -= 160;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y += 160;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [view removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
