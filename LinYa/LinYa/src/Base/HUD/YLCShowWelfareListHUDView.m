//
//  YLCShowWelfareListHUDView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCShowWelfareListHUDView.h"
#import "YLCScreenTypeCell.h"
#define SCREEN_CELL  @"YLCScreenTypeCell"
@interface YLCShowWelfareListHUDView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCShowWelfareListHUDView
{
    UICollectionView *screenTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _selectList = [NSMutableArray arrayWithCapacity:0];
        _signal = [RACSubject subject];
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    
    [screenTable reloadData];
}
- (void)setSelectList:(NSMutableArray *)selectList{
    _selectList = selectList;
    if (JK_IS_ARRAY_NIL(_selectList)) {
        _selectList = [NSMutableArray arrayWithCapacity:0];
    }
    [screenTable reloadData];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 10;
    
    screenTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    screenTable.backgroundColor = [UIColor whiteColor];
    
    [screenTable registerClass:[YLCScreenTypeCell class] forCellWithReuseIdentifier:SCREEN_CELL];
    
    screenTable.delegate = self;
    
    screenTable.dataSource = self;
    
    [self addSubview:screenTable];
    
    [screenTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(20, 10, 20, 10));
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCScreenTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SCREEN_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_dataList.count) {
        NSDictionary *dict = _dataList[indexPath.row];
        cell.title = dict[@"welfare_name"];
    }
    
    BOOL hasSelect = NO;
    for (NSString *selectIndex in _selectList) {
        if ([selectIndex integerValue] == indexPath.row) {
            hasSelect = YES;
        }
    }
    
    cell.isSelect = hasSelect;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(90, 30);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *selectString = [NSString stringWithFormat:@"%ld",indexPath.row];
    if ([_selectList containsObject:selectString]) {
        [_selectList removeObject:selectString];
    }else{
        [_selectList addObject:selectString];
    }
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (NSString *indexString in _selectList) {
        [container addObject:_dataList[[indexString integerValue]]];
    }
    ;
    [_signal sendNext:@{@"select":_selectList.copy,@"message":container.copy}];
    
    [screenTable reloadData];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
