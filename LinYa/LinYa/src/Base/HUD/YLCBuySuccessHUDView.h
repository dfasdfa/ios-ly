//
//  YLCBuySuccessHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCBuySuccessHUDView : UIView
@property (nonatomic ,copy)NSString *tipString;
- (void)show;
@end
