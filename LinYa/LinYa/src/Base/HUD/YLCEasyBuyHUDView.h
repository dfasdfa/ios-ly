//
//  YLCEasyBuyHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCEasyBuyHUDView : UIView
@property (nonatomic ,strong)RACSubject *buttonSignal;
- (void)show;
@end
