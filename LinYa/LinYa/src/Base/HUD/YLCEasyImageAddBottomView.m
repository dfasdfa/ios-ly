//
//  YLCEasyImageAddBottomView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyImageAddBottomView.h"

@implementation YLCEasyImageAddBottomView
{
    UIButton *deleteBtn;
    UIButton *uploadBtn;
    UIView *view;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.signal = [RACSubject subject];
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    self.alpha = 0.8;
    
    deleteBtn = [YLCFactory createCommondBtnWithTitle:@"删除图片" backgroundColor:RGB(81, 143, 233) titleColor:[UIColor whiteColor]];
    
    deleteBtn.tag = 1444+1;
    
    [self addSubview:deleteBtn];
    
    uploadBtn = [YLCFactory createCommondBtnWithTitle:@"上传图片" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    uploadBtn.tag = 1444+2;
    
    [self addSubview:uploadBtn];
    
    [deleteBtn addTarget:self action:@selector(didClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [uploadBtn addTarget:self action:@selector(didClick:) forControlEvents:UIControlEventTouchUpInside];
    
    WS(weakSelf)
    
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_centerX).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(150);
        make.height.equalTo(40);
    }];
    
    [uploadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_centerX).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(150);
        make.height.equalTo(40);
    }];
}
- (void)didClick:(UIButton *)sender{
    [_signal sendNext:[NSString stringWithFormat:@"%ld",sender.tag-1444]];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y -= 60;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                         
                     }];
    
}
- (void)dissMiss{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.origin.y += 60;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         [view removeFromSuperview];
                         [self removeFromSuperview];
                     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
