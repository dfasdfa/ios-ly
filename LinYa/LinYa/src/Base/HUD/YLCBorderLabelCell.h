//
//  YLCBorderLabelCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCBorderLabelCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *tipString;
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,assign)NSInteger type;//0为有边框 1没有边框
@end
