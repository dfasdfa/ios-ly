//
//  YLCResumeEmptyHUDView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCResumeEmptyHUDView : UIView
@property (nonatomic ,strong)RACSubject *subject;
- (void)show;
@end
