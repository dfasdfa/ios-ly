//
//  YLCPickShowView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, YLCPickerType) {
    YLCPickerTypeNormal,
    YLCPickerTypeDate,
};
@interface YLCPickShowView : UIView
@property (nonatomic ,assign)YLCPickerType pickType;
@property (nonatomic ,strong)NSArray *rowArr;
@property (nonatomic ,strong)RACSubject *selectSignal;
- (void)show;
- (void)dissMiss;
@end
