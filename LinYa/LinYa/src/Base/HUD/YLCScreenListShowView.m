//
//  YLCScreenListShowView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCScreenListShowView.h"
@interface YLCScreenListShowView()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCScreenListShowView
{
    UITableView *listTable;
    UIView *bottomView;
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.selectSignal = [RACSubject subject];
        
        self.dissSignal = [RACSubject subject];
        
        listTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        
        listTable.backgroundColor = [UIColor whiteColor];
        
        listTable.delegate = self;
        
        listTable.dataSource = self;
        
        listTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self addSubview:listTable];
    }
    return self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list_table_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"list_table_cell"];
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 43.5, self.frameWidth, 0.5)];
        
        lineView.backgroundColor = YLC_THEME_COLOR;
        
        [cell.contentView addSubview:lineView];
    }
    if (indexPath.row<_list.count) {
        cell.textLabel.text = _list[indexPath.row];
    }
    return cell;
}
- (void)setList:(NSArray *)list{
    _list = list;
    
    [listTable reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_selectSignal sendNext:indexPath];
    [self dissMiss];
}
- (void)dissApear{
    [self dissMiss];
}
- (void)show{
    _hasShow = YES;
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frameY, self.frameWidth, keyWindow.frameHeight-self.frameY)];
    
    bottomView.backgroundColor = [UIColor blackColor];
    
    bottomView.alpha = 0.3;
    
    [keyWindow addSubview:bottomView];
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissApear)];
    
    [bottomView addGestureRecognizer:ges];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = _list.count*44;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished)
     {
         
     }];
}
- (void)dissMiss{
    _hasShow = NO;
    bottomView.alpha = 0;
    [UIView animateWithDuration:0.05
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = 0;
                         self.frame = frame;
                         
                     }
                     completion:^(BOOL finished)
     {
         
         [_dissSignal sendNext:nil];
         [bottomView removeFromSuperview];
         bottomView = nil;
         [self removeFromSuperview];
     }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [listTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
