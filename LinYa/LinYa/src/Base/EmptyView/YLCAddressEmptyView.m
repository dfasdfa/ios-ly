//
//  YLCAddressEmptyView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAddressEmptyView.h"

@implementation YLCAddressEmptyView
{
    UIImageView *emptyView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    self.insertSubject = [RACSubject subject];
    
    emptyView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"address_empty"]];
    
    [self addSubview:emptyView];
    
    UILabel *emptyLabel = [YLCFactory createLabelWithFont:16 color:RGB(153, 153, 153)];
    
    emptyLabel.text = @"未有地址信息";
    
    [self addSubview:emptyLabel];
    
    UIButton *emptyBtn = [YLCFactory createLabelBtnWithTitle:@"+ 添加新地址" titleColor:YLC_THEME_COLOR];
    
    emptyBtn.backgroundColor = [UIColor whiteColor];
    
    [emptyBtn addTarget:self action:@selector(didTouch) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:emptyBtn];
    
    WS(weakSelf)
    
    [emptyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top).offset(30);
        make.width.height.equalTo(150);
    }];
    
    [emptyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(emptyView.mas_bottom).offset(20);
    }];
    
    [emptyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(emptyLabel.mas_bottom).offset(100);
        make.height.equalTo(40);
    }];
}
- (void)didTouch{
    [self.insertSubject sendNext:nil];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
