//
//  YLCScreenTypeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCScreenTypeCell.h"

@implementation YLCScreenTypeCell
{
    UILabel *typeLabel;
    UIImageView *typeView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    typeLabel = [YLCFactory createLabelWithFont:14];
    
    [self.contentView addSubview:typeLabel];
    
    typeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"train_type"]];
    
    [self.contentView addSubview:typeView];
    
    self.layer.cornerRadius = 5;
    
    self.layer.masksToBounds = YES;
    
    self.layer.borderWidth = 0.5;
}
- (void)setType:(NSInteger)type{
    if (type==1) {
        self.layer.borderWidth = 0;
        typeView.hidden = YES;
    }else{
        self.layer.borderWidth = 0.5;
    }
    
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        self.layer.borderColor = YLC_THEME_COLOR.CGColor;
        typeLabel.textColor = YLC_THEME_COLOR;
        typeView.hidden = NO;
    }else{
        self.layer.borderColor = YLC_GRAY_TEXT_COLOR.CGColor;
        typeLabel.textColor = YLC_GRAY_TEXT_COLOR;
        typeView.hidden = YES;
    }

}
- (void)setTitle:(NSString *)title{
    typeLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [typeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.width.equalTo(10);
        make.height.equalTo(10);
    }];
}
@end
