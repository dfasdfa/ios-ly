//
//  YLCBottomBtnView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBottomBtnView.h"

@implementation YLCBottomBtnView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    _talkBtn = [self commonBtnWithTitle:@"立即沟通" color:[UIColor whiteColor] titleColor:YLC_THEME_COLOR];
    
    _talkBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
    
    [self addSubview:_talkBtn];
    
    _applyBtn = [self commonBtnWithTitle:@"申请职位" color:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [self addSubview:_applyBtn];

}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [_talkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_centerX).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(145);
        make.height.equalTo(45);
    }];
    
    [_applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_centerX).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(145);
        make.height.equalTo(45);
    }];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    
    return button;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
