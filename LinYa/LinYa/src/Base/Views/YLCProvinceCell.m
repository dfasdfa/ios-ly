//
//  YLCProvinceCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProvinceCell.h"

@implementation YLCProvinceCell
{
    UILabel *titleLabel;
    UIImageView *leftSelectedView;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    leftSelectedView = [[UIImageView alloc] init];
    
    leftSelectedView.backgroundColor = YLC_THEME_COLOR;
    
    [self addSubview:leftSelectedView];
    
    titleLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:titleLabel];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        self.backgroundColor = [UIColor whiteColor];
        titleLabel.textColor = YLC_THEME_COLOR;
        leftSelectedView.hidden = NO;
    }else{
        self.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = RGB(51, 51, 51);
        leftSelectedView.hidden = YES;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [leftSelectedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(5);
        make.height.equalTo(25);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.centerX.equalTo(weakSelf.mas_centerX);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
