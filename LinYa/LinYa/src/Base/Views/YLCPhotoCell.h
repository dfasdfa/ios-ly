//
//  YLCPhotoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCPhotoModel;
@interface YLCPhotoCell : UICollectionViewCell
@property (nonatomic ,strong)YLCPhotoModel *model;
-(UIImageView*)getImageView;
@end
