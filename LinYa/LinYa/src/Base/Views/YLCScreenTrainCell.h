//
//  YLCScreenTrainCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCScreenTrainCell;
@protocol YLCScreenTrainCellDelegate <NSObject>
- (void)didSelectWithSelectIndex:(NSInteger)selectIndex
                        indexRow:(NSInteger)indexRow;
@end
@interface YLCScreenTrainCell : UICollectionViewCell
@property (nonatomic ,assign)NSInteger selectIndex;
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,weak)id<YLCScreenTrainCellDelegate>delegate;
@property (nonatomic ,assign)NSInteger type;//0 横列布局 1是数列布局
@end
