//
//  YLCPickerView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPickerView.h"
#import "YLCPickModel.h"
@interface YLCPickerView()<UIPickerViewDelegate,UIPickerViewDataSource>
@end
@implementation YLCPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.changeSignal = [RACSubject subject];
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return _pickerDataArr.count;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    YLCPickModel *model = _pickerDataArr[component];
    return model.rowArr.count;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return self.frameWidth/_pickerDataArr.count;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/3, [pickerView rowSizeForComponent:component].height)];
    
    rowLabel.font = [UIFont systemFontOfSize:17];
    
    rowLabel.textColor = YLC_THEME_COLOR;
    
    rowLabel.textAlignment = NSTextAlignmentCenter;
    
    if (component<_pickerDataArr.count) {
        YLCPickModel *model = _pickerDataArr[component];
        if (row<model.rowArr.count) {
            rowLabel.text = model.rowArr[row];
        }
        if (row==model.selectIndex) {
            rowLabel.textColor = YLC_THEME_COLOR;
        }else{
            rowLabel.textColor = [UIColor blackColor];
        }
    }
    return rowLabel;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    YLCPickModel *model = _pickerDataArr[component];
    model.selectIndex = row;
    NSMutableArray *pickArr = [_pickerDataArr mutableCopy];
    [pickArr setObject:model atIndexedSubscript:component];
    _pickerDataArr = pickArr.copy;
    [self reloadComponent:component];
    [_changeSignal sendNext:_pickerDataArr];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
