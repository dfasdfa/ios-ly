//
//  YLCTimeButtonView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTimeButtonView.h"

@implementation YLCTimeButtonView
{
    UIButton *timeButton;
    NSTimer *timeLimitTimer;
    NSString *_title;
    NSInteger _timeLimit;
    NSInteger runTimeLimit;
}
- (UIButton *)timerBtn{
    return timeButton;
}
- (id)initWithFrame:(CGRect)frame
              title:(NSString *)title
          timeLimit:(NSInteger)timeLimit{
    self = [super initWithFrame:frame];
    if (self) {
        self.touchSignal = [RACSubject subject];
        
        _timeLimit = timeLimit;
        
        _title = title;
        
        timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        timeButton.titleLabel.font = [UIFont systemFontOfSize:14];
        
        [timeButton setTitle:title forState:UIControlStateNormal];
        
        [timeButton setTitle:title forState:UIControlStateDisabled];
        
        [timeButton setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
        
        [timeButton setTitleColor:RGB(185, 185, 185) forState:UIControlStateDisabled];
        
        timeButton.backgroundColor = [UIColor whiteColor];
        
        timeButton.layer.cornerRadius = 5;
        
        timeButton.layer.masksToBounds = YES;
        
        timeButton.userInteractionEnabled = YES;
        
        timeButton.layer.borderColor = YLC_THEME_COLOR.CGColor;
        
        timeButton.layer.borderWidth = 1;
        
        [timeButton addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        
        timeButton.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        
        [self addSubview:timeButton];

    }
    return self;
}
- (void)resetBtn{
    [self invalidateTimer];
}
- (void)updateResentPasscodeBtnTitle {
    runTimeLimit--;
    
    if (runTimeLimit <= 0) {
        [self invalidateTimer];
        [timeButton setTitle:_title forState:UIControlStateNormal];
        [timeButton setTitle:_title forState:UIControlStateDisabled];
        timeButton.userInteractionEnabled = YES;
        timeButton.backgroundColor = [UIColor whiteColor];
    }else {
        timeButton.userInteractionEnabled = NO;
        [timeButton setTitle:[NSString stringWithFormat:@"剩余%ld秒", (long)runTimeLimit] forState:UIControlStateNormal];
        [timeButton setTitle:[NSString stringWithFormat:@"剩余%ld秒", (long)runTimeLimit] forState:UIControlStateDisabled];
        timeButton.backgroundColor = [UIColor whiteColor];
    }
}
- (void)setUpTimer{
    runTimeLimit = _timeLimit;
    
    [self invalidateTimer];
    
    timeLimitTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateResentPasscodeBtnTitle) userInfo:nil repeats:YES];
}
- (void)invalidateTimer {
    if (timeLimitTimer) {
        [timeLimitTimer invalidate];
        timeLimitTimer = nil;
        runTimeLimit = 0;
        [self updateResentPasscodeBtnTitle];
    }
}
- (void)buttonClick{
    [self setUpTimer];
    [_touchSignal sendNext:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
