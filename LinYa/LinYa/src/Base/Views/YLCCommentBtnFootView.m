//
//  YLCCommentBtnFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCommentBtnFootView.h"

@implementation YLCCommentBtnFootView
{
    UIButton *button;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        button = [YLCFactory createCommondBtnWithTitle:@"" backgroundColor:[UIColor clearColor] titleColor:[UIColor clearColor]];
        
        [self addSubview:button];
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    [button setTitle:title forState:UIControlStateNormal];
}
- (void)setTextColor:(UIColor *)textColor{
    [button setTitleColor:textColor forState:UIControlStateNormal];
}
- (void)setBackgroundColor:(UIColor *)backgroundColor{
    [button setBackgroundColor:backgroundColor];
}
@end
