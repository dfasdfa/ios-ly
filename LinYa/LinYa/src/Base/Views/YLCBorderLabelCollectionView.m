//
//  YLCBorderLabelCollectionView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBorderLabelCollectionView.h"
#import "YLCBoderLabelCell.h"
#define BORDER_CELL @"YLCBoderLabelCell"
@interface YLCBorderLabelCollectionView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCBorderLabelCollectionView
{
    UICollectionView *labelTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumInteritemSpacing = 5;
    
    labelTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    labelTable.backgroundColor = [UIColor clearColor];
    
    [labelTable registerClass:[YLCBoderLabelCell class] forCellWithReuseIdentifier:BORDER_CELL];
    
    labelTable.delegate = self;
    
    labelTable.dataSource = self;
    
    [self addSubview:labelTable];
}
- (void)setLabelList:(NSArray *)labelList{
    
    _labelList = labelList;
    
    [labelTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _labelList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCBoderLabelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BORDER_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_labelList.count) {
        cell.labelString = _labelList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = [YLCBoderLabelCell configCellHeightWithString:_labelList[indexPath.row]];
    return CGSizeMake(width+10, 30);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [labelTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
