//
//  YLCBuySuccessHUD.m
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBuySuccessHUD.h"

@implementation YLCBuySuccessHUD
{
    UIView *view;
    UIImageView *backgroundView;
    UIView *iconView;
    UILabel *tipLabel;
    UIButton *cancelBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success_background"]];
    
    [self addSubview:backgroundView];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
