//
//  YLCListShowView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCListShowView.h"
#import "YLCHUDViewManager.h"
@interface YLCListShowView()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCListShowView
{
    UITableView *listTable;
    UIView *bottomView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.selectSignal = [RACSubject subject];
        
        self.dissSignal = [RACSubject subject];
        
        listTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        listTable.backgroundColor = [UIColor whiteColor];
        
        listTable.delegate = self;
        
        listTable.dataSource = self;
        
        listTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self addSubview:listTable];
    }
    return self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _list.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list_table_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"list_table_cell"];
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
//        UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 43.5, self.frameWidth, 0.5)];
//
//        lineView.backgroundColor = YLC_THEME_COLOR;
//
//        [cell.contentView addSubview:lineView];
    }
    if (indexPath.row<_list.count) {
        cell.textLabel.text = _list[indexPath.row];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_selectSignal sendNext:indexPath];
    [self dissMiss];
}
- (void)dissApear{
    [self dissMiss];
}
- (void)show{
    [[YLCHUDViewManager shareManager] setListShowState:YES];
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frameY, self.frameWidth, keyWindow.frameHeight-self.frameY)];
    
    bottomView.backgroundColor = [UIColor blackColor];
    
    bottomView.alpha = 0.3;
    
    [keyWindow addSubview:bottomView];
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissApear)];
    
    [bottomView addGestureRecognizer:ges];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = _list.count*44;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished)
                     {
                         
     }];
}
- (void)dissMiss{
    [[YLCHUDViewManager shareManager] setListShowState:NO];
    bottomView.alpha = 0;
    [UIView animateWithDuration:0.05
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = 0;
                         self.frame = frame;
                         
                     }
                     completion:^(BOOL finished)
     {
         
         [_dissSignal sendNext:nil];
         [bottomView removeFromSuperview];
         bottomView = nil;
         [self removeFromSuperview];
     }];
}
- (void)layoutSubviews{
    [super layoutSubviews];

    [listTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
