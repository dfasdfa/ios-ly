//
//  YLCListShowView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCListShowView : UIView
@property (nonatomic ,strong)NSArray *list;
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,strong)RACSubject *dissSignal;
- (void)show;
- (void)dissMiss;
@end
