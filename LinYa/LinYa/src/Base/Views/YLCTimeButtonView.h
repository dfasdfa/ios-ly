//
//  YLCTimeButtonView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface YLCTimeButtonView : UIView
@property (nonatomic ,strong)RACSubject *touchSignal;
@property (nonatomic ,readonly)UIButton *timerBtn;
- (id)initWithFrame:(CGRect)frame
              title:(NSString *)title
          timeLimit:(NSInteger)timeLimit;
//进入倒计时
- (void)setUpTimer;
//重置按钮状态
- (void)resetBtn;
@end
