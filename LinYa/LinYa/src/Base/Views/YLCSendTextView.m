//
//  YLCSendTextView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSendTextView.h"

@implementation YLCSendTextView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.textView = [[UITextView alloc] init];
        
        self.textView.backgroundColor = RGB(235, 235, 235);
        
        [self addSubview:self.textView];
        
        self.textView.placeholder = @"优质评论优先展示...";
        
        self.textView.layer.cornerRadius = 5;
        
        self.textView.layer.masksToBounds = YES;
        
        _sendBtn = [YLCFactory createCommondBtnWithTitle:@"发表" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
        
        [self addSubview:_sendBtn];
        
        [_textView.rac_textSignal subscribeNext:^(id x) {
            if ([YLCFactory isStringOk:x]) {
                _sendBtn.backgroundColor = YLC_THEME_COLOR;
                _sendBtn.enabled = YES;
            }else{
                _sendBtn.backgroundColor = RGB(183, 183, 183);
                _sendBtn.enabled = NO;
            }
        }];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(5, 5, 40, 5));
    }];
    
    [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textView.mas_right);
        make.top.equalTo(_textView.mas_bottom).offset(5);
        make.width.equalTo(60);
        make.height.equalTo(30);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
