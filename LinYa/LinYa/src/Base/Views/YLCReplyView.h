//
//  YLCReplyView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCReplyView : UIView
@property (nonatomic ,strong)UITextField *inputTextView;
@property (nonatomic ,strong)UIButton *collectBtn;
@property (nonatomic ,strong)UIButton *talkBtn;
@property (nonatomic ,strong)UIButton *likeBtn;
@property (nonatomic ,strong)UIButton *shareBtn;
@property (nonatomic ,assign)BOOL isVideo;
@property (nonatomic ,assign)NSInteger type;
@end
