//
//  YLCLikeButton.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLikeButton.h"
@interface YLCLikeButton()

@end
@implementation YLCLikeButton
{
    BOOL didLike;
    BOOL isAnimation;
    UILabel *titleLabel;
    UIImageView *likeImageView;
}
- (BOOL)liked{
    return didLike;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
                  normalImage:(NSString *)normalImage
                  selectImage:(NSString *)selectImage
                  normalColor:(UIColor *)normalColor
                selectedColor:(UIColor *)selectedColor{
    if (self = [super initWithFrame:frame]) {
        [self createCustomView];
        self.normalImage = normalImage;
        self.selectImage = selectImage;
        self.normalColor = normalColor;
        self.selectColor = selectedColor;
    }
    return self;
}
- (void)createCustomView{
    self.likeSignal = [RACSubject subject];
    
    likeImageView = [[UIImageView alloc] init];
    
    [self addSubview:likeImageView];
    
    titleLabel = [YLCFactory createLabelWithFont:14 color:[UIColor blackColor]];
    
    [self addSubview:titleLabel];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didLike)];
    
    [self addGestureRecognizer:ges];
}
- (void)didLike{
    if (_noTouch) {
        return;
    }
    didLike = !didLike;
    
    self.count = [NSString stringWithFormat:@"%ld",[self.count integerValue]+[self addOrSubtract]];
    
    [self.likeSignal sendNext:didLike?@"1":@"0"];
    [self reloadUI];
}
- (NSInteger)addOrSubtract{
    return didLike?1:-1;
}
- (void)setCount:(NSString *)count{
    _count = count;
    [self reloadUI];
}
- (void)setNormalImage:(NSString *)normalImage {
    _normalImage = normalImage;
    [self reloadUI];
}
- (void)setNormalColor:(UIColor *)normalColor
{
    _normalColor = normalColor;
    [self reloadUI];
}
- (void)setSelectColor:(UIColor *)selectColor{
    _selectColor = selectColor;
    [self reloadUI];
}
- (void)setSelectImage:(NSString *)selectImage{
    _selectImage = selectImage;
    [self reloadUI];
}
- (void)configureLikeStatus:(BOOL)isLike count:(NSInteger)count animated:(BOOL)animated{
    didLike = isLike;
    isAnimation = animated;
    self.count = [NSString stringWithFormat:@"%ld",count];
    [self reloadUI];
}
- (void)reloadUI{
    if (didLike) {
        titleLabel.textColor = _selectColor;
        likeImageView.image = [UIImage imageNamed:_selectImage];
    }else{
        titleLabel.textColor = _normalColor;
        likeImageView.image = [UIImage imageNamed:_normalImage];
    }
    titleLabel.text = self.count;
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    WS(weakSelf)
    [likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(likeImageView.mas_right).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
