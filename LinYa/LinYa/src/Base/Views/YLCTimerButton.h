//
//  YLCTimerButton.h
//  YouLe
//
//  Created by 初程程 on 2018/2/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTimerButton : UIButton
@property (nonatomic ,strong)NSString *timeTitle;
@property (nonatomic ,strong)NSString *timeLimit;
@end
