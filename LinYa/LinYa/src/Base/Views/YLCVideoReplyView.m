//
//  YLCVideoReplyView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoReplyView.h"

@implementation YLCVideoReplyView
- (void)createCustomView{
    
    _likeBtn = [YLCFactory createBtnWithImage:@"share_like"];
    
    [self addSubview:_likeBtn];
    
    _shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
    
    [self addSubview:_shareBtn];
    
    _inputTextView = [[UITextField alloc] init];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    
    _inputTextView.leftView = leftView;
    
    _inputTextView.leftViewMode = UITextFieldViewModeAlways;
    
    _inputTextView.placeholder = @"发表回帖...";
    
    _inputTextView.backgroundColor = RGB(234,237,241);
    
    _inputTextView.layer.cornerRadius = 20;
    
    _inputTextView.layer.masksToBounds = YES;
    
    [self addSubview:_inputTextView];
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    
    [_inputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.right.equalTo(weakSelf.mas_right).offset(-170);
        make.height.equalTo(40);
    }];
    
    [_likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_inputTextView.mas_right).offset(12);
        make.top.equalTo(_inputTextView.mas_top);
        make.width.equalTo(40);
        make.height.equalTo(40);
    }];
    
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_likeBtn.mas_right).offset(10);
        make.centerY.equalTo(_likeBtn.mas_centerY);
        make.width.height.equalTo(40);
    }];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
