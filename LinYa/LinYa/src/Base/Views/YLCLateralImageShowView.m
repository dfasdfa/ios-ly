//
//  YLCLateralImageShowView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLateralImageShowView.h"
#import "YLCPhotoCell.h"
#import "SDPhotoBrowser.h"
#import "YLCPhotoModel.h"
#define PHOTO_CELL @"YLCPhotoCell"
@interface YLCLateralImageShowView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SDPhotoBrowserDelegate>
@end
@implementation YLCLateralImageShowView
{
    UICollectionView *photoTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 10;
    
    photoTable = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flow];
    
    photoTable.backgroundColor = [UIColor clearColor];
    
    [photoTable registerClass:[YLCPhotoCell class] forCellWithReuseIdentifier:PHOTO_CELL];
    
    photoTable.delegate = self;
    
    photoTable.dataSource = self;
    
    [self addSubview:photoTable];
}
- (void)setPhotoList:(NSArray *)photoList{
    _photoList = photoList;
    [photoTable reloadData];
}
- (void)setCellSize:(CGSize)cellSize{
    _cellSize = cellSize;
    
    [photoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];
    if (indexPath.row<_photoList.count) {
        cell.model = _photoList[indexPath.row];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return _cellSize;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SDPhotoBrowser *photoBrowser = [SDPhotoBrowser new];
    photoBrowser.delegate = self;
    photoBrowser.currentImageIndex = indexPath.row;
    photoBrowser.imageCount = _photoList.count;
    photoBrowser.sourceImagesContainerView = collectionView;
    [photoBrowser show];
}
- (NSURL *)photoBrowser:(SDPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index{
    YLCPhotoModel *model = _photoList[index];
    return [NSURL URLWithString:model.imagePath];
}

- (UIImage *)photoBrowser:(SDPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index{
    YLCPhotoCell * cell = (YLCPhotoCell *)[photoTable cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    return [cell getImageView].image;

}
- (void)layoutSubviews{
    [super layoutSubviews];
    [photoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
