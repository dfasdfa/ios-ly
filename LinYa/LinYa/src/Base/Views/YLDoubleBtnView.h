//
//  YLDoubleBtnView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLDoubleBtnView;
@protocol YLDoubleBtnViewDelegate <NSObject>
- (void)didTouchWithType:(BOOL)isLeft;
@end
@interface YLDoubleBtnView : UICollectionReusableView
@property (nonatomic ,weak)id<YLDoubleBtnViewDelegate>delegate;
@end
