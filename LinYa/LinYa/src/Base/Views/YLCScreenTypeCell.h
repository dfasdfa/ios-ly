//
//  YLCScreenTypeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCScreenTypeCell : UICollectionViewCell
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,assign)NSInteger type;//0 横列布局 1是数列布局
@end
