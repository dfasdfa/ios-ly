//
//  YLCSendTextView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCSendTextView : UIView
@property (nonatomic ,strong)UITextView *textView;
@property (nonatomic ,strong)UIButton *sendBtn;
@end
