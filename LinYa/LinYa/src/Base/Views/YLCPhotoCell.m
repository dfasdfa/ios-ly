//
//  YLCPhotoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPhotoCell.h"
#import "YLCPhotoModel.h"
@implementation YLCPhotoCell
{
    UIImageView *imageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:imageView];
    }
    return self;
}
- (void)setModel:(YLCPhotoModel *)model{
    if (!model.isWeb) {
        imageView.image = [UIImage imageNamed:model.placeholder];
    }else{
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.imagePath] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
-(UIImageView*)getImageView{
    return imageView;
}
@end
