//
//  YLCCommentBtnFootView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCCommentBtnFootView : UICollectionReusableView
@property (nonatomic ,strong)UIColor *textColor;
@property (nonatomic ,strong)UIColor *backgroundColor;
@property (nonatomic ,copy)NSString *title;
@end
