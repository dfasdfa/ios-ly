//
//  YLCScreenAreaView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCScreenAreaView.h"
#import "YLCHUDViewManager.h"
#import "YLCAreaModel.h"
#import "YLCProvinceCell.h"
#import "YLCCityCell.h"
@interface YLCScreenAreaView()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCScreenAreaView
{
    UITableView *provinceTable;
    UITableView *cityTable;
    UITableView *areaTable;
    UIView *bottomView;
    NSInteger provinceIndex;
    NSInteger cityIndex;
    NSInteger areaIndex;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.selectSignal = [RACSubject subject];
        self.dissSignal = [RACSubject subject];
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    provinceIndex = 0;
    
    cityIndex = 0;
    
    areaIndex = 0;
    
    provinceTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    provinceTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    provinceTable.delegate = self;
    
    provinceTable.separatorStyle = UITableViewCellEditingStyleNone;
    
    provinceTable.dataSource = self;
    
    [self addSubview:provinceTable];
    
    provinceTable.tableFooterView = [[UIView alloc] init];
    
    cityTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    cityTable.separatorStyle = UITableViewCellEditingStyleNone;
    
    cityTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    cityTable.delegate = self;
    
    cityTable.dataSource = self;
    
    [self addSubview:cityTable];
    
    cityTable.tableFooterView = [[UIView alloc] init];
    
    areaTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    areaTable.separatorStyle = UITableViewCellEditingStyleNone;
    
    areaTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    areaTable.delegate = self;
    
    areaTable.dataSource = self;
    
    [self addSubview:areaTable];
    
    areaTable.tableFooterView = [[UIView alloc] init];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView==provinceTable) {
        return [self provinceList].count;
    }
    if (tableView==cityTable) {
        return [self cityList].count;
    }
    return [self areaList].count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==provinceTable) {
        YLCProvinceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"screen_provinceCell"];
        
        if (!cell) {
            cell = [[YLCProvinceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"screen_provinceCell"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row<[self provinceList].count) {
            YLCAreaModel *provinceModel = [self provinceList][indexPath.row];
            cell.title = provinceModel.areaname;
        }
        if (indexPath.row==provinceIndex) {
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
        
        return cell;
        
    }
    if (tableView==cityTable) {
        YLCCityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"screen_city"];
        
        if (!cell) {
            cell  = [[YLCCityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"screen_city"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (indexPath.row < [self cityList].count) {
            YLCAreaModel *cityModel = [self cityList][indexPath.row];
            cell.title = cityModel.areaname;
        }
        if (indexPath.row==cityIndex) {
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
        return cell;
        
    }
    if (tableView==areaTable) {
        YLCCityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"screen_area"];
        
        if (!cell) {
            cell  = [[YLCCityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"screen_area"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (indexPath.row< [self areaList].count) {
            YLCAreaModel *areaModel = [self areaList][indexPath.row];
              cell.title = areaModel.areaname;
        }
        if (indexPath.row==areaIndex) {
            cell.isSelect = YES;
        }else{
            cell.isSelect = NO;
        }
        
        return cell;
        
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"screen_area_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"screen_area_cell"];
        
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==provinceTable) {
        provinceIndex = indexPath.row;
        cityIndex = 0;
        [cityTable reloadData];
        [areaTable reloadData];
        [provinceTable reloadData];
    }
    if (tableView==cityTable) {
        cityIndex = indexPath.row;
        areaIndex = 0;
        if (JK_IS_ARRAY_NIL([self areaList])) {
            
            [self.selectSignal sendNext:[self cityList][cityIndex]];
            [self dissMiss];
        }else{
            [cityTable reloadData];
            [areaTable reloadData];
        }
    }
    if (tableView==areaTable) {
        areaIndex = indexPath.row;
        [self.selectSignal sendNext:[self areaList][areaIndex]];
        [self dissMiss];
    }
    
}
- (NSArray *)provinceList{
    return [[YLCAccountManager shareManager] getProvinceList];
}
- (NSArray *)cityList{
    if (JK_IS_ARRAY_NIL([self provinceList])) {
        return @[];
    }
    YLCAreaModel *areaModel = [self provinceList][provinceIndex];
    return [[YLCAccountManager shareManager] getCityListWithSuperId:areaModel.id];
}
- (NSArray *)areaList{
    
    NSArray *cityList = [self cityList];
    if (JK_IS_ARRAY_NIL(cityList)) {
        return @[];
    }
    YLCAreaModel *provinceModel = [self provinceList][provinceIndex];
    YLCAreaModel *cityModel = cityList[cityIndex];
    return [[YLCAccountManager shareManager] getAreaListWithCityId:cityModel.id provinceId:provinceModel.id];
}

- (void)show{
    [[YLCHUDViewManager shareManager] setListShowState:YES];
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frameY, self.frameWidth, keyWindow.frameHeight-self.frameY)];
    
    bottomView.backgroundColor = [UIColor blackColor];
    
    bottomView.alpha = 0.3;
    
    [keyWindow addSubview:bottomView];
    
    [keyWindow addSubview:self];
    
    [keyWindow bringSubviewToFront:self];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dissMiss)];
    
    [bottomView addGestureRecognizer:ges];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = 10*44;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished)
     {
         
     }];
}
- (void)dissMiss{
    [[YLCHUDViewManager shareManager] setListShowState:NO];
    bottomView.alpha = 0;
    [UIView animateWithDuration:0.05
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGRect frame = self.frame;
                         frame.size.height = 0;
                         self.frame = frame;
                         
                     }
                     completion:^(BOOL finished)
     {
         
         [_dissSignal sendNext:nil];
         [bottomView removeFromSuperview];
         bottomView = nil;
         [self removeFromSuperview];
     }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    provinceTable.frame = CGRectMake(0, 0, self.frameWidth/3, self.frameHeight);
    
    cityTable.frame = CGRectMake(provinceTable.frameMaxX, 0, self.frameWidth/3, self.frameHeight);
    
    areaTable.frame = CGRectMake(cityTable.frameMaxX, 0, self.frameWidth/3, self.frameHeight);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
