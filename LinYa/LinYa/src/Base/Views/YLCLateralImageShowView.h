//
//  YLCLateralImageShowView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCLateralImageShowView : UIView
@property (nonatomic ,assign)BOOL enable;
@property (nonatomic ,strong)NSArray *photoList;
@property (nonatomic ,assign)CGSize cellSize;
@end
