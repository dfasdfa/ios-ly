//
//  YLCBottomBtnView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCBottomBtnView : UIView
@property (nonatomic ,strong)UIButton *talkBtn;
@property (nonatomic ,strong)UIButton *applyBtn;
@end
