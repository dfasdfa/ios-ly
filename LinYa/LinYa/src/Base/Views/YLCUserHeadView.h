//
//  YLCUserHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/5/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCUserHeadView : UIView
@property (nonatomic ,assign)BOOL canGo;
@property (nonatomic ,copy)NSString *userId;
@property (nonatomic ,assign)BOOL isCare;
- (void)configHeadViewWithImageUrl:(NSString *)imageUrl
                       placeholder:(NSString *)placeholder
                        isGoCenter:(BOOL)isGoCenter
                            userId:(NSString *)userId;
@end
