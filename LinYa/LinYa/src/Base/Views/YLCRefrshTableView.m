//
//  YLCRefrshTableView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRefrshTableView.h"
@interface YLCRefrshTableView()

@end
@implementation YLCRefrshTableView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.reloadSignal = [RACSubject subject];
        
        self.scrollView = [self customScrollView];

        [self.scrollView setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(reloadData) footAction:@selector(didTriggerLoadNextPageData)];
        
        [self addSubview:self.scrollView];
        
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];
    }
    return self;
}
- (void)setIsOnlyHead:(BOOL)isOnlyHead{
    if (isOnlyHead) {
        self.scrollView.mj_footer = nil;
    }
}
- (void)beganRefresh{
    [self.scrollView.mj_header beginRefreshing];
}
- (void)resetNoMore{
    [self.scrollView.mj_header endRefreshing];
    [self.scrollView.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [self.scrollView.mj_header endRefreshing];
    [self.scrollView.mj_footer endRefreshingWithNoMoreData];
}
- (void)reloadData{
    [self.reloadSignal sendNext:@"0"];
}
- (void)didTriggerLoadNextPageData{
    
    NSInteger page = [_dataArr count]/[PAGE_LIMIT integerValue];
    
    [self.reloadSignal sendNext:[NSString stringWithFormat:@"%ld",page]];
}
- (UIScrollView *)customScrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    }
    return _scrollView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
