//
//  YLCPickerView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCPickerView : UIPickerView
@property (nonatomic ,strong)NSArray *pickerDataArr;
@property (nonatomic ,strong)RACSubject *changeSignal;
@end
