//
//  YLCStarView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCStarView.h"

@implementation YLCStarView
{
    UIImageView *yelloView;
    UIImageView *grayView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.starSignal = [RACSubject subject];
        
        grayView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        grayView.userInteractionEnabled = YES;
        
        grayView.contentMode = UIViewContentModeCenter;
        
        [self addSubview:grayView];
        
        yelloView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        yelloView.userInteractionEnabled = YES;
        
        yelloView.contentMode = UIViewContentModeCenter;
        
        [self addSubview:yelloView];
    }
    return self;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSSet *allTouches = [event allTouches];
    UITouch *touch = [allTouches anyObject];
    CGPoint point = [touch locationInView:[touch view]];
    int x = point.x;
    NSLog(@"%d",x);
    CGFloat oneWidth = self.frameWidth/5;
    NSString *starString = @"1";
    if (x<oneWidth) {
        starString = @"1";
    }
    if (oneWidth<x&&x<oneWidth*2) {
        
        starString = @"2";
    }
    if (x>2*oneWidth&&x<3*oneWidth) {
        starString = @"3";
    }
    if (x>3*oneWidth&&x<4*oneWidth) {
        starString = @"4";
    }
    if (x>4*oneWidth&&x<self.frameWidth) {
        starString = @"5";
    }
    if (_canTouch) {
        [self.starSignal sendNext:starString];
    }
}
- (void)setScore:(CGFloat)score{
    CGFloat width = self.frameWidth*score/5;
        
    yelloView.frame = CGRectMake(0, 0, width, self.frameHeight);
}
- (void)setYelloName:(NSString *)yelloName{
    
    UIImage *yelloImage = [UIImage imageNamed:yelloName];
    
    yelloView.backgroundColor = [UIColor colorWithPatternImage:[self commonScaledImageFromImage:yelloImage]];
}
- (void)setGrayName:(NSString *)grayName{
    
    UIImage *grayImage = [UIImage imageNamed:grayName];
    
    grayView.backgroundColor = [UIColor colorWithPatternImage:[self commonScaledImageFromImage:grayImage]];
}
- (UIImage *)commonScaledImageFromImage:(UIImage *)img {
    CGSize newSize = CGSizeMake(img.size.width+4, img.size.height);
    
    UIGraphicsBeginImageContext(newSize);
    
    [img drawInRect:CGRectMake(2,0,newSize.width-5,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [grayView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
