//
//  YLCUserHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCUserHeadView.h"
#import "YLCUserCenterViewController.h"
@implementation YLCUserHeadView
{
    UIImageView *headView;
}
- (instancetype)init
{
    return [self initWithFrame:CGRectZero];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    headView = [[UIImageView alloc] init];
    
    headView.userInteractionEnabled = YES;
    
    
    
    [self addSubview:headView];
    
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsZero);
        
    }];
    
    UITapGestureRecognizer *headGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headClick)];
    
    [headView addGestureRecognizer:headGes];
}

- (void)configHeadViewWithImageUrl:(NSString *)imageUrl
                       placeholder:(NSString *)placeholder
                        isGoCenter:(BOOL)isGoCenter
                            userId:(NSString *)userId{
    [headView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:placeholder]];
    
    self.canGo = YES;
    
    self.userId = userId;
    
//    headView.layer.cornerRadius = headView.frameWidth/2;
//
//    headView.layer.masksToBounds = YES;
    
//    [self layoutIfNeeded];
//
//    [self setNeedsLayout];

}
- (void)headClick{
    if (!_canGo) {
        return;
    }
    YLCUserCenterViewController *con = [[YLCUserCenterViewController alloc] init];
    
    con.userId = _userId;
    
    con.isCare = _isCare;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    CAShapeLayer *mask = [CAShapeLayer new];
    
    mask.path = [UIBezierPath bezierPathWithOvalInRect:headView.bounds].CGPath;
    
    headView.layer.mask = mask;
//    if (self.frameWidth>0) {
    
//    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
