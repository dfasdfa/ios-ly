//
//  YLCBoderLabelCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBoderLabelCell.h"

@implementation YLCBoderLabelCell
{
    UILabel *tipLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    tipLabel = [YLCFactory createLabelWithFont:14 color:[UIColor blueColor]];
    
    tipLabel.layer.borderColor = [UIColor blueColor].CGColor;
    
    tipLabel.layer.borderWidth = 1;
    
    tipLabel.layer.cornerRadius = 5;
    
    tipLabel.layer.masksToBounds = YES;
    
    tipLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView addSubview:tipLabel];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (void)setLabelString:(NSString *)labelString{
    tipLabel.text = labelString;
}
+ (CGFloat)configCellHeightWithString:(NSString *)string{
    CGSize size = [YLCFactory getStringWithSizeWithString:string height:10 font:14];
    
    return size.width;
}
@end
