//
//  YLDoubleBtnView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLDoubleBtnView.h"

@implementation YLDoubleBtnView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIButton *leftBtn = [self commonBtnWithTitle:@"重置" textColor:RGB(183, 183, 183) backgroundColor:[UIColor whiteColor]];
        
        [leftBtn addTarget:self action:@selector(leftTouch) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:leftBtn];
        
        UIButton *rightBtn = [self commonBtnWithTitle:@"确定" textColor:[UIColor whiteColor] backgroundColor:YLC_THEME_COLOR];
        
        [rightBtn addTarget:self action:@selector(rightTouch) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:rightBtn];
        
        leftBtn.frame = CGRectMake(0, 0, self.frameWidth/2, self.frameHeight);
        
        rightBtn.frame = CGRectMake(leftBtn.frameMaxX, 0, self.frameWidth/2, self.frameHeight);
    }
    return self;
}
- (void)leftTouch{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didTouchWithType:)) {
        [self.delegate didTouchWithType:YES];
    }
}
- (void)rightTouch{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didTouchWithType:)) {
        [self.delegate didTouchWithType:NO];
    }
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                       textColor:(UIColor *)textColor
                 backgroundColor:(UIColor *)backgroundColor{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:textColor forState:UIControlStateNormal];
    
    button.backgroundColor = backgroundColor;
    
    return button;
}

@end
