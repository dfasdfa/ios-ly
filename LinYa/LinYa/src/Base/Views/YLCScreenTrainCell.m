//
//  YLCScreenTrainCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCScreenTrainCell.h"
#import "YLCScreenTypeCell.h"
#define SCREEN_TYPE @"YLCScreenTypeCell"
@interface YLCScreenTrainCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCScreenTrainCell
{
    UILabel *titleLabel;
    UICollectionView *typeTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}

- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:titleLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    typeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    typeTable.backgroundColor = [UIColor whiteColor];
    
    [typeTable registerClass:[YLCScreenTypeCell class] forCellWithReuseIdentifier:SCREEN_TYPE];
    
    typeTable.delegate = self;
    
    typeTable.dataSource = self;
    
    typeTable.alwaysBounceVertical = NO;
    
    [self.contentView addSubview:typeTable];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(5);
        make.top.equalTo(weakSelf.mas_top).offset(5);
    }];
    
    [typeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-5);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-5);
    }];
}
- (void)setType:(NSInteger)type{
    _type = type;
    [typeTable reloadData];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    
    [typeTable reloadData];
}
- (void)setSelectIndex:(NSInteger)selectIndex{
    
    _selectIndex = selectIndex;
    
    [typeTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCScreenTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SCREEN_TYPE forIndexPath:indexPath];
    
    if (indexPath.row==_selectIndex) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    if (indexPath.row<_dataList.count) {
        cell.title = _dataList[indexPath.row];
    }
    cell.type = _type;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_type==1) {
        return CGSizeMake(self.frameWidth, 30);
    }
    return CGSizeMake(70, 30);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectIndex = indexPath.row;
    
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectWithSelectIndex:indexRow:)) {
        [self.delegate didSelectWithSelectIndex:_selectIndex indexRow:_index];
    }
    
    [typeTable reloadData];
    
}
@end
