//
//  YLCReplyView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReplyView.h"

@implementation YLCReplyView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGB(253, 253, 253);
        [self createCustomView];
        [self layoutFrame];
    }
    return self;
}
- (void)setIsVideo:(BOOL)isVideo{
    
    _isVideo = isVideo;
    
    [self configUI];
}
- (void)setType:(NSInteger)type{
    
    _type = type;
    
    [self remakeFrame];
}
- (void)createCustomView{
    
    _collectBtn = [YLCFactory createBtnWithImage:@"collect_normal" title:@"收藏" titleColor:RGB(155, 155, 155) frame:CGRectMake(0, 0, 50, 50)];
    
    [_collectBtn setImage:[UIImage imageNamed:@"collect_selected"] forState:UIControlStateSelected];
    
    [_collectBtn setImage:[UIImage imageNamed:@"collect_selected"] forState:UIControlStateHighlighted];
    
    [self addSubview:_collectBtn];
    
    _inputTextView = [[UITextField alloc] init];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    
    _inputTextView.leftView = leftView;
    
    _inputTextView.leftViewMode = UITextFieldViewModeAlways;
    
    _inputTextView.placeholder = @"发表回帖...";
    
    _inputTextView.backgroundColor = RGB(234,237,241);
    
    _inputTextView.layer.cornerRadius = 20;
    
    _inputTextView.layer.masksToBounds = YES;
    
    _inputTextView.textColor = RGBA(148,153,156,0.6);
    
    _inputTextView.font = [UIFont systemFontOfSize:14];
    
    [self addSubview:_inputTextView];
    
    _talkBtn = [YLCFactory createCommondBtnWithTitle:@"聊一聊" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    _talkBtn.layer.cornerRadius = 0;
    
    [self addSubview:_talkBtn];
    
    _likeBtn = [YLCFactory createBtnWithImage:@"share_like_normal"];
    
    [_likeBtn setImage:[UIImage imageNamed:@"share_like"] forState:UIControlStateSelected];
    
    [self addSubview:_likeBtn];
    
    _shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
    
    [self addSubview:_shareBtn];

}
- (void)configUI{
    if (_isVideo) {
        _talkBtn.hidden = YES;
        _collectBtn.hidden = YES;
        _likeBtn.hidden = NO;
        _shareBtn.hidden = NO;
    }else{
        _talkBtn.hidden = NO;
        _collectBtn.hidden = NO;
        _likeBtn.hidden = YES;
        _shareBtn.hidden = YES;
    }
}
- (void)remakeFrame{
    WS(weakSelf)

    if (_type==0||_type==5) {
        _talkBtn.hidden = YES;
        _collectBtn.hidden = NO;
        _likeBtn.hidden = YES;
        _shareBtn.hidden = YES;
        [_collectBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(22);
            make.top.equalTo(weakSelf.mas_top).offset(10);
            make.width.equalTo(50);
            make.height.equalTo(50);
        }];
        
        [_inputTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_collectBtn.mas_right).offset(18);
            make.centerY.equalTo(_collectBtn.mas_centerY);
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.height.equalTo(36);
        }];
    }
    
    if (_type==3) {
        
        _talkBtn.hidden = YES;
        _collectBtn.hidden = YES;
        _likeBtn.hidden = NO;
        _shareBtn.hidden = YES;
        
        [_likeBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.mas_right).offset(-14);
            make.top.equalTo(weakSelf.mas_top).offset(15);
            make.width.height.equalTo(40);
        }];
        
        [_inputTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(13);
            make.centerY.equalTo(_likeBtn.mas_centerY);
            make.right.equalTo(_likeBtn.mas_left).offset(-23);
            make.height.equalTo(36);
        }];
    }
    
    if (_type==4) {
        _talkBtn.hidden = NO;
        _collectBtn.hidden = NO;
        _likeBtn.hidden = YES;
        _shareBtn.hidden = YES;
        
        [_talkBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.mas_right);
            make.top.equalTo(weakSelf.mas_top);
            make.width.equalTo(100);
            make.height.equalTo(50);
        }];
        
        [_collectBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_talkBtn.mas_left).offset(-21);
            make.top.equalTo(weakSelf.mas_top).offset(10);
            make.width.equalTo(50);
            make.height.equalTo(50);
        }];
        
        [_inputTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(13);
            make.centerY.equalTo(_collectBtn.mas_centerY);
            make.right.equalTo(_collectBtn.mas_left).offset(-18);
            make.height.equalTo(36);
        }];
    }
}
- (void)layoutFrame{
    
    WS(weakSelf)
    
    [_inputTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.right.equalTo(weakSelf.mas_right).offset(-170);
        make.height.equalTo(40);
    }];
    
    [_collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_inputTextView.mas_right).offset(12);
        make.top.equalTo(_inputTextView.mas_top);
        make.width.equalTo(50);
        make.height.equalTo(50);
    }];
    
    [_talkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(100);
        make.height.equalTo(50);
    }];
    
    [_likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_inputTextView.mas_right).offset(12);
        make.top.equalTo(_inputTextView.mas_top);
        make.width.equalTo(40);
        make.height.equalTo(40);
    }];
    
    [_shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_likeBtn.mas_right).offset(10);
        make.centerY.equalTo(_likeBtn.mas_centerY);
        make.width.height.equalTo(40);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
