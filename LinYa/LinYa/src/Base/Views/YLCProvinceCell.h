//
//  YLCProvinceCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCProvinceCell : UITableViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,assign)BOOL isSelect;
@end
