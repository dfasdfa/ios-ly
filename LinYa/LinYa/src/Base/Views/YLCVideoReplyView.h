//
//  YLCVideoReplyView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCVideoReplyView : UIView
@property (nonatomic ,strong)UITextField *inputTextView;
@property (nonatomic ,strong)UIButton *likeBtn;
@property (nonatomic ,strong)UIButton *shareBtn;
@end
