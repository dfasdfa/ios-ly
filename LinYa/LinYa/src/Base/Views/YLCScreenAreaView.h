//
//  YLCScreenAreaView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCScreenAreaView : UIView
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,strong)RACSubject *dissSignal;
- (void)show;
- (void)dissMiss;
@end
