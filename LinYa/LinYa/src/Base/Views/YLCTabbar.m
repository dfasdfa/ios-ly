//
//  ZLTabbar.m
//  ZLTabbarController
//
//  Created by YYKit on 2017/8/7.
//  Copyright © 2017年 kzkj. All rights reserved.
//

#import "YLCTabbar.h"
#import "RYMessageManager.h"
#import <RongIMLib/RongIMLib.h>
@interface YLCTabbar ()

@property (nonatomic,strong) UIButton *irregularItem;//不规则的按钮，可以放在在中间或者其它任意一个地方
@property (nonatomic,strong)UILabel *redCornerLabel; //红点
@end

@implementation YLCTabbar
- (void)hiddenRed{
    [self getUnreadCount];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [[RYMessageManager shareManager].messageSignal subscribeNext:^(id x) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self getUnreadCount];
            });
            
        }];
        [self setBarTintColor:[UIColor whiteColor]];
        //TODO:加载子视图
        [self createChildrenViews];
    }
    return self;
}
- (void)getUnreadCount{
    
    int count = [[RCIMClient sharedRCIMClient] getUnreadCount:@[@(ConversationType_PRIVATE)]];
    if (count<=0) {
        self.redCornerLabel.hidden = YES;
    }else{
        self.redCornerLabel.hidden = NO;
    }
    if (count>99) {
        count=99;
    }
    self.redCornerLabel.text = [NSString stringWithFormat:@"%d",count];
}

#pragma mark 创建子视图
- (void)createChildrenViews
{
    self.backgroundColor = [UIColor whiteColor];

    //TODO:去掉tabbar的分割线
//    [self setBackgroundImage:[UIImage new]];
//    [self setShadowImage:[UIImage new]];
    self.redCornerLabel = [YLCFactory createLabelWithFont:12];
    self.redCornerLabel.backgroundColor = [UIColor redColor];
    self.redCornerLabel.textColor = [UIColor whiteColor];
    self.redCornerLabel.textAlignment = NSTextAlignmentCenter;
    self.redCornerLabel.layer.cornerRadius = 10;
    self.redCornerLabel.layer.masksToBounds = YES;
    self.redCornerLabel.hidden = YES;
    [self addSubview:self.redCornerLabel];
    
    self.irregularItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.irregularItem setBackgroundImage:[UIImage imageNamed:@"tabbar_normal3"] forState:UIControlStateNormal];
    [self.irregularItem setBackgroundImage:[UIImage imageNamed:@"tabbar_normal3"] forState:UIControlStateHighlighted];
    [self.irregularItem setContentMode:UIViewContentModeCenter];
    [self.irregularItem addTarget:self action:@selector(irregularItemSelcted) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.irregularItem];
    [self getUnreadCount];
}

#pragma mark 执行不规则按钮的点击事件
- (void)irregularItemSelcted
{
    if (self.selectedIrrBtn)
    {
        self.selectedIrrBtn();
    }
}

- (void)didselectedIrrBtnWithBlock:(IrrBtnSelectedBlock)selectedIrrBtn
{
    self.selectedIrrBtn = selectedIrrBtn;
}


#pragma mark layoutSubviews
- (void)layoutSubviews
{
    [super layoutSubviews];

    //TODO:重新排布系统按钮的位置，空出需要自定义按钮的位置，系统按钮的类型是UITabBarButton
    Class class = NSClassFromString(@"UITabBarButton");
    
    CGFloat irreWidth = 60;
    CGFloat btnWidth = 49;
    CGFloat space = (self.frameWidth-4*49-irreWidth)/6;
    
    //TODO:这里设置自定义tabbarItem的位置
    self.irregularItem.frame = CGRectMake(btnWidth*2+3*space, -11, irreWidth, irreWidth);
    self.redCornerLabel.frame = CGRectMake(btnWidth*4+4*space-10, 2, 20, 20);

    NSInteger btnIndex = 0;
    for (UIView *btn in self.subviews)
    {
        if ([btn isKindOfClass:class])
        {
            //TODO:如果是系统的UITabBarButton，调整子控件位置，空出自定义UITabBarButton的位置
            //TODO:按钮宽度为Tabbar宽度平分4块
            
            if (btnIndex < 2)
            {
                //TODO: -3- 在这里为irregularItem的索引值
                btn.frame = CGRectMake(btnWidth*btnIndex+space*(btnIndex+1), 0, btnWidth, btnWidth);
            }
            else if(btnIndex==2){
                btn.frame = CGRectMake(-200, 0, 0, 0);
            }
            else{
                btn.frame = CGRectMake(btnWidth*(btnIndex-1)+space*(btnIndex+1)+irreWidth, 0, btnWidth, btnWidth);
            }
            btnIndex ++;

            if (btnIndex == 0)
            {
                btnIndex ++;
            }
        }
    }
    [self bringSubviewToFront:self.irregularItem];
    [self bringSubviewToFront:self.redCornerLabel];
}

#pragma mark 重写hitTest方法，去监听irregularItem的点击，目的是为了让突出在外面的部分 在点击时也有反应
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    /**判断当前手指是否点击到了irregularItem。如果是，则相应按钮点击；如果是其它，则由系统处理**/
    //当前view是否被隐藏？如果隐藏，就不做其他处理了。
    if (self.isHidden == NO)
    {
        //TODO:将当前tabbar的触摸点转换成坐标系，转换到irregularItem的身上，生成一个新的点
        CGPoint new_point = [self convertPoint:point toView:self.irregularItem];

        //TODO:如果这个新的点是在irregularItem上，那么处理点点击事件最合适的view就是irregularItem
        if ([self.irregularItem pointInside:new_point withEvent:event])
        {
            return self.irregularItem;
        }
    }
    return [super hitTest:point withEvent:event];
}



@end
