//
//  YLCRefrshTableView.h
//  LinYa
//
//  Created by 初程程 on 2018/5/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCRefrshTableView : UIView
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic ,strong)NSArray *dataArr;
@property (nonatomic ,strong)RACSubject *reloadSignal;
@property (nonatomic ,assign)BOOL isOnlyHead;
- (UIScrollView *)customScrollView;
- (void)beganRefresh;
- (void)resetNoMore;
- (void)loadedAllData;
@end
