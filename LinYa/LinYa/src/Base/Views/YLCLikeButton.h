//
//  YLCLikeButton.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCLikeButton : UIView
@property (nonatomic ,copy)NSString *normalImage;
@property (nonatomic ,copy)NSString *selectImage;
@property (nonatomic ,strong)UIColor *normalColor;
@property (nonatomic ,strong)UIColor *selectColor;
@property (nonatomic ,assign)BOOL withCount;
@property (nonatomic ,copy)NSString *count;
@property (nonatomic ,assign,readonly)BOOL liked;
@property (nonatomic ,strong)RACSubject *likeSignal;
@property (nonatomic ,assign)BOOL noTouch;
- (instancetype)initWithFrame:(CGRect)frame
                  normalImage:(NSString *)normalImage
                  selectImage:(NSString *)selectImage
                  normalColor:(UIColor *)normalColor
                selectedColor:(UIColor *)selectedColor;
- (void)configureLikeStatus:(BOOL)isLike count:(NSInteger)count animated:(BOOL)animated;
@end
