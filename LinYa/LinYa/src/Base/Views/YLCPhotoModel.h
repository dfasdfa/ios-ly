//
//  YLCPhotoModel.h
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCPhotoModel : NSObject
@property (nonatomic ,assign)BOOL isWeb;
@property (nonatomic ,copy)NSString *imagePath;
@property (nonatomic ,copy)NSString *imageBase64;
@property (nonatomic ,copy)NSString *placeholder;
@end
