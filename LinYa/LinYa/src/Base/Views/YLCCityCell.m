//
//  YLCCityCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCityCell.h"

@implementation YLCCityCell
{
    UIView *backView;
    UILabel *titleLabel;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backView = [[UIView alloc] init];
    
    backView.backgroundColor = YLC_THEME_COLOR;
    
    [self.contentView addSubview:backView];
    
    titleLabel = [YLCFactory createLabelWithFont:14];

    titleLabel.backgroundColor = [UIColor clearColor];

    [self.contentView addSubview:titleLabel];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        backView.backgroundColor = YLC_THEME_COLOR;
        titleLabel.textColor = [UIColor whiteColor];
    }else{
        backView.backgroundColor = [UIColor whiteColor];
        titleLabel.textColor = RGB(51, 51, 51);
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(100);
        make.height.equalTo(30);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.centerX.equalTo(weakSelf.mas_centerX);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
