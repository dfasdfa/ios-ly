//
//  YLCStarView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCStarView : UIView
@property (nonatomic ,copy)NSString *yelloName;
@property (nonatomic ,copy)NSString *grayName;
@property (nonatomic ,assign)CGFloat score;
@property (nonatomic ,assign)BOOL canTouch;
@property (nonatomic ,strong)RACSubject *starSignal;
@end
