//
//  YLCPickModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCPickModel : NSObject
@property (nonatomic ,assign)NSInteger selectIndex;
@property (nonatomic ,strong)NSArray *rowArr;
@end
