//
//  YLCScreenModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YLCPickModel.h"
@interface YLCScreenModel : NSObject
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,strong)NSArray *screenList;
@property (nonatomic ,assign)NSInteger selectIndex;
@property (nonatomic ,copy)NSString *value;
@property (nonatomic ,assign)NSInteger showType; //0是正常列表 1是地区列表
@property (nonatomic ,assign)BOOL isChanged;
@end
