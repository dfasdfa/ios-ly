//
//  YLCHUDShow.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCHUDShow : NSObject
+ (void)showOnlyOneSelectAlertWithTarget:(id)target
                                 Message:(NSString *)message
                               leftTitle:(NSString *)leftTitle
                                selector:(void (^)(NSInteger index))selector;
+ (void)showSignInHUDWithIntegral:(NSString *)integral;
+ (void)showBuyHUD:(RACSubject *)signal;
+ (void)showBuySuccessHUDWithTip:(NSString *)tip;
+ (void)showAlertViewWithMessage:(NSString *)message
                           title:(NSString *)title;
+ (void)showPickerListWithList:(NSArray *)list
                        signal:(RACSubject *)signal;
+ (void)showTimePickWithSignal:(RACSubject *)signal;
+ (void)showCustomMessageHUDWithTitleList:(NSArray *)titleList
                                   signal:(RACSubject *)signal
                                   isMore:(BOOL)isMore;
+ (void)showSelectListMessageHUDWithTitleList:(NSArray *)titleList
                                       signal:(RACSubject *)signal
                                       isMore:(BOOL)isMore
                                        title:(NSString *)title
                                         type:(NSInteger)type;
+ (void)showAreaPickWithSignal:(RACSubject *)signal;
+ (void)showPriceWithSignal:(RACSubject *)signal;
+ (void)showScreenTrainWithTitleList:(NSArray *)titleList
                          selectList:(NSArray *)selectList
                              signal:(RACSubject *)signal;
+ (void)showOneLineWithTitleList:(NSArray *)titleList
                      selectList:(NSArray *)selectList
                          signal:(RACSubject *)signal;
+ (void)showResumeHUDWithSignal:(RACSubject *)signal;
+ (void)showSelectAlertWithTarget:(id)target
                          Message:(NSString *)message
                        leftTitle:(NSString *)leftTitle
                       rightTitle:(NSString *)rightTitle
                         selector:(void (^)(NSInteger index))selector;
+ (void)showFieldAlertControllerWithTarget:(id)target
                                     title:(NSString *)title
                                   message:(NSString *)message
                               placeholder:(NSString *)placeholder
                                 leftTitle:(NSString *)leftTitle
                                rightTitle:(NSString *)rightTitle
                               confirmLeft:(BOOL)isLeft
                                    signal:(void(^)(NSString *fieldText))signal;
+ (void)showShareHUDWithContent:(NSString *)content
                      shareLink:(NSString *)shareLink;
@end
