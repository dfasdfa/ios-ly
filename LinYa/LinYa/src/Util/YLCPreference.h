//
//  YLCPreference.h
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCPreference : NSObject
//global
+ (void)setGlobalKey:(NSString *)key
               value:(NSString*)value
           syncWrite:(BOOL)syncWrite;
+ (NSString *)getGlobalKey:(NSString *)key;

+ (void)setGlobalArrayKey:(NSString *)key
                    value:(NSArray *)value
                syncWrite:(BOOL)syncWrite;

+ (NSArray *)getGlobalArrayKey:(NSString *)key;

+ (void)setGlobalDictKey:(NSString*)key
                   value:(NSDictionary*)value
               syncWrite:(BOOL)syncWrite;

+ (NSDictionary*)getGlobalDictKey:(NSString *)key;

+ (void)removeGlobalKey:(NSString*)key
              syncWrite:(BOOL)syncWrite;

+ (BOOL)getGlobalBoolKey:(NSString *)key;

+ (void)setGlobalBoolKey:(NSString *)key
                   value:(BOOL)value
               syncWrite:(BOOL)syncWrite;

@end
