//
//  YLCTool.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCTool : NSObject
+ (NSInteger)getTimeStampWithTimeString:(NSString *)timeString
                                 format:(NSString *)format;
+ (NSString *)getTimeStringWithStamp:(NSInteger)stamp
                             format:(NSString *)format;
+ (NSString *)changeFormatWithTimeString:(NSString *)timeString
                               oldFormat:(NSString *)oldFormat
                               newFormat:(NSString *)newFormat;
+ (NSString *)normalChangedWithTimeString:(NSString *)timeString;
@end
