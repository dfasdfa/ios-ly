//
//  JKLocationManager.m
//  SHBao
//
//  Created by 初程程 on 16/11/25.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import "JKLocationManager.h"

@implementation JKLocationManager
{
    CLLocationManager *_locationManager;
    BOOL isWarning;
}
+ (JKLocationManager *)sharedInstance{
    static dispatch_once_t  onceToken;
    static JKLocationManager * sSharedInstance;
    
    dispatch_once(&onceToken, ^{
        sSharedInstance = [[JKLocationManager alloc] init];
    });
    return sSharedInstance;
}
- (void)setWarning{
    isWarning = YES;
}
- (BOOL)getWarning{
    return isWarning;
}
-(CLLocationManager *)locManager{
    if (!_locManager) {
        _locManager = [[CLLocationManager alloc] init];
//        if (IOS_7) {
//            [_locManager startUpdatingLocation];
//        }else{
//            
//            [_locManager requestAlwaysAuthorization];
//        }
        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locManager.distanceFilter = kCLDistanceFilterNone;
    }
    return _locManager;
}
-(CLGeocoder *)geocoder{
    if (!_geocoder) {
        _geocoder = [[CLGeocoder alloc]init];
    }
    return _geocoder;
}
-(void)startLocationWith:(CLLocationAccuracy)location_accuracy{
    
    self.locManager.delegate = self;
//    if (IOS_8) {
//        
//        [self.locManager requestWhenInUseAuthorization];
//        
//    }
    if ([self.locManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locManager requestWhenInUseAuthorization];
    }
    [self.locManager setDesiredAccuracy:location_accuracy];
    
    
}
- (void)stopLocation{
    [self.locManager stopUpdatingLocation];
    self.locManager.delegate = nil;
//    self.locManager = nil;
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"开始定位了【%@】",locations.lastObject);
    CLLocation *lastLocation = locations.lastObject;
    _locationMessage(lastLocation);
//    [self stopLocation];
    
}
- (void)getCurrentLocationMessageWithIsFirst:(BOOL)isFirst completion:(void(^)(CLLocation *currentLocation))currentBlock{
    _locationMessage = currentBlock;
    [self.locManager startUpdatingLocation];
    [self valieLocationWithFirst:isFirst];
}
- (void)valieLocationWithFirst:(BOOL)isFirst{
    
    if (([CLLocationManager locationServicesEnabled]==YES&&[CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways)||[CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse) {
        
    }
    else{
        if (!isFirst) {
           
            [SVProgressHUD showErrorWithStatus:@"您尚未开启定位权限，请前去设置-隐私中开启定位权限"];
        }
        
        CLLocation *zeroLocation = [[CLLocation alloc] initWithLatitude:0.00000 longitude:0.00000];
        
        _locationMessage(zeroLocation);
        
        
    }
    

}
- (BOOL)valieLocationRight{
    if (([CLLocationManager locationServicesEnabled]==YES&&[CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways)||[CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse){
        return YES;
    }
    return NO;
}
@end
