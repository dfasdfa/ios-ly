//
//  JKLocationManager.h
//  SHBao
//
//  Created by 初程程 on 16/11/25.
//  Copyright © 2016年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface JKLocationManager : NSObject<CLLocationManagerDelegate>
@property (nonatomic,strong) CLLocationManager *locManager;//获取用户位置
@property(nonatomic,strong) CLGeocoder *geocoder;//反地理编码
@property (nonatomic ,copy)void (^locationMessage)(CLLocation *location);
+ (JKLocationManager *)sharedInstance;
- (void)setWarning;
- (BOOL)getWarning;
- (void)startLocationWith:(CLLocationAccuracy)location_accuracy;
- (void)stopLocation;
- (void)getCurrentLocationMessageWithIsFirst:(BOOL)isFirst completion:(void(^)(CLLocation *currentLocation))currentBlock;
- (BOOL)valieLocationRight;
@end
