//
//  RYMessageManager.h
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RYMessageManager : NSObject
@property (nonatomic ,strong)RACSubject *messageSignal;
@property (nonatomic ,strong)RACSubject *loginSuccessSignal;
+ (RYMessageManager *)shareManager;
- (void)loginConnectWithToken:(NSString *)token;
- (void)rongyunLogin;
- (void)observerMessage;
@end
