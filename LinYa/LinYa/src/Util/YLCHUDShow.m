//
//  YLCHUDShow.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHUDShow.h"
#import "YLCSignInHUDView.h"
#import "YLCEasyBuyHUDView.h"
#import "YLCBuySuccessHUDView.h"
#import "YLCPickShowView.h"
#import "YLCTimePickShowView.h"
#import "YLCCustomMessageHUDView.h"
#import "YLCAreaPickerView.h"
#import "YLCShowPriceHUDView.h"
#import "YLCScreenTrainHUDView.h"
#import "YLCResumeEmptyHUDView.h"
#import "YLCNavigationViewController.h"
#import "YLCTabBarViewController.h"
#import "AppDelegate.h"
#import <ReactiveCocoa/RACReturnSignal.h>
#import "YLCShareHUDView.h"
@implementation YLCHUDShow
+ (void)showFieldAlertControllerWithTarget:(id)target
                                     title:(NSString *)title
                                   message:(NSString *)message
                               placeholder:(NSString *)placeholder
                                 leftTitle:(NSString *)leftTitle
                                rightTitle:(NSString *)rightTitle
                               confirmLeft:(BOOL)isLeft
                                    signal:(void(^)(NSString *fieldText))signal
{
    __block NSString *fieldText  = @"";
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:leftTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (isLeft) {
            signal(fieldText);
        }
    }];
    
    [alert addAction:action];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:rightTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (!isLeft) {
            signal(fieldText);
        }
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = placeholder;
        textField.keyboardType = UIKeyboardTypeNumberPad;
        RACSignal *fieldSignal  =  [textField.rac_textSignal bind:^RACStreamBindBlock{
            
            return ^RACStream *(id value, BOOL *stop){
                //信号返回输出
                
                return [RACReturnSignal return:value];
            };
        }];
        [fieldSignal subscribeNext:^(id x) {
            
            fieldText = x;
        }];
        
    }];
    
    [alert addAction:action2];
    
    [target presentViewController:alert animated:YES completion:nil];
}
+ (void)showOnlyOneSelectAlertWithTarget:(id)target
                                 Message:(NSString *)message
                               leftTitle:(NSString *)leftTitle
                                selector:(void (^)(NSInteger index))selector{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:leftTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        selector(0);
    }];
    
    [alert addAction:action];
    
    if ([target isKindOfClass:[UIViewController class]]) {
        [target presentViewController:alert animated:YES completion:^{
            
        }];
    }else{
        
        YLCTabBarViewController *tabbar = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
        
        YLCNavigationViewController *nav = tabbar.selectedViewController;
        
        
        [nav presentViewController:alert animated:YES completion:^{
            
        }];
    }
}
+ (void)showSelectAlertWithTarget:(id)target
                          Message:(NSString *)message
                        leftTitle:(NSString *)leftTitle
                       rightTitle:(NSString *)rightTitle
                         selector:(void (^)(NSInteger index))selector{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:leftTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        selector(0);
    }];
    
    [alert addAction:action];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:rightTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        selector(1);
    }];
    
    [alert addAction:action2];
    
    if ([target isKindOfClass:[UIViewController class]]) {
        [target presentViewController:alert animated:YES completion:^{
            
        }];
    }else{
        
        YLCTabBarViewController *tabbar = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
        
        YLCNavigationViewController *nav = tabbar.selectedViewController;
        
        
        [nav presentViewController:alert animated:YES completion:^{
            
        }];
    }
}
+ (void)showAlertViewWithMessage:(NSString *)message
                           title:(NSString *)title{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
    
    [alertView show];
}
+ (void)showSignInHUDWithIntegral:(NSString *)integral{
    YLCSignInHUDView *hudView = [[YLCSignInHUDView alloc] initWithFrame:CGRectMake(kScreenWidth/2-130, -80, 260, 120) integral:integral];
    
    [hudView show];
}
+ (void)showBuyHUD:(RACSubject *)signal{
    YLCEasyBuyHUDView *hudView = [[YLCEasyBuyHUDView alloc] initWithFrame:CGRectMake(kScreenWidth/2-130, -160, 260, 360)];
    
    [hudView.buttonSignal subscribeNext:^(id x) {
        [signal sendNext:x];
    }];
    
    [hudView show];
}
+ (void)showBuySuccessHUDWithTip:(NSString *)tip{
    YLCBuySuccessHUDView *hudView = [[YLCBuySuccessHUDView alloc] initWithFrame:CGRectMake(kScreenWidth/2-130, -160, 260, 237)];
    hudView.tipString = tip;
    [hudView show];
}
+ (void)showResumeHUDWithSignal:(RACSubject *)signal{
    YLCResumeEmptyHUDView *hudView = [[YLCResumeEmptyHUDView alloc] initWithFrame:CGRectMake(kScreenWidth/2-130, -160, 260, 237)];
    
    hudView.subject = signal;
    
    [hudView show];
}
+ (void)showPickerListWithList:(NSArray *)list
                        signal:(RACSubject *)signal
{
    
    YLCPickShowView *showView = [[YLCPickShowView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260)];
    
    showView.rowArr = list;
    
    showView.selectSignal = signal;
    
    [showView show];
    
}
+ (void)showTimePickWithSignal:(RACSubject *)signal{
    YLCTimePickShowView *showView = [[YLCTimePickShowView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260)];
    
    showView.confirmSignal = signal;
    
    [showView show];
}
+ (void)showCustomMessageHUDWithTitleList:(NSArray *)titleList
                                   signal:(RACSubject *)signal
                                   isMore:(BOOL)isMore{
    YLCCustomMessageHUDView *hud = [[YLCCustomMessageHUDView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260) titleList:titleList];
    hud.confirmSignal = signal;
    hud.canSelectMore = isMore;
    [hud show];
}
+ (void)showSelectListMessageHUDWithTitleList:(NSArray *)titleList
                                       signal:(RACSubject *)signal
                                       isMore:(BOOL)isMore
                                        title:(NSString *)title
                                         type:(NSInteger)type{
    YLCCustomMessageHUDView *hud = [[YLCCustomMessageHUDView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260) titleList:titleList];
    hud.confirmSignal = signal;
    hud.canSelectMore = isMore;
    hud.type = type;
    hud.title = title;
    [hud show];
}

+ (void)showAreaPickWithSignal:(RACSubject *)signal{
    YLCAreaPickerView *picker = [[YLCAreaPickerView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 260)];
    picker.confirmSignal = signal;
    [picker show];
}
+ (void)showPriceWithSignal:(RACSubject *)signal{
    YLCShowPriceHUDView *view = [[YLCShowPriceHUDView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 160)];
    view.confirmSignal = signal;
    [view show];
}
+ (void)showScreenTrainWithTitleList:(NSArray *)titleList
                          selectList:(NSArray *)selectList
                              signal:(RACSubject *)signal
{
    YLCScreenTrainHUDView *hudView = [[YLCScreenTrainHUDView alloc] initWithFrame:CGRectMake(kScreenWidth, 0, 250, kScreenHeight)];
    
    hudView.titleList = titleList;
    
    hudView.selectList = selectList;
    
    hudView.confirmSignal = signal;
    
    [hudView show];
}

+ (void)showOneLineWithTitleList:(NSArray *)titleList
                      selectList:(NSArray *)selectList
                          signal:(RACSubject *)signal
{
    YLCScreenTrainHUDView *hudView = [[YLCScreenTrainHUDView alloc] initWithFrame:CGRectMake(kScreenWidth, 0, 250, kScreenHeight)];
    
    hudView.titleList = titleList;
    
    hudView.selectList = selectList;
    
    hudView.confirmSignal = signal;
    
    hudView.type = 1;
    
    [hudView show];
}
+ (void)showShareHUDWithContent:(NSString *)content
                      shareLink:(NSString *)shareLink{
    YLCShareHUDView *picker = [[YLCShareHUDView alloc] initWithFrame:CGRectMake(0, kScreenHeight, kScreenWidth, 160)];
    
    picker.shareContent = content;
    
    picker.shareLink = shareLink;
    
    [picker show];
}
@end
