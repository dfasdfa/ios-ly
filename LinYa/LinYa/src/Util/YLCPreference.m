//
//  YLCPreference.m
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPreference.h"

@implementation YLCPreference
//global
+ (void)setGlobalKey:(NSString *)key
               value:(NSString*)value
           syncWrite:(BOOL)syncWrite
{
    
    if (JK_IS_STR_NIL(key)) {
        return;
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:value forKey:key];
    
    if (syncWrite)
    {
        [userDefault synchronize];
    }
}

+ (NSString*)getGlobalKey:(NSString *)key
{
    
    if (JK_IS_STR_NIL(key)) {
        return nil;
    }
    NSString *ret = [[NSUserDefaults standardUserDefaults] stringForKey:key];
    return ret;
}
+ (void)setGlobalArrayKey:(NSString *)key
                    value:(NSArray *)value
                syncWrite:(BOOL)syncWrite{
    if (JK_IS_STR_NIL(key)) {
        return;
    }
    
    NSUserDefaults *userDefalut = [NSUserDefaults standardUserDefaults];
    [userDefalut setObject:value forKey:key];
    
    if (syncWrite) {
        [userDefalut synchronize];
    }
}

+ (NSArray *)getGlobalArrayKey:(NSString *)key{
    if (JK_IS_STR_NIL(key)) {
        return nil;
    }
    NSArray *ret = [[NSUserDefaults standardUserDefaults] arrayForKey:key];
    
    return ret;
}

+ (void)setGlobalDictKey:(NSString*)key
                   value:(NSDictionary*)value
               syncWrite:(BOOL)syncWrite{
    if (JK_IS_STR_NIL(key)) {
        return;
    }
    NSUserDefaults *userDefalut = [NSUserDefaults standardUserDefaults];
    
    [userDefalut setObject:value forKey:key];
    
    if (syncWrite) {
        [userDefalut synchronize];
    }
}

+ (NSDictionary*)getGlobalDictKey:(NSString *)key{
    if (JK_IS_STR_NIL(key)) {
        return nil;
    }
    
    NSDictionary *ret = [[NSUserDefaults standardUserDefaults] dictionaryForKey:key];
    
    return ret;
}

+ (void)removeGlobalKey:(NSString*)key
              syncWrite:(BOOL)syncWrite{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setGlobalBoolKey:(NSString *)key
                   value:(BOOL)value
               syncWrite:(BOOL)syncWrite{
    if (JK_IS_STR_NIL(key)) {
        return;
    }
    NSUserDefaults *userDefalut = [NSUserDefaults standardUserDefaults];
    
    [userDefalut setBool:value forKey:key];
    
    if (syncWrite) {
        [userDefalut synchronize];
    }
}
+ (BOOL)getGlobalBoolKey:(NSString *)key{
    if (JK_IS_STR_NIL(key)) {
        return NO;
    }
    BOOL ret = [[NSUserDefaults standardUserDefaults] boolForKey:key];
    
    return ret;
}

@end
