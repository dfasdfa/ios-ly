//
//  YLCWxPayManager.h
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
@interface YLCWxPayManager : NSObject<WXApiDelegate>
@property (nonatomic ,strong)RACSubject *paySignal;
@property (nonatomic ,strong)RACSubject *loginSignal;
+ (YLCWxPayManager *)shareManager;
- (void)wxPayWithParam:(id)param;
- (void)wxLogin;
- (void)shareWithContent:(NSString *)content
                   title:(NSString *)title
               shareLink:(NSString *)shareLink
                   scene:(int)scene;
@end
