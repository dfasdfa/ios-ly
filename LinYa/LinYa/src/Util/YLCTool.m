//
//  YLCTool.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTool.h"
#import "YLCNavigationViewController.h"

@implementation YLCTool
+ (NSInteger)getTimeStampWithTimeString:(NSString *)timeString
                                 format:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:format];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate* date = [formatter dateFromString:timeString];
    
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    
    return timeSp;
}
+ (NSString *)getTimeStringWithStamp:(NSInteger)stamp
                             format:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:format];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:stamp];
    
    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    
    return confromTimespStr;
}
+ (NSString *)changeFormatWithTimeString:(NSString *)timeString
                               oldFormat:(NSString *)oldFormat
                               newFormat:(NSString *)newFormat{
    NSInteger timeStamp = [self getTimeStampWithTimeString:timeString format:oldFormat];
    
    return [self getTimeStringWithStamp:timeStamp format:newFormat];
}
+ (NSString *)normalChangedWithTimeString:(NSString *)timeString{
    return [self changeFormatWithTimeString:timeString oldFormat:@"YYYY-MM-dd HH:mm:ss" newFormat:@"YYYY-MM-dd"];
}
@end
