//
//  YLCWxPayManager.m
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWxPayManager.h"

@interface YLCWxPayManager()
@end
@implementation YLCWxPayManager

+ (YLCWxPayManager *)shareManager{
    static dispatch_once_t  onceToken;
    
    static YLCWxPayManager * sSharedInstance;
    
    dispatch_once(&onceToken, ^{
        
        sSharedInstance = [[YLCWxPayManager alloc] init];
        
    });
    
    return sSharedInstance;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}
- (void)commonInit{
    self.loginSignal = [RACSubject subject];
    
    self.paySignal = [RACSubject subject];
}
- (void)wxPayWithParam:(id)param{
    PayReq *request = [[PayReq alloc] init];
    
    request.partnerId = param[@"partnerid"];
    
    request.prepayId = param[@"prepayid"];
    
    request.package = param[@"package"];
    
    request.nonceStr = param[@"noncestr"];
    
    request.timeStamp = [param[@"timestamp"] intValue];
    
    request.sign = param[@"sign"];
    
    [WXApi sendReq:request];
}
- (void)wxLogin{
    SendAuthReq *req = [[SendAuthReq alloc] init];
    
    req.openID = WXAppid;
    
    req.scope = @"snsapi_userinfo";
    
    req.state = @"com.weijue.linya";
    
    [WXApi sendReq:req];
}
/*
 scene
    WXSceneTimeline(朋友圈)
    WXSceneSession(聊天)
 
 **/
- (void)shareWithContent:(NSString *)content
                   title:(NSString *)title
               shareLink:(NSString *)shareLink
                   scene:(int)scene{
    
    SendMessageToWXReq *rep = [[SendMessageToWXReq alloc] init];

    WXMediaMessage *message = [WXMediaMessage message];
    
    message.title = content;
    
    message.messageExt = content;
    
    WXWebpageObject *webpageObject = [WXWebpageObject object];

    webpageObject.webpageUrl = shareLink;
    
    message.mediaObject = webpageObject;
    
    rep.bText = NO;
    
    rep.message = message;
    
    rep.scene = scene;
    
    [WXApi sendReq:rep];
}
- (NSData *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return UIImageJPEGRepresentation(newImage, 0.8);
}
- (void)onReq:(BaseReq *)req{
    NSLog(@"%@",req);
}
- (void)onResp:(BaseResp *)resp{
    NSLog(@"%@",resp);
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        [self fetchWXLoginWith:(SendAuthResp *)resp];
    }
    if ([resp isKindOfClass:[PayResp class]]) {
        [self fetchWXPayWith:(PayResp *)resp];
    }
    
    
}
- (void)fetchWXPayWith:(PayResp *)resp{
    switch (resp.errCode) {
        case WXSuccess:
            [self.paySignal sendNext:@""];
            [SVProgressHUD showSuccessWithStatus:@"支付成功"];
            break;
        default:
            [SVProgressHUD showErrorWithStatus:@"支付失败"];
            break;
    }
}
- (void)fetchWXLoginWith:(SendAuthResp *)resp{
    if (resp.errCode==0) {
        [self.loginSignal sendNext:resp];
    }else if (resp.errCode==-4){
        [SVProgressHUD showErrorWithStatus:@"用户拒绝授权"];
    }else if (resp.errCode==-2){
        [SVProgressHUD showErrorWithStatus:@"用户取消"];
    }else{
        [SVProgressHUD showErrorWithStatus:resp.errStr];
    }
}
@end
