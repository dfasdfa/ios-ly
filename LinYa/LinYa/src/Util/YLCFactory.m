//
//  YLCFactory.m
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFactory.h"
#import "YLCNavigationViewController.h"
#import "YLCTabBarViewController.h"
#import "AppDelegate.h"
@implementation YLCFactory
+ (UILabel *)normalLabel{
    return [self createLabelWithFont:14];
}
+ (UILabel *)createLabelWithFont:(CGFloat)font{
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:font];
    return label;
}
+ (UILabel *)createLabelWithFont:(CGFloat)font
                           color:(UIColor *)color{
    UILabel *label = [self createLabelWithFont:font];
    
    label.textColor = color;
    
    return label;
}
+ (UITextField *)createFieldWithPlaceholder:(NSString *)placeholder{
    UITextField *field = [[UITextField alloc] init];
    field.backgroundColor = [UIColor whiteColor];
    field.layer.cornerRadius = 5;
    field.layer.masksToBounds = YES;
    field.placeholder = placeholder;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 40)];
    field.leftView = leftView;
    field.leftViewMode = UITextFieldViewModeAlways;
    return field;
}
+ (UITextField *)createFieldWithPlaceholder:(NSString *)placeholder
                             backgroudColor:(UIColor *)backgroundColor
{
    UITextField *field = [self createFieldWithPlaceholder:placeholder];
    
    field.textAlignment = NSTextAlignmentCenter;
    
    field.backgroundColor = backgroundColor;
    
    field.leftViewMode = UITextFieldViewModeNever;

    return field;
}
+ (UITextField *)createPasswordFieldWithPlaceholder:(NSString *)placeholder
                                     rightImageName:(NSString *)rightImageName
                                            command:(RACCommand *)command{
    UITextField *field = [self createFieldWithPlaceholder:placeholder];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setImage:[UIImage imageNamed:rightImageName] forState:UIControlStateNormal];
    rightBtn.rac_command = command;
    rightBtn.frame = CGRectMake(20, 10, 30, 30);
    field.rightView = rightView;
    return field;
}
+ (UITextField *)createTitleFieldWithPlaceholder:(NSString *)placeholder
                                           title:(NSString *)title{
    UITextField *field = [[UITextField alloc] init];
    
    field.placeholder = placeholder;
    
    CGSize size = [YLCFactory getStringWithSizeWithString:title height:40 font:14];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width+20, 40)];
    
    UILabel *titleLabel = [self createLabelWithFont:15 color:RGB(133, 139, 141)];
    
    titleLabel.text = title;
    
    [leftView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftView.mas_left);
        make.centerY.equalTo(leftView.mas_centerY);
    }];
    
    field.leftView = leftView;
    
    field.leftViewMode = UITextFieldViewModeAlways;
    
    return field;
}
+ (UIButton *)createCommondBtnWithTitle:(NSString *)title
                        backgroundColor:(UIColor *)backgroundColor
                             titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.backgroundColor = backgroundColor;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.layer.cornerRadius = 5;
    
    button.layer.masksToBounds = YES;
    
    return button;
}
+ (UITextField *)createFieldWithLeftImage:(NSString *)imageName
                              placeholder:(NSString *)placeholder{
    UITextField *field = [self createFieldWithPlaceholder:placeholder];
    
    field.layer.cornerRadius = 0;
    
    field.layer.masksToBounds = NO;
    
    UIImageView *leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    
    leftView.contentMode = UIViewContentModeCenter;
    
    leftView.frame = CGRectMake(20, 5, 30, 30);
    
    field.leftView = leftView;
    
    field.leftViewMode = UITextFieldViewModeAlways;
    
    return field;
}
+ (UIButton *)createLabelBtnWithTitle:(NSString *)title
                           titleColor:(UIColor *)titleColor{
    UIButton *titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [titleBtn setTitle:title forState:UIControlStateNormal];
    
    [titleBtn setTitleColor:titleColor forState:UIControlStateNormal];
    
    return titleBtn;
}
+ (UIButton *)circleBtnWithImage:(NSString *)imageName
                    cornerRadius:(CGFloat)cornerRadius{
    UIButton *circleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    circleBtn.layer.cornerRadius = cornerRadius;
    
    circleBtn.layer.masksToBounds = YES;
    
    [circleBtn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];

    return circleBtn;
}
+ (UIButton *)createBtnWithImage:(NSString *)imageName{
    return [self circleBtnWithImage:imageName cornerRadius:0];
}
+ (UIButton *)creatBtnWithCornerRadius:(CGFloat)cornerRadius
                                 title:(NSString *)title
                       backgroundColor:(UIColor *)color
                             textColor:(UIColor *)textColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.cornerRadius = cornerRadius;
    
    button.layer.masksToBounds = YES;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:textColor forState:UIControlStateNormal];
    
    button.backgroundColor = color;
    
    return button;
}
+ (UIButton *)creatCornerAndBorderWithCornerRadius:(CGFloat)cornerRadius
                                             title:(NSString *)title
                                   backgroundColor:(UIColor *)color
                                         textColor:(UIColor *)textColor
                                       borderColor:(UIColor *)borderColor
                                       borderWidth:(CGFloat)borderWidth{
    
    UIButton *button = [self creatBtnWithCornerRadius:cornerRadius title:title backgroundColor:color textColor:textColor];
    
    button.layer.borderWidth = borderWidth;
    
    button.layer.borderColor = borderColor.CGColor;
    
    return button;
}
+ (UIButton*)createBtnWithImage:(NSString *)imageName
                          title:(NSString *)title
                     titleColor:(UIColor *)titleColor
                          frame:(CGRect)frame
{
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton* btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=frame;
    UILabel* label=[[UILabel alloc] initWithFrame:CGRectMake(0,image.size.height+3,frame.size.width,frame.size.height-image.size.height-3)];
    label.text=title;
    label.textAlignment=NSTextAlignmentCenter;
    label.textColor = titleColor;
    label.font=[UIFont systemFontOfSize:12];
    [btn addSubview:label];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:image forState:UIControlStateHighlighted];
    
    btn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, frame.size.height-image.size.height-3, 0);
    return btn;
}
+ (UIBarButtonItem *)cornerCommontBarItemWithTitle:(NSString *)title
                                          selector:(SEL)selector
                                            target:(id)target{
    UIButton *button = [self createLabelBtnWithTitle:title titleColor:[UIColor whiteColor]];
    
    button.frame = CGRectMake(0, 0, 80, 25);
    
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 5;
    
    button.layer.masksToBounds = YES;
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}
+ (UIBarButtonItem *)createTitleWitiIconBarButtonWithImageName:(NSString *)imageName
                                                         title:(NSString *)title
                                                    titleColor:(UIColor *)titleColor
                                                      selector:(SEL)selector
                                                        target:(id)target{
    UIButton *button = [self createBtnWithImage:imageName title:title titleColor:titleColor frame:CGRectMake(0, 0, 40, 40)];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return backItem;
}
+ (UIBarButtonItem *)createImageBarButtonWithImageName:(NSString *)imageName
                                              selector:(SEL)selector
                                                target:(id)target{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.frame = CGRectMake(0, 0, 40, 40);
    
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return backItem;
}
+ (UIBarButtonItem *)createTitleBarButtonItemWithTitle:(NSString *)title
                                            titleColor:(UIColor *)titleColor
                                              selector:(SEL)selector
                                                target:(id)target
{
    UIButton *button = [self createLabelBtnWithTitle:title titleColor:titleColor];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return backItem;
}
+ (YLCNavigationViewController *)currentNav{
    YLCTabBarViewController *tabBarVc = (YLCTabBarViewController *)[self appDelegate].window.rootViewController;
    
    if (tabBarVc) {
        
        return tabBarVc.selectedViewController;
        
    }else{
        
        return nil;
        
    }
}
+ (AppDelegate *)appDelegate {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return appDelegate;
}
+ (BOOL)isStringOk:(NSString *)str{
    if (![str isKindOfClass:[NSString class]]) {
        return NO;
    }
    if ([[str lowercaseString] isEqualToString:@"(null)"]) {
        return NO;
    }
    if ([[str lowercaseString] isEqualToString:@"<null>"]) {
        return NO;
    }
    if ([[str lowercaseString] isEqualToString:@"null"]) {
        return NO;
    }
    if (str != nil && [str length] >0 && ![@"" isEqualToString:str]) {
        return YES;
    }else {
        return NO;
    }
}
+ (CGSize)getStringSizeWithString:(NSString *)string width:(CGFloat)width font:(float)font{
    CGSize stringSize = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil].size;
    return stringSize;
}
+ (CGSize)getStringWithSizeWithString:(NSString *)string height:(CGFloat)height font:(float)font{
    CGSize stringSize = [string boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil].size;
    return stringSize;
}
+ (void)callPhoneWithNumber:(NSString *)number{
    NSString *num = [[NSString alloc] initWithFormat:@"telprompt://%@",number];
    [self openUrl:num];
}
+ (void)openUrl:(NSString *)url{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
+ (UILabel *)labelWithCorner:(CGFloat)corner
                 borderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                        font:(CGFloat)font{
    UILabel *label = [self createLabelWithFont:font];
    
    label.textAlignment = NSTextAlignmentCenter;
    
    label.layer.borderColor = borderColor.CGColor;
    
    label.layer.borderWidth = borderWidth;
    
    label.layer.cornerRadius = corner;
    
    label.layer.masksToBounds = YES;
    
    return label;
}
//image
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
+ (NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string
                                                  FirstCount:(NSInteger)firstCount
                                                 secondCount:(NSInteger)seconCount
                                                   firstFont:(CGFloat)firstFont
                                                  secondFont:(CGFloat)secondFont
                                                  firstColor:(UIColor *)firstColor
                                                 secondColor:(UIColor *)secondColor{
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:firstFont], NSFontAttributeName,firstColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(0,firstCount)];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:secondFont], NSFontAttributeName,secondColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(firstCount, seconCount)];
    
    return AttributedString;
    
}
+ (NSMutableAttributedString *)fontGetAttributedStringWithString:(NSString *)string
                                                  FirstCount:(NSInteger)firstCount
                                                 secondCount:(NSInteger)seconCount
                                                   firstFont:(UIFont *)firstFont
                                                  secondFont:(UIFont *)secondFont
                                                  firstColor:(UIColor *)firstColor
                                                 secondColor:(UIColor *)secondColor{
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:firstFont, NSFontAttributeName,firstColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(0,firstCount)];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:secondFont, NSFontAttributeName,secondColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(firstCount, seconCount)];
    
    return AttributedString;
    
}

+ (NSMutableAttributedString *)getThreeCountAttributedStringWithString:(NSString *)string
                                                            FirstCount:(NSInteger)firstCount
                                                           secondCount:(NSInteger)seconCount
                                                            thirdCount:(NSInteger)thirdCount
                                                            firstColor:(UIColor *)firstColor
                                                           secondColor:(UIColor *)secondColor
                                                            thirdColor:(UIColor *)thirdColor
                                                             firstFont:(CGFloat)firstFont
                                                            secondFont:(CGFloat)secondFont
                                                             thirdFont:(CGFloat)thirdFont
{
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:firstFont], NSFontAttributeName,firstColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(0,firstCount)];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:secondFont], NSFontAttributeName,secondColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(firstCount, seconCount)];
    
    [AttributedString setAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:thirdFont], NSFontAttributeName,thirdColor, NSForegroundColorAttributeName, nil] range:NSMakeRange(firstCount+seconCount, thirdCount)];
    
    return AttributedString;
    
}
+ (CGRect)layoutFrame{
    return CGRectMake(0, 0, kScreenWidth, kScreenHeight-49-64);
}
+ (CGRect)normalLayoutFrame{
    return CGRectMake(0, 0, kScreenWidth, kScreenHeight-64);
}
+ (UIViewController *)getCurrentVC  
{  
    UIViewController *result = nil;  
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];  
    if (window.windowLevel != UIWindowLevelNormal)  
    {  
        NSArray *windows = [[UIApplication sharedApplication] windows];  
        for(UIWindow * tmpWin in windows)  
        {  
            if (tmpWin.windowLevel == UIWindowLevelNormal)  
            {  
                window = tmpWin;  
                break;  
            }  
        }  
    }  
    
    UIView *frontView = [[window subviews] objectAtIndex:0];  
    id nextResponder = [frontView nextResponder];  
    
    if ([nextResponder isKindOfClass:[UIViewController class]])  
        result = nextResponder;  
    else  
        result = window.rootViewController;  
    
    return result;  
}
+ (UIViewController *)getPresentedViewController
{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    if (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    
    return topVC;
}
+ (void)downLoadHTMLAttrWithContent:(NSString *)content
                               head:(NSString *)head
                           complete:(void (^)(NSAttributedString *htmlAttr))complete{
    __block NSMutableAttributedString *attr = [NSMutableAttributedString new];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *contentString = [NSString stringWithFormat:@"%@%@",head,content];
        attr =  [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            complete(attr);
        });
    });
}
+ (NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"&" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@";" intoString:&text];
        //替换字符
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return html;
}
+ (NSAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString
{
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}
//将 &lt 等类似的字符转化为HTML中的“<”等
+ (NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}

@end
