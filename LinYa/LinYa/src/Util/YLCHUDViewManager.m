//
//  YLCHUDViewManager.m
//  YouLe
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHUDViewManager.h"
#import "YLCListShowView.h"
#import "YLCScreenAreaView.h"
@implementation YLCHUDViewManager
{
    BOOL listState;
    YLCListShowView *showView;
    YLCScreenAreaView *areaShowView;
    RACSubject *dissMissSignal;
    RACSubject *didSelectSignal;
}
+ (YLCHUDViewManager *)shareManager{
    static dispatch_once_t  onceToken;
    
    static YLCHUDViewManager * sSharedInstance;
    
    dispatch_once(&onceToken, ^{
        
        sSharedInstance = [[YLCHUDViewManager alloc] init];
        
    });
    
    return sSharedInstance;
}
- (void)setListShowState:(BOOL)state{
    listState = state;
}
- (BOOL)getListState{
    return listState;
}
- (void)showListViewWithList:(NSArray *)list
                      signal:(RACSubject *)signal
                        rect:(CGRect)rect
                  dissSignal:(RACSubject *)dissSignal
                        type:(NSInteger)type{
    if ([self getListState]) {
        [dissMissSignal sendNext:nil];
        [self dissMissListView];
        return;
    }
    dissMissSignal = dissSignal;
    didSelectSignal = signal;
    if (type==0) {
        [self showNormalListViewWithList:list signal:signal rect:rect dissSignal:dissSignal];
    }else{
        [self showAreaListWithList:list signal:signal rect:rect dissSignal:dissSignal];
    }

}
- (void)showNormalListViewWithList:(NSArray *)list
                            signal:(RACSubject *)signal
                              rect:(CGRect)rect
                        dissSignal:(RACSubject *)dissSignal{
    showView = [[YLCListShowView alloc] initWithFrame:CGRectMake(0, rect.origin.y+40,kScreenWidth, 0)];
    
    showView.list = list;

    
    [showView.selectSignal subscribeNext:^(id x) {
        [didSelectSignal sendNext:x];
        showView = nil;
        dissMissSignal = nil;
        didSelectSignal = nil;
    }];
    
    [showView.dissSignal subscribeNext:^(id x) {
        [dissSignal sendNext:nil];
    }];
    
    [showView show];
}
- (void)showAreaListWithList:(NSArray *)list
                      signal:(RACSubject *)signal
                        rect:(CGRect)rect
                  dissSignal:(RACSubject *)dissSignal{
    
    areaShowView = [[YLCScreenAreaView alloc] initWithFrame:CGRectMake(0, rect.origin.y+40,kScreenWidth, 0)];
    
    [areaShowView.selectSignal subscribeNext:^(id x) {
        [didSelectSignal sendNext:x];
        showView = nil;
        dissMissSignal = nil;
        didSelectSignal = nil;
    }];
    
    [showView.dissSignal subscribeNext:^(id x) {
        [dissSignal sendNext:nil];
    }];
    
    [areaShowView show];
}
- (void)dissMissListView{
    if (showView) {
        [showView dissMiss];
        showView = nil;
        dissMissSignal = nil;
        didSelectSignal = nil;
    }
    if (areaShowView) {
        [areaShowView dissMiss];
        areaShowView = nil;
        dissMissSignal = nil;
        didSelectSignal = nil;
    }

}
@end
