//
//  YLCFactory.h
//  YouLe
//
//  Created by 初程程 on 2018/1/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YLCNavigationViewController,AppDelegate;
@interface YLCFactory : NSObject
//label
+ (UILabel *)normalLabel;
+ (UILabel *)createLabelWithFont:(CGFloat)font;
+ (UILabel *)createLabelWithFont:(CGFloat)font
                           color:(UIColor *)color;
+ (UILabel *)labelWithCorner:(CGFloat)corner
                 borderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                        font:(CGFloat)font;
//field
+ (UITextField *)createFieldWithPlaceholder:(NSString *)placeholder;
+ (UITextField *)createPasswordFieldWithPlaceholder:(NSString *)placeholder
                                     rightImageName:(NSString *)rightImageName
                                            command:(RACCommand *)command;
+ (UITextField *)createFieldWithPlaceholder:(NSString *)placeholder
                             backgroudColor:(UIColor *)backgroundColor;

+ (UITextField *)createTitleFieldWithPlaceholder:(NSString *)placeholder
                                           title:(NSString *)title;
+ (UITextField *)createFieldWithLeftImage:(NSString *)imageName
                              placeholder:(NSString *)placeholder;
//button
+ (UIButton *)createCommondBtnWithTitle:(NSString *)title
                              backgroundColor:(UIColor *)backgroundColor
                             titleColor:(UIColor *)titleColor;
+ (UIButton *)circleBtnWithImage:(NSString *)imageName
                    cornerRadius:(CGFloat)cornerRadius;
+ (UIButton *)createLabelBtnWithTitle:(NSString *)title
                           titleColor:(UIColor *)titleColor;
+ (UIButton *)creatBtnWithCornerRadius:(CGFloat)cornerRadius
                                 title:(NSString *)title
                       backgroundColor:(UIColor *)color
                             textColor:(UIColor *)textColor;
+ (UIButton *)createBtnWithImage:(NSString *)imageName;
+ (UIButton*)createBtnWithImage:(NSString *)imageName
                          title:(NSString *)title
                     titleColor:(UIColor *)titleColor
                          frame:(CGRect)frame;
+ (UIButton *)creatCornerAndBorderWithCornerRadius:(CGFloat)cornerRadius
                                             title:(NSString *)title
                                   backgroundColor:(UIColor *)color
                                         textColor:(UIColor *)textColor
                                       borderColor:(UIColor *)borderColor
                                       borderWidth:(CGFloat)borderWidth;
+ (UIBarButtonItem *)cornerCommontBarItemWithTitle:(NSString *)title
                                          selector:(SEL)selector
                                            target:(id)target;

+ (UIBarButtonItem *)createImageBarButtonWithImageName:(NSString *)imageName
                                              selector:(SEL)selector
                                                target:(id)target;

+ (UIBarButtonItem *)createTitleBarButtonItemWithTitle:(NSString *)title
                                            titleColor:(UIColor *)titleColor
                                              selector:(SEL)selector
                                                target:(id)target;

+ (UIBarButtonItem *)createTitleWitiIconBarButtonWithImageName:(NSString *)imageName
                                                         title:(NSString *)title
                                                    titleColor:(UIColor *)titleColor
                                                      selector:(SEL)selector
                                                        target:(id)target;
//get root
+ (YLCNavigationViewController *)currentNav;
//check
+ (BOOL)isStringOk:(NSString *)str;
//string size
+ (CGSize)getStringSizeWithString:(NSString *)string width:(CGFloat)width font:(float)font;
+ (CGSize)getStringWithSizeWithString:(NSString *)string height:(CGFloat)height font:(float)font;

+ (void)callPhoneWithNumber:(NSString *)number;
+ (void)openUrl:(NSString *)url;
//image
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;

//attributedString
+ (NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string
                                                  FirstCount:(NSInteger)firstCount
                                                 secondCount:(NSInteger)seconCount
                                                   firstFont:(CGFloat)firstFont
                                                  secondFont:(CGFloat)secondFont
                                                  firstColor:(UIColor *)firstColor
                                                 secondColor:(UIColor *)secondColor;
+ (NSMutableAttributedString *)fontGetAttributedStringWithString:(NSString *)string
                                                      FirstCount:(NSInteger)firstCount
                                                     secondCount:(NSInteger)seconCount
                                                       firstFont:(UIFont *)firstFont
                                                      secondFont:(UIFont *)secondFont
                                                      firstColor:(UIColor *)firstColor
                                                     secondColor:(UIColor *)secondColor;
+ (NSMutableAttributedString *)getThreeCountAttributedStringWithString:(NSString *)string
                                                            FirstCount:(NSInteger)firstCount
                                                           secondCount:(NSInteger)seconCount
                                                            thirdCount:(NSInteger)thirdCount
                                                            firstColor:(UIColor *)firstColor
                                                           secondColor:(UIColor *)secondColor
                                                            thirdColor:(UIColor *)thirdColor
                                                             firstFont:(CGFloat)firstFont
                                                            secondFont:(CGFloat)secondFont
                                                             thirdFont:(CGFloat)thirdFont;
+ (CGRect)layoutFrame;
+ (CGRect)normalLayoutFrame;
+ (UIViewController *)getCurrentVC;
+ (UIViewController *)getPresentedViewController;
+ (void)downLoadHTMLAttrWithContent:(NSString *)content
                           complete:(void (^)(NSAttributedString *htmlAttr))complete;
+ (NSString *)htmlEntityDecode:(NSString *)string;
@end
