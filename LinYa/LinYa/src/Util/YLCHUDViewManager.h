//
//  YLCHUDViewManager.h
//  YouLe
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCHUDViewManager : NSObject
+ (YLCHUDViewManager *)shareManager;
- (void)setListShowState:(BOOL)state;
- (BOOL)getListState;
- (void)showListViewWithList:(NSArray *)list
                      signal:(RACSubject *)signal
                        rect:(CGRect)rect
                  dissSignal:(RACSubject *)dissSignal
                        type:(NSInteger)type;
- (void)dissMissListView;
@end
