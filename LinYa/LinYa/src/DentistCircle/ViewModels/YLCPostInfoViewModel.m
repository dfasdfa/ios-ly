//
//  YLCPostInfoViewModel.m
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPostInfoViewModel.h"
#import "YLCDiscussModel.h"
#import "YLCDiscussReplyModel.h"
@interface YLCPostInfoViewModel()
@end
@implementation YLCPostInfoViewModel
{
    NSArray *disCussDataList;
}
- (NSArray *)discussList{
    return disCussDataList;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.model = [[YLCPostInfoModel alloc] init];
    }
    return self;
}
@end
