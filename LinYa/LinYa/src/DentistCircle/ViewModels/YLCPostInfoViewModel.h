//
//  YLCPostInfoViewModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YLCPostInfoModel.h"
@interface YLCPostInfoViewModel : NSObject
@property (nonatomic ,strong ,readonly)NSArray *discussList;
@property (nonatomic ,strong)YLCPostInfoModel *model;
@end
