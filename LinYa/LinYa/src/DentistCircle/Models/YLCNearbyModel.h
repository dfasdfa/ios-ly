//
//  YLCNearbyModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCNearbyModel : NSObject
@property (nonatomic ,copy)NSString *distance;
@property (nonatomic ,copy)NSString *follow_num;
@property (nonatomic ,copy)NSString *funs_num;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *position;
@property (nonatomic ,copy)NSString *sex_flag;
@property (nonatomic ,copy)NSString *user_id;
@end
