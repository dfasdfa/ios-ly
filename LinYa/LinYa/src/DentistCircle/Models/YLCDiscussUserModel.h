//
//  YLCDiscussUserModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCDiscussUserModel : NSObject
/**
 用户昵称
 */
@property (nonatomic ,strong)NSString *nickName;
/**
 用户等级
 */
@property (nonatomic ,strong)NSString *level;
/**
 icon链接
 */
@property (nonatomic ,strong)NSString *icon;
/**
 个性签名
 */
@property (nonatomic ,strong)NSString *signature;
@end
