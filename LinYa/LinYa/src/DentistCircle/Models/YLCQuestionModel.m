//
//  YLCQuestionModel.m
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCQuestionModel.h"

@implementation YLCQuestionModel
- (void)downLoadHTMLAttrWithComplete:(void (^)(void))complete{
    __block NSMutableAttributedString *attr = [NSMutableAttributedString new];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        CGFloat width = kScreenWidth-50;
        NSString *headString = [NSString stringWithFormat:@"<head><style>img{max-width:%.0fpx !important;}</style></head>",width];
        NSString *contentString = [NSString stringWithFormat:@"%@%@",headString,self.content];
        attr =  [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.htmlAttr = attr;
            self.hasLoad = YES;
            complete();
        });
    });
}
- (void)getHeigthWithWidth:(CGFloat)width{
//    NSString *headString = @"<head><style>img{max-width:200px !important;}</style></head>";
//    NSString *contentString = [NSString stringWithFormat:@"%@%@",headString,self.content];
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//
//    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    CGSize size = [YLCFactory getStringSizeWithString:self.title width:width font:15];
    
    CGFloat imgHeight = JK_IS_ARRAY_NIL(_imglist)?0:110;
    
    self.height = size.height + 50 + imgHeight;
}
@end
