//
//  YLCDiscussReplyModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YLCReplyUserModel.h"
@interface YLCDiscussReplyModel : NSObject
/**
 回复内容
 */
@property (nonatomic ,copy)NSString *content;
/**
 回复的ID
 */
@property (nonatomic ,copy)NSString *id;
/**
 回复时间
 */
@property (nonatomic ,copy)NSString *time;
/**
 是否是回复他人的
 */
@property (nonatomic ,assign)BOOL isToUser;
/**
 用于判断的字符串
 */
//@property (nonatomic ,copy)NSString *isToUser;
/**
 被回复人的model
 */
@property (nonatomic ,strong)YLCReplyUserModel *toUser;
/**
 回复人的model
 */
@property (nonatomic ,strong)YLCReplyUserModel *user;
- (NSString *)getContentString;
- (CGFloat)getCellHeightWithCellWidth:(CGFloat)cellWidth;
@end
