//
//  YLCReplyUserModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCReplyUserModel : NSObject
/**
 用户名
 */
@property (nonatomic ,copy)NSString *name;
/**
 用户id
 */
@property (nonatomic ,copy)NSString *id;
@end
