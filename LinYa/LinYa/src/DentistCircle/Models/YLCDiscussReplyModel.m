//
//  YLCDiscussReplyModel.m
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDiscussReplyModel.h"

@implementation YLCDiscussReplyModel

- (NSString *)getContentString{
    NSString *contentString = @"";
    contentString = self.user.name;
    if (self.isToUser) {
        contentString = [NSString stringWithFormat:@"%@回复%@",contentString,self.toUser.name];
    }
    
    contentString = [NSString stringWithFormat:@"%@:%@",contentString,self.content];

    return contentString;
}
- (CGFloat)getCellHeightWithCellWidth:(CGFloat)cellWidth{
    CGSize size = [YLCFactory getStringSizeWithString:[self getContentString] width:cellWidth font:14];
    return size.height+2;
}
@end
