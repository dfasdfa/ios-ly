//
//  YLCDiscussModel.m
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDiscussModel.h"
#import "YLCDiscussReplyModel.h"
@implementation YLCDiscussModel
- (CGFloat)getCellHeightWithWidth:(CGFloat)width{
    CGFloat height = 80;
//    for (YLCDiscussReplyModel *model in self.reply) {
//        height+=[model getCellHeightWithCellWidth:width-80-10];
//    }
    for (NSDictionary *content in self.reply) {
        height+=[YLCFactory getStringSizeWithString:content[@"reply"] width:width-80-10 font:14].height;
    }
    
    CGSize size = [YLCFactory getStringSizeWithString:self.content width:width-70 font:17];
    
    height+=size.height;
//    height+=20;
//
//    height+=50;
//
//    height+=20;
//
//    height+=[YLCFactory getStringSizeWithString:self.content width:width-80-10 font:16].height;
//
//    height+=20;
//
//    height+=50;
    
    return height;
}
@end
