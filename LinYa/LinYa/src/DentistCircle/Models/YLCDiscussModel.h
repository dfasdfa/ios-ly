//
//  YLCDiscussModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YLCDiscussUserModel.h"
@interface YLCDiscussModel : NSObject
@property (nonatomic ,copy)NSString *id;
/**
 问答ID
 */
@property (nonatomic ,copy)NSString *case_id;
/**
 回答人的ID
 */
@property (nonatomic ,copy)NSString *comment_user_id;
/**
 回答人昵称
 */
@property (nonatomic ,copy)NSString *comment_user_name;
/**
 回答人头像
 */
@property (nonatomic ,copy)NSString *comment_user_img;
/**
 帖子内容
 */
@property (nonatomic ,copy)NSString *content;
/**
 回答数量
 */
@property (nonatomic ,copy)NSString *reply_num;
/**
 添加时间
 */
@property (nonatomic ,copy)NSString *add_time;
/**
 帖子标题
 */
@property (nonatomic ,copy)NSString *title;
/**
 帖子内的图片
 */
@property (nonatomic ,copy)NSString *picture;
/**
 回复列表
 */
@property (nonatomic ,strong)NSArray *reply;
@property (nonatomic ,copy)NSString *top_num;
/**
 发帖人信息
 */
@property (nonatomic ,strong)YLCDiscussUserModel *create_user;
- (CGFloat)getCellHeightWithWidth:(CGFloat)width;
@end
