//
//  YLCQuestionModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCQuestionModel : NSObject
@property (nonatomic ,strong)NSMutableAttributedString *htmlAttr;
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *class_id;
@property (nonatomic ,copy)NSString *classify;
/**
 回答数量
 */
@property (nonatomic ,copy)NSString *comment_num;
/**
 问题内容
 */
@property (nonatomic ,copy)NSString *content;
/**
 问题精简内容
 */
@property (nonatomic ,copy)NSString *desc;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *img_id;
@property (nonatomic ,copy)NSString *img_num;
/**
 配图
 */
@property (nonatomic ,strong)NSArray *imglist;
/**
 用户头像
 */
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *main_img;
/**
 昵称
 */
@property (nonatomic ,copy)NSString *nick_name;
/**
 打赏次数
 */
@property (nonatomic ,copy)NSString *praise_num;
@property (nonatomic ,copy)NSString *title;
/**
 点赞数
 */
@property (nonatomic ,copy)NSString *top_num;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *user_name;
/**
 浏览次数
 */
@property (nonatomic ,copy)NSString *view_num;
/**
 是否解决
 */
@property (nonatomic ,copy)NSString *is_solve;
@property (nonatomic ,copy)NSString *is_follow;
@property (nonatomic ,copy)NSString *is_self;
@property (nonatomic ,copy)NSString *is_collect;
@property (nonatomic ,copy)NSString *is_top;
@property (nonatomic ,copy)NSString *share_link;
@property (nonatomic ,copy)NSString *share_title;
/**
 采纳的答案
 */
@property (nonatomic ,strong)NSDictionary *solve;
@property (nonatomic ,assign)CGFloat height;
@property (nonatomic ,assign)BOOL hasLoad;
@property (nonatomic ,copy)NSString *type;
- (void)getHeigthWithWidth:(CGFloat)width;
- (void)downLoadHTMLAttrWithComplete:(void (^)(void))complete;
@end
