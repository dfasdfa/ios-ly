//
//  YLCPostInfoModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCPostInfoModel : NSObject
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *iconPath;
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *time;
@property (nonatomic ,strong)NSArray *headImageList;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *seeNumber;
@end
