//
//  YLCQuestionViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCQuestionViewController.h"
#import "YLCQuestionView.h"
#import "YLCQuestionModel.h"
@interface YLCQuestionViewController ()
@property (nonatomic ,strong)YLCQuestionView *questionView;
@end

@implementation YLCQuestionViewController
{
    NSArray *questionArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.questionView beganRefresh];
}
- (YLCQuestionView *)questionView{
    if (!_questionView) {
        _questionView = [[YLCQuestionView alloc] initWithFrame:CGRectZero];
    }
    return _questionView;
}
- (void)initDataSource{

    questionArr = @[];
    
}
- (void)reloadDataWithPageNum:(NSInteger)pageNum{
    NSString *page = [NSString stringWithFormat:@"%ld",(long)pageNum];
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    NSString *url = _isMine?@"app/user/cases":@"app/case/lists";
    NSDictionary *param = @{};
    if (_isMine) {
        param = @{@"token":model.token,@"userId":model.user_id,@"page":page};
    }else{
        param = @{@"page":page};
        
    }
    if (_type==1) {
        url = @"app/collect/lists";
        param = @{@"token":model.token,@"userId":model.user_id,@"type":@"4",@"page":@"0"};
    }
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:url delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            self.questionView.questionList = @[];
            [self.questionView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = questionArr.mutableCopy;
        
        if ([page integerValue]==0) {
            [container removeAllObjects];
        }
        if (list.count<20) {
            [self.questionView loadedAllData];
        }else{
            [self.questionView resetNoMore];
        }
        for (NSDictionary *dict in list) {
            
            YLCQuestionModel *model = [YLCQuestionModel modelWithDict: dict];
            
            [container addObject:model];
        }
        questionArr = container.copy;
        self.questionView.questionList = questionArr;
    }];
}
- (void)createCustomView{
    
    [self.view addSubview:self.questionView];
    
    [self.questionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 64+49, 0));
    }];
    @weakify(self);
    [self.questionView.reloadSignal subscribeNext:^(id x) {
        @strongify(self)
        [self reloadDataWithPageNum:[x integerValue]];
    }];
    
}
- (NSString *)title{
    return @"问答";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
