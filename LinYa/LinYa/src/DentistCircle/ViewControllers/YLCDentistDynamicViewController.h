//
//  YLCDentistDynamicViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCDentistDynamicViewController : YLCBaseViewController
@property (nonatomic ,assign)NSInteger type;//0是全部  1是我的  2是收藏
@property (nonatomic ,copy)NSString *userId;
@end
