//
//  YLCDynamicInfoViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDynamicInfoViewController.h"
#import "YLCPostInfoMainView.h"
#import "YLCPostInfoViewModel.h"
#import "YLCQuestionModel.h"
#import "YLCDiscussModel.h"
#import "IQKeyboardManager.h"
@interface YLCDynamicInfoViewController ()
@property (nonatomic ,strong)YLCPostInfoMainView *mainView;
@property (nonatomic ,strong)YLCPostInfoViewModel *viewModel;
@end

@implementation YLCDynamicInfoViewController
{
    YLCQuestionModel *questionModel;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomViews];
    [self bindViewModel];
    [self layoutFrame];
}
- (void)initDataSource{
    self.viewModel = [[YLCPostInfoViewModel alloc] init];
    if (JK_IS_STR_NIL(_caseId)) {
        return;
    }
    switch (_type) {
        case 0:
            [self loadQuestionData];
            break;
        case 4:
            [self loadGoodsData];
            break;
        case 5:
            [self loadTieziInfo];
            break;
        default:
            break;
    }
}

- (void)loadTieziInfo{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];

    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"forumId":_caseId} method:@"POST" urlPath:@"app/forum/detail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        questionModel = [YLCQuestionModel modelWithDict:responseObject];
        self.mainView.model = questionModel;
        self.mainView.comment_num = questionModel.comment_num;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"forumId":_caseId,@"page":@"0"} method:@"POST" urlPath:@"app/forum/commentLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        [self catchDiscussListWithList:responseObject];
    }];
}
- (void)loadQuestionData{
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"caseId":_caseId} method:@"POST" urlPath:@"app/case/detail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        questionModel = [YLCQuestionModel modelWithDict:responseObject];
        self.mainView.model = questionModel;
        self.mainView.comment_num = questionModel.comment_num;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"caseId":_caseId,@"page":@"0"} method:@"POST" urlPath:@"app/case/commentLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        [self catchDiscussListWithList:responseObject];
}];
}
- (void)loadGoodsData{
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"goodsId":_caseId} method:@"POST" urlPath:@"app/goods/detail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        YLCGoodsModel *model = [YLCGoodsModel modelWithDict:responseObject];
        
        model.goodsDescription = responseObject[@"description"];
        
        model.goodsId = responseObject[@"id"];
        
        self.mainView.goodModel = model;
        
        self.mainView.comment_num = model.comment_num;
    }];

    [YLCNetWorking loadNetServiceWithParam:@{@"page":@"0",@"goodsId":_caseId} method:@"POST" urlPath:@"app/goods/commentLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        [self catchDiscussListWithList:responseObject];
    }];
}
- (void)catchDiscussListWithList:(NSArray *)list{
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    for (NSDictionary *dict in list) {
        
        YLCDiscussModel *model = [YLCDiscussModel modelWithDict:dict];
        
        [container addObject:model];
    }
    
    self.mainView.dataList = container.copy;
}
- (void)createCustomViews{
    
    self.mainView = [[YLCPostInfoMainView alloc] init];
    
    self.mainView.infoState = _type;
    
    [self.view addSubview:self.mainView];
}
- (void)bindViewModel{
    @weakify(self)
    [self.mainView.reloadSignal subscribeNext:^(id x) {
        @strongify(self)
        [self initDataSource];
    }];
}
- (void)layoutFrame{
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSString *)title{
    switch (_type) {
        case 0:
            return @"问答";
            break;
        case 4:
            return @"物品详情";
        case 5:
            return @"帖子详情";
        default:
            break;
    }
    return @"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
