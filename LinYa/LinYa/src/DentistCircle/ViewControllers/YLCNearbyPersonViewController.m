//
//  YLCNearbyPersonViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNearbyPersonViewController.h"
#import "YLCNearbyPersonView.h"
#import "YLCScreenView.h"
#import "YLCScreenModel.h"
#import "JKLocationManager.h"
#import "YLCNearbyModel.h"
#import "YLCHUDViewManager.h"
@interface YLCNearbyPersonViewController ()<YLCScreenViewDelegate>
@property (nonatomic ,strong)YLCNearbyPersonView *personView;
@property (nonatomic ,strong)YLCScreenView *screenView;
@end

@implementation YLCNearbyPersonViewController
{
    CLLocationCoordinate2D coord;
    NSArray *nearbyList;
    NSArray *jobTypeNameList;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[YLCHUDViewManager shareManager] dissMissListView];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCustomView];
    [self bindViewModel];
}
- (void)initDataSource{
    nearbyList = @[];
    
    [[JKLocationManager sharedInstance] startLocationWith:kCLLocationAccuracyBest];
    
    [[JKLocationManager sharedInstance] getCurrentLocationMessageWithIsFirst:YES completion:^(CLLocation *currentLocation) {
        [[JKLocationManager sharedInstance] stopLocation];
        coord = currentLocation.coordinate;
        [self reloadDataWithSex:@"" work:@"" page:@"0"];
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobTypeNameList = responseObject;
        YLCScreenModel *workModel = self.screenView.screenArr[1];
        workModel.screenList = jobTypeNameList;
        NSMutableArray *container = self.screenView.screenArr.mutableCopy;
        
        [container setObject:workModel atIndexedSubscript:1];
        
        self.screenView.screenArr = container.copy;
    }];
}
- (void)reloadDataWithSex:(NSString *)sex
                     work:(NSString *)work
                     page:(NSString *)page{
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    NSString *lng = [NSString stringWithFormat:@"%.5f",coord.longitude];
    
    NSString *lat = [NSString stringWithFormat:@"%.5f",coord.latitude];
    NSDictionary *param = @{};
    if ([lng integerValue]==0) {
        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"page":page,@"sex":sex,@"position":work};
    }else{
        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"page":page,@"sex":sex,@"position":work,@"lng":lng,@"lat":lat};
    }
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/user/nearby" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            [self.personView loadedAllData];
            self.personView.nearByList = @[];
            return ;
        }
        
        NSArray *list = responseObject;
        
        NSMutableArray *container = nearbyList.mutableCopy;
        
        if ([page isEqualToString:@"0"]) {
            [container removeAllObjects];
        }
        if (list.count<20) {
            [self.personView loadedAllData];
        }else{
            [self.personView resetNoMore];
        }
        for (NSDictionary *dict in responseObject) {
            YLCNearbyModel *model = [YLCNearbyModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        nearbyList = container.copy;
        
        self.personView.nearByList = nearbyList;
    }];
}
- (void)createCustomView{
    
    self.screenView = [[YLCScreenView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 42)];
    
    YLCScreenModel *sexModel = [[YLCScreenModel alloc] init];
    
    sexModel.title = @"性别";
    
    sexModel.value = @"";
    
    sexModel.screenList = @[@"男",@"女"];
    
    YLCScreenModel *workModel = [[YLCScreenModel alloc] init];
    
    workModel.title = @"职业";
    
    workModel.value = @"";
    
//    workModel.screenList = jobTypeNameList;
    
    self.screenView.screenArr = @[sexModel,workModel];
    
    self.screenView.delegate = self;
    
//    [self.view addSubview:self.screenView];
    
    self.personView = [[YLCNearbyPersonView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, self.view.frameHeight)];
    @weakify(self)
    [self.personView.reloadSignal subscribeNext:^(id x) {
        @strongify(self)
        if ([x isEqualToString:@"0"]) {
            [self reloadDataWithSex:@"" work:@"" page:x];
        }else{
            YLCScreenModel *sexModel = self.screenView.screenArr[0];
            YLCScreenModel *workModel = self.screenView.screenArr[1];
            [self reloadDataWithSex:sexModel.value work:workModel.value page:x];
        }
    }];
    
    [self.view addSubview:self.personView];
    
    [self.personView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 64+49, 0));
    }];
}
- (void)screenMessageDidChangeWithDataList:(NSArray *)dataList{
    NSMutableArray *container = dataList.mutableCopy;
    YLCScreenModel *sexModel = dataList[0];
    if (sexModel.isChanged) {
        sexModel.value = [NSString stringWithFormat:@"%ld",sexModel.selectIndex+1];
    }
    [container setObject:sexModel atIndexedSubscript:0];
    YLCScreenModel *workModel = dataList[1];
    
    if (workModel.isChanged) {
        workModel.value = jobTypeNameList[workModel.selectIndex];
    }
    [container setObject:workModel atIndexedSubscript:1];
    self.screenView.screenArr = container.copy;
    
    [self reloadDataWithSex:sexModel.value work:workModel.value page:@"0"];
}
- (void)bindViewModel{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
