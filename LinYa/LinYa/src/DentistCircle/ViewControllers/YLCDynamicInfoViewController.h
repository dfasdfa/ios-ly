//
//  YLCDynamicInfoViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCDynamicInfoViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *caseId;
@property (nonatomic ,assign)NSInteger type; //0问答 4物品  5帖子
@end
