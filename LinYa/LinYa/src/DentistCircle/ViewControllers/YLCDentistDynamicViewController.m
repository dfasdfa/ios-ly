//
//  YLCDentistDynamicViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDentistDynamicViewController.h"
#import "YLCDentistDynamicView.h"
#import "YLCDynamicInfoViewController.h"
#import "YLCQuestionModel.h"

@interface YLCDentistDynamicViewController ()
@property (nonatomic ,strong)YLCDentistDynamicView *dynamicView;
@end

@implementation YLCDentistDynamicViewController
{
    NSArray *dataList;
}
- (YLCDentistDynamicView *)dynamicView{
    if (!_dynamicView) {
        _dynamicView = [[YLCDentistDynamicView alloc] initWithFrame:CGRectZero];
    }
    return _dynamicView;

}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.dynamicView beganRefresh];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
    [self bindViewModel];
}
- (void)initDataSource{
    dataList = @[];
//    [self reloadDataWithPage:@"0"];
    
}
- (void)reloadDataWithPage:(NSString *)page{
    if (JK_IS_STR_NIL(_userId)) {
        _userId = @"";
    }
    NSString *path = @"";
    NSDictionary *param = @{};
    if (_type==2) {
        path = @"app/collect/lists";
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
        
        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"type":@"5",@"page":@"0"};
    }else{
        path = @"app/forum/lists";
        param = @{@"page":page,@"userId":_userId};
    }
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            self.dynamicView.dentistData = @[];
            [self.dynamicView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        if (list.count<20) {
            [self.dynamicView loadedAllData];
        }else{
            [self.dynamicView resetNoMore];
        }
        NSMutableArray *container = dataList.mutableCopy;
        
        if ([page integerValue]==0) {
            [container removeAllObjects];
        }
        
        for (NSDictionary *dict in responseObject) {
            YLCQuestionModel *model = [YLCQuestionModel modelWithDict:dict];
            
            [model getHeigthWithWidth:self.view.frameWidth-18];
            
            [container addObject:model];
        }
        dataList  =  container.copy;
        self.dynamicView.dentistData = container.copy;
    }];
    
}
- (void)createCustomView{
    
    [self.view addSubview:self.dynamicView];
    
    [self.dynamicView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 64+49, 0));
    }];
    
}
- (void)bindViewModel{
    @weakify(self);
    [self.dynamicView.selectSignal subscribeNext:^(id x) {
        @strongify(self);
        NSIndexPath *indexPath = x;
        [self gotoDynamicInfoWithIndex:indexPath.row];
    }];
    
    [self.dynamicView.reloadSignal subscribeNext:^(id x) {
        @strongify(self)
        [self reloadDataWithPage:x];
    }];
}
- (void)gotoDynamicInfoWithIndex:(NSInteger)index{
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.type = 5;
    
    YLCQuestionModel *model = dataList[index];
    
    con.caseId = model.id;
    
    [self.navigationController pushViewController:con animated:YES];
    
}
- (NSString *)title{
    return @"帖子";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
