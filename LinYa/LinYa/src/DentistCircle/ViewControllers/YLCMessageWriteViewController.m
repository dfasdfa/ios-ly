//
//  YLCMessageWriteViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMessageWriteViewController.h"
#import "YLCSubFieldCell.h"
#import "YLCSelectCell.h"
#import "YLCProgramAddCellModel.h"
#import "YLCButtonFootView.h"
#import "YLCPickModel.h"
#import "YLCRegisSuccessViewController.h"
#define FIELD_CELL @"YLCSubFieldCell"
#define SELECT_CELL @"YLCSelectCell"
#define BUTTON_FOOT @"YLCButtonFootView"
@interface YLCMessageWriteViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCSubFieldCellDelegate>

@end

@implementation YLCMessageWriteViewController
{
    UICollectionView *messageTable;
    NSArray *cellArr;
    NSString *name;
    NSString *phone;
    NSString *company;
    NSString *work;
    NSArray *jobList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    YLCProgramAddCellModel *nameModel = [[YLCProgramAddCellModel alloc] init];
    
    nameModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *nameMessageModel = [[YLCProgramSelectModel alloc] init];
    
    nameMessageModel.title  =@"姓名";
    
    nameMessageModel.placeholder = @"请输入姓名";
    
    nameModel.cellModel = nameMessageModel;
    
    YLCProgramAddCellModel *phoneModel = [[YLCProgramAddCellModel alloc] init];
    
    phoneModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *phoneMessageModel = [[YLCProgramSelectModel alloc] init];
    
    phoneMessageModel.title  =@"手机号码";
    
    phoneMessageModel.placeholder = @"请输入手机号码";
    
    phoneModel.cellModel = phoneMessageModel;
    
    YLCProgramAddCellModel *positionModel = [[YLCProgramAddCellModel alloc] init];
    
    positionModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *positionMessageModel = [[YLCProgramSelectModel alloc] init];
    
    positionMessageModel.title = @"你的单位";
    
    positionMessageModel.placeholder = @"请输入您的单位";
    
    positionModel.cellModel = positionMessageModel;
    
    YLCProgramAddCellModel *jobModel = [[YLCProgramAddCellModel alloc] init];
    
    jobModel.cellType = YLCSelectCellType;
    
    YLCProgramSelectModel *eduMessageModel = [[YLCProgramSelectModel alloc] init];
    
    eduMessageModel.title = @"职务";
    
    eduMessageModel.placeholder = @"职位名称";
    
    jobModel.cellModel = eduMessageModel;
    
    cellArr = @[nameModel,phoneModel,positionModel,jobModel];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobList = responseObject;
    }];

}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [messageTable registerClass:[YLCSubFieldCell class] forCellWithReuseIdentifier:FIELD_CELL];
    
    [messageTable registerClass:[YLCSelectCell class] forCellWithReuseIdentifier:SELECT_CELL];
    
    [messageTable registerClass:[YLCButtonFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self.view addSubview:messageTable];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return cellArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramAddCellModel *model = cellArr[indexPath.row];
    
    if (model.cellType==YLCSubFieldCellType) {
        YLCSubFieldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FIELD_CELL forIndexPath:indexPath];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        if (indexPath.row==0) {
            selectModel.subString = name;
        }
        if (indexPath.row==1) {
            selectModel.subString = phone;
        }
        if (indexPath.row==2) {
            selectModel.subString = company;
        }
        cell.model = selectModel;
        
        cell.index = indexPath.row;
        
        cell.delegate = self;
        
        return cell;
    }
    if (model.cellType==YLCSelectCellType) {
        YLCSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SELECT_CELL forIndexPath:indexPath];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        cell.title = selectModel.title;
        
        cell.titleColor = RGB(133, 139, 141);
        
        cell.subColor = RGB(213, 213, 213);
        
        cell.subString = work;
        
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }

    return [[UICollectionViewCell alloc] init];
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionFooter) {
        YLCButtonFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT forIndexPath:indexPath];
        footView.title = @"提交";
        
        [footView.footBtn addTarget:self action:@selector(gotoPostMessage) forControlEvents:UIControlEventTouchUpInside];
        return footView;
    }
    
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frameWidth, 50);
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 60);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==3) {
        [self showJobList];
    }
}
- (void)subDidchangeWithTxt:(NSString *)text
                      index:(NSInteger)index{
    if (index==0) {
        name = text;
    }
    if (index==1) {
        phone = text;
    }
    if (index==2) {
        company = text;
    }
}
- (void)showJobList{
    if (JK_IS_ARRAY_NIL(jobList)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    
    YLCPickModel *pickModel = [[YLCPickModel alloc] init];
    pickModel.rowArr = jobList;
    pickModel.selectIndex = 0;
    RACSubject *signal = [RACSubject subject];
    [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
    [signal subscribeNext:^(id x) {
        YLCPickModel *model = x[0];
        work = model.rowArr[model.selectIndex];
        [messageTable reloadData];
    }];
}
- (void)gotoPostMessage{
    if (JK_IS_STR_NIL(_activityId)) {
        [SVProgressHUD showErrorWithStatus:@"活动信息异常"];
        return;
    }
    
    if (JK_IS_STR_NIL(name)) {
        [SVProgressHUD showErrorWithStatus:@"请输入真实姓名"];
        return;
    }
    
    if (JK_IS_STR_NIL(phone)) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    
    if (JK_IS_STR_NIL(company)) {
        [SVProgressHUD showErrorWithStatus:@"请输入您的单位"];
        return;
    }
    
    if (JK_IS_STR_NIL(work)) {
        [SVProgressHUD showErrorWithStatus:@"请选择您的职业"];
        return;
    }
    if (_type==0) {
        [self gotoTrainPost];
    }else{
        [self gotoActivityPost];
    }
    
}
- (void)gotoTrainPost{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"trainId":_activityId,@"company":company,@"contact":phone,@"position":work,@"realName":name} method:@"POST" urlPath:@"app/train/doEntroll" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didFinishJoin)) {
            [self.delegate didFinishJoin];
        }
        YLCRegisSuccessViewController *con = [[YLCRegisSuccessViewController alloc] init];
        
        NSString *tipText = @"活动报名成功！\n\n请在活动当天在现场完成签到";
        
        NSMutableAttributedString *attr = [YLCFactory getAttributedStringWithString:tipText FirstCount:7 secondCount:tipText.length-7 firstFont:17 secondFont:13 firstColor:YLC_THEME_COLOR secondColor:RGB(215, 215, 215)];
        
        con.tipAttr = attr;
        
        [self.navigationController pushViewController:con animated:YES];
    }];
}
- (void)gotoActivityPost{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];

    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"activityId":_activityId,@"company":company,@"contact":phone,@"position":work,@"realName":name} method:@"POST" urlPath:@"app/activity/doEntroll" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didFinishJoin)) {
            [self.delegate didFinishJoin];
        }
//        [SVProgressHUD showSuccessWithStatus:@"报名成功！"];
//        [self.navigationController popViewControllerAnimated:YES];
        YLCRegisSuccessViewController *con = [[YLCRegisSuccessViewController alloc] init];
        
        NSString *tipText = @"活动报名成功！\n\n请在活动当天在现场完成签到";
        
        NSMutableAttributedString *attr = [YLCFactory getAttributedStringWithString:tipText FirstCount:7 secondCount:tipText.length-7 firstFont:17 secondFont:13 firstColor:YLC_THEME_COLOR secondColor:RGB(215, 215, 215)];
        
        con.tipAttr = attr;
        
        [self.navigationController pushViewController:con animated:YES];
    }];
}
- (NSString *)title{
    return @"信息填写";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
