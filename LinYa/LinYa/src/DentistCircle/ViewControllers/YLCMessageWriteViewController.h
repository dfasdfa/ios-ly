//
//  YLCMessageWriteViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@class YLCMessageWriteViewController;
@protocol YLCMessageWriteViewControllerDelegate <NSObject>
- (void)didFinishJoin;
@end
@interface YLCMessageWriteViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *activityId;
@property (nonatomic ,assign)NSInteger type;//0是培训报名列表 1是活动列表
@property (nonatomic ,weak)id<YLCMessageWriteViewControllerDelegate>delegate;
@end
