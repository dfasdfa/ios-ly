//
//  YLCQuestionViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCQuestionViewController : YLCBaseViewController
@property (nonatomic ,assign)BOOL isMine;
@property (nonatomic ,assign)NSInteger type;//0是普通列表 1是收藏列表
@end
