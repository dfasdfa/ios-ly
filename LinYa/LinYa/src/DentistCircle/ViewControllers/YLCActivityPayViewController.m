//
//  YLCActivityPayViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCActivityPayViewController.h"

@interface YLCActivityPayViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityPriceLabel;

@end

@implementation YLCActivityPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _activityPriceLabel.text = [NSString stringWithFormat:@"%@元",_fee];
}
- (IBAction)buyClick:(id)sender {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
