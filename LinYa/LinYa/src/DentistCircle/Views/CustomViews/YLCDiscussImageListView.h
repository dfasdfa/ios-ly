//
//  YLCDiscussImageListView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCDiscussImageListView : UIView
@property (nonatomic ,strong)NSArray *imageList;
@end
