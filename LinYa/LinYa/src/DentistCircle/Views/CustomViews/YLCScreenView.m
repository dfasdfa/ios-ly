//
//  YLCScreenView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCScreenView.h"
#import "YLCPullListCell.h"
#import "YLCScreenModel.h"
#define PULL_CELL @"YLCPullListCell"
@interface YLCScreenView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCPullListCellDelegate>
@end
@implementation YLCScreenView
{
    UICollectionView *screenView;
    UIImageView *lineView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumInteritemSpacing = 0;
    
    screenView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    screenView.alwaysBounceVertical = NO;
    
    screenView.alwaysBounceHorizontal = NO;
    
    screenView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [screenView registerClass:[YLCPullListCell class] forCellWithReuseIdentifier:PULL_CELL];
    
    screenView.delegate = self;
    
    screenView.dataSource = self;
    
    [self addSubview:screenView];
    
    lineView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    lineView.backgroundColor = YLC_GRAY_COLOR;
    
    [self addSubview:lineView];
}
- (void)setScreenArr:(NSArray *)screenArr{
    _screenArr = screenArr;
    
    [screenView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _screenArr.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCPullListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PULL_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_screenArr.count) {
        
        YLCScreenModel *model = _screenArr[indexPath.row];
        
        cell.title = model.title;
        
        cell.screenType = model.showType;
        
        cell.showArr = model.screenList;
        
    }
    cell.index = indexPath.row;
    cell.delegate = self;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.frameWidth/_screenArr.count, self.frameHeight-0.5);
    
}
- (void)didSelectScreenAtIndex:(NSInteger)selectIndex rowIndex:(NSInteger)rowIndex{
    YLCScreenModel *screenModel = _screenArr[rowIndex];
    screenModel.selectIndex = selectIndex;
    screenModel.isChanged = YES;
    NSMutableArray *container = _screenArr.mutableCopy;
    [container setObject:screenModel atIndexedSubscript:rowIndex];
    _screenArr = [container copy];
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, screenMessageDidChangeWithDataList:)) {
        [self.delegate screenMessageDidChangeWithDataList:_screenArr];
    }
}
- (void)didAreaChangeWithLocationID:(NSString *)locationId rowIndex:(NSInteger)rowIndex{
    YLCScreenModel *screenModel = _screenArr[rowIndex];
    screenModel.value = locationId;
    screenModel.isChanged = YES;
    NSMutableArray *container = _screenArr.mutableCopy;
    [container setObject:screenModel atIndexedSubscript:rowIndex];
    _screenArr = [container copy];
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, screenMessageDidChangeWithDataList:)) {
        [self.delegate screenMessageDidChangeWithDataList:_screenArr];
    }
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [screenView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0.5, 0));
        
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(screenView.mas_bottom).offset(0.5);
        make.height.equalTo(0.5);
        make.left.equalTo(screenView.mas_left);
        make.width.equalTo(screenView.frameWidth);
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
