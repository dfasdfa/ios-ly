//
//  YLCNearbyPersonView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNearbyPersonView.h"
#import "YLCNearbyPersonCell.h"
#import "YLCNearbyModel.h"
#import "YLCChatBaseViewController.h"
#define NEAR_CELL  @"YLCNearbyPersonCell"
@interface YLCNearbyPersonView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCNearbyPersonView
{
    UICollectionView *nearbyTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.reloadSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    nearbyTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    nearbyTable.backgroundColor = [UIColor whiteColor];
    
    [nearbyTable registerClass:[YLCNearbyPersonCell class] forCellWithReuseIdentifier:NEAR_CELL];
    
    [nearbyTable setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(headReload) footAction:@selector(didTriggerLoadNextPageData)];
    
    nearbyTable.delegate = self;
    
    nearbyTable.dataSource = self;
    
    
    [self addSubview:nearbyTable];
}
- (void)headReload{
    [self.reloadSignal sendNext:@"0"];
}
- (void)didTriggerLoadNextPageData{
    NSInteger page = _nearByList.count/[PAGE_LIMIT integerValue];
    NSString *pageString = [NSString stringWithFormat:@"%ld",page];
    [self.reloadSignal sendNext:pageString];
}
- (void)resetNoMore{
    [nearbyTable.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [nearbyTable.mj_footer endRefreshingWithNoMoreData];
}
- (void)setNearByList:(NSArray *)nearByList{
    _nearByList = nearByList;
    [nearbyTable reloadData];
    [nearbyTable.mj_header endRefreshing];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _nearByList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCNearbyPersonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NEAR_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_nearByList.count) {
        cell.model = _nearByList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 83);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (![YLCLoginManager checkLoginStateWithTarget:self]) {
        return;
    }
    YLCNearbyModel *model = _nearByList[indexPath.row];
    YLCChatBaseViewController *con = [[YLCChatBaseViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:model.user_id];
    con.title = model.nick_name;
    con.hidesBottomBarWhenPushed = YES;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];

}
- (void)layoutSubviews{
    [super layoutSubviews];
    [nearbyTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
