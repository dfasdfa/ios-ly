//
//  YLCDiscussImageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDiscussImageCell.h"

@implementation YLCDiscussImageCell
{
    UIImageView *imageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:imageView];
}
- (void)setImageName:(NSString *)imageName{
    imageView.image = [UIImage imageNamed:imageName];
}
- (void)setImagePath:(NSString *)imagePath{
    [imageView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}

@end
