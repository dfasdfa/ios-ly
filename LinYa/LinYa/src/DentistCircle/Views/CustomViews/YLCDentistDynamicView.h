//
//  YLCDentistDynamicView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCDentistDynamicView : UIView
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,strong)RACSubject *reloadSignal;
@property (nonatomic ,strong)NSArray *dentistData;
- (void)beganRefresh;
- (void)resetNoMore;
- (void)loadedAllData;
@end
