//
//  YLCDiscussImageListView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDiscussImageListView.h"
#import "YLCDiscussImageCell.h"
#define IMAGE_CELL @"YLCDiscussImageCell"
@interface YLCDiscussImageListView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCDiscussImageListView
{
    UICollectionView *imageTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 10;
    
    imageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    imageTable.backgroundColor = [UIColor whiteColor];
    
    [imageTable registerClass:[YLCDiscussImageCell class] forCellWithReuseIdentifier:IMAGE_CELL];
    
    imageTable.delegate = self;
    
    imageTable.dataSource = self;
    
    [self addSubview:imageTable];
}
- (void)setImageList:(NSArray *)imageList{
    _imageList = imageList;
    
    [imageTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _imageList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCDiscussImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IMAGE_CELL forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.frameWidth-_imageList.count*10+10)/3, 120);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [imageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
