//
//  YLCDentistDynamicView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDentistDynamicView.h"
#import "YLCDynamicCell.h"
#import "YLCQuestionModel.h"
#define DYNAMIC_CELL @"YLCDynamicCell"
@interface YLCDentistDynamicView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCDentistDynamicView
{
    UICollectionView *dynamicTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.selectSignal = [RACSubject subject];
        self.reloadSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    dynamicTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    dynamicTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [dynamicTable registerClass:[YLCDynamicCell class] forCellWithReuseIdentifier:DYNAMIC_CELL];
    
    dynamicTable.delegate = self;
    
    dynamicTable.dataSource = self;
    
    [dynamicTable setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(reloadData) footAction:@selector(didTriggerLoadNextPageData)];
    
    [dynamicTable setupOnlyHeadRefrshWithTarget:self headAction:@selector(reloadData)];
    
    [self addSubview:dynamicTable];
}
- (void)beganRefresh{
    [dynamicTable.mj_header beginRefreshing];
}
- (void)setDentistData:(NSArray *)dentistData{
    _dentistData = dentistData;
    
    if (JK_IS_ARRAY_NIL(dentistData)) {
        YLCEmptyView *view = [[YLCEmptyView alloc] init];
        
        [dynamicTable showEmptyViewWithEmptyView:view];
    }
    
    [dynamicTable reloadData];
    
    [dynamicTable.mj_header endRefreshing];
}
- (void)resetNoMore{
    [dynamicTable.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [dynamicTable.mj_footer endRefreshingWithNoMoreData];
}
- (void)reloadData{
    [self.reloadSignal sendNext:@"0"];
}
- (void)didTriggerLoadNextPageData{
    
    NSInteger page = [_dentistData count]/[PAGE_LIMIT integerValue];
    
    [self.reloadSignal sendNext:[NSString stringWithFormat:@"%ld",page]];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dentistData.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCDynamicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DYNAMIC_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_dentistData.count) {
        YLCQuestionModel *model = _dentistData[indexPath.row];
        
        cell.photoList = model.imglist;
        
        cell.model = model;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCQuestionModel *model = _dentistData[indexPath.row];
    
    return CGSizeMake(self.frameWidth, model.height+60);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.selectSignal sendNext:indexPath];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [dynamicTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
