//
//  YLCPostInfoHeadView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPostInfoHeadView.h"
#import "YLCPhotoCell.h"
#import "YLCPhotoModel.h"
#define PHOTO_CELL  @"YLCPhotoCell"
@interface YLCPostInfoHeadView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCPostInfoHeadView
{
    UIView *backgroundView;
    UILabel *titleLabel;
    UIImageView *watchIconView;
    UILabel *watchNumberLabel;
    UILabel *replyNumberLabel;
    UIImageView *lineView;
    YLCUserHeadView *customIconView;
    UILabel *nameLabel;
    UILabel *timeLabel;
    YYLabel *contentLabel;
    UICollectionView *imageTable;
    UIButton *careBtn;
    NSArray *photoList;
    UIButton *wxBtn;
    UIButton *likeBtn;
    UIButton *wxCircleBtn;
    UILabel *likeNumberLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = YLC_COMMON_BACKCOLOR;
        [self createCustomViews];
    }
    return self;
}

- (void)createCustomViews{
    
    backgroundView = [[UIView alloc] init];
    
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:backgroundView];
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(42, 42, 42)];
    
    titleLabel.numberOfLines = 0;
    
    [self addSubview:titleLabel];
    
    watchIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"watch_icon"]];
    
    [self addSubview:watchIconView];
    
    watchNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(148, 154, 155)];
    
    [self addSubview:watchNumberLabel];
    
    replyNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(148, 154, 155)];
    
    [self addSubview:replyNumberLabel];
    
    lineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dymic_line"]];
    
    [self addSubview:lineView];
    
    customIconView = [[YLCUserHeadView alloc] init];
    
    [self addSubview:customIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:nameLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:timeLabel];
    
    contentLabel = [YYLabel new];
    
    contentLabel.numberOfLines = 0;
    
    [self addSubview:contentLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 5;
    
    flow.minimumInteritemSpacing = 5;
    
    imageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    [imageTable registerClass:[YLCPhotoCell class] forCellWithReuseIdentifier:PHOTO_CELL];
    
    imageTable.delegate = self;
    
    imageTable.dataSource = self;
    
    imageTable.bounces = NO;
    
    imageTable.alwaysBounceVertical = NO;
    
    imageTable.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:imageTable];
    
    careBtn = [YLCFactory createCommondBtnWithTitle:@"+ 关注" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [careBtn addTarget:self action:@selector(careSomeone:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:careBtn];
    
    likeBtn = [YLCFactory createBtnWithImage:@"share_like_normal"];
    
    [likeBtn setImage:[UIImage imageNamed:@"share_like"] forState:UIControlStateSelected];
    
    [likeBtn addTarget:self action:@selector(didLikeClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:likeBtn];
    
    
    wxBtn = [YLCFactory createBtnWithImage:@"wx_share"];
    
    [wxBtn addTarget:self action:@selector(wxShare) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:wxBtn];
    
    wxCircleBtn = [YLCFactory createBtnWithImage:@"wx_circle"];
    
    [wxCircleBtn addTarget:self action:@selector(wxCircleShare) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:wxCircleBtn];
}
- (void)careSomeone:(UIButton *)sender{
    
    if ([_model.is_follow isEqualToString:@"1"]) {
        return;
    }
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":_model.user_id} method:@"POST" urlPath:@"app/concern/doFollow" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [careBtn setTitle:@"已关注" forState:UIControlStateNormal];
        careBtn.backgroundColor = RGB(201, 201, 200);
        _model.is_follow = @"1";
        [SVProgressHUD showSuccessWithStatus:@"关注成功"];
    }];
}

- (void)didLikeClick:(UIButton *)sender{
    NSString *tip = @"";
    if (sender.selected) {
        tip = @"取消成功";
    }else{
        tip = @"点赞成功";
    }
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"caseId":_model.id,@"userId":userModel.user_id,@"token":userModel.token} method:@"POST" urlPath:@"app/case/top" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        sender.selected = !sender.selected;
        [SVProgressHUD showSuccessWithStatus:tip];
    }];
}
- (void)wxShare{
    
    
    [[YLCWxPayManager shareManager] shareWithContent:_model.share_title title:@"" shareLink:_model.share_link scene:WXSceneSession];
}
- (void)wxCircleShare{
    
    
    [[YLCWxPayManager shareManager] shareWithContent:_model.share_title title:@"" shareLink:_model.share_link scene:WXSceneTimeline];
}

- (void)setModel:(YLCQuestionModel *)model{
    _model = model;
    careBtn.selected = [model.is_follow isEqualToString:@"1"];
    titleLabel.text = model.title;
    nameLabel.text = [model.nick_name notNullString];
    timeLabel.text = [model.add_time notNullString];
    contentLabel.text = [model.content notNullString];
    watchNumberLabel.text = [model.view_num notNullString];
    replyNumberLabel.text = [NSString stringWithFormat:@"%@回帖",model.comment_num];
    if ([model.is_follow isEqualToString:@"1"]) {
        [careBtn setTitle:@"已关注" forState:UIControlStateNormal];
        careBtn.backgroundColor = RGB(201, 201, 200);
    }else{
        [careBtn setTitle:@"+  关注" forState:UIControlStateNormal];
        careBtn.backgroundColor = YLC_THEME_COLOR;
    }
    
    likeBtn.selected = [model.is_top isEqualToString:@"1"];
    
    [customIconView configHeadViewWithImageUrl:model.imgsrc placeholder:@"image_placeholder" isGoCenter:YES userId:model.user_id];
    
    customIconView.isCare = [model.is_follow isEqualToString:@"1"];
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (NSString *url in _model.imglist) {
        YLCPhotoModel *model = [[YLCPhotoModel alloc] init];
        
        model.isWeb = YES;
        
        model.imagePath = url;
        
        [container addObject:model];
    }
    photoList = container.copy;
    
    [imageTable reloadData];
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    if ([model.user_id isEqualToString:userModel.user_id]) {
        careBtn.hidden = YES;
    }else{
        careBtn.hidden = NO;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return photoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];
    
    if (indexPath.row<photoList.count) {
        cell.model = photoList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth-20, 200);
}
- (void)careSomeOne{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":_model.user_id} method:@"POST" urlPath:@"app/concern/doFollow" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        [SVProgressHUD showSuccessWithStatus:@"关注成功"];
    }];
}
- (void)cancelCare{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":_model.user_id} method:@"POST" urlPath:@"app/concern/cancelFollow" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"取消成功"];
    }];
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 10, 0));
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(18);
        make.top.equalTo(backgroundView.mas_top).offset(21);
        make.width.equalTo(backgroundView.frameWidth-40);
        make.height.equalTo(40);
    }];
    
    [watchIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.width.equalTo(16);
        make.top.equalTo(titleLabel.mas_bottom).offset(11);
        make.height.equalTo(12);
    }];
    
    [watchNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(watchIconView.mas_right).offset(3);
        make.centerY.equalTo(watchIconView.mas_centerY);
    }];
    
    [replyNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(watchNumberLabel.mas_right).offset(3);
        make.centerY.equalTo(watchNumberLabel.mas_centerY);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(5);
        make.right.equalTo(backgroundView.mas_right).offset(-5);
        make.top.equalTo(watchIconView.mas_bottom).offset(5);
        make.height.equalTo(0.5);
    }];

    [customIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(18);
        make.top.equalTo(lineView.mas_bottom).offset(11);
        make.width.equalTo(45);
        make.height.equalTo(45);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customIconView.mas_right).offset(10);
        make.top.equalTo(customIconView.mas_top).offset(2);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.bottom.equalTo(customIconView.mas_bottom);
    }];
    
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customIconView.mas_left);
        make.right.equalTo(backgroundView.mas_right).offset(-18);
        make.top.equalTo(customIconView.mas_bottom).offset(10);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [imageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentLabel.mas_left);
        make.right.equalTo(contentLabel.mas_right);
        make.top.equalTo(contentLabel.mas_bottom).offset(10);
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-10);
    }];
    
    [careBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(timeLabel.mas_bottom);
        make.right.equalTo(backgroundView.mas_right).offset(-17);
        make.width.equalTo(67);
        make.height.equalTo(30);
    }];
    
    [likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(30);
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-20);
        make.width.height.equalTo(60);
    }];
    
    [wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-20);
        make.left.equalTo(likeBtn.mas_right).offset(60);
        make.width.height.equalTo(60);
    }];
    
    [wxCircleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-20);
        make.left.equalTo(wxBtn.mas_right).offset(60);
        make.width.height.equalTo(60);
    }];
}
@end
