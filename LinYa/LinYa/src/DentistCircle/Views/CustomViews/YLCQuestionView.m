//
//  YLCQuestionView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCQuestionView.h"
#import "YLCQuestionCell.h"
#import "YLCDynamicInfoViewController.h"
#import "YLCQuestionModel.h"
#define QUESTION_CELL @"YLCQuestionCell"
@interface YLCQuestionView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCQuestionView
{
    UICollectionView *questionTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.reloadSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    questionTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    questionTable.alwaysBounceVertical = YES;
    
    questionTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [questionTable registerClass:[YLCQuestionCell class] forCellWithReuseIdentifier:QUESTION_CELL];
    
    questionTable.delegate = self;
    
    questionTable.dataSource = self;
    
    [questionTable setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(reloadData) footAction:@selector(didTriggerLoadNextPageData)];
    
    [self addSubview:questionTable];
    
    
}
- (void)setQuestionList:(NSArray *)questionList{
    
    _questionList = questionList;
    
    [questionTable reloadData];
    
    [questionTable.mj_header endRefreshing];
}
- (void)beganRefresh{
    [questionTable.mj_header beginRefreshing];
}
- (void)resetNoMore{
    [questionTable.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [questionTable.mj_footer endRefreshingWithNoMoreData];
}
- (void)reloadData{
    [self.reloadSignal sendNext:@"0"];
}
- (void)didTriggerLoadNextPageData{
    
    NSInteger page = [_questionList count]/[PAGE_LIMIT integerValue];
    
    [self.reloadSignal sendNext:[NSString stringWithFormat:@"%ld",page]];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _questionList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCQuestionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:QUESTION_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_questionList.count) {
        
        YLCQuestionModel *model = _questionList[indexPath.row];
        
        cell.model = model;
        
        [cell configUI];
    }
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCQuestionModel *model = _questionList[indexPath.row];
    CGFloat heigth = 120;
    CGSize questionSize = [YLCFactory getStringSizeWithString:model.desc width:self.frameWidth-24 font:15];
    CGSize replySize = CGSizeZero;
    if (!JK_IS_DICT_NIL(model.solve)) {
        replySize = [YLCFactory getStringSizeWithString:model.solve[@"content"] width:self.frameWidth-24-28 font:15];
        
        replySize.height += 34;
    }
    
    return CGSizeMake(self.frameWidth, heigth+questionSize.height+replySize.height);
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    YLCQuestionModel *model = _questionList[indexPath.row];
    
    con.caseId = model.id;
    
    con.type = 0;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    [questionTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
