//
//  YLCScreenView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCScreenView;
@protocol YLCScreenViewDelegate <NSObject>
- (void)screenMessageDidChangeWithDataList:(NSArray *)dataList;
@end
@interface YLCScreenView : UIView
@property (nonatomic ,strong)NSArray *screenArr;
@property (nonatomic ,weak)id<YLCScreenViewDelegate>delegate;
@end
