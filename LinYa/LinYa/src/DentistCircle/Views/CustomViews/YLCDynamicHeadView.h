//
//  YLCDynamicHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCQuestionModel.h"
@interface YLCDynamicHeadView : UICollectionReusableView
@property (nonatomic ,strong)YLCQuestionModel *model;
@end
