//
//  YLCPostInfoHeadView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCQuestionModel.h"
@interface YLCPostInfoHeadView : UICollectionReusableView
@property (nonatomic ,strong)YLCQuestionModel *model;
@end
