//
//  YLCQuestionView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCQuestionView : UIView
@property (nonatomic ,strong)NSArray *questionList;
@property (nonatomic ,strong)RACSubject *reloadSignal;
- (void)beganRefresh;
- (void)resetNoMore;
- (void)loadedAllData;
@end
