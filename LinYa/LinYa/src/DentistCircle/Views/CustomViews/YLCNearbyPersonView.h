//
//  YLCNearbyPersonView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCNearbyPersonView : UIView
@property (nonatomic ,strong)NSArray *nearByList;
@property (nonatomic ,strong)RACSubject *reloadSignal;
- (void)resetNoMore;
- (void)loadedAllData;
@end
