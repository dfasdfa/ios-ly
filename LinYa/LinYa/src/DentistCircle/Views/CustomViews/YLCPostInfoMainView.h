//
//  YLCPostInfoMainView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCQuestionModel.h"
#import "YLCVideoLiftInfoModel.h"
#import "YLCGoodsModel.h"
typedef NS_ENUM(NSUInteger ,YLCDynamicInfoState) {
    QuestionNormalState = 0,
    QuestionMineState,
    QuestionMineEditState,
    VideoLifeState,
    GoodNormalState,
    TieziState
};
@interface YLCPostInfoMainView : UIView
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,strong)YLCQuestionModel *model;
@property (nonatomic ,strong)YLCGoodsModel *goodModel;
@property (nonatomic ,assign)YLCDynamicInfoState infoState;
@property (nonatomic ,strong)YLCVideoLiftInfoModel *videoModel;
@property (nonatomic ,copy)NSString *comment_num;
@property (nonatomic ,strong)RACSubject *reloadSignal;
- (void)videoStop;
@end
