//
//  YLCNearbyPersonCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNearbyPersonCell.h"

@implementation YLCNearbyPersonCell
{
    UIImageView *personHeadIconView;
    UILabel *nameLabel;
    UIImageView *sexTypeView;
    UILabel *fansNumberLabel;
    UILabel *attentionNumberLabel;
    UIImageView *areaIconView;
    UILabel *distanceLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    personHeadIconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:personHeadIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(62, 66, 67)];
    
    [self.contentView addSubview:nameLabel];
    
    sexTypeView = [[UIImageView alloc] init];
    
    sexTypeView.image = [UIImage imageNamed:@"sex_man"];
    
    [self.contentView addSubview:sexTypeView];
    
    attentionNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
    
    
    [self.contentView addSubview:attentionNumberLabel];
    
    fansNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
    
    
    [self.contentView addSubview:fansNumberLabel];
    
    distanceLabel = [YLCFactory createLabelWithFont:12 color:RGB(134, 150, 153)];
    
    
    
    [self.contentView addSubview:distanceLabel];
    
    areaIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location_icon"]];
    
    [self.contentView addSubview:areaIconView];
}
- (void)setModel:(YLCNearbyModel *)model{
    [personHeadIconView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    nameLabel.text = model.nick_name;
    
    attentionNumberLabel.text = [NSString stringWithFormat:@"关注：%@",model.follow_num];
    
    fansNumberLabel.text = [NSString stringWithFormat:@"粉丝：%@",model.funs_num];
    
    distanceLabel.text = model.distance;
    
    if ([model.sex_flag isEqualToString:@"male"]) {
        sexTypeView.image = [UIImage imageNamed:@"sex_man"];
    }else{
        sexTypeView.image = [UIImage imageNamed:@"sex_girl"];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [personHeadIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(18);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.width.equalTo(52);
        make.height.equalTo(53);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(personHeadIconView.mas_top).offset(4);
        make.left.equalTo(personHeadIconView.mas_right).offset(8);
    }];
    
    [sexTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.left.equalTo(nameLabel.mas_right).offset(7);
        make.width.equalTo(11);
        make.height.equalTo(12);
    }];
    
    [attentionNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.bottom.equalTo(personHeadIconView.mas_bottom).offset(-4);
    }];
    
    [fansNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(attentionNumberLabel.mas_top);
        make.left.equalTo(attentionNumberLabel.mas_right).offset(8);
    }];
    
    [distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-18);
        make.bottom.equalTo(fansNumberLabel.mas_bottom);
    }];
    
    [areaIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(distanceLabel.mas_left).offset(-5);
        make.centerY.equalTo(distanceLabel.mas_centerY);
        make.width.equalTo(10);
        make.height.equalTo(13);
    }];
}
@end
