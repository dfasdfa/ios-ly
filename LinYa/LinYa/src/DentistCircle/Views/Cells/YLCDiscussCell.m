//
//  YLCDiscussCell.m
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDiscussCell.h"
#import "MLLinkLabel.h"
#import "YLCCommentInfoCell.h"
#import "YLCDiscussModel.h"
#import "YLCDiscussReplyModel.h"
#import "YLCLikeButton.h"
#import "YLCDiscussImageListView.h"
#import "YLCQuestionModel.h"
@interface YLCDiscussCell()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCDiscussCell
{
    YLCUserHeadView *userIconView;
    UILabel *userNickNameLabel;
    YLCDiscussImageListView *imageListView;
    MLLinkLabel *contentLabel;
    UIView *commentBackView;
    UITableView *commentInfoTable;
    UILabel *timeLabel;
    UIButton *replyBtn;
    UIImageView *verLine;
//    YLCLikeButton *likeBtn;
    UIButton *acceptBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomViews];
        [self layoutFrame];
    }
    return self;
}
- (void)setModel:(YLCDiscussModel *)model{
    _model = model;
    
    userNickNameLabel.text = model.comment_user_name;
    
    contentLabel.text = model.content;
    
    [userIconView configHeadViewWithImageUrl:@"model.comment_user_img" placeholder:@"image_placeholder" isGoCenter:YES userId:model.comment_user_id];
    
    timeLabel.text = model.add_time;
    
    [commentInfoTable reloadData];
    
    if (JK_IS_ARRAY_NIL(model.reply)) {
        commentBackView.hidden = YES;
    }else{
        commentBackView.hidden = NO;
    }
    
    CGFloat height = 0;
    for (NSDictionary *content in _model.reply) {
        
        height+=[YLCFactory getStringSizeWithString:content[@"reply"] width:self.frameWidth-80-10 font:14].height;
    }
    
    [commentBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(height+10);
    }];
    
//    [likeBtn configureLikeStatus:NO count:[model.top_num integerValue] animated:NO];
}
//- (void)setType:(NSInteger)type{
//
//
//
//
//    if (type==0&&![userModel.user_id isEqualToString:_model.comment_user_id]) {
//        acceptBtn.hidden = NO;
//    }else{
//        acceptBtn.hidden = YES;
//    }
//}
- (void)configAcceptWithModel:(YLCQuestionModel *)questionModel{
    self.infoModel = questionModel;
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(userModel.user_id)) {
        acceptBtn.hidden = YES;
        return;
    }
    if ([userModel.user_id isEqualToString:questionModel.user_id]&&![userModel.user_id isEqualToString:_model.comment_user_id]) {
        acceptBtn.hidden = NO;
    }else{
        acceptBtn.hidden = YES;
    }
//    if ([_infoModel.is_solve isEqualToString:@"1"]) {
//        [acceptBtn ]
//    }
}
- (void)createCustomViews{
    userIconView = [[YLCUserHeadView alloc] init];
    
    [self.contentView addSubview:userIconView];
    
    userNickNameLabel = [YLCFactory createLabelWithFont:14 color:RGB(0, 139, 143)];
    
    [self.contentView addSubview:userNickNameLabel];
    
    contentLabel = [[MLLinkLabel alloc] initWithFrame:CGRectZero];
    
    contentLabel.preferredMaxLayoutWidth = self.frameWidth-80;
    
    contentLabel.textColor = RGB(59,63,64);
    
    contentLabel.numberOfLines = 0;
    
    [self.contentView addSubview:contentLabel];
    
//    imageListView = [[YLCDiscussImageListView alloc] initWithFrame:CGRectZero];
//
//    imageListView.imageList = @[];
//
//    [self.contentView addSubview:imageListView];
    
    commentBackView = [[UIView alloc] initWithFrame:CGRectZero];
    
    commentBackView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [self.contentView addSubview:commentBackView];
    
    commentInfoTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    commentInfoTable.backgroundColor = [UIColor clearColor];
    
    commentInfoTable.delegate = self;
    
    commentInfoTable.dataSource = self;
    
    commentInfoTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    commentInfoTable.bounces = NO;
    
    [commentBackView addSubview:commentInfoTable];
    
    timeLabel = [YLCFactory createLabelWithFont:13 color:RGB(128,128,128)];
    
    [self.contentView addSubview:timeLabel];
    
    replyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.contentView addSubview:replyBtn];
    
    verLine = [[UIImageView alloc] init];
    
    verLine.backgroundColor = YLC_GRAY_COLOR;
    
    [self.contentView addSubview:verLine];
    
//    likeBtn = [[YLCLikeButton alloc] initWithFrame:CGRectZero normalImage:@"like_normal" selectImage:@"like_selected" normalColor:RGB(128,128,128) selectedColor:RGB(128,128,128)];
//
//    [likeBtn configureLikeStatus:NO count:0 animated:YES];
//
//    [self.contentView addSubview:likeBtn];
    
    acceptBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:5 title:@"采纳" backgroundColor:[UIColor whiteColor] textColor:YLC_THEME_COLOR borderColor:YLC_THEME_COLOR borderWidth:0.5];
    
    acceptBtn.hidden = YES;
    
    [acceptBtn addTarget:self action:@selector(acceptAnswer) forControlEvents:UIControlEventTouchUpInside];
    
    acceptBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [self.contentView addSubview:acceptBtn];
}
- (void)acceptAnswer{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"commentId":_model.id,@"caseId":_model.case_id} method:@"POST" urlPath:@"app/case/doSolve" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"采纳成功！"];
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _model.reply.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YLCCommentInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YLCCommentInfoCell"];
    
    if (!cell) {
        cell = [[YLCCommentInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"YLCCommentInfoCell"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row<_model.reply.count) {
        NSDictionary *replyDic = _model.reply[indexPath.row];
        cell.content = replyDic[@"reply"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    YLCDiscussReplyModel *model = _model.reply[indexPath.row];
//    return [model getCellHeightWithCellWidth:self.frameWidth];
    NSDictionary *content = _model.reply[indexPath.row];
    return [YLCFactory getStringSizeWithString:content[@"reply"] width:self.frameWidth font:14].height;
}
- (void)layoutFrame{
    WS(weakSelf)
    [userIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(13);
        make.top.equalTo(weakSelf.mas_top).offset(20);
        make.width.equalTo(44);
        make.height.equalTo(44);
    }];
    
    [userNickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(userIconView.mas_top);
        make.left.equalTo(userIconView.mas_right).offset(9);
    }];
    
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(userNickNameLabel.mas_bottom).offset(10);
        make.left.equalTo(userNickNameLabel.mas_left);
        make.width.equalTo(weakSelf.frameWidth-70);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
//    [imageListView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(contentLabel.mas_left);
//        make.top.equalTo(contentLabel.mas_bottom).offset(10);
//        make.width.equalTo(weakSelf.frameWidth-79);
//        make.height.equalTo(100);
//    }];
    
    
    
    //    for (YLCDiscussReplyModel *model in _model.reply) {
    //        height += [model getCellHeightWithCellWidth:self.frameWidth-80-10];
    //    }
    
    [commentBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentLabel.mas_left);
        make.top.equalTo(contentLabel.mas_bottom).offset(10);
        make.width.equalTo(self.frameWidth-80);
        make.height.equalTo(10);
    }];
    

    
    [commentInfoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(5, 5, 5, 5));
    }];
    
    [replyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(commentBackView.mas_bottom).offset(10);
        make.right.equalTo(commentBackView.mas_right);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNickNameLabel.mas_right).offset(9);
        make.centerY.equalTo(userNickNameLabel.mas_centerY);
    }];
    
    [verLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(commentBackView.mas_left);
        make.right.equalTo(commentBackView.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
//    [likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-10);
//        make.centerY.equalTo(userNickNameLabel.mas_centerY);
//        make.width.equalTo(40);
//        make.height.equalTo(20);
//    }];
    
    [acceptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(userNickNameLabel.mas_centerY);
        make.width.equalTo(50);
        make.height.equalTo(25);
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];

}
@end
