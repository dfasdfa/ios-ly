//
//  YLCQuestionCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCQuestionModel.h"
@interface YLCQuestionCell : UICollectionViewCell
@property (nonatomic ,strong)YLCQuestionModel *model;
- (void)configUI;
@end
