//
//  YLCCommentInfoCell.m
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCommentInfoCell.h"
#import "MLLinkLabel.h"
#import "YLCDiscussReplyModel.h"
@interface YLCCommentInfoCell()<MLLinkLabelDelegate>
@end
@implementation YLCCommentInfoCell
{
    MLLinkLabel *contentLabel;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    contentLabel = [MLLinkLabel new];
    
    contentLabel.delegate = self;
    
    [self.contentView addSubview:contentLabel];
    
    contentLabel.preferredMaxLayoutWidth = self.frameWidth;
    
    contentLabel.numberOfLines = 0;
    
    contentLabel.font = [UIFont systemFontOfSize:14];
    
    contentLabel.textColor = [UIColor blackColor];
    
    contentLabel.dataDetectorTypes = MLDataDetectorTypeURL|MLDataDetectorTypeAttributedLink;
    
    contentLabel.linkTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#1ea199"]};
    
    contentLabel.backgroundColor = [UIColor clearColor];
    
    self.hyb_lastViewInCell = contentLabel;
    
    self.hyb_bottomOffsetToCell = 2.0;
}
//- (void)setModel:(YLCDiscussReplyModel *)model{
//    _model = model;
//    NSString *contentString = [model getContentString];
//    
//    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:contentString];
//    
//    [attrText setAttributes:@{NSLinkAttributeName : [NSString stringWithFormat:@"@@%@@@",model.user.id]} range:[contentString rangeOfString:model.user.name]];
//    
//    contentLabel.linkTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#1ea199"]};
//    
//    contentLabel.attributedText = attrText;
//}
- (void)setContent:(NSString *)content{
    contentLabel.text = [content notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(2, 0, 0, 0));
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
