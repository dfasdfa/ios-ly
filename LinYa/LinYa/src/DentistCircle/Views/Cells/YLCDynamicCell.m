//
//  YLCDynamicCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDynamicCell.h"
#import "YLCDynamicPhotoCell.h"
#import "YLCDynamicInfoViewController.h"
#define PHOTO_CELL  @"YLCDynamicPhotoCell"
@interface YLCDynamicCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCDynamicCell
{
    YLCUserHeadView *headIconView;
    UILabel *nameLabel;
    UILabel *dynamicLabel;
    UICollectionView *photoTable;
    UILabel *timeLabel;
    UILabel *typeLabel;
    UILabel *replyNumberLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    headIconView = [[YLCUserHeadView alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview:headIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(148, 156, 157)];
    
    [self.contentView addSubview:nameLabel];
    
    dynamicLabel = [YLCFactory createLabelWithFont:15 color:RGB(42, 44, 45)];
    
    dynamicLabel.numberOfLines = 0;
    
    [self.contentView addSubview:dynamicLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];

    flow.minimumLineSpacing = 8;

    flow.minimumInteritemSpacing = 8;
    
    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;

    photoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];

    photoTable.backgroundColor = [UIColor whiteColor];
    
    [photoTable registerClass:[YLCDynamicPhotoCell class] forCellWithReuseIdentifier:PHOTO_CELL];

    photoTable.delegate = self;

    photoTable.dataSource = self;

    [self.contentView addSubview:photoTable];
    
    typeLabel = [YLCFactory createLabelWithFont:14];
    
    typeLabel.textColor = YLC_THEME_COLOR;
    
    typeLabel.text = @"  ";
    
    [self.contentView addSubview:typeLabel];
    
    replyNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(170, 174, 175)];
    
    [self.contentView addSubview:replyNumberLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:12 color:RGB(168, 175, 176)];
    
    [self.contentView addSubview:timeLabel];
    
    [self createFrame];
}
- (void)setModel:(YLCQuestionModel *)model{
    _model = model;
    
    [headIconView configHeadViewWithImageUrl:model.imgsrc placeholder:@"image_placeholder" isGoCenter:YES userId:model.user_id];
    
    headIconView.isCare = [model.is_follow isEqualToString:@"1"];
    
    nameLabel.text = [model.nick_name notNullString];
    
    dynamicLabel.text = model.title;

    replyNumberLabel.text = [NSString stringWithFormat:@"%@回帖",model.comment_num];
    
    timeLabel.text = [NSString stringWithFormat:@"%@ 发布",model.add_time];
    
    [self updateFrame];
}
- (void)setPhotoList:(NSArray *)photoList{
    _photoList = photoList;
    [photoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCDynamicPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];

    if (indexPath.row<_photoList.count) {
        cell.imagePath = _photoList[indexPath.row];
    }

    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(110, 80);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self gotoDynamicInfoWithIndex:0];
}
- (void)gotoDynamicInfoWithIndex:(NSInteger)index{
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.type = 5;
    
    YLCQuestionModel *model = _model;
    
    con.caseId = model.id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)createFrame{
    WS(weakSelf)
    
    [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(14);
        make.left.equalTo(weakSelf.mas_left).offset(9);
        make.width.equalTo(35);
        make.height.equalTo(35);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_right).offset(9);
        make.centerY.equalTo(headIconView.mas_centerY);
    }];
    
    [dynamicLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_left);
        make.top.equalTo(headIconView.mas_bottom).offset(9);
        make.width.equalTo(weakSelf.frameWidth-18);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [photoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.top.equalTo(dynamicLabel.mas_bottom).offset(9);
        make.height.equalTo(110);
    }];
    
    [typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_left);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-5);
    }];
    
    [replyNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_left);
        make.centerY.equalTo(typeLabel.mas_centerY);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.centerY.equalTo(typeLabel.mas_centerY);
    }];
}
- (void)updateFrame{
    CGFloat imgTableHeight = JK_IS_ARRAY_NIL(_model.imglist)?0:110;
    WS(weakSelf)
    [photoTable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.top.equalTo(dynamicLabel.mas_bottom).offset(9);
        make.height.equalTo(imgTableHeight);
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];

}
@end
