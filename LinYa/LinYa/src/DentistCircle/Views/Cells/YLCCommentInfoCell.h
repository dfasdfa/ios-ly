//
//  YLCCommentInfoCell.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCDiscussReplyModel;
@interface YLCCommentInfoCell : UITableViewCell
//@property (nonatomic ,strong)YLCDiscussReplyModel *model;
@property (nonatomic ,copy)NSString *content;
@end
