//
//  YLCReplyNumberCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReplyNumberCell.h"

@implementation YLCReplyNumberCell
{
    UIImageView *iconView;
    UILabel *numberLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reply_num"]];
    
    [self.contentView addSubview:iconView];
    
    numberLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:numberLabel];
}
- (void)setNumber:(NSString *)number{
    if (JK_IS_STR_NIL(number)) {
        number = @"0";
    }
    NSString *titleString = @"";
    if (_type==0) {
        titleString = @"回答列表";
    }
    if (_type==5) {
        titleString = @"回帖列表";
    }
    if (_type==4) {
        titleString = @"留言列表";
    }
    if (_type==3) {
        titleString = @"评论列表";
    }
    numberLabel.text = [NSString stringWithFormat:@"%@(%@)",titleString,number];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(20);
    }];
    
    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_right).offset(10);
        make.centerY.equalTo(iconView.mas_centerY);
    }];
}
@end
