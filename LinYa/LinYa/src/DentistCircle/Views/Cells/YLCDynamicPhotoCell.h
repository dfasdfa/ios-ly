//
//  YLCDynamicPhotoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCDynamicPhotoCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *imagePath;
@end
