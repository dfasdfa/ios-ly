//
//  YLCDynamicPhotoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCDynamicPhotoCell.h"

@implementation YLCDynamicPhotoCell
{
    UIImageView *photoView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        photoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        [self.contentView addSubview:photoView];
    }
    return self;
}
- (void)setImagePath:(NSString *)imagePath{
    [photoView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:imagePath]];
    [self setNeedsDisplay];
    [self setNeedsLayout];
}
- (void)layoutSubviews{
    [super layoutSubviews];

}
@end
