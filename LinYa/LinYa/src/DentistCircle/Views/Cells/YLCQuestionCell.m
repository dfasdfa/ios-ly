//
//  YLCQuestionCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCQuestionCell.h"

@implementation YLCQuestionCell
{
    YLCUserHeadView *iconView;
    UILabel *nameLabel;
    UILabel *questionLabel;
    UIView *replyBackgoundView;
    UILabel *replyLabel;
    UILabel *typeLabel;
    UIView *replyCustomView;
    YLCUserHeadView *replyIconView;
    UILabel *replyNameLabel;
    UILabel *replyNumberLabel;
    UILabel *timeLabel;
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
        [self loadFrame];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[YLCUserHeadView alloc] init];
    
    [self.contentView addSubview:iconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(148, 156, 157)];
    
    [self.contentView addSubview:nameLabel];
    
    questionLabel = [YLCFactory createLabelWithFont:15 color:RGB(42, 44, 45)];
    
    questionLabel.numberOfLines = 0;
    
    [self.contentView addSubview:questionLabel];
    
    replyBackgoundView = [[UIView alloc] init];
    
    replyBackgoundView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [self.contentView addSubview:replyBackgoundView];
    
    replyLabel = [YLCFactory createLabelWithFont:15 color:RGB(139, 148, 150)];
    
    replyLabel.numberOfLines = 0;
    
    [replyBackgoundView addSubview:replyLabel];
    
    typeLabel = [YLCFactory createLabelWithFont:14 color:RGB(0, 154, 218)];
    
    [self.contentView addSubview:typeLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:14 color:RGB(139, 148, 150)];
    
    [self.contentView addSubview:timeLabel];
    
    replyIconView = [[YLCUserHeadView alloc] init];
    
    [self.contentView addSubview:replyIconView];
    
    replyNameLabel = [YLCFactory createLabelWithFont:15 color:RGB(139, 148, 150)];
    
    [self.contentView addSubview:replyNameLabel];
    
    replyNumberLabel = [YLCFactory createLabelWithFont:15 color:RGB(139, 148, 150)];
    
    [self.contentView addSubview:replyNumberLabel];
}
- (void)configUI{
    
    [iconView configHeadViewWithImageUrl:_model.imgsrc placeholder:@"image_placeholder" isGoCenter:YES userId:_model.user_id];
    
    nameLabel.text = _model.nick_name;
    
    questionLabel.text = _model.title;
    
    NSString *typeString = @"";
    if ([_model.is_solve isEqualToString:@"1"]) {
        replyBackgoundView.hidden = NO;
        typeString = @"已解决";
        replyIconView.hidden = NO;
        replyNameLabel.hidden = NO;
    }else{
        replyBackgoundView.hidden = YES;
        typeString = @"问.未解决";
        replyIconView.hidden = YES;
        replyNameLabel.hidden = YES;
    }
    if (!JK_IS_ARRAY_NIL(_model.imglist)) {
        typeString = typeString;
    }else{
        typeString = [NSString stringWithFormat:@"%@ 图",typeString];
    }
    typeLabel.text = typeString;
    
    timeLabel.text = [NSString stringWithFormat:@"%@ 发布",_model.add_time];
    
    if (!JK_IS_DICT_NIL(_model.solve)) {
        [replyIconView configHeadViewWithImageUrl:_model.solve[@"comment_user_img"] placeholder:@"image_placeholder" isGoCenter:YES userId:_model.solve[@"comment_user_id"]];
        
        replyLabel.text = _model.solve[@"content"];
    }
    if ([YLCFactory isStringOk:_model.comment_num]) {
        replyNumberLabel.text = [NSString stringWithFormat:@"%@人回答",_model.comment_num];
    }else{
        replyNumberLabel.text = @"0人回答";
    }
    [self updateFrame];
    
}
- (void)loadFrame{
    WS(weakSelf)
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(14);
        make.top.equalTo(weakSelf.mas_top).offset(16);
        make.width.equalTo(35);
        make.height.equalTo(35);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_right).offset(10);
        make.centerY.equalTo(iconView.mas_centerY);
    }];
    
    [questionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_left);
        make.top.equalTo(iconView.mas_bottom).offset(9);
        make.width.equalTo(weakSelf.frameWidth-24);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [replyBackgoundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(questionLabel.mas_bottom).offset(6);
        make.left.equalTo(questionLabel.mas_left).offset(2);
        make.right.equalTo(questionLabel.mas_right).offset(-2);
        make.height.equalTo(0);
    }];
    
    [replyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(replyBackgoundView.mas_left).offset(12);
        make.top.equalTo(replyBackgoundView.mas_top).offset(17);
        make.right.equalTo(replyBackgoundView.mas_right).offset(-12);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(17);
        make.top.equalTo(replyBackgoundView.mas_bottom).offset(14);
    }];
    
    [replyIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(typeLabel.mas_right).offset(15);
        make.centerY.equalTo(typeLabel.mas_centerY);
        make.width.height.equalTo(30);
    }];
    
    [replyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(replyIconView.mas_right).offset(10);
        make.centerY.equalTo(replyIconView.mas_centerY);
    }];
    
    [replyNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(replyNameLabel.mas_right).offset(10);
        make.centerY.equalTo(replyNameLabel.mas_centerY);
    }];

    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-17);
        make.centerY.equalTo(typeLabel.mas_centerY);
    }];
}
- (void)updateFrame{
    if ([_model.is_solve isEqualToString:@"1"]) {
        CGSize size = [YLCFactory getStringSizeWithString:_model.solve[@"content"] width:self.frameWidth-28-24 font:15];
        [replyBackgoundView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(questionLabel.mas_bottom).offset(6);
            make.left.equalTo(questionLabel.mas_left).offset(2);
            make.right.equalTo(questionLabel.mas_right).offset(-2);
            make.height.equalTo(size.height+34);
        }];
        
        [replyIconView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(typeLabel.mas_right).offset(15);
            make.centerY.equalTo(typeLabel.mas_centerY);
            make.width.height.equalTo(30);
        }];
        
        [replyNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(replyIconView.mas_right).offset(10);
            make.centerY.equalTo(replyIconView.mas_centerY);
        }];
        
        [replyNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(replyNameLabel.mas_right).offset(10);
            make.centerY.equalTo(replyNameLabel.mas_centerY);
        }];
    }else{
        [replyBackgoundView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(questionLabel.mas_bottom).offset(6);
            make.left.equalTo(questionLabel.mas_left).offset(2);
            make.right.equalTo(questionLabel.mas_right).offset(-2);
            make.height.equalTo(0);
        }];
        
        [replyNumberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(typeLabel.mas_right).offset(15);
            make.centerY.equalTo(typeLabel.mas_centerY);
        }];
    }
}
@end
