//
//  YLCNearbyPersonCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCNearbyModel.h"
@interface YLCNearbyPersonCell : UICollectionViewCell
@property (nonatomic ,strong)YLCNearbyModel *model;
@end
