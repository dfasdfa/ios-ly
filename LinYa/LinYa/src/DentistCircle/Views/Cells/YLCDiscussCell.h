//
//  YLCDiscussCell.h
//  YouLe
//
//  Created by 初程程 on 2018/1/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCDiscussModel,YLCQuestionModel;
@interface YLCDiscussCell : UICollectionViewCell
@property (nonatomic ,strong)YLCDiscussModel *model;
@property (nonatomic ,strong)YLCQuestionModel *infoModel;
@property (nonatomic ,assign)NSInteger type;
- (void)configAcceptWithModel:(YLCQuestionModel *)questionModel;
@end
