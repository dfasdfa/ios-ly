//
//  YLCPullListCell.h
//  YouLe
//
//  Created by 初程程 on 2018/1/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCPullListCell;
@protocol YLCPullListCellDelegate <NSObject>
- (void)didSelectScreenAtIndex:(NSInteger)selectIndex
                      rowIndex:(NSInteger)rowIndex;
@optional
- (void)didAreaChangeWithLocationID:(NSString *)locationId
                           rowIndex:(NSInteger)rowIndex;
@end
@interface YLCPullListCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,strong)NSArray *showArr;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,assign)NSInteger screenType;//0是普通列表 1是地区列表
@property (nonatomic ,weak)id<YLCPullListCellDelegate>delegate;
@end
