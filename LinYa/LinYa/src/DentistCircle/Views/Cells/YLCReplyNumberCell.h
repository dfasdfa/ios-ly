//
//  YLCReplyNumberCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
//typedef NS_ENUM(NSUInteger ,YLCDynamicInfoState) {
//    QuestionNormalState = 0,
//    QuestionMineState,
//    QuestionMineEditState,
//    VideoLifeState,
//    GoodNormalState,
//    TieziState
//};
@interface YLCReplyNumberCell : UICollectionViewCell
@property (nonatomic ,assign)NSInteger type;
@property (nonatomic ,copy)NSString *number;
@end
