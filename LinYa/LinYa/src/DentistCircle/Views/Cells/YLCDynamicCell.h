//
//  YLCDynamicCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCQuestionModel.h"
@interface YLCDynamicCell : UICollectionViewCell
@property (nonatomic ,strong)NSArray *photoList;
@property (nonatomic ,strong)YLCQuestionModel *model;
@end
