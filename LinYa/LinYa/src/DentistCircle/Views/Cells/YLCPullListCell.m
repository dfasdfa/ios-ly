//
//  YLCPullListCell.m
//  YouLe
//
//  Created by 初程程 on 2018/1/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPullListCell.h"
#import "YLCHUDViewManager.h"
#import "YLCAreaModel.h"
@implementation YLCPullListCell
{
    UILabel *titleLabel;
    UIImageView *subArrowView;
    
}
- (void)dealloc{
    [[YLCHUDViewManager shareManager] dissMissListView];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    
    titleLabel = [YLCFactory createLabelWithFont:15];
    
    titleLabel.userInteractionEnabled = YES;
    
    [self.contentView addSubview:titleLabel];
    
    subArrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"topArrow"]];
    
    subArrowView.contentMode = UIViewContentModeCenter;
    
    subArrowView.userInteractionEnabled = YES;
    
    [self.contentView addSubview:subArrowView];
    
    UITapGestureRecognizer *showListGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showList)];
    
    [self addGestureRecognizer:showListGes];
}
- (void)showList{
    if (![[YLCHUDViewManager shareManager] getListState]) {
        [self subArrowChangeWithShow:YES];
    }
    CGRect rect = [self rectInWindow];
    
    RACSubject *selectSignal = [RACSubject subject];
    
    [selectSignal subscribeNext:^(id x) {
        
        if (_screenType==0) {
            NSIndexPath *indexPath = x;
            if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectScreenAtIndex:rowIndex:)) {
                [self.delegate didSelectScreenAtIndex:indexPath.row rowIndex:_index];
            }
        }else{
            YLCAreaModel *model = x;
            if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didAreaChangeWithLocationID:rowIndex:)) {
                [self.delegate didAreaChangeWithLocationID:model.id rowIndex:_index];
            }
        }
    }];
    
    RACSubject *dissMissSignal = [RACSubject subject];
    
    [dissMissSignal subscribeNext:^(id x) {
        [self subArrowChangeWithShow:NO];
    }];
    
    [[YLCHUDViewManager shareManager] showListViewWithList:_showArr signal:selectSignal rect:rect dissSignal:dissMissSignal type:_screenType];
}
- (void)subArrowChangeWithShow:(BOOL)isShow{
    
    if (isShow) {
        subArrowView.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    }else{
        subArrowView.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
    }
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [subArrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_right).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
}
@end
