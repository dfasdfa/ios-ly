//
//  YLCReplyMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCReplyMessageCell : UICollectionViewCell
@property (nonatomic ,strong)NSDictionary *dict;
@property (nonatomic ,assign)NSInteger type;//0是回复我的  1是给我留言
@end
