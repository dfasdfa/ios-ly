//
//  YLCReplyMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReplyMessageCell.h"
@interface YLCReplyMessageCell()
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

@implementation YLCReplyMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}
- (void)setDict:(NSDictionary *)dict{
    _messageLabel.text = dict[@"content"];
    _timeLabel.text = dict[@"add_time"];
    [_userIconView sd_setImageWithURL:[NSURL URLWithString:dict[@"imgsrc"]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    if (_type==0) {
        NSString *nickString = [NSString stringWithFormat:@"%@ 回复 我",dict[@"nick_name"]];
        
        NSMutableAttributedString *attr = [YLCFactory getAttributedStringWithString:nickString FirstCount:4 secondCount:nickString.length-4 firstFont:16 secondFont:16 firstColor:YLC_THEME_COLOR secondColor:RGB(30, 30, 30)];
        
        _userNameLabel.attributedText = attr;
        
    }else{
        _userNameLabel.text = dict[@"nick_name"];
        _userNameLabel.textColor = RGB(30, 30, 30);
    }
    
    
}
//if (indexPath.row==1) {
//    if (!JK_IS_ARRAY_NIL(_commentList)) {
//        NSDictionary *dict = _commentList[0];
//        cell.message = dict[@"content"];
//        cell.timeString = dict[@"add_time"];
//    }
//
//    cell.iconName = @"message_re_mine";
//
//    cell.title = @"回复\"我的\"消息";
//}
//if (indexPath.row==2) {
//    if (!JK_IS_ARRAY_NIL(_replyList)) {
//        NSDictionary *dict = _replyList[0];
//        cell.message = dict[@"content"];
//        cell.timeString = dict[@"add_time"];
//    }
//
//    cell.iconName = @"message_send_me";
//
//    cell.title = @"给我留言的消息";
//}
@end
