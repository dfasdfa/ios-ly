//
//  YLCMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCMessageCell : UITableViewCell
@property (nonatomic ,copy)NSString *iconName;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *message;
@property (nonatomic ,copy)NSString *timeString;
@property (nonatomic ,copy)NSString *unreadCount;
@end
