//
//  YLCMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMessageCell.h"

@implementation YLCMessageCell
{
    UIImageView *iconView;
    UILabel *nameLabel;
    UILabel *redPointLabel;
    UILabel *timeLabel;
    UILabel *messageLabel;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:iconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(62, 66, 67)];
    
    [self.contentView addSubview:nameLabel];
    
    redPointLabel = [YLCFactory createLabelWithFont:11 color:[UIColor whiteColor]];
    
    redPointLabel.text = @"";
    
    redPointLabel.layer.cornerRadius = 7;
    
    redPointLabel.layer.masksToBounds = YES;
    
    redPointLabel.textAlignment = NSTextAlignmentCenter;
    
    redPointLabel.hidden = YES;
    
    redPointLabel.backgroundColor = RGB(247, 74, 74);
    
    [self.contentView addSubview:redPointLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:11 color:RGB(145, 152, 154)];
    
    
    [self.contentView addSubview:timeLabel];
    
    messageLabel = [YLCFactory createLabelWithFont:13 color:RGB(142, 147, 149)];
    
    
    [self.contentView addSubview:messageLabel];
}
- (void)setUnreadCount:(NSString *)unreadCount{
    if ([unreadCount integerValue]==0) {
        redPointLabel.hidden = YES;
    }else{
        redPointLabel.hidden = NO;
    }
    
    if ([unreadCount integerValue]>99) {
        unreadCount = @"99";
    }
    redPointLabel.text = unreadCount;
}
- (void)setTitle:(NSString *)title{
    nameLabel.text = [title notNullString];
}
- (void)setIconName:(NSString *)iconName{
    iconView.image = [UIImage imageNamed:iconName];
}
- (void)setMessage:(NSString *)message{
    messageLabel.text = [message notNullString];
}
- (void)setTimeString:(NSString *)timeString{
    timeLabel.text = [timeString notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(22);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.and.height.equalTo(45);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconView.mas_top);
        make.left.equalTo(iconView.mas_right).offset(10);
    }];
    
    [redPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLabel.mas_top);
        make.left.equalTo(nameLabel.mas_right).offset(3);
        make.width.and.height.equalTo(14);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.centerY.equalTo(nameLabel.mas_centerY);
    }];
    
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(iconView.mas_bottom);
        make.left.equalTo(nameLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-80);
        make.height.equalTo(15);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
