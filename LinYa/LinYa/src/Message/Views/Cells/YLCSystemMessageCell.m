//
//  YLCSystemMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSystemMessageCell.h"

@implementation YLCSystemMessageCell
{
    UIImageView *backgroundView;
    UILabel *titleLabel;
    UILabel *timeLabel;
    UILabel *messageLabel;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"system_background"]];
    
    [self addSubview:backgroundView];
    
//    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(63, 67, 68)];
//
//    titleLabel.text = @"系统2.0更新提醒";
//
//    [backgroundView addSubview:titleLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:16 color:RGB(54, 56, 58)];
    
    [backgroundView addSubview:timeLabel];
    
    messageLabel = [YLCFactory createLabelWithFont:15 color:RGB(54, 56, 58)];
    
    messageLabel.numberOfLines = 0;
    
    [backgroundView addSubview:messageLabel];
}
- (void)setDataDict:(NSDictionary *)dataDict{
    if ([YLCFactory isStringOk:dataDict[@"add_time"]]) {
        timeLabel.text = [YLCTool normalChangedWithTimeString:dataDict[@"add_time"]];
    }else{
        timeLabel.text = @"";
    }
    
    if ([YLCFactory isStringOk:dataDict[@"content"]]) {
        messageLabel.text = dataDict[@"content"];
    }else{
        messageLabel.text = @"";
    }
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_left);
        make.top.equalTo(timeLabel.mas_bottom).offset(20);
        make.width.equalTo(weakSelf.frameWidth-40);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
//    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(weakSelf.mas_bottom);
//        make.left.equalTo(weakSelf.mas_left);
//        make.width.equalTo(weakSelf.frameWidth);
//        make.height.equalTo(3);
//    }];
}
@end
