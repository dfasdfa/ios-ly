//
//  YLCSystemMessageView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSystemMessageView.h"
#import "YLCSystemMessageCell.h"
#define MESSAGE_CELL  @"YLCSystemMessageCell"
@interface YLCSystemMessageView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCSystemMessageView
{
    UICollectionView *messageTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [messageTable registerClass:[YLCSystemMessageCell class] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self addSubview:messageTable];
}
- (void)setMessageList:(NSArray *)messageList{
    _messageList = messageList;
    [messageTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _messageList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCSystemMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MESSAGE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_messageList.count) {
        cell.dataDict = _messageList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 128);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
