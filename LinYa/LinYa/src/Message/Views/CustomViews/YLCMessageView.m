//
//  YLCMessageView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMessageView.h"
#import "YLCMessageCell.h"

#define MESSAGE_CELL  @"YLCMessageCell"
@interface YLCMessageView()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCMessageView
{
    UITableView *messageTable;
    BOOL replyFinish;
    BOOL systemFinish;
    BOOL commentFinish;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        messageTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        messageTable.backgroundColor = [UIColor whiteColor];
        
        messageTable.delegate = self;
        
        messageTable.dataSource = self;
        
        messageTable.tableFooterView = [[UIView alloc] init];
        
        self.selectSignal = [RACSubject subject];
        
        [self addSubview:messageTable];
        
        [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];
    }
    return self;
}
- (void)setUnreadCount:(NSString *)unreadCount{
    _unreadCount = unreadCount;
    dispatch_async(dispatch_get_main_queue(), ^{
        [messageTable reloadData];
    });
    
}
- (void)setLastMessage:(NSString *)lastMessage{
    _lastMessage = lastMessage;
    dispatch_async(dispatch_get_main_queue(), ^{
        [messageTable reloadData];
    });
}
- (void)setReplyList:(NSArray *)replyList{
    _replyList = replyList;
    [messageTable reloadData];
}
- (void)setSystemList:(NSArray *)systemList{
    _systemList = systemList;

    [messageTable reloadData];
}
- (void)setCommentList:(NSArray *)commentList{
    _commentList = commentList;
    [messageTable reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YLCMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:MESSAGE_CELL];
    
    if (!cell) {
        cell = [[YLCMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MESSAGE_CELL];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (indexPath.row==0) {
//        cell.iconName = @"message_chat";
//        cell.title = @"我的聊天消息";
//        cell.unreadCount = _unreadCount;
//        cell.message = _lastMessage;
    }
    if (indexPath.row==0) {
        if (!JK_IS_ARRAY_NIL(_commentList)) {
            NSDictionary *dict = _commentList[0];
            cell.message = dict[@"content"];
            cell.timeString = dict[@"add_time"];
        }
        
        cell.iconName = @"message_re_mine";

        cell.title = @"回复\"我的\"消息";
    }
    if (indexPath.row==1) {
        if (!JK_IS_ARRAY_NIL(_replyList)) {
            NSDictionary *dict = _replyList[0];
            cell.message = dict[@"content"];
            cell.timeString = dict[@"add_time"];
        }
        
        cell.iconName = @"message_send_me";

        cell.title = @"给我留言的消息";
    }
    if (indexPath.row==2) {
        if (!JK_IS_ARRAY_NIL(_systemList)) {
            NSDictionary *dict = _systemList[0];
            cell.message = dict[@"content"];
            cell.timeString = dict[@"add_time"];
        }
        
        cell.iconName = @"message_system";

        cell.title = @"系统消息";
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [self.selectSignal sendNext:indexPath];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
