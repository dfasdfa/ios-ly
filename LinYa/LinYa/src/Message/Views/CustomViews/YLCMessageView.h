//
//  YLCMessageView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCRefrshTableView.h"
@interface YLCMessageView : UIView
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,strong)NSArray *commentList;
@property (nonatomic ,strong)NSArray *replyList;
@property (nonatomic ,strong)NSArray *systemList;
@property (nonatomic ,copy)NSString *unreadCount;
@property (nonatomic ,copy)NSString *lastMessage;
@end
