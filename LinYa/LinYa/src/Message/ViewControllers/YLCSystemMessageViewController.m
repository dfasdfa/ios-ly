//
//  YLCSystemMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSystemMessageViewController.h"
#import "YLCSystemMessageView.h"
@interface YLCSystemMessageViewController ()
@property (nonatomic ,strong)YLCSystemMessageView *messageView;
@end

@implementation YLCSystemMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    self.messageView = [[YLCSystemMessageView alloc] initWithFrame:[YLCFactory layoutFrame]];
    
    self.messageView.messageList = _systemList;
    
    [self.view addSubview:self.messageView];
}
- (NSString *)title{
    return @"系统消息";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
