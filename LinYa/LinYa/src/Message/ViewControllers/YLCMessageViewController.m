//
//  YLCMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMessageViewController.h"
#import "YLCMessageView.h"
#import "YLCSystemMessageViewController.h"
#import "YLCChatBaseViewController.h"
#import "YLCRYConversationListViewController.h"
#import "YLCReplyMessageViewController.h"
#import <RongIMLib/RongIMLib.h>
#import "RYMessageManager.h"
@interface YLCMessageViewController ()
@property (nonatomic ,strong)YLCMessageView *messageView;
@end

@implementation YLCMessageViewController
{
    NSArray *commentList;
    NSArray *replyList;
    NSArray *systemList;
    BOOL hasLoad;
}
- (YLCMessageView *)messageView{
    if (!_messageView) {
        _messageView = [[YLCMessageView alloc] initWithFrame:[YLCFactory layoutFrame]];
    }
    return _messageView;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self getUnreadCount];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.messageView beganRefresh];
    [self createCustomView];
    [self bindViewModel];
    [self observeMessage];
}
- (void)observeMessage{
    [[RYMessageManager shareManager].messageSignal subscribeNext:^(id x) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getUnreadCount];
        });
    }];
}
- (void)getUnreadCount{

    int count = [[RCIMClient sharedRCIMClient] getUnreadCount:@[@(ConversationType_PRIVATE)]];
    self.messageView.unreadCount = [NSString stringWithFormat:@"%d",    count];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
    NSArray *conversationList = [[RCIMClient sharedRCIMClient]
                                 getConversationList:@[@(ConversationType_PRIVATE),
                                                       @(ConversationType_DISCUSSION),
                                                       @(ConversationType_GROUP),
                                                       @(ConversationType_SYSTEM),
                                                       @(ConversationType_APPSERVICE),
                                                       @(ConversationType_PUBLICSERVICE)]];
    if (conversationList.count>0) {
        RCConversation *conver = conversationList[0];
        
        if ([conver.objectName isEqualToString:@"RC:ImgMsg"]) {
            self.messageView.lastMessage = @"[图片]";
        }else if ([conver.objectName isEqualToString:@"RC:TxtMsg"]){
            self.messageView.lastMessage = [conver.lastestMessage conversationDigest];
        }else{
            self.messageView.lastMessage = @"[音视频]";
        }
    }
}
- (void)initDataSource{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/comment" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.commentList = @[];
            return ;
        }
        commentList = responseObject;
        self.messageView.commentList = commentList;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/commentReply" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.replyList = @[];
            return ;
        }
        replyList = responseObject;
        self.messageView.replyList = replyList;
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"outType":@"0",@"page":@"0",@"token":userModel.token} method:@"POST" urlPath:@"app/message/system" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            self.messageView.systemList = @[];
            return ;
        }
        systemList = responseObject;
        self.messageView.systemList = systemList;
    }];
}
- (void)createCustomView{
    self.messageView.frame = CGRectMake(0, 0, self.view.frameWidth, 150);
    [self.view addSubview:self.messageView];
    
}
- (void)bindViewModel{
    

    
//    [self.messageView.reloadSignal subscribeNext:^(id x) {
//        [self initDataSource];
//    }];
}
- (NSString *)title{
    return @"消息";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
