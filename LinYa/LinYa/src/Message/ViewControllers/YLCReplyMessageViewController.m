//
//  YLCReplyMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReplyMessageViewController.h"
#import "YLCReplyMessageCell.h"
#import "YLCDynamicInfoViewController.h"
#import "YLCVideoLiftInfoViewController.h"
#define MESSAGE_CELL @"YLCReplyMessageCell"
@interface YLCReplyMessageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCReplyMessageViewController
{
    UICollectionView *messageTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
}

- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = [UIColor whiteColor];
    
//    [messageTable registerClass:[YLCReplyMessageCell class] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    [messageTable registerNib:[UINib nibWithNibName:@"YLCReplyMessageCell" bundle:nil] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self.view addSubview:messageTable];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (JK_IS_ARRAY_NIL(_messageList)) {
        return 0;
    }
    return _messageList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCReplyMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MESSAGE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_messageList.count) {
        cell.type = _type;
        cell.dict = _messageList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 80);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = _messageList[indexPath.row];
    NSString *type = dict[@"type"];
    NSString *caseId = dict[@"relate_id"];
    if ([type isEqualToString:@"1"]||[type isEqualToString:@"4"]||[type isEqualToString:@"2"]) {
        [self gotoDynamicInfoWithType:type caseId:caseId];
    }
    if ([type isEqualToString:@"3"]) {
        [self gotoVideoInfoWithType:type caseId:caseId];
    }
}
- (void)gotoDynamicInfoWithType:(NSString *)type
                         caseId:(NSString *)caseId{
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    if ([type isEqualToString:@"1"]) {
        con.type = 0;
    }
    if ([type isEqualToString:@"4"]) {
        con.type = 4;
    }
    if ([type isEqualToString:@"2"]) {
        con.type = 5;
    }
    con.caseId = caseId;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)gotoVideoInfoWithType:(NSString *)type
                       caseId:(NSString *)caseId{
    YLCVideoLiftInfoViewController *con = [[YLCVideoLiftInfoViewController alloc] init];
    
    if ([type isEqualToString:@"3"]) {
        con.type = 0;
    }
    con.video_id = caseId;
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    if (_type==0) {
        return @"回复我的消息";
    }else{
        return @"评论我的消息";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
