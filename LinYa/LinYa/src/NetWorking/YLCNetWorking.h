//
//  YLCNetWorking.h
//  LinYa
//
//  Created by 初程程 on 2018/3/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCNetWorking : NSObject
+ (void)loadNetServiceWithParam:(id)param
                         method:(NSString *)method
                        urlPath:(NSString *)urlPath
                       delegate:(id)delegate
                       response:(void (^)(id responseObject ,NSError *error))responseBlock;
+ (void)loadThireNetServiceWithParam:(id)param
                              method:(NSString *)method
                             urlPath:(NSString *)urlPath
                            delegate:(id)delegate
                            response:(void (^)(id responseObject ,NSError *error))responseBlock;
+ (void)uploadImageWithParam:(id)param
                   imageList:(NSArray *)imgList
                    fileName:(NSString *)fileName
                      method:(NSString *)method
                     urlPath:(NSString *)urlPath
                    delegate:(id)delegate
                    response:(void (^)(id responseObject ,NSError *error))responseBlock;
@end
