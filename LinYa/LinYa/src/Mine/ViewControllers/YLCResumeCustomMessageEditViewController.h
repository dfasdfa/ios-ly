//
//  YLCResumeCustomMessageEditViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCResumeModel.h"
@interface YLCResumeCustomMessageEditViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *resumeId;
@property (nonatomic ,strong)YLCResumeModel *resumeModel;
@end
