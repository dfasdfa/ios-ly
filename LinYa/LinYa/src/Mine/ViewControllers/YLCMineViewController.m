//
//  YLCMineViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMineViewController.h"
#import "YLCMineHeadView.h"
#import "YLCSelectCell.h"
#import "YLCTitleAndIconModel.h"
#import "YLCMineCell.h"
#import "YLCCustomModel.h"
#import "YLCEasyBuyManagerViewController.h"
#import "YLCSetViewController.h"
#define MINE_HEAD    @"YLCMineHeadView"
#define MINE_SELECT  @"YLCSelectCell"
#define MINE_CELL    @"YLCMineCell"
@interface YLCMineViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong)UICollectionView *mineTable;
@end

@implementation YLCMineViewController
{
    YLCMineHeadView *headView;
    NSArray *cellDataList;
    BOOL shouldReload;
    YLCCustomModel *customModel;
}
- (UIBarButtonItem *)rightBarButtonItem{
    return [YLCFactory createImageBarButtonWithImageName:@"" selector:@selector(gotoSet) target:self];
}
- (void)gotoSet{
    YLCSetViewController *con = [[YLCSetViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (UICollectionView *)mineTable{
    if (!_mineTable) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 0.5;
        
        _mineTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        _mineTable.backgroundColor = YLC_COMMON_BACKCOLOR;
        
        [_mineTable registerClass:[YLCSelectCell class] forCellWithReuseIdentifier:MINE_SELECT];
        
        [_mineTable registerClass:[YLCMineCell class] forCellWithReuseIdentifier:MINE_CELL];
        
        [_mineTable registerClass:[YLCMineHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MINE_HEAD];
        
        _mineTable.delegate = self;
        
        _mineTable.dataSource = self;
    }
    return _mineTable;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self reloadData];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundColor:RGB(250, 170, 54)];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    NSArray *titleArr = @[@"易沟通"];
    
    NSArray *subArr = @[@""];
    
    for (int i = 0; i<1; i++) {
        
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = titleArr[i];
        
        model.subText = subArr[i];
        
        [container addObject:model];
    }
    cellDataList = container.copy;
}
- (void)reloadData{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(model.user_id)||JK_IS_STR_NIL(model.token)) {
        [headView configUIWithModel:nil];
        return;
    }
    
    if ([YLCFactory isStringOk:model.token]) {
        [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id} method:@"POST" urlPath:@"app/user/allDetail" delegate:self response:^(id responseObject, NSError *error) {
            if (error) {
                [[YLCAccountManager shareManager] removeAccount];
                [headView configUIWithModel:nil];
            }else{
                customModel = [YLCCustomModel modelWithDict:responseObject];
                
                [headView configUIWithModel:customModel];
                
                [_mineTable reloadData];
            }
        }];
    }

}
- (void)createCustomView{
    [self.view addSubview:self.mineTable];
    [self.mineTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return cellDataList.count+1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<1) {
        YLCSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MINE_SELECT forIndexPath:indexPath];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        cell.titleColor = RGB(119, 121, 122);
        
        cell.subColor = RGB(118, 123, 124);
        
        if (indexPath.row<cellDataList.count) {
            YLCTitleAndIconModel *model = cellDataList[indexPath.row];
            
            cell.title = model.title;
            
            if (indexPath.row==0) {
                cell.subString = customModel.ygt[@"end_day"];
            }
        }
        
        return cell;
    }else{
        YLCMineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MINE_CELL forIndexPath:indexPath];
        
        cell.target = self;
        
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MINE_HEAD forIndexPath:indexPath];
        
        return headView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 270);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<cellDataList.count) {
        return CGSizeMake(self.view.frameWidth, 60);
    }else{
        return CGSizeMake(self.view.frameWidth, 230);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCEasyBuyManagerViewController *con = [[YLCEasyBuyManagerViewController alloc] init];
        
        con.ygt = customModel.ygt;
        
        [self.navigationController pushViewController:con animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
