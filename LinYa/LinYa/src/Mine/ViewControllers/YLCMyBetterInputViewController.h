//
//  YLCMyBetterInputViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCResumeModel.h"
@interface YLCMyBetterInputViewController : YLCBaseViewController

@property (nonatomic ,strong)YLCResumeModel *resumeModel;
@end
