//
//  YLCExpListViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCExpListViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,assign)NSInteger type; //1为工作经历 2为学习经历
@property (nonatomic ,copy)NSString *resumeId;
@end
