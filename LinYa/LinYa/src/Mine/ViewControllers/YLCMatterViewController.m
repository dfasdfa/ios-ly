//
//  YLCMatterViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMatterViewController.h"
#import "YLCMatterCell.h"
#import "YLCMatterModel.h"
#import "YLCMatterInfoViewController.h"
#import "YLCWebViewViewController.h"
#import "YLCNewTypeView.h"
#define MATTER_CELL  @"YLCMatterCell"
@interface YLCMatterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCMatterViewController
{
    UICollectionView *matterTable;
    NSArray *matterList;
    UIButton *typeBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    if (_type==0) {
        [self loadTopList];
    }else if(_type==1){
        [self loadInfoList];
    }else{
        [self loadDataList];
    }
}
- (void)loadTopList{
}
- (void)loadDataList{
    if (JK_IS_STR_NIL(_categoryId)) {
        _categoryId = @"";
    }
    [self reloadMaterialWithPage:@"0" categoryId:_categoryId];
}
- (void)reloadMaterialWithPage:(NSString *)page
                    categoryId:(NSString *)categoryId{
    [YLCNetWorking loadNetServiceWithParam:@{@"page":page,@"categoryId":categoryId} method:@"POST" urlPath:@"app/material/dataLists" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            
            return ;
            
        }
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            
            YLCMatterModel *model = [YLCMatterModel modelWithDict:dict];
            
            [container addObject:model];
            
        }
        
        matterList = container.copy;
        
        [matterTable reloadData];
        
    }];
}
- (void)loadInfoList{
    [YLCNetWorking loadNetServiceWithParam:@{@"page":@"0"} method:@"POST" urlPath:@"app/material/infoLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            
            YLCMatterModel *model = [YLCMatterModel modelWithDict:dict];
            
            [container addObject:model];
            
        }
        
        matterList = container.copy;
        
        [matterTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    matterTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    matterTable.backgroundColor = [UIColor whiteColor];
    
    [matterTable registerNib:[UINib nibWithNibName:@"YLCMatterCell" bundle:nil] forCellWithReuseIdentifier:MATTER_CELL];
    
    matterTable.delegate = self;
    
    matterTable.dataSource = self;
    
    [self.view addSubview:matterTable];
    
    [matterTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    typeBtn = [YLCFactory createBtnWithImage:@"matter_type"];
    
    [typeBtn addTarget:self action:@selector(gotoTypeView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:typeBtn];
    WS(weakSelf)
    [typeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.view.mas_right).offset(-50);
        make.bottom.equalTo(weakSelf.view.mas_bottom).offset(-70);
        make.width.height.equalTo(60);
    }];
    if (_type==2) {
        typeBtn.hidden = NO;
    }else{
        typeBtn.hidden = YES;
    }
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return matterList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMatterCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MATTER_CELL forIndexPath:indexPath];
    
    if (indexPath.row<matterList.count) {
        
        cell.model = matterList[indexPath.row];
        
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 100);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMatterModel *model = matterList[indexPath.row];
//
    YLCMatterInfoViewController *con = [[YLCMatterInfoViewController alloc] init];

    con.infoModel = model;

    [self.navigationController pushViewController:con animated:YES];
}
- (void)gotoTypeView{
    YLCNewTypeView *newView = [[YLCNewTypeView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, self.view.frameHeight)];
    
    [newView.reloadSignal subscribeNext:^(id x) {
        [self reloadMaterialWithPage:@"0" categoryId:x];
    }];
    
    [newView show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
