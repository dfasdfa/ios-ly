//
//  YLCMyBetterInputViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyBetterInputViewController.h"

@interface YLCMyBetterInputViewController ()

@end

@implementation YLCMyBetterInputViewController
{
    UILabel *tipLabel;
    UITextView *inputView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    tipLabel = [YLCFactory createLabelWithFont:12 color:RGB(155,163,165)];
    
    [self.view addSubview:tipLabel];
    
    inputView = [[UITextView alloc] initWithFrame:CGRectZero];
    
    inputView.backgroundColor = [UIColor whiteColor];
    
    inputView.placeholder = @"请在此输入您的优势和特长....";
    
    inputView.placeholderAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:RGB(189,197,200)};
    
    [self.view addSubview:inputView];
    
    WS(weakSelf)
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(10);
        make.top.equalTo(weakSelf.view.mas_top).offset(10);
    }];
    
    [inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(tipLabel.mas_left);
        make.top.equalTo(tipLabel.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.view.frameWidth-20);
        make.height.equalTo(150);
    }];
    
    UIButton *saveBtn = [YLCFactory createCommondBtnWithTitle:@"保存" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [saveBtn addTarget:self action:@selector(saveBetter) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:saveBtn];
    
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(10);
        make.right.equalTo(weakSelf.view.mas_right).offset(-10);
        make.top.equalTo(inputView.mas_bottom).offset(20);
        make.height.equalTo(40);
    }];
}
- (void)saveBetter{
    if (JK_IS_STR_NIL(_resumeModel.id)) {
        _resumeModel.id = @"0";
    }
    if (JK_IS_STR_NIL(inputView.text)) {
        [SVProgressHUD showErrorWithStatus:@"请输入您的优势"];
        return;
    }
    YLCAccountModel *accountModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":accountModel.token,@"userId":accountModel.user_id,@"realName":_resumeModel.real_name,@"resumeId":_resumeModel.id,@"birthday":_resumeModel.birthday,@"sex":_resumeModel.sex,@"education":_resumeModel.education,@"work_year":_resumeModel.work_year,@"mobile":_resumeModel.mobile,@"advantage":inputView.text} method:@"POST" urlPath:@"app/resume/doInfo" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (NSString *)title{
    return @"我的优势";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
