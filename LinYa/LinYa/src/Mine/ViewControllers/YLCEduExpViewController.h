//
//  YLCEduExpViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCEduExpViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,copy)NSString *workId;
@property (nonatomic ,copy)NSString *resumeId;
@property (nonatomic ,assign)BOOL isEdit;
@end
