//
//  YLCMineSetViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/26.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMineSetViewController.h"
#import "YLCChangePasswordViewController.h"
#import "YLCTabBarViewController.h"
#import "AppDelegate.h"
@interface YLCMineSetViewController ()

@end

@implementation YLCMineSetViewController
{
    UIView *backView;
    UILabel *titleLabel;
    UIImageView *subArrowView;
    UIButton *logoutBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
}
- (void)createCustomView{
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, 29, self.view.frameWidth, 40)];
    
    backView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backView];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoChangePassword)];
    
    [backView addGestureRecognizer:ges];
    
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(215, 215, 215)];
    
    titleLabel.text = @"修改密码";
    
    [backView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left).offset(10);
        make.centerY.equalTo(backView.mas_centerY);
    }];
    
    subArrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
    
    [backView addSubview:subArrowView];
    
    [subArrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(backView.mas_right).offset(-10);
        make.centerY.equalTo(backView.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(13);
    }];
    
    logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    logoutBtn.backgroundColor = [UIColor whiteColor];
    
    logoutBtn.layer.borderColor = YLC_GRAY_COLOR.CGColor;
    
    logoutBtn.layer.borderWidth = 0.5;
    
    logoutBtn.layer.cornerRadius = 20;
    
    logoutBtn.layer.masksToBounds = YES;
    
    [logoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    
    [logoutBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
    
    [logoutBtn addTarget:self action:@selector(gotoLogout) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:logoutBtn];
    
    [logoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backView.mas_centerX);
        make.top.equalTo(backView.mas_bottom).offset(20);
        make.width.equalTo(backView.frameWidth-40);
        make.height.equalTo(40);
    }];
}
- (void)gotoChangePassword{
    YLCChangePasswordViewController *con = [[YLCChangePasswordViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)gotoLogout{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(userModel.user_id)&&JK_IS_STR_NIL(userModel.token)) {
        return;
    }
    
    [[YLCAccountManager shareManager] removeAccount];
    [SVProgressHUD showSuccessWithStatus:@"退出成功！"];
    [self.navigationController popToRootViewControllerAnimated:NO];
    YLCTabBarViewController *tabbar = (YLCTabBarViewController *)[AppDelegate appDelegate].window.rootViewController;
    tabbar.selectedIndex = 0;
}
- (NSString *)title{
    return @"设置";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
