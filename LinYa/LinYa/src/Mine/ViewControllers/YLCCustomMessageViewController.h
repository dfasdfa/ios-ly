//
//  YLCCustomMessageViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCCustomModel.h"
@interface YLCCustomMessageViewController : YLCBaseViewController
@property (nonatomic ,strong)YLCCustomModel *model;
@end
