//
//  YLCWithDrawViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWithDrawViewController.h"

@interface YLCWithDrawViewController ()

@end

@implementation YLCWithDrawViewController
{
    UIView *topBackgroundView;
    UIView *bottomBackgroundView;
    UILabel *accountTitleLabel;
    UIImageView *subArrowView;
    UILabel *moneyTitleLabel;
    UILabel *tipLabel;
    UITextField *moneyField;
    UIImageView *lineView;
    UILabel *currentMoneyLabel;
    UILabel *cutLabel;
    UIButton *withDrawBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
}
- (void)createCustomView{
    
    bottomBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frameWidth, 110)];
    
    bottomBackgroundView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:bottomBackgroundView];
    
    moneyTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(47, 49, 59)];
    
    moneyTitleLabel.text = @"提现金额";
    
    [bottomBackgroundView addSubview:moneyTitleLabel];
    
    [moneyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomBackgroundView.mas_left).offset(10);
        make.top.equalTo(bottomBackgroundView.mas_top).offset(15);
    }];
    
    tipLabel = [YLCFactory createLabelWithFont:12 color:RGB(120, 125, 126)];
    
    tipLabel.text = @"＊提现需审核，通过后提现至您的微信零钱里";
    
    [self.view addSubview:tipLabel];
    
    moneyField = [[UITextField alloc] init];
    
    moneyField.placeholder = @"请输入提现金额";
    
    moneyField.leftViewMode = UITextFieldViewModeAlways;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
    label.text = @"￥";
    
    label.textColor = RGB(47, 49, 59);
    
    label.font = [UIFont systemFontOfSize:15];
    
    moneyField.leftView = label;
    
    [bottomBackgroundView addSubview:moneyField];
    
    [moneyField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyTitleLabel.mas_left);
        make.top.equalTo(moneyTitleLabel.mas_bottom).offset(20);
        make.width.equalTo(bottomBackgroundView.frameWidth-20);
        make.height.equalTo(30);
    }];
    
    currentMoneyLabel = [YLCFactory createLabelWithFont:12 color:RGB(130, 138, 140)];
    
    currentMoneyLabel.text = [NSString stringWithFormat:@"当前可提现余额：￥%@",_leftMoney];
    
    [bottomBackgroundView addSubview:currentMoneyLabel];
    
    [currentMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(bottomBackgroundView.mas_bottom).offset(-10);
        make.left.equalTo(moneyField.mas_left);
    }];
    
    cutLabel = [YLCFactory createLabelWithFont:12 color:RGB(130, 138, 140)];
    
    cutLabel.text = @"提现金额需大于100元";
    
    [bottomBackgroundView addSubview:cutLabel];
    
    [cutLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomBackgroundView.mas_right).offset(-10);
        make.centerY.equalTo(currentMoneyLabel.mas_centerY);
    }];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomBackgroundView.mas_bottom).offset(10);
        make.left.equalTo(bottomBackgroundView.mas_left).offset(20);
    }];
    
    withDrawBtn = [YLCFactory createCommondBtnWithTitle:@"提现" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    [withDrawBtn addTarget:self action:@selector(withDrawSuccess) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:withDrawBtn];
    
    [withDrawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bottomBackgroundView.mas_centerX);
        make.top.equalTo(tipLabel.mas_bottom).offset(10);
        make.width.equalTo(self.view.frameWidth-40);
        make.height.equalTo(45);
    }];
}
- (void)withDrawSuccess{
    if ([moneyField.text floatValue]<100) {
        [SVProgressHUD showErrorWithStatus:@"提现金额必须大于100元"];
        return;
    }
    if ([moneyField.text floatValue]>[_leftMoney floatValue]) {
        [SVProgressHUD showErrorWithStatus:@"提现金额不能大于余额"];
        return;
    }
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"money":moneyField.text} method:@"POST" urlPath:@"app/wxPay/userWithdraw" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [YLCHUDShow showBuySuccessHUDWithTip:@"提现申请提交成功！"];
    }];
    
}
- (NSString *)title{
    return @"提现";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
