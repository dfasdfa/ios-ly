//
//  YLCEditCustomMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEditCustomMessageViewController.h"

@interface YLCEditCustomMessageViewController ()

@end

@implementation YLCEditCustomMessageViewController
{
    UITextField *messageField;
    UILabel *tipLabel;
    UIButton *confirmBtn;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.reloadSignal = [RACSubject subject];
    messageField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, self.view.frameWidth, 50)];
    
    messageField.backgroundColor = [UIColor whiteColor];
    
    messageField.placeholder = @"请输入内容";
    
    messageField.text = _text;
    
    messageField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 50)];
    
    messageField.leftView = leftView;
    
    messageField.leftViewMode = UITextFieldViewModeAlways;
    
    [self.view addSubview:messageField];
    
    tipLabel = [YLCFactory createLabelWithFont:12 color:RGB(138, 149, 155)];
    
    tipLabel.text = @"字数不超过25个字";
    
    [self.view addSubview:tipLabel];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(messageField.mas_left).offset(30);
        make.top.equalTo(messageField.mas_bottom).offset(11);
    }];
    
    confirmBtn = [YLCFactory createCommondBtnWithTitle:@"提交" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [confirmBtn addTarget:self action:@selector(gotoConfirmPost) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:confirmBtn];
    WS(weakSelf)
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(messageField.mas_left).offset(14);
        make.top.equalTo(tipLabel.mas_bottom).offset(42);
        make.width.equalTo(weakSelf.view.frameWidth-28);
        make.height.equalTo(42);
    }];
}
- (void)gotoConfirmPost{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(messageField.text)||[messageField.text isEqualToString:_text]) {
        [YLCHUDShow showAlertViewWithMessage:@"请输入新内容" title:@"提示"];
        return;
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id,_key:messageField.text} method:@"POST" urlPath:@"app/user/updateUser" delegate:self response:^(id responseObject, NSError *error) {
        if (!error) {
            [SVProgressHUD showSuccessWithStatus:@"添加成功"];
//            [self.reloadSignal sendNext:@{@"index":[NSString stringWithFormat:@"%ld",_index],@"text":messageField.text}];
            [self.delegate editCustomMessageToDict:@{@"index":[NSString stringWithFormat:@"%ld",_index],@"text":messageField.text}];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (NSString *)title{
    return @"编辑内容";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
