//
//  YLCMyOrderViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyOrderChildViewController.h"
#import "YLCMyOrderCell.h"
#import "YLCMyOrderDetailViewController.h"
#define ORDER_CELL @"YLCMyOrderCell"
@interface YLCMyOrderChildViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCMyOrderChildViewController
{
    UICollectionView *orderTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    orderTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    orderTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [orderTable registerClass:[YLCMyOrderCell class] forCellWithReuseIdentifier:ORDER_CELL];
    
    orderTable.delegate = self;
    
    orderTable.dataSource = self;
    
    [self.view addSubview:orderTable];
    
    [orderTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCMyOrderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ORDER_CELL forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 203);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMyOrderDetailViewController *con = [[YLCMyOrderDetailViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
