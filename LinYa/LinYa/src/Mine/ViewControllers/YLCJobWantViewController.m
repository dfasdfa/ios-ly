//
//  YLCJobWantViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCJobWantViewController.h"
#import "YLCPickModel.h"
@interface YLCJobWantViewController ()

@end

@implementation YLCJobWantViewController
{
    UIView *backView;
    UILabel *moneyTitleLabel;
    UILabel *moneyField;
    UIView *jobBackView;
    UILabel *jobTitleLabel;
    UILabel *jobLabel;
    UIView *jobAreaBackView;
    UILabel *areaTitleLabel;
//    UILabel *areaLabel;
    UILabel *areaInputField;
    NSString *locationId;
    NSArray *moneyList;
    NSString *moneyString;
    NSArray *jobList;
    NSString *jobString;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/salary" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            [container addObject:dict[@"salary_name"]];
        }
        moneyList = [container copy];
    }];
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobList = responseObject;
    }];
}
- (void)createCustomView{
    backView = [[UIView alloc] init];
    
    backView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backView];
    
    moneyTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(133,139,141)];
    
    
    moneyTitleLabel.text = @"期望薪资:";
    
    [backView addSubview:moneyTitleLabel];
    
    moneyField = [YLCFactory createLabelWithFont:15 color:RGB(47, 49, 50)];
    
    moneyField.text = @"请输入您的期望薪资";
    moneyField.userInteractionEnabled = YES;
    [backView addSubview:moneyField];
    
    UITapGestureRecognizer *moneyGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMoneyList)];
    
    [moneyField addGestureRecognizer:moneyGes];
    
    jobBackView = [[UIView alloc] init];
    
    [backView addSubview:jobBackView];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showJobList)];
    
    [jobBackView addGestureRecognizer:ges];
    
    jobTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(133,139,141)];
    
    jobTitleLabel.text = @"期望职位:";
    
    [jobBackView addSubview:jobTitleLabel];
    
    jobLabel = [YLCFactory createLabelWithFont:15 color:RGB(47,49,50)];
    
    jobLabel.text = @"请选择期望的职业";
    
    [jobBackView addSubview:jobLabel];
    
    jobAreaBackView = [[UIView alloc] init];
    
    jobAreaBackView.backgroundColor = [UIColor whiteColor];
    
    [backView addSubview:jobAreaBackView];
    
    areaTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(133,139,141)];
    
    areaTitleLabel.text = @"求职区域:";
    
    [jobAreaBackView addSubview:areaTitleLabel];
    
//    areaLabel = [YLCFactory createLabelWithFont:15 color:RGB(205,212,215)];
    
    areaInputField = [YLCFactory createLabelWithFont:15 color:RGB(47, 49, 50)];
    
    areaInputField.text = @"请选择期望的求职区域";
    
    areaInputField.userInteractionEnabled = YES;
    
    [jobAreaBackView addSubview:areaInputField];
    
    UITapGestureRecognizer *areaGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAreaList)];
    
    [areaInputField addGestureRecognizer:areaGes];
    
    WS(weakSelf)
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(weakSelf.view.mas_left);
        
        make.top.equalTo(weakSelf.view.mas_top).offset(10);
        
        make.right.equalTo(weakSelf.view.mas_right);
        
        make.height.equalTo(181);
    }];
    
    [moneyTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left).offset(10);
        make.top.equalTo(backView.mas_top);
        make.width.equalTo(70);
        make.height.equalTo(60);
    }];
    
    [moneyField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyTitleLabel.mas_right).offset(10);
        make.top.equalTo(backView.mas_top);
        make.right.equalTo(backView.mas_right).offset(-10);
        make.height.equalTo(60);
    }];
    
    [jobBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left);
        make.right.equalTo(backView.mas_right).offset(-10);
        make.top.equalTo(moneyField.mas_bottom).offset(0.5);
        make.height.equalTo(60);
    }];
    
    [jobTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobBackView.mas_left).offset(10);
        make.centerY.equalTo(jobBackView.mas_centerY);
    }];
    
    [jobLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(jobTitleLabel.mas_centerY);
    }];
    
    [jobAreaBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backView.mas_left);
        make.right.equalTo(backView.mas_right);
        make.top.equalTo(jobBackView.mas_bottom).offset(0.5);
        make.height.equalTo(60);
    }];
    
    [areaTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobAreaBackView.mas_left).offset(10);
        make.centerY.equalTo(jobAreaBackView.mas_centerY);
    }];
    
    [areaInputField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaTitleLabel.mas_right).offset(10);
        make.centerY.equalTo(jobAreaBackView.mas_centerY);
        make.width.equalTo(weakSelf.view.frameWidth-150);
        make.height.equalTo(60);
    }];
    
    UIButton *saveBtn = [YLCFactory createCommondBtnWithTitle:@"保存" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [saveBtn addTarget:self action:@selector(saveJobWant) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:saveBtn];
    
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left).offset(10);
        make.right.equalTo(weakSelf.view.mas_right).offset(-10);
        make.top.equalTo(backView.mas_bottom).offset(20);
        make.height.equalTo(40);
    }];

}
- (void)showMoneyList{
    [self showMoneyPicker:moneyList index:0];
}
- (void)showJobList{
    if (JK_IS_ARRAY_NIL(jobList)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    RACSubject *eduSubject = [RACSubject subject];

    [YLCHUDShow showCustomMessageHUDWithTitleList:jobList signal:eduSubject isMore:YES];
    
    [eduSubject subscribeNext:^(id x) {
        NSString *selectMessageString = @"";
        for (NSString *selectString in x) {
            selectMessageString = [selectMessageString stringByAppendingString:@" "];
            selectMessageString = [selectMessageString stringByAppendingString:jobList[[selectString integerValue]]];
        }
        
        jobLabel.text = selectMessageString;
        
        jobString = selectMessageString;
    }];
}
- (void)saveJobWant{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    if (JK_IS_STR_NIL(_resumeId)) {
        _resumeId = @"";
    }
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"resumeId":_resumeId,@"salary":moneyField.text,@"position":jobString,@"locationId":locationId} method:@"POST" urlPath:@"app/resume/doIntension" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"保存成功！"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (void)showAreaList{
    RACSubject *subject = [RACSubject subject];
    [YLCHUDShow showAreaPickWithSignal:subject];
    [subject subscribeNext:^(id x) {
        YLCAreaModel *model = x;
        
        areaInputField.text = model.areaname;

        locationId = model.id;
        
    }];
}
- (void)showMoneyPicker:(NSArray *)list index:(NSInteger)indexPath{
    if (JK_IS_ARRAY_NIL(list)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    YLCPickModel *pickModel = [[YLCPickModel alloc] init];
    pickModel.rowArr = list;
    pickModel.selectIndex = 0;
    RACSubject *signal = [RACSubject subject];
    [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
    [signal subscribeNext:^(id x) {
        YLCPickModel *model = x[0];
        moneyString = model.rowArr[model.selectIndex];
        moneyField.text = moneyString;
    }];
}
- (NSString *)title{
    return @"求职意向";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
