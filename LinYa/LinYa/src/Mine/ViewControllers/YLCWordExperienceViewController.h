//
//  YLCWordExperienceViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCWordExperienceViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,copy)NSString *eduId;
@property (nonatomic ,assign)BOOL isEdit;
@property (nonatomic ,copy)NSString *resumeId;
@end
