//
//  YLCMySendJobViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMySendJobViewController.h"
#import "YLCMySendJobCell.h"
#import "YLCInviteInfoViewController.h"
#define JOB_CELL @"YLCMySendJobCell"
@interface YLCMySendJobViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCMySendJobViewController
{
    UICollectionView *sendTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [self reloadDataWithPage:1];
}
- (void)reloadDataWithPage:(NSInteger)pageNum{
    NSString *page = [NSString stringWithFormat:@"%ld",pageNum];
    
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id,@"page":page} method:@"POST" urlPath:@"app/user/recruit" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    sendTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    sendTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [sendTable registerClass:[YLCMySendJobCell class] forCellWithReuseIdentifier:JOB_CELL];
    
    sendTable.delegate = self;
    
    sendTable.dataSource = self;
    
    [self.view addSubview:sendTable];
    
    [sendTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMySendJobCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:JOB_CELL forIndexPath:indexPath];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 118);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCInviteInfoViewController *con = [[YLCInviteInfoViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    return @"我发布的职位";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
