//
//  YLCResumeViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeViewController.h"
#import "YLCResumeHeadView.h"
#import "YLCJobResumeCell.h"
#import "YLCEducationResumeCell.h"
#import "YLCResumeMyBetterCell.h"
#import "YLCButtonFootView.h"
#import "YLCJobWantViewController.h"
#import "YLCResumeModel.h"
#import "YLCMyBetterInputViewController.h"
#import "YLCExpListViewController.h"
#define RESUME_HEAD  @"YLCResumeHeadView"
#define JOB_CELL     @"YLCJobResumeCell"
#define EDU_CELL     @"YLCEducationResumeCell"
#define BETTER_CELL  @"YLCResumeMyBetterCell"
#define BUTTON_FOOT  @"YLCButtonFootView"
@interface YLCResumeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCResumeViewController
{
    UICollectionView *resumeTable;
    YLCResumeModel *resumeModel;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCustomView];
}
- (void)initDataSource{
    [self loadMineResume];
}
- (void)loadMineResume{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    NSDictionary *param = @{};
    if (JK_IS_STR_NIL(_uid)) {
        _uid = @"";
    param=@{@"userId":userModel.user_id,@"token":userModel.token,@"uid":_uid};
    }else{
    param=@{@"uid":_uid,@"userId":userModel.user_id,@"token":userModel.token};
    }
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:@"app/resume/index" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        resumeModel = [YLCResumeModel modelWithDict:responseObject];
        
        [resumeTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    resumeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    resumeTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [resumeTable registerClass:[YLCJobResumeCell class] forCellWithReuseIdentifier:JOB_CELL];
    
    [resumeTable registerClass:[YLCEducationResumeCell class] forCellWithReuseIdentifier:EDU_CELL];
    
    [resumeTable registerClass:[YLCResumeMyBetterCell class] forCellWithReuseIdentifier:BETTER_CELL];
    
    [resumeTable registerClass:[YLCResumeHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:RESUME_HEAD];
    
    [resumeTable registerClass:[YLCButtonFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT];
    
    resumeTable.delegate = self;
    
    resumeTable.dataSource = self;
    
    [self.view addSubview:resumeTable];
    
    [resumeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCJobResumeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:JOB_CELL forIndexPath:indexPath];
        
        cell.model = resumeModel;
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCEducationResumeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:EDU_CELL forIndexPath:indexPath];
        
        cell.isEdit = _isExit;
        
        cell.index = indexPath.row;
        
        cell.dataList = resumeModel.work_list;
        
        return cell;
    }
    if (indexPath.row==2) {
        YLCEducationResumeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:EDU_CELL forIndexPath:indexPath];
        cell.isEdit = _isExit;
        
        cell.index = indexPath.row;
        
        cell.dataList = resumeModel.edu_list;
        
        return cell;
    }
    
    YLCResumeMyBetterCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BETTER_CELL forIndexPath:indexPath];
    
    cell.myBetter = resumeModel.advantage;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if (kind==UICollectionElementKindSectionHeader) {
        
        YLCResumeHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:RESUME_HEAD forIndexPath:indexPath];
        
        headView.resumeModel = resumeModel;
        
        headView.resumeType = _resumeType;
        
        return headView;
        
    }
    if (kind==UICollectionElementKindSectionFooter) {
        
        YLCButtonFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT forIndexPath:indexPath];
        [footView.footBtn addTarget:self action:@selector(gotoSave) forControlEvents:UIControlEventTouchUpInside];
        return footView;
        
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(self.view.frameWidth, 175);
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (!_isExit) {
        return CGSizeZero;
    }
    return CGSizeMake(self.view.frameWidth, 60);
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.view.frameWidth, 130);
    }
    if (indexPath.row==1) {
        if (JK_IS_ARRAY_NIL(resumeModel.work_list)) {
            return CGSizeMake(self.view.frameWidth, 100);
        }
        return CGSizeMake(self.view.frameWidth, 100+resumeModel.work_list.count*60);
    }
    if (indexPath.row==2) {
        if (JK_IS_ARRAY_NIL(resumeModel.edu_list)) {
            return CGSizeMake(self.view.frameWidth, 100);
        }
        return CGSizeMake(self.view.frameWidth, 100+resumeModel.edu_list.count*60);
    }
    if (indexPath.row==3) {
        CGSize size = [YLCFactory getStringSizeWithString:resumeModel.advantage width:self.view.frameWidth-20 font:15];
        return CGSizeMake(self.view.frameWidth, 60+size.height);
    }
    return CGSizeZero;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_resumeType==1) {
        return;
    }
    if (indexPath.row==0) {
        YLCJobWantViewController *con = [[YLCJobWantViewController alloc] init];
        
        con.resumeId = resumeModel.id;
        
        [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    }
    if (indexPath.row==1) {
        [self gotoExpListWithIndex:1];
    }
    if (indexPath.row==2) {
        [self gotoExpListWithIndex:2];
    }
    if (indexPath.row==3) {
        [self gotoAdvance];
    }
}
- (void)gotoSave{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)gotoExpListWithIndex:(NSInteger)index{
    
    if (_resumeType==1) {
        return;
    }
    
    YLCExpListViewController *con = [[YLCExpListViewController alloc] init];
    
    con.type = index;
    
    con.resumeId = resumeModel.id;
    
    if (index==1) {
        con.dataList = resumeModel.work_list;
    }else{
        con.dataList = resumeModel.edu_list;
    }
    
    [self.navigationController pushViewController:con animated:YES];
    
}
- (void)gotoAdvance{
    if (_resumeType==1) {
        return;
    }
    YLCMyBetterInputViewController *con = [[YLCMyBetterInputViewController alloc] init];
    
    con.resumeModel = resumeModel;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 10, 0);
}
- (NSString *)title{
    return @"简历";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
