//
//  YLCMyAccountViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCFundModel.h"
@interface YLCMyAccountDetailViewController : YLCBaseViewController
@property (nonatomic ,strong)YLCFundModel *model;
@end
