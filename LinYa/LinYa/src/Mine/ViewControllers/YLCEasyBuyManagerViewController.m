//
//  YLCEasyBuyManagerViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyBuyManagerViewController.h"
#import "YLCEasyReBuyCell.h"
#import "YLCEasyBuyTimeCell.h"
#import "YLCBuyRecordCell.h"
#import "YLCRechargeCenterViewController.h"
#import "YLCYgtModel.h"
#import "YLCPayViewController.h"
#define BUY_RECORD  @"YLCBuyRecordCell"
#define EASY_TIME   @"YLCEasyBuyTimeCell"
#define RE_BUY      @"YLCEasyReBuyCell"
@interface YLCEasyBuyManagerViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCEasyBuyManagerViewController
{
    UICollectionView *easyBuyTable;
    NSArray *buyList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"page":@"0"} method:@"POST" urlPath:@"app/user/sygt" delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            YLCYgtModel *model = [YLCYgtModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        buyList = [container copy];
        
        [easyBuyTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    easyBuyTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    easyBuyTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [easyBuyTable registerClass:[YLCEasyReBuyCell class] forCellWithReuseIdentifier:RE_BUY];
    
    [easyBuyTable registerClass:[YLCEasyBuyTimeCell class] forCellWithReuseIdentifier:EASY_TIME];
    
    [easyBuyTable registerClass:[YLCBuyRecordCell class] forCellWithReuseIdentifier:BUY_RECORD];
    
    easyBuyTable.delegate = self;

    easyBuyTable.dataSource = self;
    
    [self.view addSubview:easyBuyTable];
    
    [easyBuyTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCEasyBuyTimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:EASY_TIME forIndexPath:indexPath];
        
        cell.endTime = _ygt[@"end_day"];
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCEasyReBuyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RE_BUY forIndexPath:indexPath];
        
        return cell;
    }
    if (indexPath.row==2) {
        YLCBuyRecordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BUY_RECORD forIndexPath:indexPath];
        
        cell.buyList = buyList;
        
        return cell;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.view.frameWidth-20, 81);
    }
    if (indexPath.row==1) {
        return CGSizeMake(self.view.frameWidth-20, 61);
    }
    if (indexPath.row==2) {
        return CGSizeMake(self.view.frameWidth-20, buyList.count*44+84);
    }
    return CGSizeZero;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPayViewController *con = [[YLCPayViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 0, 0);
}
- (NSString *)title{
    return @"购买";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
