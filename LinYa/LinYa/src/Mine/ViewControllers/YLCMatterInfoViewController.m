//
//  YLCMatterInfoViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMatterInfoViewController.h"
#import "YLCMatterInfoHeadView.h"
#import "YLCWebLabelCell.h"
#define INFO_HEAD @"YLCMatterInfoHeadView"
#define MATTER_CELL  @"YLCWebLabelCell"
@interface YLCMatterInfoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCMatterInfoViewController
{
    UICollectionView *matterTable;
    UIView *bottomView;
    UILabel *payLabel;
    UILabel *downLoadNumberLabel;
    UIButton *downLoadBtn;
    UIWebView *webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initDataSource];
    [self createCustomView];
    [self setModel];
}
- (void)initDataSource{
//    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"" delegate:self response:^(id responseObject, NSError *error) {
//        if (error) {
//            return ;
//        }
//    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    matterTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    matterTable.backgroundColor = [UIColor whiteColor];
    
    [matterTable registerNib:[UINib nibWithNibName:@"YLCWebLabelCell" bundle:nil] forCellWithReuseIdentifier:MATTER_CELL];
    
    [matterTable registerClass:[YLCMatterInfoHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:INFO_HEAD];
    
    matterTable.delegate = self;
    
    matterTable.dataSource = self;
    
    [self.view addSubview:matterTable];
    
    [matterTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 50, 0));
    }];
    
    bottomView = [[UIView alloc] init];
    
    bottomView.backgroundColor = [UIColor whiteColor];
    
//    [self.view addSubview:bottomView];
    
    payLabel = [YLCFactory createLabelWithFont:14];
    
    [bottomView addSubview:payLabel];
    
    downLoadNumberLabel = [YLCFactory createLabelWithFont:14];
    
    [bottomView addSubview:downLoadNumberLabel];
    
//    downLoadBtn = [YLCFactory createCommondBtnWithTitle:@"下载" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
//    
//    [bottomView addSubview:downLoadBtn];
    
//    WS(weakSelf)
    
//    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(matterTable.mas_left);
//        make.right.equalTo(matterTable.mas_right);
//        make.bottom.equalTo(weakSelf.view.mas_bottom);
//        make.height.equalTo(50);
//    }];
//
//    [payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(bottomView.mas_left).offset(10);
//        make.centerY.equalTo(bottomView.mas_centerY);
//    }];
//
//    [downLoadNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(payLabel.mas_right).offset(5);
//        make.centerY.equalTo(payLabel.mas_centerY);
//    }];
    
//    [downLoadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(bottomView.mas_right);
//        make.centerY.equalTo(bottomView.mas_centerY);
//        make.width.equalTo(100);
//        make.height.equalTo(50);
//    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCWebLabelCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MATTER_CELL forIndexPath:indexPath];
    
    cell.content = _infoModel.content;
    
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        YLCMatterInfoHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:INFO_HEAD forIndexPath:indexPath];
     
        headView.model = _infoModel;
        
        return headView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 120);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *contentString = [self htmlEntityDecode:[NSString stringWithFormat:@"<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"/>%@",_infoModel.content]];
    NSAttributedString *attrStr = [self attributedStringWithHTMLString:contentString];
    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(self.view.frameWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return CGSizeMake(self.view.frameWidth, rect.size.height);
}
- (void)setModel{
    
    NSString *pay = [NSString stringWithFormat:@"需支付%@元",_infoModel.coin];
    NSAttributedString *payAttr = [YLCFactory getThreeCountAttributedStringWithString:pay FirstCount:3 secondCount:_infoModel.coin.length thirdCount:1 firstColor:RGB(51, 51, 51) secondColor:YLC_THEME_COLOR thirdColor:RGB(51, 51, 51) firstFont:15 secondFont:15 thirdFont:15];
    
    payLabel.attributedText = payAttr;
    
    NSString *downString = [NSString stringWithFormat:@"%@人已下载",_infoModel.down_num];
    
    NSAttributedString *downLoadAttr = [YLCFactory getAttributedStringWithString:downString FirstCount:_infoModel.down_num.length+1 secondCount:3 firstFont:15 secondFont:15 firstColor:YLC_THEME_COLOR secondColor:RGB(51, 51, 51)];
    
    downLoadNumberLabel.attributedText = downLoadAttr;
}
- (NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"&" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@";" intoString:&text];
        //替换字符
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    return html;
}
- (NSAttributedString *)attributedStringWithHTMLString:(NSString *)htmlString
{
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                               NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    
    return [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
}
//将 &lt 等类似的字符转化为HTML中的“<”等
- (NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}
- (NSString *)title{
    return @"资料详情";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
