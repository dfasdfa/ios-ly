//
//  YLCSetViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSetViewController.h"

@interface YLCSetViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation YLCSetViewController
{
    UITableView *setTable;
    NSArray *titleArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    titleArr = @[@"清除缓存",@"修改密码"];
}
- (void)createCustomView{
    setTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    setTable.backgroundColor = [UIColor whiteColor];
    
    setTable.delegate = self;
    
    setTable.dataSource = self;
    
    [self.view addSubview:setTable];
    
    [setTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return titleArr.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"set_table_cell"];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"set_table_cell"];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (indexPath.row<titleArr.count) {
        
        cell.textLabel.text = titleArr[indexPath.row];
        
    }
    if (indexPath.row==0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2fM",(float)[[SDImageCache sharedImageCache] getSize]/1024/1024];
    }
    
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 50)];
    
    UIButton *logoutBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:7 title:@"退出登录" backgroundColor:[UIColor whiteColor] textColor:YLC_THEME_COLOR borderColor:YLC_THEME_COLOR borderWidth:0.5];
 
    [logoutBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    
    [footView addSubview:logoutBtn];
    
    [logoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(footView.mas_centerX);
        make.centerY.equalTo(footView.mas_centerY);
        make.width.equalTo(footView.frameWidth-40);
        make.height.equalTo(40);
    }];
    
    return footView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        
    }
}
- (void)cleanMemoryAndDisk{
    
    [[SDImageCache sharedImageCache] clearMemory];
    
    [SVProgressHUD showSuccessWithStatus:@"清理完成"];
    
    [setTable reloadData];
}
- (void)logout{
    
    [[YLCAccountManager shareManager] removeAccount];
    
    [SVProgressHUD showSuccessWithStatus:@"退出成功"];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSString *)title{
    return @"设置";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
