//
//  YLCMyAccountViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyAccountDetailViewController.h"
#import "YLCAccountHeadView.h"
#import "YLCAccountCell.h"
#define ACCOUNT_HEAD @"YLCAccountHeadView"
#define ACCOUNT_CELL @"YLCAccountCell"
@interface YLCMyAccountDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong)UICollectionView *accountTable;
@end

@implementation YLCMyAccountDetailViewController
- (UICollectionView *)accountTable{
    if (!_accountTable) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 0;
        
        _accountTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        _accountTable.backgroundColor = [UIColor clearColor];
        
        [_accountTable registerClass:[YLCAccountCell class] forCellWithReuseIdentifier:ACCOUNT_CELL];
        
        [_accountTable registerClass:[YLCAccountHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ACCOUNT_HEAD];
        
        _accountTable.delegate = self;
        
        _accountTable.dataSource = self;
    }
    return _accountTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
//    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
//    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id} method:@"POST" urlPath:@"app/user/account" delegate:self response:^(id responseObject, NSError *error) {
//
//    }];
}
- (void)createCustomView{
    [self.view addSubview:self.accountTable];
    
    [self.accountTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _model.details.count+1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCAccountCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ACCOUNT_CELL forIndexPath:indexPath];
    if (indexPath.row==0) {
        cell.title = @"时间";
        cell.middle = @"内容";
        cell.last = @"金额";
    }else{
        NSDictionary *dict = _model.details[indexPath.row-1];
        
        cell.title = dict[@"add_time"];
        NSString *typeString = @"";
        NSString *coin = dict[@"coin"];
        NSString *type = dict[@"type"];
        if ([coin floatValue]>0) {
            switch ([type integerValue]) {
                case 1:
                    typeString = @"充值";
                    break;
                case 2:
                    typeString = @"签到";
                    break;
                case 3:
                    typeString = @"打赏";
                    break;
                case 4:
                    typeString = @"发病例";
                    break;
                case 5:
                    typeString = @"发动态";
                    break;
                case 6:
                    typeString = @"发论坛";
                    break;
                case 7:
                    typeString = @"点赞";
                    break;
                case 8:
                    typeString = @"评论";
                    break;
                case 9:
                    typeString = @"回复";
                    break;
                default:
                    break;
            }
        }else{
            switch ([type integerValue]) {
                case 1:
                    typeString = @"打赏";
                    break;
                case 2:
                    typeString = @"发布供应商";
                    break;
                case 3:
                    typeString = @"发布招聘";
                    break;
                case 4:
                    typeString = @"发布培训";
                    break;
                case 5:
                    typeString = @"发布活动";
                    break;
                case 6:
                    typeString = @"发现下载";
                    break;
                case 7:
                    typeString = @"招聘置顶";
                    break;
                case 8:
                    typeString = @"招聘刷新";
                    break;
                default:
                    break;
            }

        }
        cell.middle = typeString;
        cell.last = dict[@"coin"];
    }
    
    cell.index = indexPath.row;
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if (kind==UICollectionElementKindSectionHeader) {
        
        YLCAccountHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ACCOUNT_HEAD forIndexPath:indexPath];
        
        headView.model = _model;
        
        return headView;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.view.frameWidth, 44);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return CGSizeMake(self.view.frameWidth, 100);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 0, 0);
}
- (NSString *)title{
    return @"账户明细";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
