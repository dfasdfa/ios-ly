//
//  YLCExpListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCExpListViewController.h"
#import "YLCResumeExpListCell.h"
#import "YLCWordExperienceViewController.h"
#import "YLCEduExpViewController.h"
#define EXP_CELL @"YLCResumeExpListCell"
@interface YLCExpListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCResumeExpListCellDelegate>

@end

@implementation YLCExpListViewController
{
    UICollectionView *listView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{

}
- (void)loadEduData{
    
}
- (void)loadWorkData{
    
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    listView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    listView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [listView registerNib:[UINib nibWithNibName:@"YLCResumeExpListCell" bundle:nil] forCellWithReuseIdentifier:EXP_CELL];
    
    listView.delegate = self;
    
    listView.dataSource = self;
    
    [self.view addSubview:listView];
    
    [listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCResumeExpListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:EXP_CELL forIndexPath:indexPath];
    if (_type==1) {
        NSDictionary *dict = _dataList[indexPath.row];
        cell.title1 = [NSString stringWithFormat:@"%@ ~ %@",dict[@"start_time"],dict[@"end_time"]];
        
        cell.title2 = dict[@"company_name"];
        
        cell.sub2 = dict[@"postion"];
    }else{
        NSDictionary *dict = _dataList[indexPath.row];
        
        cell.title1 = [NSString stringWithFormat:@"%@ 毕业",[YLCTool normalChangedWithTimeString:dict[@"end_time"]]];
        
        cell.sub1 = [NSString stringWithFormat:@"%@",dict[@"profession"]];
        
        cell.title2 = dict[@"school_name"];
        
        BOOL isFormal = [dict[@"is_formal"] isEqualToString:@"1"];
        
        cell.sub2 = [NSString stringWithFormat:@"%@ %@",isFormal?@"统招":@"非统招",dict[@"diploma_txt"]];
    }
    cell.delegate = self;
    cell.index = indexPath.row;
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (JK_IS_ARRAY_NIL(_dataList)) {
        return 0;
    }
    return _dataList.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 150);
}
- (void)deleteOrEditWithIsDelete:(BOOL)isDelete index:(NSInteger)index{
    NSDictionary *dict = _dataList[index];
    if (isDelete) {
        if (_type==1) {
            [self deleteWorkWithDict:dict];
        }else{
            [self deleteEduWithDict:dict];
        }
    }else{
        if (_type==1) {
            [self editWorkWithDict:dict];
        }else{
            [self editEduWithDict:dict];
        }
    }
}
- (void)deleteWorkWithDict:(NSDictionary *)dict {
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"workId":dict[@"id"],@"resumeId":_resumeId} method:@"POST" urlPath:@"app/resume/delWorkExperience" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error) {
            
            return ;
            
        }
        
        [SVProgressHUD showSuccessWithStatus:@"删除成功！"];
        
        NSMutableArray *container = [_dataList mutableCopy];
        
        [container removeObject:dict];
        
        [listView reloadData];
        
    }];
}
- (void)deleteEduWithDict:(NSDictionary *)dict{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"eduId":dict[@"id"],@"resumeId":_resumeId} method:@"POST" urlPath:@"app/resume/delWorkExperience" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error) {
            
            return ;
            
        }
        
        [SVProgressHUD showSuccessWithStatus:@"删除成功！"];
        
        NSMutableArray *container = [_dataList mutableCopy];
        
        [container removeObject:dict];
        
        [listView reloadData];
        
    }];
}
- (void)editWorkWithDict:(NSDictionary *)dict{
    YLCEduExpViewController *con = [[YLCEduExpViewController alloc] init];
    
    con.workId = dict[@"id"];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)editEduWithDict:(NSDictionary *)dict{
    YLCWordExperienceViewController *con = [[YLCWordExperienceViewController alloc] init];
    
    con.eduId = dict[@"id"];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    if (_type==1) {
        return @"工作经历";
    }else{
        return @"学习经历";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
