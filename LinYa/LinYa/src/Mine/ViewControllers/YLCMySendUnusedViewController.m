//
//  YLCMySendUnusedViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMySendUnusedViewController.h"
#import "YLCMyOrderCell.h"
#import "YLCGoodsModel.h"
#import "YLCDynamicInfoViewController.h"
#define ORDER_CELL  @"YLCMyOrderCell"
@interface YLCMySendUnusedViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCMyOrderCellDelegate>

@end

@implementation YLCMySendUnusedViewController
{
    UICollectionView *unusedTable;
    NSArray *goodList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [self loadGoodsWithPageNumber:@"0"];
}
- (void)loadGoodsWithPageNumber:(NSString *)pageNumber
{
    
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"page":pageNumber,@"userId":model.user_id,@"outFollow":@""} method:@"POST" urlPath:@"app/user/goods" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in list) {
            
            YLCGoodsModel *model = [YLCGoodsModel modelWithDict:dict];
            
            model.goodsDescription = dict[@"description"];
            
            model.goodsId = dict[@"id"];
            
            [container addObject:model];
        }
        
        goodList = container.copy;
        
        [unusedTable reloadData];
        
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    unusedTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    unusedTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [unusedTable registerClass:[YLCMyOrderCell class] forCellWithReuseIdentifier:ORDER_CELL];
    
    unusedTable.delegate = self;
    
    unusedTable.dataSource = self;
    
    [self.view addSubview:unusedTable];
    
    [unusedTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return goodList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMyOrderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ORDER_CELL forIndexPath:indexPath];
    
    if (indexPath.row<goodList.count) {
        
        YLCGoodsModel *model = goodList[indexPath.row];
        
        cell.orderTitle = [NSString stringWithFormat:@"物品编号：%@",model.goodsId];
        
        
//        @property (nonatomic ,copy)NSString *iconPath;
//        @property (nonatomic ,copy)NSString *goodsTitle;
//        @property (nonatomic ,copy)NSAttributedString *priceAttr;
//        @property (nonatomic ,copy)NSString *refundTitle;
//        @property (nonatomic ,copy)NSString *quickTitle;
        
        cell.iconPath = model.main_img;
        
        cell.goodsTitle = model.title;
        
        NSString *string = [NSString stringWithFormat:@"￥%@(含运费%@元)",model.price,@"0.00"];
        
        cell.priceAttr = [YLCFactory getThreeCountAttributedStringWithString:string FirstCount:1 secondCount:model.price.length thirdCount:string.length-model.price.length-1 firstColor:YLC_THEME_COLOR secondColor:YLC_THEME_COLOR thirdColor:RGB(183, 183, 183) firstFont:12 secondFont:16 thirdFont:14];
        
        if ([model.is_publish isEqualToString:@"1"]) {
            cell.refundTitle = @"我要下架";
            cell.publish = @"0";
        }else{
            cell.refundTitle = @"我要上架";
            cell.publish = @"1";
        }
        cell.caseId = model.goodsId;
        cell.isPublish = model.is_publish;
        
        cell.quickTitle = @"确认发货";
        
        cell.delegate = self;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 200);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCGoodsModel *model = goodList[indexPath.row];
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.caseId = model.goodsId;
    
    con.type = 4;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)didChangeSuccess{
    [self loadGoodsWithPageNumber:@"0"];
}
- (NSString *)title{
    return @"发布的闲置物品";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
