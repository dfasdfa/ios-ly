//
//  YLCMyOrderDetailViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyOrderDetailViewController.h"
#import "YLCLogisticsProgressCell.h"
#import "YLCOrderCustomMessageCell.h"
#import "YLCOrderGoodsMessageCell.h"
#import "YLCCustomLeaveWordCell.h"
#import "YLCOrderDetailFootView.h"
#define LOGIS_PROGRESS  @"YLCLogisticsProgressCell"
#define CUSTOM_MESSAGE  @"YLCCustomMessageCell"
#define ORDER_GOODS     @"YLCOrderGoodsMessageCell"
#define LEAVE_WORD      @"YLCCustomLeaveWordCell"
#define DETAIL_FOOT     @"YLCOrderDetailFootView"
@interface YLCMyOrderDetailViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>

@end

@implementation YLCMyOrderDetailViewController
{
    UICollectionView *detailTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    detailTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    detailTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [detailTable registerClass:[YLCLogisticsProgressCell class] forCellWithReuseIdentifier:LOGIS_PROGRESS];
    
    [detailTable registerClass:[YLCOrderCustomMessageCell class] forCellWithReuseIdentifier:CUSTOM_MESSAGE];
    
    [detailTable registerClass:[YLCOrderGoodsMessageCell class] forCellWithReuseIdentifier:ORDER_GOODS];
    
    [detailTable registerClass:[YLCCustomLeaveWordCell class] forCellWithReuseIdentifier:LEAVE_WORD];
    
    [detailTable registerClass:[YLCOrderDetailFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:DETAIL_FOOT];
    
    detailTable.delegate = self;
    
    detailTable.dataSource = self;
    
    [self.view addSubview:detailTable];
    
    [detailTable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsZero);
        
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 4;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        
        YLCLogisticsProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOGIS_PROGRESS forIndexPath:indexPath];
        
        cell.progress = 0;
        
        return cell;
        
    }
    
    if (indexPath.row==1) {
        
        YLCOrderCustomMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CUSTOM_MESSAGE forIndexPath:indexPath];
        
        return cell;
    }
    
    if (indexPath.row==2) {
        
        YLCOrderGoodsMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ORDER_GOODS forIndexPath:indexPath];
        
        return cell;
    }
    
    YLCCustomLeaveWordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LEAVE_WORD forIndexPath:indexPath];
    
    return cell;
    
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionFooter) {
        YLCOrderDetailFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:DETAIL_FOOT forIndexPath:indexPath];
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 60);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        
        return CGSizeMake(self.view.frameWidth, 80);
    }
    if (indexPath.row==1) {
        
        return CGSizeMake(self.view.frameWidth, 50);
    }
    if (indexPath.row==2) {
        return CGSizeMake(self.view.frameWidth, 237);
    }
    return CGSizeMake(self.view.frameWidth, 113);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}
- (NSString *)title{
    return @"订单详情";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
