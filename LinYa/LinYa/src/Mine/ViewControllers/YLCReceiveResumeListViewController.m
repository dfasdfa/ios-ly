//
//  YLCReceiveResumeListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReceiveResumeListViewController.h"
#import "YLCReceiveResumeCell.h"
#import "YLCReceiveResumeModel.h"
#import "YLCResumeViewController.h"
@interface YLCReceiveResumeListViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation YLCReceiveResumeListViewController
{
    UICollectionView *resumeTable;
    NSArray *resumeList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self intiDataSource];
    [self createCustomView];
}
- (void)intiDataSource{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"page":@"0"} method:@"POST" urlPath:@"app/user/recieveResume" delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            YLCReceiveResumeModel *model = [YLCReceiveResumeModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        resumeList = container.copy;
        
        [resumeTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    resumeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    resumeTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [resumeTable registerNib:[UINib nibWithNibName:@"YLCReceiveResumeCell" bundle:nil] forCellWithReuseIdentifier:@"YLCReceiveResumeCell"];
    
    resumeTable.delegate = self;
    
    resumeTable.dataSource = self;
    
    [self.view addSubview:resumeTable];
    
    [resumeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return resumeList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCReceiveResumeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YLCReceiveResumeCell" forIndexPath:indexPath];
    
    if (indexPath.row<resumeList.count) {
        cell.model  =resumeList[indexPath.row];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self gotoResumeWithIndex:indexPath.row];
}
- (void)gotoResumeWithIndex:(NSInteger)index{
    YLCReceiveResumeModel *model = resumeList[index];
    
    YLCResumeViewController *con = [[YLCResumeViewController alloc] init];
    
    con.isExit = NO;
    
    con.resumeType = 1;
    
    con.uid = model.user[@"id"];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 100);
}
- (NSString *)title{
    return @"收到的简历";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
