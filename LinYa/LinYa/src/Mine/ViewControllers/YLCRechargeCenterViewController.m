//
//  YLCRechargeCenterViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRechargeCenterViewController.h"
#import "YLCRechargeMessageCell.h"
#import "YLCPayWayCell.h"
#import "YLCPayFootView.h"
#import "YLCPayModel.h"
#define RECHARGE_MESSAGE @"YLCRechargeMessageCell"
#define FOOT @"YLCPayFootView"
#define WAY_CELL @"YLCPayWayCell"
#import "WXApi.h"
@interface YLCRechargeCenterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCRechargeMessageCellDelegate,WXApiDelegate>

@end

@implementation YLCRechargeCenterViewController
{
    UICollectionView *rechargeTable;
    NSString *payWay;
    NSString *price;
    BOOL isReload;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    payWay = @"2";
    YLCPayModel *model = _payArr[0];
    price = model.price;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    rechargeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    rechargeTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [rechargeTable registerClass:[YLCPayWayCell class] forCellWithReuseIdentifier:WAY_CELL];
    
    [rechargeTable registerClass:[YLCRechargeMessageCell class] forCellWithReuseIdentifier:RECHARGE_MESSAGE];
    
    [rechargeTable registerClass:[YLCPayFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT];
    
    rechargeTable.delegate = self;
    
    rechargeTable.dataSource = self;
    
    [self.view addSubview:rechargeTable];
    
    [rechargeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCRechargeMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RECHARGE_MESSAGE forIndexPath:indexPath];
        
        cell.payArr = _payArr;
        
        cell.delegate = self;
        
        cell.rechargeType = _rechargeType;
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCPayWayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WAY_CELL forIndexPath:indexPath];
        if (!isReload) {
            isReload = YES;
            [cell.waySignal subscribeNext:^(id x) {
                NSIndexPath *indexPath = x;
                [self fetchPayWayWithIndex:indexPath.row];
            }];
        }
        cell.payWay = payWay;
        
        cell.rechargeType = _rechargeType;
        
        cell.subString = price;
        
        return cell;
    }
    return nil;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionFooter) {
        YLCPayFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT forIndexPath:indexPath];
        [footView.buyBtn addTarget:self action:@selector(gotoBuyProduct) forControlEvents:UIControlEventTouchUpInside];
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.view.frameWidth, 190);
    }
    if (indexPath.row==1) {
        if (_rechargeType==1) {
            return CGSizeMake(self.view.frameWidth, 136);
        }else{
            if ([payWay isEqualToString:@"3"]) {
                return CGSizeMake(self.view.frameWidth, 331);
            }else{
                return CGSizeMake(self.view.frameWidth, 281);
            }
        }
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 28+40);
}
- (void)gotoBuyProduct{
    if (_rechargeType==1) {
        [self wxPay];
    }
}
- (void)wxPay{
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"payMoney":price} method:@"POST" urlPath:@"app/wxPay/orderPay" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        [[YLCWxPayManager shareManager] wxPayWithParam:responseObject];
        
        [[YLCWxPayManager shareManager].paySignal subscribeNext:^(id x) {
            
        }];
    }];
}
- (void)onReq:(BaseReq *)req{
    
    if ([req isKindOfClass:[PayReq class]]) {
        
    }
}
- (void)fetchPayWayWithIndex:(NSInteger)index{
    if (index==0) {
        payWay = @"2";
    }
    if (index==1) {
        payWay = @"1";
    }
    if (index==2) {
        payWay = @"3";
    }

    [rechargeTable reloadData];
}
- (void)didSelectPayWithSelectIndex:(NSInteger)index{
    if (index<_payArr.count-1) {
        YLCPayModel *model = _payArr[index];
        
        price = model.price;
        [rechargeTable reloadData];
    }
    if (index == _payArr.count-1) {
        [YLCHUDShow showFieldAlertControllerWithTarget:self title:@"请输入金额" message:@"" placeholder:@"" leftTitle:@"确定" rightTitle:@"取消" confirmLeft:YES signal:^(NSString *fieldText) {
            price = fieldText;
            [rechargeTable reloadData];
        }];
    }
    
    
}
- (NSString *)title{
    return @"购买";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
