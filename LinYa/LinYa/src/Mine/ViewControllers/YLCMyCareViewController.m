//
//  YLCMyCareViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyCareViewController.h"
#import "YLCMyCareCell.h"
#import "DCRoundSwitch.h"
#import "DVSwitch.h"
#import "YLCCareModel.h"
#define MY_CARE @"YLCMyCareCell"
@interface YLCMyCareViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCMyCareCellDelegate>

@end

@implementation YLCMyCareViewController
{
    DVSwitch *careSwitch;
    UICollectionView *careTable;
    BOOL _isCare;
    NSArray *careList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    careList = @[];
    _isCare = YES;
    [self reloadDataWithIndex:0];
}
- (void)reloadDataWithIndex:(NSInteger)index{
    if (index==0) {
        [self loadMyCare];
    }else{
        [self loadCareMe];
    }
}
- (void)loadMyCare{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id} method:@"POST" urlPath:@"app/concern/follow" delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            careList = @[];
            [careTable reloadData];
            return ;
        }
        NSMutableArray *container = careList.mutableCopy;
        
        [container removeAllObjects];
        
        for (NSDictionary *dict in responseObject) {
            
            YLCCareModel *model = [YLCCareModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        careList = container.copy;
        
        [careTable reloadData];
    }];
}
- (void)loadCareMe{
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id} method:@"POST" urlPath:@"app/concern/fun" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            careList = @[];
            [careTable reloadData];
            return ;
        }
        NSMutableArray *container = careList.mutableCopy;
        
        [container removeAllObjects];
        
        for (NSDictionary *dict in responseObject) {
            
            YLCCareModel *model = [YLCCareModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        careList = container.copy;
        
        [careTable reloadData];
        
    }];
}
- (void)createCustomView{
    
    UIView *switchBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 60)];
    
    [self.view addSubview:switchBackView];
    
    if (_type==1) {
        switchBackView.hidden = YES;
    }
    
    careSwitch = [[DVSwitch alloc] initWithStringsArray:@[@"我关注的", @"关注我的"]];
    
    careSwitch.labelTextColorInsideSlider = [UIColor whiteColor];
    
    careSwitch.labelTextColorOutsideSlider = YLC_THEME_COLOR;
    
    careSwitch.sliderColor = YLC_THEME_COLOR;
    
    careSwitch.backgroundColor = [UIColor whiteColor];
    
    WS(weakSelf)
    
    [careSwitch setPressedHandler:^(NSUInteger index) {
        
        [weakSelf reloadDataWithIndex:index];
        
        _isCare = index==0;
        
    }];
    
    [switchBackView addSubview:careSwitch];
    
    [careSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(weakSelf.view.mas_top).offset(20);
        make.width.equalTo(170);
        make.height.equalTo(30);
    }];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    careTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    careTable.backgroundColor = [UIColor whiteColor];
    
    [careTable registerClass:[YLCMyCareCell class] forCellWithReuseIdentifier:MY_CARE];
    
    careTable.delegate = self;
    
    careTable.dataSource = self;
    
    [self.view addSubview:careTable];
    
    [careTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(_type==1?0:60, 0, 0, 0));
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return careList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMyCareCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MY_CARE forIndexPath:indexPath];
    cell.delegate = self;
    if (indexPath.row<careList.count) {
        cell.model = careList[indexPath.row];
    }
    cell.isCare = _isCare;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 84);
}
- (void)careOrCancelWithIsCare:(BOOL)isCare index:(NSInteger)index{
    YLCCareModel *model = careList[index];
    if (isCare) {
        [self carePersonWithModel:model];
    }else{
        [self cancelCareWithModel:model];
    }
}
- (void)carePersonWithModel:(YLCCareModel *)model{
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":model.id} method:@"POST" urlPath:@"app/concern/doFollow" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error) {
            return ;
        }
        
        [self reloadDataWithIndex:_isCare?0:1];
        [SVProgressHUD showSuccessWithStatus:@"关注成功！"];
        
    }];
    
}
- (void)cancelCareWithModel:(YLCCareModel *)model{
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":model.id} method:@"POST" urlPath:@"app/concern/cancelFollow" delegate:self response:^(id responseObject, NSError *error) {
        
        if (error) {
            return ;
        }
        [self reloadDataWithIndex:_isCare?0:1];
        [SVProgressHUD showSuccessWithStatus:@"取消成功！"];
        
    }];
    
}
- (NSString *)title{
    return @"我的关注";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
