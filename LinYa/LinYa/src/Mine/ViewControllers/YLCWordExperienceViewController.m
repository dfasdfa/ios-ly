//
//  YLCWordExperienceViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWordExperienceViewController.h"
#import "YLCSubFieldCell.h"
#import "YLCSelectCell.h"
#import "YLCSchoolMessageCell.h"
#import "YLCRecruitmentCell.h"
#import "YLCProgramAddCellModel.h"
#import "YLCProgramSelectModel.h"
#import "YLCEduFootView.h"
#import "YLCPickModel.h"
#define SUB_FIELD  @"YLCSubFieldCell"
#define SELECT_CELL  @"YLCSelectCell"
#define SCHOOL_MESSAGE @"YLCSchoolMessageCell"
#define RE_CELL      @"YLCRecruitmentCell"
#define FOOT_VIEW    @"YLCEduFootView"
@interface YLCWordExperienceViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCSubFieldCellDelegate,YLCEduFootViewDelegate,YLCRecruitmentCellDelegate,YLCSchoolMessageCellDelegate>

@end

@implementation YLCWordExperienceViewController
{
    UICollectionView *workTable;
    NSArray *cellArr;
    NSString *isFormal;
    NSString *schoolName;
    NSString *profession;
    NSString *startTime;
    NSString *endTimeString;
    NSString *diploma;
    NSArray *eduList;
    NSString *edu;
    NSString *eduIdenti;
    NSArray *allEduList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    isFormal = @"1";
    
    YLCProgramAddCellModel *schoolNameModel = [[YLCProgramAddCellModel alloc] init];
    
    schoolNameModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *schoolMessageModel = [[YLCProgramSelectModel alloc] init];
    
    schoolMessageModel.title  =@"学校名称";
    
    schoolMessageModel.placeholder = @"学校名称";
    
    schoolNameModel.cellModel = schoolMessageModel;
    
    YLCProgramAddCellModel *timeModel = [[YLCProgramAddCellModel alloc] init];
    
    timeModel.cellType = YLCTimeCellType;
    
    YLCProgramAddCellModel *specialtyModel = [[YLCProgramAddCellModel alloc] init];
    
    specialtyModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *specialtyMessageModel = [[YLCProgramSelectModel alloc] init];
    
    specialtyMessageModel.title = @"专业";
    
    specialtyMessageModel.placeholder = @"请输入专业";
    
    specialtyModel.cellModel = specialtyMessageModel;
    
    YLCProgramAddCellModel *eduModel = [[YLCProgramAddCellModel alloc] init];
    
    eduModel.cellType = YLCSelectCellType;
    
    YLCProgramSelectModel *eduMessageModel = [[YLCProgramSelectModel alloc] init];
    
    eduMessageModel.title = @"学历";
    
    eduMessageModel.placeholder = @"点击选择";
    
    eduModel.cellModel = eduMessageModel;
    
    YLCProgramAddCellModel *recruitmentModel = [[YLCProgramAddCellModel alloc] init];
    
    recruitmentModel.cellType = YLCDoubleSelectType;
    
    YLCProgramSelectModel *recruitMessageModel = [[YLCProgramSelectModel alloc] init];
    
    recruitMessageModel.title = @"统招";
    
    recruitMessageModel.subString = @"1";
    
    recruitmentModel.cellModel = recruitMessageModel;
    
    cellArr = @[schoolNameModel,timeModel,specialtyModel,eduModel,recruitmentModel];
    
    _dataList = cellArr;
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/education" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in responseObject) {
            [container addObject:dict[@"salary_name"]];
        }
        eduList = container.copy;
        allEduList = responseObject;
    }];

}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0.5;
    
    workTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    workTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [workTable registerClass:[YLCSubFieldCell class] forCellWithReuseIdentifier:SUB_FIELD];
    
    [workTable registerClass:[YLCSelectCell class] forCellWithReuseIdentifier:SELECT_CELL];
    
    [workTable registerClass:[YLCSchoolMessageCell class] forCellWithReuseIdentifier:SCHOOL_MESSAGE];
    
    [workTable registerClass:[YLCRecruitmentCell class] forCellWithReuseIdentifier:RE_CELL];
    
    UINib *nib = [UINib nibWithNibName:@"YLCEduFootView" bundle:nil];
    
    [workTable registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT_VIEW];
    
    workTable.delegate = self;
    
    workTable.dataSource = self;
    
    [self.view addSubview:workTable];
    
    [workTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramAddCellModel *model = _dataList[indexPath.row];
    
    if (model.cellType==YLCSubFieldCellType) {
        YLCSubFieldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SUB_FIELD forIndexPath:indexPath];
        
        YLCProgramSelectModel *cellModel = model.cellModel;
        
        if (indexPath.row==0) {
            cellModel.subString = schoolName;
        }else{
            cellModel.subString = profession;
        }
        
        cell.model = model.cellModel;
        
        cell.index = indexPath.row;
        
        cell.delegate = self;
        
        return cell;
    }
    if (model.cellType==YLCSelectCellType) {
        YLCSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SELECT_CELL forIndexPath:indexPath];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        cell.title = selectModel.title;
        
        cell.titleColor = RGB(133, 139, 141);
        
        cell.subColor = RGB(213, 213, 213);
        
        cell.subString = [YLCFactory isStringOk:edu]?edu:selectModel.placeholder;
        
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
    if (model.cellType==YLCTimeCellType) {
        YLCSchoolMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SCHOOL_MESSAGE forIndexPath:indexPath];
        
        cell.delegate = self;
        
        return cell;
    }
    YLCRecruitmentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RE_CELL forIndexPath:indexPath];
    
    cell.model = model.cellModel;
    
    cell.delegate = self;
    
    return cell;

}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCProgramAddCellModel *model = _dataList[indexPath.row];
    
    if (model.cellType==YLCDoubleSelectType) {
        
        return CGSizeMake(self.view.frameWidth, 60);
        
    }
    if (model.cellType==YLCTimeCellType) {
        return CGSizeMake(self.view.frameWidth, 100);
    }
    
    return CGSizeMake(self.view.frameWidth, 60);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionFooter) {
        YLCEduFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT_VIEW forIndexPath:indexPath];
        
        footView.delegate = self;
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 105);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        [self showEduList];
    }
}
- (void)subDidchangeWithTxt:(NSString *)text index:(NSInteger)index{
    if (index==0) {
        schoolName = text;
    }
    if (index==2) {
        profession = text;
    }
}
- (void)didClickWithIsDelete:(BOOL)isDelete{
    if (isDelete) {
        if (JK_IS_STR_NIL(_eduId)) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
            
            [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"eduId":_eduId,@"resumeId":_resumeId} method:@"POST" urlPath:@"app/resume/delWorkExperience" delegate:self response:^(id responseObject, NSError *error) {
                
                if (error) {
                    
                    return ;
                    
                }
                [SVProgressHUD showSuccessWithStatus:@"删除成功！"];
                [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:YES];
            }];
        }
    }else{
        [self saveWorkExp];
    }
}
- (void)didSelectAtIndex:(NSInteger)index{
    isFormal = [NSString stringWithFormat:@"%ld",index];
}
- (void)showEduList{
    if (JK_IS_ARRAY_NIL(eduList)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    
    YLCPickModel *pickModel = [[YLCPickModel alloc] init];
    pickModel.rowArr = eduList;
    pickModel.selectIndex = 0;
    RACSubject *signal = [RACSubject subject];
    [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
    [signal subscribeNext:^(id x) {
        YLCPickModel *model = x[0];
        edu = model.rowArr[model.selectIndex];
        NSDictionary *dict = allEduList[model.selectIndex];
        eduIdenti = [NSString stringWithFormat:@"%@",dict[@"id"]];
        [workTable reloadData];
    }];
}
- (void)sendTimeWithBeganTime:(NSString *)beganTime
                      endTime:(NSString *)endTime{
    startTime = beganTime;
    endTimeString = endTime;
}
- (void)saveWorkExp{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if (JK_IS_STR_NIL(_eduId)) {
        _eduId = @"";
    }
    if (JK_IS_STR_NIL(eduIdenti)) {
        eduIdenti = @"";
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"eduId":_eduId,@"schoolName":schoolName,@"profession":profession,@"startTime":startTime,@"endTime":endTimeString,@"diploma":eduIdenti,@"isFormal":isFormal} method:@"POST" urlPath:@"app/resume/doEduExperience" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        if (_isEdit) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功！"];
            
            [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:YES];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"添加成功！"];
            
            [self.navigationController popViewControllerAnimated:YES];
        }

    }];
}
- (NSString *)title{
    return @"教育经历";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
