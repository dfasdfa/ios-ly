//
//  YLCResumeViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCResumeViewController : YLCBaseViewController
@property (nonatomic ,assign)BOOL isExit;
@property (nonatomic ,assign)NSInteger resumeType; //0代表我的简历 1代表他人的
@property (nonatomic ,copy)NSString *uid;
@end
