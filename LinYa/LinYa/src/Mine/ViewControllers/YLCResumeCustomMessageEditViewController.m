//
//  YLCResumeCustomMessageEditViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeCustomMessageEditViewController.h"
#import "YLCCustomIconView.h"
#import "YLCRecruitmentCustomNameCell.h"
#import "YLCSubSelectCell.h"
#import "YLCRecruitmentPhoneCell.h"
#import "YLCButtonFootView.h"
#import "YLCPickModel.h"
#define ICON_HEAD  @"YLCCustomIconView"
#define NAME_CELL  @"YLCRecruitmentCustomNameCell"
#define SELECT_CELL @"YLCSubSelectCell"
#define PHONE_CELL @"YLCRecruitmentPhoneCell"
#define BUTTON_FOOT @"YLCButtonFootView"
@interface YLCResumeCustomMessageEditViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCRecruitmentCustomNameCellDelegate,YLCRecruitmentPhoneCellDelegate>

@end

@implementation YLCResumeCustomMessageEditViewController
{
    UICollectionView *messageTable;
    NSArray *dataArr;
    NSString *customSex;
    NSArray *titleArr;
    NSArray *placeholderArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *realName = [YLCFactory isStringOk:_resumeModel.real_name]?_resumeModel.real_name:@"";
    NSString *birthDay = [YLCFactory isStringOk:_resumeModel.birthday]?_resumeModel.birthday:@"";
    NSString *edu = [YLCFactory isStringOk:_resumeModel.education]?_resumeModel.education:@"";
    NSString *workYear = [YLCFactory isStringOk:_resumeModel.work_year]?_resumeModel.work_year:@"";
    NSString *phone = [YLCFactory isStringOk:_resumeModel.mobile]?_resumeModel.mobile:@"";
    dataArr = @[realName,birthDay,edu,workYear,phone];
    placeholderArr = @[@"",@"请选择生日",@"请选择学历",@"清选择工作经验",@""];
    if ([YLCFactory isStringOk:_resumeModel.sex]&&![_resumeModel.sex isEqualToString:@"0"]) {
        customSex = _resumeModel.sex;
    }else{
        customSex = @"1";
    }
    
    titleArr = @[@"姓  名",@"出生年月:",@"最高学历:",@"工作经验:",@"手机号码:"];
    [self createCustomView];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0.5;
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [messageTable registerClass:[YLCRecruitmentCustomNameCell class] forCellWithReuseIdentifier:NAME_CELL];
    
    [messageTable registerClass:[YLCRecruitmentPhoneCell class] forCellWithReuseIdentifier:PHONE_CELL];
    
    [messageTable registerClass:[YLCSubSelectCell class] forCellWithReuseIdentifier:SELECT_CELL];
    
    [messageTable registerClass:[YLCCustomIconView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ICON_HEAD];
    
    [messageTable registerClass:[YLCButtonFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self.view addSubview:messageTable];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCRecruitmentCustomNameCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NAME_CELL forIndexPath:indexPath];
        
        cell.name = dataArr[0];
        
        cell.sex = customSex;
        
        cell.delegate = self;
        
        return cell;
    }
    if (indexPath.row>0&&indexPath.row<4) {
        YLCSubSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SELECT_CELL forIndexPath:indexPath];
        
        cell.placeholder = placeholderArr[indexPath.row];
        
        cell.text = dataArr[indexPath.row];
        
        cell.title = titleArr[indexPath.row];
        
        return cell;
    }
    YLCRecruitmentPhoneCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHONE_CELL forIndexPath:indexPath];
    
    cell.title = titleArr[indexPath.row];
    
    cell.message = dataArr[indexPath.row];
    
    cell.delegate = self;
    
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        YLCCustomIconView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ICON_HEAD forIndexPath:indexPath];
        
        return headView;
    }
    if (kind==UICollectionElementKindSectionFooter) {
        YLCButtonFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT forIndexPath:indexPath];
        
        [footView.footBtn addTarget:self action:@selector(saveMessage) forControlEvents:UIControlEventTouchUpInside];
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 60);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 100);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 80);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==1) {
        RACSubject *timeSubject = [RACSubject subject];
        [YLCHUDShow showTimePickWithSignal:timeSubject];
        [timeSubject subscribeNext:^(id x) {
            [self changeDataWithMessage:x index:1];
        }];
    }
    if (indexPath.row==2) {
        RACSubject *eduSubject = [RACSubject subject];
        [YLCHUDShow showCustomMessageHUDWithTitleList:@[@"小学",@"大学",@"中专",@"大专",@"博士生",@"研究生",@"博士后"] signal:eduSubject isMore:NO];
        [eduSubject subscribeNext:^(id x) {
            [self changeDataWithMessage:x index:2];
        }];
    }
    if (indexPath.row==3) {
        YLCPickModel *pickModel = [[YLCPickModel alloc] init];
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (int i = 0; i<100; i++) {
            [container addObject:[NSString stringWithFormat:@"%d",i]];
        }
        pickModel.rowArr = container.copy;
        
        pickModel.selectIndex = 0;
        RACSubject *signal = [RACSubject subject];
        [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
        [signal subscribeNext:^(id x) {
            YLCPickModel *model = x[0];
            [self changeDataWithMessage:model.rowArr[model.selectIndex] index:3];
        }];
    }
}
- (void)phoneMessageDidChangedWithPhone:(NSString *)phoneNum{
    NSMutableArray *container = dataArr.mutableCopy;
    
    [container setObject:phoneNum atIndexedSubscript:4];
    
    dataArr = container.copy;
}
- (void)customNameMessageDidChangedWithName:(NSString *)name{
    NSMutableArray *container = dataArr.mutableCopy;
    
    [container setObject:name atIndexedSubscript:0];
    
    dataArr = container.copy;
}
- (void)sexDidChangedWithSex:(NSString *)sex{
    customSex = sex;
    [messageTable reloadData];
}
- (void)changeDataWithMessage:(NSString *)message
                        index:(NSInteger)index{
    NSMutableArray *container = dataArr.mutableCopy;
    
    [container setObject:message atIndexedSubscript:index];
    
    dataArr = container.copy;
    
    [messageTable reloadData];
}
- (void)saveMessage{
    YLCAccountModel *accountModel = [[YLCAccountManager shareManager] currentAccount];
    if (JK_IS_STR_NIL(_resumeId)) {
        _resumeId = @"";
    }
    if ([YLCFactory isStringOk:_resumeModel.advantage]) {
        _resumeModel.advantage = @"";
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"token":accountModel.token,@"userId":accountModel.user_id,@"realName":dataArr[0],@"resumeId":_resumeId,@"birthday":dataArr[1],@"sex":customSex,@"education":dataArr[2],@"work_year":dataArr[3],@"mobile":dataArr[4],@"advantage":_resumeModel.advantage} method:@"POST" urlPath:@"app/resume/doInfo" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (NSString *)title{
    return @"基本资料";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
