//
//  YLCMySendViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMySendViewController.h"
#import "YLCEasyCommunicateView.h"
#import "YLCTitleAndIconModel.h"
#import "YLCDentistDynamicViewController.h"
#import "YLCQuestionViewController.h"
#import "YLCMySendUnusedViewController.h"
#import "YLCVideoLifeViewController.h"
#import "YLCTrainRegistrationViewController.h"
#import "YLCMySendJobViewController.h"
#import "YLCInviteViewController.h"
@interface YLCMySendViewController ()

@end

@implementation YLCMySendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSArray *list = @[@"帖子",@"问答",@"闲置物品",@"招聘",@"培训",@"活动",@"小视频",@"",@""];
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (int i = 0; i<list.count; i++) {
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = list[i];
        
        model.iconName = [NSString stringWithFormat:@"send_%d",i+1];
        
        [container addObject:model];
    }
    
    YLCEasyCommunicateView *easyView = [[YLCEasyCommunicateView alloc] initWithFrame:CGRectZero];
    
    easyView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    easyView.imageSize = CGSizeMake(35, 32);
    
    easyView.cellSize = CGSizeMake((self.view.frameWidth-1.5)/3,(self.view.frameWidth-1.5)/3);
    
    easyView.listArr = container.copy;
    
    [easyView.touchSignal subscribeNext:^(id x) {
        NSIndexPath *indexPath = x;
        if (indexPath.row==0) {
            [self gotoCircle];
        }
        if (indexPath.row==1) {
            [self gotoQuestion];
        }
        if (indexPath.row==2) {
            [self gotoUnused];
        }
        if (indexPath.row==3) {
            [self gotoJob];
        }
        if (indexPath.row==4) {
            [self gotoMyTrain];
        }
        if (indexPath.row==5) {
            [self gotoActivity];
        }
        if (indexPath.row==6) {
            [self gotoVideoLift];
        }
    }];
    
    [self.view addSubview:easyView];
    
    WS(weakSelf)
    
    [easyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.top.equalTo(weakSelf.view.mas_top);
        make.width.equalTo(weakSelf.view.frameWidth);
        make.height.equalTo(weakSelf.view.frameWidth);
    }];
}
//我发布的职位
- (void)gotoJob{

    YLCInviteViewController *con = [[YLCInviteViewController alloc] init];
    
    con.isMine = YES;
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发的帖子
- (void)gotoCircle{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    YLCDentistDynamicViewController *con = [[YLCDentistDynamicViewController alloc] init];
    
    con.type = 1;
    
    con.userId = userModel.user_id;
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发的问题
- (void)gotoQuestion{
    YLCQuestionViewController *con = [[YLCQuestionViewController alloc] init];
    
    con.isMine = YES;
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发布的闲置
- (void)gotoUnused{
    
    YLCMySendUnusedViewController *con = [[YLCMySendUnusedViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发布的活动
- (void)gotoActivity{
    YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
    
    con.viewType = 1;
    
    con.type = 3;
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发布的视频
- (void)gotoVideoLift{
    
    YLCVideoLifeViewController *con = [[YLCVideoLifeViewController alloc] init];
    
    con.isMine = YES;
    
    [self.navigationController pushViewController:con animated:YES];
}
//我发布的培训
- (void)gotoMyTrain{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
    
    con.type = 3;
    
    con.viewType = 0;
    
    con.userId = userModel.user_id;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    return @"我的发布";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
