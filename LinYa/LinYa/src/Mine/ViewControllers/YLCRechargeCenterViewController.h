//
//  YLCRechargeCenterViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCRechargeCenterViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *payArr;
@property (nonatomic ,assign)NSInteger rechargeType; //0表示购买易购通 1表示充值
@end
