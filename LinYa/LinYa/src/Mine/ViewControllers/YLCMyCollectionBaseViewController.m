//
//  YLCMyCollectionBaseViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/5/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyCollectionBaseViewController.h"
#import "DLCustomSlideView.h"
#import "DLLRUCache.h"
#import "DLScrollTabbarView.h"
#import "YLCDentistDynamicViewController.h"
#import "YLCQuestionViewController.h"
#import "YLCTrainRegistrationViewController.h"
#import "YLCUnusedViewController.h"
@interface YLCMyCollectionBaseViewController ()<DLCustomSlideViewDelegate>
@property (nonatomic ,strong)DLCustomSlideView *slideView;
@end

@implementation YLCMyCollectionBaseViewController
{
    NSMutableArray *itemArray_;
}
- (DLCustomSlideView *)slideView{
    if (!_slideView) {
        _slideView = [[DLCustomSlideView alloc] initWithFrame:self.view.bounds];
    }
    return _slideView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *titleArr = @[@"帖子",@"问答",@"培训",@"活动",@"闲置"];
    DLLRUCache *cache = [[DLLRUCache alloc] initWithCount:titleArr.count];
    DLScrollTabbarView *tabbar = [[DLScrollTabbarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 42)];
    tabbar.backgroundColor = RGB(243, 243, 241);
    tabbar.tabItemNormalColor = RGB(110, 117, 119);
    tabbar.tabItemSelectedColor = YLC_THEME_COLOR;
    tabbar.tabItemNormalFontSize = 16.0f;
    tabbar.trackColor = YLC_THEME_COLOR;
    itemArray_ = [NSMutableArray array];
    for (int i=0; i<titleArr.count
         ; i++) {
        DLScrollTabbarItem *item = [DLScrollTabbarItem itemWithTitle:titleArr[i] width:self.view.frameWidth/titleArr.count];
        
        [itemArray_ addObject:item];
    }
    tabbar.tabbarItems = itemArray_;
    self.slideView.tabbar = tabbar;
    self.slideView.cache = cache;
    self.slideView.tabbarBottomSpacing = 5;
    self.slideView.baseViewController = self;
    self.slideView.delegate = self;
    self.slideView.hiddenPan = YES;
    [self.slideView setup];
    self.slideView.selectedIndex = 0;
    [self.view addSubview:self.slideView];
}
- (NSInteger)numberOfTabsInDLCustomSlideView:(DLCustomSlideView *)sender{
    return 5;
}
- (UIViewController *)DLCustomSlideView:(DLCustomSlideView *)sender controllerAt:(NSInteger)index{
    if (index==0) {
        YLCDentistDynamicViewController *con = [[YLCDentistDynamicViewController alloc] init];
        
        con.type = 2;
        
        return con;
    }
    if (index==1) {
        YLCQuestionViewController *con = [[YLCQuestionViewController alloc] init];
        
        con.type = 1;
        
        con.isMine = YES;
        
        return con;
    }
    if (index==2) {
        YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
        
        con.viewType = 0;
        
        con.type = 1;
        
        return con;
    }
    if (index==3) {
        YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
        
        con.viewType = 1;
        
        con.type = 1;
        
        return con;
    }
    if (index==4) {
        YLCUnusedViewController *con = [[YLCUnusedViewController alloc] init];
        
        con.showScreen = NO;
        
        con.type = 2;
        
        return con;
    }
    return [[YLCBaseViewController alloc] init];
}
- (NSString *)title{
    return @"我的收藏";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
