//
//  YLCEditCustomMessageViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
@protocol YLCEditCustomMessageDelegate <NSObject>

- (void)editCustomMessageToDict:(NSDictionary *)dict;

@end
@interface YLCEditCustomMessageViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *text;
@property (nonatomic ,copy)NSString *key;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,strong)RACSubject *reloadSignal;
@property (nonatomic ,weak)id<YLCEditCustomMessageDelegate>delegate;
@end
