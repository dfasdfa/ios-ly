//
//  YLCEduExpViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEduExpViewController.h"
#import "YLCSubFieldCell.h"
#import "YLCSelectCell.h"
#import "YLCSchoolMessageCell.h"
#import "YLCRecruitmentCell.h"
#import "YLCProgramAddCellModel.h"
#import "YLCProgramSelectModel.h"
#import "YLCEduFootView.h"
#import "YLCPickModel.h"
#define SUB_FIELD  @"YLCSubFieldCell"
#define SELECT_CELL  @"YLCSelectCell"
#define SCHOOL_MESSAGE @"YLCSchoolMessageCell"
#define RE_CELL      @"YLCRecruitmentCell"
#define FOOT_VIEW    @"YLCEduFootView"
@interface YLCEduExpViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCSubFieldCellDelegate,YLCEduFootViewDelegate,YLCSchoolMessageCellDelegate>

@end

@implementation YLCEduExpViewController
{
    UICollectionView *workTable;
    NSArray *cellArr;
    NSArray *jobList;
    NSString *jobString;
    NSString *companyName;
    NSString *beganTimeString;
    NSString *endTimeString;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    YLCProgramAddCellModel *schoolNameModel = [[YLCProgramAddCellModel alloc] init];
    
    schoolNameModel.cellType = YLCSubFieldCellType;
    
    YLCProgramSelectModel *schoolMessageModel = [[YLCProgramSelectModel alloc] init];
    
    schoolMessageModel.title  =@"公司名称";
    
    schoolMessageModel.placeholder = @"公司名称";
    
    schoolNameModel.cellModel = schoolMessageModel;
    
    YLCProgramAddCellModel *timeModel = [[YLCProgramAddCellModel alloc] init];
    
    timeModel.cellType = YLCTimeCellType;
    
    YLCProgramAddCellModel *specialtyModel = [[YLCProgramAddCellModel alloc] init];
    
    specialtyModel.cellType = YLCSelectCellType;
    
    YLCProgramAddCellModel *eduModel = [[YLCProgramAddCellModel alloc] init];
    
    eduModel.cellType = YLCSelectCellType;
    
    YLCProgramSelectModel *eduMessageModel = [[YLCProgramSelectModel alloc] init];
    
    eduMessageModel.title = @"职位名称";
    
    eduMessageModel.placeholder = @"职位名称";
    
    eduModel.cellModel = eduMessageModel;
    
    cellArr = @[schoolNameModel,eduModel,timeModel];
    
    _dataList = cellArr;
    
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/position" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        jobList = responseObject;
    }];

}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0.5;
    
    workTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    workTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [workTable registerClass:[YLCSubFieldCell class] forCellWithReuseIdentifier:SUB_FIELD];
    
    [workTable registerClass:[YLCSelectCell class] forCellWithReuseIdentifier:SELECT_CELL];
    
    [workTable registerClass:[YLCSchoolMessageCell class] forCellWithReuseIdentifier:SCHOOL_MESSAGE];
    
    [workTable registerClass:[YLCRecruitmentCell class] forCellWithReuseIdentifier:RE_CELL];
    
    UINib *nib = [UINib nibWithNibName:@"YLCEduFootView" bundle:nil];
    
    [workTable registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT_VIEW];
    
    workTable.delegate = self;
    
    workTable.dataSource = self;
    
    [self.view addSubview:workTable];
    
    [workTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramAddCellModel *model = _dataList[indexPath.row];
    
    if (model.cellType==YLCSubFieldCellType) {
        YLCSubFieldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SUB_FIELD forIndexPath:indexPath];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        selectModel.subString = companyName;
        
        cell.model = selectModel;
        
        cell.index = indexPath.row;
        
        cell.delegate = self;
        
        return cell;
    }
    if (model.cellType==YLCSelectCellType) {
        YLCSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SELECT_CELL forIndexPath:indexPath];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        cell.title = selectModel.title;
        
        cell.titleColor = RGB(133, 139, 141);
        
        cell.subColor = RGB(213, 213, 213);
        
        cell.subString = [YLCFactory isStringOk:jobString]?jobString:selectModel.placeholder;
        
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
    if (model.cellType==YLCTimeCellType) {
        YLCSchoolMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SCHOOL_MESSAGE forIndexPath:indexPath];
        
        cell.delegate = self;
        
        return cell;
    }
    YLCRecruitmentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RE_CELL forIndexPath:indexPath];
    
    cell.model = model.cellModel;
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCProgramAddCellModel *model = _dataList[indexPath.row];
    
    if (model.cellType==YLCDoubleSelectType) {
        
        return CGSizeMake(self.view.frameWidth, 60);
        
    }
    if (model.cellType==YLCTimeCellType) {
        return CGSizeMake(self.view.frameWidth, 100);
    }
    
    return CGSizeMake(self.view.frameWidth, 60);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionFooter) {
        YLCEduFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:FOOT_VIEW forIndexPath:indexPath];
        
        footView.delegate = self;
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 105);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        [self showJobList];
    }
}
- (void)subDidchangeWithTxt:(NSString *)text index:(NSInteger)index{
    companyName = text;
}
- (void)didClickWithIsDelete:(BOOL)isDelete{
    if (isDelete) {
        if (JK_IS_STR_NIL(_workId)) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            
        }
    }else{
        if (JK_IS_STR_NIL(_workId)) {
            _workId = @"";
        }
        if (JK_IS_STR_NIL(_resumeId)) {
            _resumeId = @"";
        }
        if (JK_IS_STR_NIL(companyName)) {
            [SVProgressHUD showErrorWithStatus:@"请输入公司名称"];
            return;
        }
        if (JK_IS_STR_NIL(jobString)) {
            [SVProgressHUD showErrorWithStatus:@"请选择职位名称"];
            return;
        }
        if (JK_IS_STR_NIL(beganTimeString)) {
            [SVProgressHUD showErrorWithStatus:@"请选择开始时间"];
            return;
        }
        if (JK_IS_STR_NIL(endTimeString)) {
            [SVProgressHUD showErrorWithStatus:@"请选择结束时间"];
            return;
        }
        
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
        
        [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"workId":_workId,@"resumeId":_resumeId,@"companyName":companyName,@"postion":jobString,@"startTime":beganTimeString,@"endTime":endTimeString} method:@"POST" urlPath:@"app/resume/doWorkExperience" delegate:self response:^(id responseObject, NSError *error) {
            if (error) {
                return ;
            }
            if (_isEdit) {
                [SVProgressHUD showSuccessWithStatus:@"修改成功！"];
                
                [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] animated:YES];
            }else{
                [SVProgressHUD showSuccessWithStatus:@"添加成功！"];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }

}
- (void)showJobList{
    if (JK_IS_ARRAY_NIL(jobList)) {
        [SVProgressHUD showErrorWithStatus:@"列表尚未加载成功，请稍后"];
        return;
    }
    
    YLCPickModel *pickModel = [[YLCPickModel alloc] init];
    pickModel.rowArr = jobList;
    pickModel.selectIndex = 0;
    RACSubject *signal = [RACSubject subject];
    [YLCHUDShow showPickerListWithList:@[pickModel] signal:signal];
    [signal subscribeNext:^(id x) {
        YLCPickModel *model = x[0];
        jobString = model.rowArr[model.selectIndex];
        [workTable reloadData];
    }];
}
- (void)sendTimeWithBeganTime:(NSString *)beganTime endTime:(NSString *)endTime{
    beganTimeString = beganTime;
    endTimeString = endTime;
}
- (NSString *)title{
    return @"工作经历";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
