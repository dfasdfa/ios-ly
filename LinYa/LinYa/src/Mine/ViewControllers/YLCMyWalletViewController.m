//
//  YLCMyWalletViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyWalletViewController.h"
#import "YLCMyAccountDetailViewController.h"
#import "YLCRechargeCenterViewController.h"
#import "YLCWithDrawViewController.h"
#import "YLCPayModel.h"
#import "YLCFundModel.h"
@interface YLCMyWalletViewController ()

@end

@implementation YLCMyWalletViewController
{
    UIImageView *walletIconView;
    UILabel *accountTitleLabel;
    UIImageView *balanceBackgroundView;
    UILabel *balanceLabel;
    UIButton *rechargeBtn;
    UILabel *rechargeTitleLabel;
    UIButton *withDrawBtn;
    UIImageView *questionView;
    YLCFundModel *fundModel;
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"交易明细" titleColor:RGB(255, 255, 255) selector:@selector(gotoDetail) target:self];
}

- (void)gotoDetail{
    
    YLCMyAccountDetailViewController *con = [[YLCMyAccountDetailViewController alloc] init];
    
    con.model = fundModel;
    
    [self.navigationController pushViewController:con animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCustomView];
    [self layoutFrame];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)initDataSource{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id} method:@"POST" urlPath:@"app/user/account" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        fundModel = [YLCFundModel modelWithDict:responseObject];
        
        NSString *text = [NSString stringWithFormat:@"￥：%@",fundModel.left_coin];
        
        NSAttributedString *attr = [YLCFactory getAttributedStringWithString:text FirstCount:2 secondCount:text.length-2 firstFont:20 secondFont:46 firstColor:RGB(101, 99, 99) secondColor:RGB(41, 39, 39)];
        
        balanceLabel.attributedText = attr;
    }];
}
- (void)createCustomView{
    self.view.backgroundColor = RGB(245, 245, 245);
    
    walletIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wallet_icon"]];
    
    [self.view addSubview:walletIconView];
    
    accountTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(142, 145, 146)];
    
    accountTitleLabel.text = @"账户余额（元）";
    
    [self.view addSubview:accountTitleLabel];
    
    balanceBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wallet_xian"]];
    
    [self.view addSubview:balanceBackgroundView];
    
    balanceLabel = [YLCFactory createLabelWithFont:43 color:RGB(41, 39, 39)];
    
    [balanceBackgroundView addSubview:balanceLabel];
    
    rechargeBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:22 title:@"充值" backgroundColor:YLC_THEME_COLOR textColor:[UIColor whiteColor] borderColor:[UIColor clearColor] borderWidth:0];
    
    [rechargeBtn addTarget:self action:@selector(gotoRecharge) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:rechargeBtn];
    
    withDrawBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:22 title:@"提现" backgroundColor:[UIColor whiteColor] textColor:RGB(112, 116, 117) borderColor:[UIColor clearColor] borderWidth:0];
    
    [withDrawBtn addTarget:self action:@selector(withDraw) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:withDrawBtn];
    
    questionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wallet_question"]];
    
    [self.view addSubview:questionView];
}
- (void)gotoRecharge{
    NSArray *titleArr = @[@"10",@"50",@"100",@"300",@"500",@"自定义"];
    
    NSArray *priceArr = @[@"10",@"50",@"100",@"300",@"500",@"0"];
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    for (int index = 0; index<titleArr.count; index++) {
        
        YLCPayModel *model = [[YLCPayModel alloc] init];
        
        model.title = titleArr[index];
        
        model.price = priceArr[index];
        
        [container addObject:model];
    }
    
    YLCRechargeCenterViewController *con = [[YLCRechargeCenterViewController alloc] init];
    
    con.rechargeType = 1;
    
    con.payArr = container.copy;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)withDraw{
    YLCWithDrawViewController *con = [[YLCWithDrawViewController alloc] init];
    
    con.leftMoney = fundModel.left_coin;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)layoutFrame{
    
    WS(weakSelf)
    
    [walletIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(weakSelf.view.mas_top).offset(30);
        make.width.equalTo(81);
        make.height.equalTo(79);
    }];
    
    [accountTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(walletIconView.mas_centerX);
        make.top.equalTo(walletIconView.mas_bottom).offset(20);
    }];
    
    [balanceBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(accountTitleLabel.mas_centerX);
        make.top.equalTo(accountTitleLabel.mas_bottom).offset(13);
        make.width.equalTo(275);
        make.height.equalTo(80);
    }];
    
    [balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(balanceBackgroundView.mas_centerX);
        make.centerY.equalTo(balanceBackgroundView.mas_centerY);
    }];
    
    [rechargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(balanceBackgroundView.mas_centerX);
        make.top.equalTo(balanceBackgroundView.mas_bottom).offset(30);
        make.width.equalTo(290);
        make.height.equalTo(45);
    }];
    
    [rechargeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(rechargeBtn.mas_centerX);
        make.centerY.equalTo(rechargeBtn.mas_centerY).offset(-10);
    }];
    
    [withDrawBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rechargeBtn.mas_bottom).offset(10);
        make.centerX.equalTo(rechargeBtn.mas_centerX);
        make.width.equalTo(290);
        make.height.equalTo(45);
    }];
    
    [questionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(withDrawBtn.mas_left);
        make.top.equalTo(withDrawBtn.mas_bottom).offset(10);
        make.width.equalTo(219);
        make.height.equalTo(12);
    }];
}
- (NSString *)title{
    return @"我的钱包";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
