//
//  YLCMyOrderBaseViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyOrderBaseViewController.h"
#import "DLCollectionTabbarView.h"
#import "DLCustomSlideView.h"
#import "YLCMyOrderChildViewController.h"
@interface YLCMyOrderBaseViewController ()<DLCustomSlideViewDelegate>
@property (nonatomic ,strong)DLCustomSlideView *slideView;
@end

@implementation YLCMyOrderBaseViewController
- (DLCustomSlideView *)slideView{
    if (!_slideView) {
        _slideView = [[DLCustomSlideView alloc] initWithFrame:self.view.bounds];
    }
    return _slideView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    DLCollectionTabbarView *tabbarView = [[DLCollectionTabbarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 60)];
    
    tabbarView.backgroundColor = [UIColor whiteColor];
    
    tabbarView.tabbarItems = @[@"待发货",@"待收货",@"全部"];
    
    self.slideView.tabbar = tabbarView;
    self.slideView.tabbarBottomSpacing = 5;
    self.slideView.baseViewController = self;
    self.slideView.delegate = self;
    [self.slideView setup];
    self.slideView.selectedIndex = 0;
    [self.view addSubview:self.slideView];
}
- (NSInteger)numberOfTabsInDLCustomSlideView:(DLCustomSlideView *)sender{
    return 3;
}
- (UIViewController *)DLCustomSlideView:(DLCustomSlideView *)sender controllerAt:(NSInteger)index{
    YLCMyOrderChildViewController *con = [[YLCMyOrderChildViewController alloc] init];
    
    con.view.backgroundColor = @[[UIColor redColor],[UIColor blueColor],[UIColor yellowColor]][index];
    
    return con;
}
- (NSString *)title{
    return @"我的订单";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
