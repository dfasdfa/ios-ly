//
//  YLCUserCenterViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCUserCenterViewController.h"
#import "DLCustomSlideView.h"
#import "DLLRUCache.h"
#import "DLScrollTabbarView.h"
#import "YLCDentistDynamicViewController.h"
#import "YLCUnusedViewController.h"
#import "YLCVideoLifeViewController.h"
#import "YLCChatBaseViewController.h"
@interface YLCUserCenterViewController ()<DLCustomSlideViewDelegate>
@property (nonatomic ,strong)DLCustomSlideView *slideView;

@end

@implementation YLCUserCenterViewController
{
    UIImageView *headView;
    UIImageView *userIconView;
    UILabel *userNameLabel;
    UILabel *followNumberLabel;
    UILabel *funsLabel;
    UIButton *backBtn;
    UIButton *messageBtn;
    NSMutableArray *itemArray_;
    UIButton *careBtn;
    NSDictionary *userData;
}
- (void)gotoBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)gotoTalk{
    if (![YLCLoginManager checkLoginStateWithTarget:self]) {
        return;
    }
    YLCChatBaseViewController *con = [[YLCChatBaseViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:_userId];
    
    con.title = userData[@"nick_name"];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (DLCustomSlideView *)slideView{
    if (!_slideView) {
        _slideView = [[DLCustomSlideView alloc] initWithFrame:CGRectMake(0, 240, self.view.frameWidth, self.view.frameHeight-240)];
    }
    return _slideView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
    [self layoutFrame];
}
- (void)initDataSource{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"myId":userModel.user_id,@"token":userModel.token,@"userId":_userId} method:@"POST" urlPath:@"app/user/baseDetail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        userData = responseObject;
        
        careBtn.selected = [[NSString stringWithFormat:@"%@",userData[@"is_follow"]] isEqualToString:@"1"];
        
        userNameLabel.text = userData[@"nick_name"];
        
        [userIconView sd_setImageWithURL:[NSURL URLWithString:userData[@"imgsrc"]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
        
        NSString *follow = userData[@"follow_num"];
        
        NSString *followString = [NSString stringWithFormat:@"%@\n我关注的",follow];
        
        NSAttributedString *followAttr = [YLCFactory getAttributedStringWithString:followString FirstCount:follow.length secondCount:followString.length-follow.length firstFont:17 secondFont:14 firstColor:[UIColor whiteColor] secondColor:[UIColor whiteColor]];
        
        followNumberLabel.attributedText = followAttr;
        
        NSString *funs = userData[@"funs_num"];
        
        NSString *funsString = [NSString stringWithFormat:@"%@\n关注我的",funs];
        
        NSAttributedString *funsAttr = [YLCFactory getAttributedStringWithString:funsString FirstCount:funs.length secondCount:funsString.length-funs.length firstFont:17 secondFont:14 firstColor:[UIColor whiteColor] secondColor:[UIColor whiteColor]];
        
        funsLabel.attributedText = funsAttr;
        
    }];
}
- (void)createCustomView{
    headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 210)];
    
    headView.image = [UIImage imageNamed:@"mine_background"];
    
    headView.userInteractionEnabled = YES;
    
    [self.view addSubview:headView];
    
    [self createSlideView];
    
    backBtn = [YLCFactory createBtnWithImage:@"back_white"];
    
    [backBtn addTarget:self action:@selector(gotoBack) forControlEvents:UIControlEventTouchUpInside];

    [headView addSubview:backBtn];
    
    userNameLabel = [YLCFactory createLabelWithFont:15];
    
    userNameLabel.textColor = [UIColor whiteColor];
    
    [headView addSubview:userNameLabel];
    
    messageBtn = [YLCFactory createBtnWithImage:@"mine_talk"];
    
    [messageBtn addTarget:self action:@selector(gotoTalk) forControlEvents:UIControlEventTouchUpInside];
    
    [headView addSubview:messageBtn];
    
    userIconView = [[UIImageView alloc] init];
    
    userIconView.layer.cornerRadius = 30;
    
    userIconView.layer.masksToBounds = YES;
    
    [headView addSubview:userIconView];
    
    followNumberLabel = [YLCFactory createLabelWithFont:14];
    
    followNumberLabel.numberOfLines = 0;
    
    followNumberLabel.textAlignment = NSTextAlignmentCenter;
    
    [headView addSubview:followNumberLabel];
    
    funsLabel = [YLCFactory createLabelWithFont:14];
    
    funsLabel.numberOfLines = 0;
    
    funsLabel.textAlignment = NSTextAlignmentCenter;
    
    [headView addSubview:funsLabel];
    
    careBtn = [YLCFactory createCommondBtnWithTitle:@"+ 关注" backgroundColor:[UIColor whiteColor] titleColor:YLC_THEME_COLOR];
    
    [careBtn setTitle:@"已关注" forState:UIControlStateSelected];
    
    [careBtn addTarget:self action:@selector(careSomeone:) forControlEvents:UIControlEventTouchUpInside];
    
    [headView addSubview:careBtn];
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    if ([_userId isEqualToString:userModel.user_id]) {
        careBtn.hidden = YES;
    }
}
- (void)careSomeone:(UIButton *)sender{
    NSString *path = @"";
    NSString *tip = @"";
    if (sender.selected) {
        path = @"app/concern/cancelFollow";
        tip = @"取消成功";
    }else{
        path = @"app/concern/doFollow";
        tip = @"关注成功";
    }
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"tUserId":_userId} method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        sender.selected = !sender.selected;
        
        [SVProgressHUD showSuccessWithStatus:tip];
    }];
}

- (void)createSlideView{
    NSArray *titleArr = @[@"帖子",@"闲置",@"视频"];
    DLLRUCache *cache = [[DLLRUCache alloc] initWithCount:titleArr.count];
    DLScrollTabbarView *tabbar = [[DLScrollTabbarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    tabbar.backgroundColor = [UIColor whiteColor];
    tabbar.tabItemNormalColor = [UIColor blackColor];
    tabbar.tabItemSelectedColor = YLC_THEME_COLOR;
    tabbar.tabItemNormalFontSize = 16.0f;
    tabbar.trackColor = [UIColor clearColor];
    itemArray_ = [NSMutableArray array];
    for (int i=0; i<titleArr.count
         ; i++) {
        DLScrollTabbarItem *item = [DLScrollTabbarItem itemWithTitle:titleArr[i] width:self.view.frameWidth/3-70];
        
        [itemArray_ addObject:item];
    }
    tabbar.tabbarItems = itemArray_;
    self.slideView.tabbar = tabbar;
    self.slideView.cache = cache;
    self.slideView.tabbarBottomSpacing = 5;
    self.slideView.baseViewController = self;
    self.slideView.delegate = self;
    [self.slideView setup];
    self.slideView.selectedIndex = 0;
    [self.view addSubview:self.slideView];
}

- (NSInteger)numberOfTabsInDLCustomSlideView:(DLCustomSlideView *)sender{
    return 3;
}
- (UIViewController *)DLCustomSlideView:(DLCustomSlideView *)sender controllerAt:(NSInteger)index{
    if (index==0) {
        YLCDentistDynamicViewController *con = [[YLCDentistDynamicViewController alloc] init];
        
        con.userId = _userId;
        
        return con;
    }
    if (index==1) {
        YLCUnusedViewController *con = [[YLCUnusedViewController alloc] init];
        
        con.userId = _userId;
        
        con.showScreen = NO;
        
        return con;
    }
    if (index==2) {
        YLCVideoLifeViewController *con = [[YLCVideoLifeViewController alloc] init];
        
        con.userId = _userId;
        
        return con;
    }
    return [[YLCBaseViewController alloc] init];
}
- (void)layoutFrame{
    
    WS(weakSelf)
    
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.top.equalTo(weakSelf.view.mas_top);
        make.height.equalTo(210);
    }];
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headView.mas_top).offset(40);
        make.left.equalTo(headView.mas_left).offset(10);
        make.width.height.equalTo(30);
    }];
    
    [userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backBtn.mas_centerY);
        make.centerX.equalTo(headView.mas_centerX);
    }];
    
    [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headView.mas_right).offset(-10);
        make.centerY.equalTo(userNameLabel.mas_centerY);
        make.width.height.equalTo(30);
    }];
    
    [userIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headView.mas_centerX);
        make.top.equalTo(userNameLabel.mas_bottom).offset(30);
        make.width.height.equalTo(60);
    }];
    
    [followNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(userIconView.mas_left).offset(-40);
        make.top.equalTo(userIconView.mas_top);
        make.width.equalTo(70);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [funsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconView.mas_right).offset(40);
        make.top.equalTo(userIconView.mas_top);
        make.width.equalTo(70);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [careBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headView.mas_bottom);
        make.centerX.equalTo(headView.mas_centerX);
        make.width.equalTo(80);
        make.height.equalTo(30);
    }];
    
    [self.slideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headView.mas_left);
        make.right.equalTo(headView.mas_right);
        make.top.equalTo(headView.mas_bottom).offset(30);
        make.bottom.equalTo(weakSelf.view.mas_bottom);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
