//
//  YLCResumeModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCResumeModel : NSObject
@property (nonatomic ,copy)NSString *birthday;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *sex;
@property (nonatomic ,copy)NSString *real_name;
@property (nonatomic ,copy)NSString *age;
@property (nonatomic ,copy)NSString *mobile;
@property (nonatomic ,copy)NSString *education;
@property (nonatomic ,copy)NSString *work_year;
@property (nonatomic ,copy)NSString *salary;
@property (nonatomic ,strong)NSDictionary *location;
@property (nonatomic ,copy)NSString *position;
@property (nonatomic ,copy)NSString *advantage;
@property (nonatomic ,strong)NSArray *work_list;
@property (nonatomic ,strong)NSArray *edu_list;
@end
