//
//  YLCYgtModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCYgtModel : NSObject
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *is_pay;
@property (nonatomic ,copy)NSString *month;
@property (nonatomic ,copy)NSString *num;
@property (nonatomic ,copy)NSString *pay_time;
@property (nonatomic ,copy)NSString *price;
@end
