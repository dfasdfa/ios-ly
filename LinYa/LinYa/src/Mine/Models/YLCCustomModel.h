//
//  YLCCustomModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCCustomModel : NSObject
@property (nonatomic ,copy)NSString *account;
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *expert;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *intro;
@property (nonatomic ,copy)NSString *location;
@property (nonatomic ,copy)NSString *location_id;
@property (nonatomic ,copy)NSString *mobile_verify;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *org_name;
@property (nonatomic ,copy)NSString *position;
@property (nonatomic ,copy)NSString *pwd;
@property (nonatomic ,copy)NSString *sex;
@property (nonatomic ,copy)NSString *status;
@property (nonatomic ,strong)NSDictionary *ygt;
@property (nonatomic ,copy)NSString *sign;
@property (nonatomic ,copy)NSString *birthday;
- (NSArray *)arrayInfo;
@end
