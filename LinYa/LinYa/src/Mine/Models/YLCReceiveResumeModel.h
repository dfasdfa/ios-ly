//
//  YLCReceiveResumeModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCReceiveResumeModel : NSObject
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *send_user_id;
@property (nonatomic ,copy)NSString *receive_user_id;
@property (nonatomic ,copy)NSString *recruit_id;
@property (nonatomic ,copy)NSString *resume_id;
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,strong)NSDictionary *resume;
@property (nonatomic ,strong)NSDictionary *user;
@end
