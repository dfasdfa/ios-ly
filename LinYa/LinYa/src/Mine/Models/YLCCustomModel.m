//
//  YLCCustomModel.m
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomModel.h"

@implementation YLCCustomModel
- (void)setSex:(NSString *)sex{
    if ([sex isEqualToString:@"1"]) {
        _sex = @"男";
    }else if([sex isEqualToString:@"2"]){
        _sex = @"女";
    }else{
        _sex = @"";
    }
}
- (void)setImgsrc:(NSString *)imgsrc{
    if ([YLCFactory isStringOk:imgsrc]) {
        _imgsrc = imgsrc;
    }else{
        _imgsrc = @"";
    }
}
- (void)setNick_name:(NSString *)nick_name{
    if ([YLCFactory isStringOk:nick_name]) {
        _nick_name = nick_name;
    }else{
        _nick_name = @"";
    }
}
- (void)setAccount:(NSString *)account{
    if ([YLCFactory isStringOk:account]) {
        _account = account;
    }else{
        _account = @"";
    }

}
- (void)setIntro:(NSString *)intro{
    if ([YLCFactory isStringOk:intro]) {
        _intro = intro;
    }else{
        _intro = @"";
    }
}
- (void)setBirthday:(NSString *)birthday{
    if ([YLCFactory isStringOk:birthday]) {
        _birthday = birthday;
    }else{
        _birthday = @"";
    }
}
- (void)setSign:(NSString *)sign{
    if ([YLCFactory isStringOk:sign]) {
        _sign = sign;
    }else{
        _sign = @"";
    }
}
- (NSArray *)arrayInfo{
    return @[_imgsrc,_nick_name,_account,_sign,_sex,_birthday];
}
@end
