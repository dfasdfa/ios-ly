//
//  YLCCareModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCCareModel : NSObject
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *followed_user_id; //自己的ID
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_both;
@property (nonatomic ,copy)NSString *is_friend;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *is_img;
@end
