//
//  YLCFundModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCFundModel : NSObject
@property (nonatomic ,copy)NSString *left_coin;
@property (nonatomic ,copy)NSString *total_charge_coin;
@property (nonatomic ,copy)NSString *total_pay_coin;
@property (nonatomic ,copy)NSString *total_income_coin;
@property (nonatomic ,strong)NSArray *details;
@end
