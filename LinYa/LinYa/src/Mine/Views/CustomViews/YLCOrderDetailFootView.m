//
//  YLCOrderDetailFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCOrderDetailFootView.h"

@implementation YLCOrderDetailFootView
{
    UIButton *cancelBtn;
    UIButton *payBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    cancelBtn = [self commonBtnWithTitle:@"取消订单" color:[UIColor whiteColor] titleColor:YLC_THEME_COLOR];
    
    cancelBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
    
    [self addSubview:cancelBtn];
    
    payBtn = [self commonBtnWithTitle:@"立即付款" color:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [self addSubview:payBtn];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_centerX).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(145);
        make.height.equalTo(45);
    }];
    
    [payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_centerX).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(145);
        make.height.equalTo(45);
    }];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    
    return button;
}
@end
