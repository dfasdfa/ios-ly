//
//  YLCEduFootView.h
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCEduFootView;
@protocol YLCEduFootViewDelegate <NSObject>
- (void)didClickWithIsDelete:(BOOL)isDelete;
@end
@interface YLCEduFootView : UICollectionReusableView
@property (nonatomic ,weak)id<YLCEduFootViewDelegate>delegate;
@end
