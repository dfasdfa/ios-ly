//
//  YLCEduFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/4/11.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEduFootView.h"
@interface YLCEduFootView()
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end
@implementation YLCEduFootView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)delegateClick:(id)sender {
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didClickWithIsDelete:)) {
        [self.delegate didClickWithIsDelete:YES];
    }
}
- (IBAction)saveClick:(id)sender {
    [self.delegate didClickWithIsDelete:NO];
}

@end
