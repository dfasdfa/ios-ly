//
//  YLCButtonFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCButtonFootView.h"

@implementation YLCButtonFootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _footBtn = [YLCFactory createCommondBtnWithTitle:@"保存" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
        
        [self addSubview:_footBtn];
        
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    [_footBtn setTitle:title forState:UIControlStateNormal];
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [_footBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.equalTo(40);
    }];
    
}
@end
