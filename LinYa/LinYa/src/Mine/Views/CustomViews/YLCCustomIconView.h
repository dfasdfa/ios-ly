//
//  YLCCustomIconView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCCustomIconView : UICollectionReusableView
@property (nonatomic ,copy)NSString *iconPath;
@end
