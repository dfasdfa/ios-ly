//
//  YLCMineHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMineHeadView.h"
#import "YLCEasyCommunicateView.h"
#import "YLCTitleAndIconModel.h"
#import "YLCMyWalletViewController.h"
#import "YLCMySendViewController.h"
#import "YLCMyOrderBaseViewController.h"
#import "YLCMyCareViewController.h"
#import "YLCLoginManager.h"
#import "YLCCustomMessageViewController.h"
#import "YLCUserCenterViewController.h"
#import "YLCMineSetViewController.h"
@implementation YLCMineHeadView
{
    UIImageView *backgroundView;
    UIImageView *headIconView;
    UIImageView *subArrow;
    UILabel *nameLabel;
    UILabel *subLabel;
    YLCEasyCommunicateView *headCollectionView;
    UIImageView *lineView;
    NSArray *list;
    UIButton *setBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        NSArray *arr = @[@{@"iconName":@"mine_account",@"title":@"我的账户"},
//                         @{@"iconName":@"mine_order",@"title":@"我的订单"},
                         @{@"iconName":@"mine_send",@"title":@"我的发布"},
                         @{@"iconName":@"mine_care",@"title":@"我的关注"}];
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary *dict in arr) {
//            @property (nonatomic ,copy)NSString *iconName;
//            @property (nonatomic ,copy)NSString *title;
            YLCTitleAndIconModel *model = [YLCTitleAndIconModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        list = [container copy];
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mine_background"]];
    
    backgroundView.userInteractionEnabled = YES;
    
    [self addSubview:backgroundView];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoLoginOrPersonCenter)];
    
    [backgroundView addGestureRecognizer:ges];
    
    setBtn = [YLCFactory createBtnWithImage:@"mine_set"];
    
    [setBtn addTarget:self action:@selector(gotoSet) forControlEvents:UIControlEventTouchUpInside];
    
    [backgroundView addSubview:setBtn];
    
    headIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"custom_placeholder"]];
    
    headIconView.userInteractionEnabled = YES;
    
    headIconView.layer.cornerRadius = 27.5;
    
    headIconView.layer.masksToBounds = YES;
    
    [backgroundView addSubview:headIconView];
    
    UITapGestureRecognizer *headGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoUserCenter)];
    
    [headIconView addGestureRecognizer:headGes];
    
    nameLabel = [YLCFactory createLabelWithFont:17 color:RGB(255, 255, 255)];
    
    nameLabel.text = @"登录/注册";
    
    [backgroundView addSubview:nameLabel];
    
    subArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mine_arrow"]];
    
    [backgroundView addSubview:subArrow];
    
    subLabel = [YLCFactory createLabelWithFont:12 color:RGBA(255, 255, 255, 0.7)];
    
    subLabel.text = @"快速登录享受更多服务";
    
    [backgroundView addSubview:subLabel];
    
    headCollectionView = [[YLCEasyCommunicateView alloc] initWithFrame:CGRectZero];
    
    headCollectionView.backgroundColor = [UIColor clearColor];
    
    headCollectionView.listArr = list;
    
    headCollectionView.alwaysBounceVertical = NO;
    
    [headCollectionView.touchSignal subscribeNext:^(id x) {
        NSIndexPath *indexPath = x;
        if (indexPath.row==0) {
            [self gotoAccountVc];
        }
//        if (indexPath.row==1) {
//            [self gotoOrder];
//        }
        if (indexPath.row==1) {
            [self gotoMySend];
        }
        if (indexPath.row==2) {
            [self gotoMyCare];
        }
    }];
    
    [self addSubview:headCollectionView];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(224, 224, 224);
    
    [self addSubview:lineView];
}
- (void)gotoSet{
    YLCMineSetViewController *con = [[YLCMineSetViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoUserCenter{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    if (![YLCFactory isStringOk:model.user_id]) {
        return;
    }
    
    YLCUserCenterViewController *con = [[YLCUserCenterViewController alloc] init];
    
    con.userId = model.user_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoLoginOrPersonCenter{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    if ([YLCFactory isStringOk:model.token]) {
        [self gotoCustomCenter];
    }else{
        [YLCLoginManager showLoginViewWithTarget:self];
    }
}
- (void)gotoCustomCenter{
    YLCCustomMessageViewController *con = [[YLCCustomMessageViewController alloc] init];
    
    con.model = self.customModel;
    
    [[self currentNavTarget] pushViewController:con animated:YES];
}
//我的订单
- (void)gotoOrder{
    YLCMyOrderBaseViewController *con = [[YLCMyOrderBaseViewController alloc] init];
    
    [[self currentNavTarget] pushViewController:con animated:YES];
}
//我的钱包
- (void)gotoAccountVc{

    YLCMyWalletViewController *con = [[YLCMyWalletViewController alloc] init];
    
    [[self currentNavTarget] pushViewController:con animated:YES];
}
//我的发布
- (void)gotoMySend{
    YLCMySendViewController *con = [[YLCMySendViewController alloc] init];
    
    [[self currentNavTarget] pushViewController:con animated:YES];
}
//我的关注
- (void)gotoMyCare{
    YLCMyCareViewController *con = [[YLCMyCareViewController alloc] init];
    
    [[self currentNavTarget] pushViewController:con animated:YES];
}
- (void)configUIWithModel:(YLCCustomModel *)model{
    self.customModel = model;
    if (model) {
        if ([YLCFactory isStringOk:model.nick_name]) {
            nameLabel.text = model.nick_name;
        }else{
            nameLabel.text = model.account;
        }
        subLabel.text = [model.sign notNullString];
        [headIconView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"custom_placeholder"]];
    }else{
        subLabel.text = @"快速登录享受更多服务";
        nameLabel.text = @"登录/注册";
        headIconView.image = [UIImage imageNamed:@"custom_placeholder"];
    }
    
}
- (UINavigationController *)currentNavTarget{
    return (UINavigationController *)[YLCFactory currentNav];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf);
    
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(weakSelf.mas_top);
        make.height.equalTo(150);
    }];
    
    [setBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(backgroundView.mas_right).offset(-20);
        make.top.equalTo(backgroundView.mas_top).offset(20);
        make.width.height.equalTo(30);
    }];
    
    [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(17);
        make.top.equalTo(backgroundView.mas_top).offset(66);
        make.width.equalTo(55);
        make.height.equalTo(55);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_right).offset(10);
        make.top.equalTo(headIconView.mas_top).offset(8);
    }];
    
    [subArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(backgroundView.mas_right).offset(-30);
        make.centerY.equalTo(headIconView.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(13);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.top.equalTo(nameLabel.mas_bottom).offset(12);
    }];
    
    [headCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(backgroundView.mas_bottom).offset(20);
        make.width.equalTo(weakSelf.frameWidth-66);
        make.height.equalTo(78);
    }];
    
    headCollectionView.imageSize = CGSizeMake(40, 40);
    
    headCollectionView.cellSize = CGSizeMake((self.frameWidth-66)/3-10, 58);
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
}

@end
