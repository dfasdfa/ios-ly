//
//  YLCAccountHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAccountHeadView.h"
#import "YLCAccountView.h"
#import "YLCTitleAndIconModel.h"
@implementation YLCAccountHeadView
{
    YLCAccountView *rechargeView;
    YLCAccountView *incomeView;
    YLCAccountView *payView;
    UIImageView *lineView1;
    UIImageView *lineView2;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    rechargeView = [[YLCAccountView alloc] initWithFrame:CGRectZero];
    
    rechargeView.title = @"累计充值";
    
    
    
    [self addSubview:rechargeView];
    
    lineView1 = [[UIImageView alloc] init];
    
    lineView1.backgroundColor = RGB(183, 183, 183);
    
    [self addSubview:lineView1];
    
    incomeView = [[YLCAccountView alloc] initWithFrame:CGRectZero];
    
    incomeView.title = @"累计收入";
    
    
    
    [self addSubview:incomeView];
    
    lineView2 = [[UIImageView alloc] init];
    
    lineView2.backgroundColor = RGB(183, 183, 183);
    
    [self addSubview:lineView2];
    
    payView = [[YLCAccountView alloc] initWithFrame:CGRectZero];
    
    payView.title = @"累计支出";
    
    
    
    [self addSubview:payView];
}
- (void)setModel:(YLCFundModel *)model{
    
    rechargeView.info = [NSString stringWithFormat:@"￥：%@",model.total_charge_coin];
    
    incomeView.info = [NSString stringWithFormat:@"￥：%@",model.total_income_coin];
    
    payView.info = [NSString stringWithFormat:@"￥：%@",model.total_pay_coin];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [incomeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top).offset(20);
        make.width.equalTo((weakSelf.frameWidth-60-1)/3);
        make.height.equalTo(60);
    }];
    
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(incomeView.mas_left);
        make.centerY.equalTo(incomeView.mas_centerY);
        make.width.equalTo(0.5);
        make.height.equalTo(40);
    }];
    
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(incomeView.mas_right);
        make.centerY.equalTo(incomeView.mas_centerY);
        make.width.equalTo(0.5);
        make.height.equalTo(40);
    }];
    
    [rechargeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lineView1.mas_left);
        make.top.equalTo(incomeView.mas_top);
        make.width.equalTo(incomeView.mas_width);
        make.height.equalTo(incomeView.mas_height);
    }];
    
    [payView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(lineView2.mas_right);
        make.top.equalTo(incomeView.mas_top);
        make.width.equalTo(incomeView.mas_width);
        make.height.equalTo(incomeView.mas_height);
    }];
}
@end
