//
//  YLCButtonFootView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCButtonFootView : UICollectionReusableView
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,strong)UIButton *footBtn;
@end
