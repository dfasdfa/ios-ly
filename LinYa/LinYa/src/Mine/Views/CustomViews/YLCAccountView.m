//
//  YLCAccountView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAccountView.h"

@implementation YLCAccountView
{
    UILabel *titleLabel;
    UILabel *customLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(142, 145, 146)];
    
    [self addSubview:titleLabel];
    
    customLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:customLabel];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setInfo:(NSString *)info{
    
    NSAttributedString *attr = [YLCFactory getAttributedStringWithString:info FirstCount:2 secondCount:info.length-2 firstFont:13 secondFont:23 firstColor:RGB(101, 99, 99) secondColor:RGB(41, 39, 39)];
    
    customLabel.attributedText = attr;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top);
    }];
    
    [customLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
