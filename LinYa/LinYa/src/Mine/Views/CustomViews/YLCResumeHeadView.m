//
//  YLCResumeHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeHeadView.h"
#import "YLCResumeCustomMessageEditViewController.h"
@implementation YLCResumeHeadView
{
    UIImageView *topBackgroundView;
    UIImageView *headIconView;
    UIImageView *sexView;
    UILabel *nameLabel;
    UILabel *ageLabel;
    UIImageView *phoneIconView;
    UILabel *phoneLabel;
    UILabel *introLabel;
    UIButton *editBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    self.backgroundColor = [UIColor whiteColor];
    
    topBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_background"]];
    
    [self addSubview:topBackgroundView];
    
    headIconView = [[UIImageView alloc] init];
    
    [self addSubview:headIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:[UIColor whiteColor]];
    
    nameLabel.font = [UIFont boldSystemFontOfSize:16];
    
    [self addSubview:nameLabel];
    
    ageLabel = [YLCFactory createLabelWithFont:13 color:RGBA(255, 255, 255, 0.7)];
    
    [self addSubview:ageLabel];
    
    phoneIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_phone"]];
    
    [self addSubview:phoneIconView];
    
    phoneLabel = [YLCFactory createLabelWithFont:14 color:RGB(111, 116, 119)];
    
    [self addSubview:phoneLabel];
    
    introLabel = [YLCFactory createLabelWithFont:13 color:RGB(60, 80, 86)];
    
    [self addSubview:introLabel];
    
    editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [editBtn setTitle:@"编辑" forState:UIControlStateNormal];
    
    [editBtn setTitleColor:RGB(136, 141, 143) forState:UIControlStateNormal];
    
    [editBtn setImage:[UIImage imageNamed:@"resume_exti"] forState:UIControlStateNormal];
    
    editBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 20);
    
    editBtn.titleEdgeInsets = UIEdgeInsetsMake(5, 15, 5, 5);

    editBtn.titleLabel.font = [UIFont systemFontOfSize:14];
 
    [editBtn addTarget:self action:@selector(gotoEdit) forControlEvents:UIControlEventTouchUpInside];
    
    editBtn.layer.borderColor = RGB(136, 141, 143).CGColor;
    
    editBtn.layer.borderWidth = 0.5;
    
    editBtn.layer.cornerRadius = 5;
    
    editBtn.layer.masksToBounds = YES;
    
    [self addSubview:editBtn];
}
- (void)setResumeModel:(YLCResumeModel *)resumeModel{
    
    _resumeModel = resumeModel;
    
    [headIconView sd_setImageWithURL:[NSURL URLWithString:resumeModel.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    nameLabel.text = [resumeModel.real_name notNullString];
    
    ageLabel.text = [NSString stringWithFormat:@"%@岁",resumeModel.age];
    
    phoneLabel.text = resumeModel.mobile;
    
    introLabel.text =[NSString stringWithFormat:@"%@ | %@年",[resumeModel.education notNullString],[resumeModel.work_year notNullString]];
}
- (void)gotoEdit{
    if (_resumeType==1) {
        return;
    }
    YLCResumeCustomMessageEditViewController *con = [[YLCResumeCustomMessageEditViewController alloc] init];
    
    con.resumeId = _resumeModel.id;
    
    con.resumeModel = _resumeModel;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [topBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(71);
    }];
    
    [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(8);
        make.centerY.equalTo(topBackgroundView.mas_bottom);
        make.width.equalTo(79);
        make.height.equalTo(79);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_right).offset(15);
        make.top.equalTo(headIconView.mas_top).offset(20);
    }];
    
    [ageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.left.equalTo(nameLabel.mas_right).offset(7);
    }];
    
    [phoneIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.top.equalTo(nameLabel.mas_bottom).offset(20);
    }];
    
    [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneIconView.mas_right).offset(5);
        make.centerY.equalTo(phoneIconView.mas_centerY);
    }];
    
    [introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneIconView.mas_left);
        make.top.equalTo(phoneLabel.mas_bottom).offset(20);
    }];
    
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(introLabel.mas_bottom);
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.width.equalTo(69);
        make.height.equalTo(26);
    }];
}
@end
