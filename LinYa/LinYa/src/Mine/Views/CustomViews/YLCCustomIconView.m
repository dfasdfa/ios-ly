//
//  YLCCustomIconView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomIconView.h"

@implementation YLCCustomIconView
{
    UIImageView *iconView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        iconView = [[UIImageView alloc] init];
        
        [self addSubview:iconView];
    }
    return self;
}
- (void)setIconPath:(NSString *)iconPath{
    [iconView sd_setImageWithURL:[NSURL URLWithString:iconPath] placeholderImage:[UIImage imageNamed:@"iamge_placeholder"]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(60);
    }];
    
}
@end
