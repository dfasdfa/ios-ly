//
//  YLCResumeHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCResumeModel.h"
@interface YLCResumeHeadView : UICollectionReusableView
@property (nonatomic ,strong)YLCResumeModel *resumeModel;
@property (nonatomic ,assign)NSInteger resumeType;//0是自己 1是他人的
@end
