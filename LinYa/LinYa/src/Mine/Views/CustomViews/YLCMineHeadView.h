//
//  YLCMineHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCCustomModel.h"
@interface YLCMineHeadView : UICollectionReusableView
@property (nonatomic ,strong)YLCCustomModel *customModel;
- (void)configUIWithModel:(YLCCustomModel *)model;

@end
