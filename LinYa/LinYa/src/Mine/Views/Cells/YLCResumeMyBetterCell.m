//
//  YLCResumeMyBetterCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeMyBetterCell.h"

@implementation YLCResumeMyBetterCell
{
    UILabel *titleLabel;
    UIImageView *arrowView;
    UIImageView *lineView;
    UILabel *betterLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(142, 148, 150)];
        
        titleLabel.text = @"我的优势";
        
        [self.contentView addSubview:titleLabel];
        
        arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
        
        [self.contentView addSubview:arrowView];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = RGB(235, 235, 235);
        
        [self.contentView addSubview:lineView];
        
        betterLabel = [YLCFactory createLabelWithFont:15 color:RGB(56, 59, 60)];

        betterLabel.numberOfLines = 0;
        
        [self.contentView addSubview:betterLabel];

    }
    return self;
}
- (void)setMyBetter:(NSString *)myBetter{
    betterLabel.text = [myBetter notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(5);
    }];
    
    [arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.centerY.equalTo(titleLabel.mas_centerY);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.height.equalTo(0.5);
    }];

    [betterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.greaterThanOrEqualTo(@10);
    }];
}
@end
