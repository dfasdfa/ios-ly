//
//  YLCRecruitmentPhoneCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRecruitmentPhoneCell.h"

@implementation YLCRecruitmentPhoneCell
{
    UILabel *titleLabel;
    UITextField *phoneField;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(108, 108, 108)];
    
    [self.contentView addSubview:titleLabel];
    
    phoneField = [YLCFactory createFieldWithPlaceholder:@"请输入您的手机号"];
    
    phoneField.leftViewMode = UITextFieldViewModeNever;
    
    [phoneField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, phoneMessageDidChangedWithPhone:)) {
            [self.delegate phoneMessageDidChangedWithPhone:x];
        }
    }];
    
    [self.contentView addSubview:phoneField];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setMessage:(NSString *)message{
    phoneField.text = [message notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_right).offset(10);
        make.centerY.equalTo(titleLabel.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-100);
        make.height.equalTo(weakSelf.frameHeight);
    }];
}
@end
