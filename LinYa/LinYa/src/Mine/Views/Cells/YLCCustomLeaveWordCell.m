//
//  YLCCustomLeaveWordCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomLeaveWordCell.h"

@implementation YLCCustomLeaveWordCell
{
    UILabel *titleLabel;
    UIView *wordBackgroundView;
    UILabel *wordLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(132, 135, 136)];
    
    titleLabel.text = @"买家留言";
    
    [self.contentView addSubview:titleLabel];
    
    wordBackgroundView = [[UIView alloc] init];
    
    wordBackgroundView.backgroundColor = RGBA(255, 246, 237, 0.64);
    
    [self.contentView addSubview:wordBackgroundView];
    
    wordLabel = [YLCFactory createLabelWithFont:14 color:RGB(63, 65, 66)];
    
    wordLabel.text = @"东西早点发，谢谢啦";
    
    [self.contentView addSubview:wordLabel];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [wordBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-5);
    }];
    
    [wordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(wordBackgroundView.mas_left).offset(5);
        make.top.equalTo(wordBackgroundView.mas_top).offset(5);
        make.width.equalTo(weakSelf.frameWidth-10-30);
        make.height.greaterThanOrEqualTo(@10);
    }];
}
@end
