//
//  YLCAccountCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAccountCell.h"

@implementation YLCAccountCell
{
    UILabel *titleLabel;
    UILabel *middleLabel;
    UILabel *lastLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:titleLabel];
    
    middleLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:middleLabel];
    
    lastLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:lastLabel];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setMiddle:(NSString *)middle{
    middleLabel.text = middle;
}
- (void)setLast:(NSString *)last{
    _last = last;
    if ([last floatValue]>0) {
        lastLabel.text = [NSString stringWithFormat:@"+%@",last];
    }else{
        lastLabel.text = last;
    }
    
}
- (void)setIndex:(NSInteger)index{
    if (index==0) {
        
        titleLabel.textAlignment = NSTextAlignmentCenter;
        middleLabel.textAlignment = NSTextAlignmentCenter;
        lastLabel.textAlignment = NSTextAlignmentCenter;
        lastLabel.textColor = [UIColor blackColor];
        self.backgroundColor = RGB(251, 247, 241);
        
    }else{
        
        titleLabel.textAlignment = NSTextAlignmentLeft;
        middleLabel.textAlignment = NSTextAlignmentLeft;
        lastLabel.textAlignment = NSTextAlignmentCenter;
        if ([_last floatValue]>0) {
            lastLabel.textColor = YLC_THEME_COLOR;
        }else{
            lastLabel.textColor = [UIColor greenColor];
        }
        
        self.backgroundColor = [UIColor whiteColor];
        
    }
}
- (void)layoutSubviews{
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo((weakSelf.frameWidth-20)/3);
        make.height.equalTo(weakSelf.frameHeight);
    }];
    
    [middleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(titleLabel.mas_top);
        make.width.equalTo((weakSelf.frameWidth-20)/3);
        make.height.equalTo(weakSelf.frameHeight);
    }];
    
    [lastLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(middleLabel.mas_top);
        make.width.equalTo((weakSelf.frameWidth-20)/3);
        make.height.equalTo(weakSelf.frameHeight);
    }];
}
@end
