//
//  YLCLogisticsinfoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCLogisticsinfoCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *normalImageName;
@property (nonatomic ,copy)NSString *selectImageName;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,assign)BOOL isSelect;
@end
