//
//  YLCSubSelectCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCSubSelectCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *text;
@property (nonatomic ,copy)NSString *placeholder;
@end
