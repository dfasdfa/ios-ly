//
//  YLCBuyRecordCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBuyRecordCell.h"
#import "YLCRecordCell.h"
#import "YLCYgtModel.h"
#define RECORD_CELL  @"YLCRecordCell"
@interface YLCBuyRecordCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCBuyRecordCell
{
    UILabel *buyRecordLabel;
    UICollectionView *recordTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    buyRecordLabel = [YLCFactory createLabelWithFont:15 color:RGB(103, 108, 109)];
    
    [self.contentView addSubview:buyRecordLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    recordTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    recordTable.backgroundColor = [UIColor whiteColor];
    
    [recordTable registerClass:[YLCRecordCell class] forCellWithReuseIdentifier:RECORD_CELL];
    
    recordTable.delegate = self;
    
    recordTable.dataSource = self;
    
    [self.contentView addSubview:recordTable];
    WS(weakSelf)
    [buyRecordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(5);
    }];
    
    [recordTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(buyRecordLabel.mas_bottom).offset(5);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.right.equalTo(weakSelf.mas_right);
    }];
}
- (void)setBuyList:(NSArray *)buyList{
    _buyList = buyList;
    
    [recordTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _buyList.count+1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCRecordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RECORD_CELL forIndexPath:indexPath];
    if (indexPath.row==0) {
        cell.isPrice = NO;
        cell.time = @"购买时间";
        cell.price = @"购买金额";
    }else{
        cell.isPrice = YES;
        if (indexPath.row-1<_buyList.count) {
            YLCYgtModel *model = _buyList[indexPath.row-1];
            cell.time = model.pay_time;
            cell.price = [NSString stringWithFormat:@"￥%@",model.price];
        }
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth-20, 44);
}
@end
