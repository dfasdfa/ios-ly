//
//  YLCRechargeMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCRechargeMessageCell;
@protocol YLCRechargeMessageCellDelegate <NSObject>
- (void)didSelectPayWithSelectIndex:(NSInteger)index;
@end
@interface YLCRechargeMessageCell : UICollectionViewCell
@property (nonatomic ,strong)NSArray *payArr;
@property (nonatomic ,assign)NSInteger rechargeType;
@property (nonatomic ,weak)id<YLCRechargeMessageCellDelegate>delegate;
@end
