//
//  YLCEasyBuyTimeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCEasyBuyTimeCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *endTime;
@property (nonatomic ,copy)NSString *starTime;
@end
