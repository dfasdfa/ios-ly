//
//  YLCCustomMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCOrderCustomMessageCell.h"

@implementation YLCOrderCustomMessageCell
{
    UIImageView *personHeadIconView;
    UILabel *nameLabel;
    UIImageView *sexTypeView;
    UILabel *fansNumberLabel;
    UILabel *attentionNumberLabel;
    UIButton *callBtn;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    personHeadIconView = [[UIImageView alloc] init];
    
    personHeadIconView.backgroundColor = YLC_THEME_COLOR;
    
    [self.contentView addSubview:personHeadIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(62, 66, 67)];
    
    nameLabel.text = @"ccc";
    
    [self.contentView addSubview:nameLabel];
    
    sexTypeView = [[UIImageView alloc] init];
    
    sexTypeView.image = [UIImage imageNamed:@"sex_man"];
    
    [self.contentView addSubview:sexTypeView];
    
    attentionNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
    attentionNumberLabel.text = @"关注：15";
    
    [self.contentView addSubview:attentionNumberLabel];
    
    fansNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
    fansNumberLabel.text = @"粉丝：15";
    
    [self.contentView addSubview:fansNumberLabel];

    callBtn = [YLCFactory createBtnWithImage:@"" title:@"联系卖家" titleColor:RGB(11,173,175) frame:CGRectMake(0, 0, 90, 30)];
    
    [self addSubview:callBtn];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(235, 235, 235);
    
    [self addSubview:lineView];
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [personHeadIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(18);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(39);
        make.height.equalTo(40);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(personHeadIconView.mas_top).offset(4);
        make.left.equalTo(personHeadIconView.mas_right).offset(8);
    }];
    
    [sexTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.left.equalTo(nameLabel.mas_right).offset(7);
        make.width.equalTo(11);
        make.height.equalTo(12);
    }];
    
    [attentionNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.bottom.equalTo(personHeadIconView.mas_bottom).offset(-4);
    }];
    
    [fansNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(attentionNumberLabel.mas_top);
        make.left.equalTo(attentionNumberLabel.mas_right).offset(8);
    }];
    
    [callBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(90);
        make.height.equalTo(30);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(callBtn.mas_left).offset(5);
        make.centerY.equalTo(callBtn.mas_centerY);
        make.width.equalTo(0.5);
        make.height.equalTo(30);
    }];
}
@end
