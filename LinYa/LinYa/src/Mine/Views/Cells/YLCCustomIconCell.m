//
//  YLCCustomIconCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomIconCell.h"

@implementation YLCCustomIconCell
{
    UILabel *titleLabel;
    UIImageView *iconView;
    UIImageView *subArrowView;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(144, 144, 144)];
    
    [self.contentView addSubview:titleLabel];
    
    iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"custom_placeholder"]];
    
    [self.contentView addSubview:iconView];
    
    subArrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    
    [self.contentView addSubview:subArrowView];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(235, 235, 235);
    
    [self.contentView addSubview:lineView];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setHeadUrl:(NSString *)headUrl{
    [iconView sd_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:[UIImage imageNamed:@"custom_placeholder"]];
}
- (void)setHeadImage:(UIImage *)headImage{
    iconView.image = headImage;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [subArrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(8);
        make.height.equalTo(14);
    }];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(subArrowView.mas_left).offset(-18);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(38);
        make.height.equalTo(38);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
}
@end
