//
//  YLCJobResumeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCJobResumeCell.h"

@implementation YLCJobResumeCell
{
    UILabel *titleLabel;
    UIImageView *arrowView;
    UIImageView *lineView;
    UIImageView *jobIconView;
    UILabel *jobLabel;
    UIImageView *areaIconView;
    UILabel *areaLabel;
    UIImageView *payIconView;
    UILabel *payLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:17 color:RGB(142, 148, 150)];
    
    titleLabel.text = @"求职意向";
    
    [self.contentView addSubview:titleLabel];
    
    arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
    
    [self.contentView addSubview:arrowView];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(225, 225, 225);
    
    [self.contentView addSubview:lineView];
    
    jobIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_ex"]];
    
    [self.contentView addSubview:jobIconView];
    
    jobLabel = [YLCFactory createLabelWithFont:15 color:RGB(63, 67, 68)];
    
    jobLabel.text = @"暂无";
    
    jobLabel.numberOfLines = 0;
    
    [self.contentView addSubview:jobLabel];
    
    areaIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_area"]];
    
    [self.contentView addSubview:areaIconView];
    
    areaLabel = [YLCFactory createLabelWithFont:15 color:RGB(63, 67, 68)];
    
    areaLabel.text = @"暂无";
    
    [self.contentView addSubview:areaLabel];
    
    payIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_money"]];
    
    [self.contentView addSubview:payIconView];
    
    payLabel = [YLCFactory createLabelWithFont:15 color:RGB(63, 67, 68)];
    
    payLabel.text = @"暂无";
    
    [self.contentView addSubview:payLabel];
}
- (void)setModel:(YLCResumeModel *)model{
    if ([YLCFactory isStringOk:model.position]) {
        jobLabel.text = model.position;
    }
    if ([YLCFactory isStringOk:model.salary]) {
        payLabel.text = model.salary;
    }
    if (!JK_IS_DICT_NIL(model.location)) {
        NSString *areaName = model.location[@"areaname"];
        areaLabel.text = areaName;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(5);
    }];
    
    [arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.centerY.equalTo(titleLabel.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(13);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.height.equalTo(0.5);
    }];
    
    [jobIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.width.equalTo(14);
        make.height.equalTo(15);
    }];
    
    [jobLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobIconView.mas_right).offset(5);
        make.centerY.equalTo(jobIconView.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-50);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [areaIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobIconView.mas_left);
        make.top.equalTo(jobLabel.mas_bottom).offset(10);
        make.width.equalTo(14);
        make.height.equalTo(15);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobLabel.mas_left);
        make.centerY.equalTo(areaIconView.mas_centerY);
    }];
    
    [payIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaIconView.mas_left);
        make.top.equalTo(areaLabel.mas_bottom).offset(10);
        make.width.equalTo(14);
        make.height.equalTo(15);
    }];
    
    [payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaLabel.mas_left);
        make.centerY.equalTo(payIconView.mas_centerY);
    }];
}
@end
