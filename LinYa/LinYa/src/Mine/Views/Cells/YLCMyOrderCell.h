//
//  YLCMyOrderCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCMyOrderCell;
@protocol YLCMyOrderCellDelegate <NSObject>
- (void)didChangeSuccess;
@end
@interface YLCMyOrderCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *orderTitle;
@property (nonatomic ,copy)NSString *iconPath;
@property (nonatomic ,copy)NSString *goodsTitle;
@property (nonatomic ,copy)NSAttributedString *priceAttr;
@property (nonatomic ,copy)NSString *refundTitle;
@property (nonatomic ,copy)NSString *quickTitle;
@property (nonatomic ,copy)NSString *isPublish;
@property (nonatomic ,copy)NSString *caseId;
@property (nonatomic ,copy)NSString *publish;
@property (nonatomic ,weak)id<YLCMyOrderCellDelegate>delegate;
@end
