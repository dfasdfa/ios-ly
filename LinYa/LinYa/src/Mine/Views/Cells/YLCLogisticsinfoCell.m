//
//  YLCLogisticsinfoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLogisticsinfoCell.h"

@implementation YLCLogisticsinfoCell
{
    UIImageView *leftLineView;
    UIImageView *partView;
    UILabel *titleLabel;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    leftLineView = [[UIImageView alloc] init];
    
    leftLineView.backgroundColor = RGB(201, 207, 210);
    
    [self.contentView addSubview:leftLineView];
    
    partView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:partView];
    
    titleLabel = [YLCFactory createLabelWithFont:12];
    
    [self.contentView addSubview:titleLabel];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(201, 207, 210);
    
    [self.contentView addSubview:lineView];
    
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        partView.image = [UIImage imageNamed:_selectImageName];
        titleLabel.textColor = YLC_THEME_COLOR;
    }else{
        partView.image = [UIImage imageNamed:_normalImageName];
        titleLabel.textColor = RGB(153,158,160);
    }
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setIndex:(NSInteger)index{
    if (index==0) {
        leftLineView.hidden = YES;
    }else{
        leftLineView.hidden = NO;
    }
    if (index==3) {
        lineView.hidden = YES;
    }else{
        lineView.hidden = NO;
        
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [partView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(19);
        make.height.equalTo(19);
    }];
    
    [leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(partView.mas_left).offset(-10);
        make.centerY.equalTo(partView.mas_centerY);
        make.height.equalTo(2);
    }];
    

    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(partView.mas_centerX);
        make.top.equalTo(partView.mas_bottom).offset(5);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(partView.mas_right).offset(10);
        make.right.equalTo(weakSelf.mas_right);
        make.centerY.equalTo(partView.mas_centerY);
        make.height.equalTo(2);
    }];
    
    
}
@end
