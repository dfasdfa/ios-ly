//
//  YLCReceiveResumeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCReceiveResumeModel.h"
@interface YLCReceiveResumeCell : UICollectionViewCell
@property (nonatomic ,strong)YLCReceiveResumeModel *model;
@end
