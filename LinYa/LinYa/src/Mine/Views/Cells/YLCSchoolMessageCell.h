//
//  YLCSchoolMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCSchoolMessageCell;
@protocol YLCSchoolMessageCellDelegate<NSObject>
- (void)sendTimeWithBeganTime:(NSString *)beganTime
                      endTime:(NSString *)endTime;
@end
@interface YLCSchoolMessageCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *beganTime;
@property (nonatomic ,copy)NSString *endTime;
@property (nonatomic ,weak)id<YLCSchoolMessageCellDelegate>delegate;
@end
