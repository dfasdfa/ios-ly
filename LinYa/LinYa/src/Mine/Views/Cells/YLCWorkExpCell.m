//
//  YLCWorkExpCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWorkExpCell.h"

@implementation YLCWorkExpCell
{
    UILabel *timeLabel;
    UILabel *subLabel;
    UILabel *companyLabel;
    UILabel *posterLabel;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        timeLabel = [YLCFactory createLabelWithFont:15 color:RGB(183, 183, 183)];
        
        [self.contentView addSubview:timeLabel];
        
        subLabel = [YLCFactory createLabelWithFont:15 color:RGB(183, 183, 183)];
        
        [self.contentView addSubview:subLabel];
        
        companyLabel = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
        
        [self.contentView addSubview:companyLabel];
        
        posterLabel = [YLCFactory createLabelWithFont:15 color:RGB(183, 183, 183)];
        
        [self.contentView addSubview:posterLabel];
    }
    return self;
}
- (void)setTitle1:(NSString *)title1{
    timeLabel.text = title1;
}
- (void)setSub1:(NSString *)sub1{
    subLabel.text = sub1;
}
- (void)setTitle2:(NSString *)title2{
    companyLabel.text = title2;
}
- (void)setSub2:(NSString *)sub2{
    posterLabel.text = sub2;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(timeLabel.mas_centerY);
    }];
    
    [companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_left);
        make.top.equalTo(timeLabel.mas_bottom).offset(10);
    }];
    
    [posterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(companyLabel.mas_centerY);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
