//
//  YLCEasyBuyTimeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyBuyTimeCell.h"

@implementation YLCEasyBuyTimeCell
{
    UILabel *buyTimeLabel;
    UILabel *endTimeLabel;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        buyTimeLabel = [YLCFactory createLabelWithFont:16 color:[UIColor blackColor]];
        
        NSString *timeString = @"购买时间：  17-09-16 21:06:31";
        
        NSAttributedString *attr = [YLCFactory getAttributedStringWithString:timeString FirstCount:5 secondCount:timeString.length-5 firstFont:16 secondFont:16 firstColor:RGB(155, 162, 163) secondColor:RGB(77, 80, 81)];
        
        buyTimeLabel.attributedText = attr;
        
        buyTimeLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:buyTimeLabel];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = RGB(235, 235, 235);
        
        [self.contentView addSubview:lineView];
        
        endTimeLabel = [YLCFactory createLabelWithFont:16 color:[UIColor blackColor]];
        
        endTimeLabel.textAlignment = NSTextAlignmentCenter;
        
        
        

        
        [self.contentView addSubview:endTimeLabel];
        
        
    }
    return self;
}
- (void)setEndTime:(NSString *)endTime{
    NSString *endTimeString = @"到期时间:";
    if (JK_IS_STR_NIL(endTime)) {
        
    }else{
        endTimeString = [NSString stringWithFormat:@"到期时间:   %@",endTime];
    }
    NSAttributedString *endAttr = [YLCFactory getAttributedStringWithString:endTimeString FirstCount:5 secondCount:endTimeString.length-5 firstFont:16 secondFont:16 firstColor:RGB(155, 162, 163) secondColor:RGB(77, 80, 81)];
    
    endTimeLabel.attributedText = endAttr;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [buyTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top);
        make.height.equalTo(weakSelf.frameHeight/2-0.25);
        make.width.equalTo(weakSelf.frameWidth);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buyTimeLabel.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(0.5);
    }];
    
    [endTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(lineView.mas_bottom);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(weakSelf.frameHeight/2-0.25);
    }];
}
@end
