//
//  YLCRecruitmentCustomNameCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRecruitmentCustomNameCell.h"

@implementation YLCRecruitmentCustomNameCell
{
    UILabel *titleLabel;
    
    UITextField *nameField;
    
    UIButton *girlBtn;
    
    UIButton *boyBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(108, 108, 108)];
    
    titleLabel.text = @"姓    名:";
    
    [self.contentView addSubview:titleLabel];
    
    nameField = [YLCFactory createFieldWithPlaceholder:@"请输入姓名"];
    
    [nameField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, customNameMessageDidChangedWithName:)) {
            [self.delegate customNameMessageDidChangedWithName:x];
        }
    }];

    [self.contentView addSubview:nameField];
    
    boyBtn = [YLCFactory createLabelBtnWithTitle:@"男" titleColor:YLC_THEME_COLOR];
    
    boyBtn.tag = 1100+1;
    
    boyBtn.layer.borderWidth = 0.5;
    
    [boyBtn addTarget:self action:@selector(sexChange:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:boyBtn];
    
    girlBtn = [YLCFactory createLabelBtnWithTitle:@"女" titleColor:YLC_THEME_COLOR];
    
    girlBtn.tag = 1102;
    
    girlBtn.layer.borderWidth = 0.5;
    
    [girlBtn addTarget:self action:@selector(sexChange:) forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:girlBtn];
}
- (void)sexChange:(UIButton *)sender{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate , sexDidChangedWithSex:)) {
        [self.delegate sexDidChangedWithSex:[NSString stringWithFormat:@"%ld",sender.tag-1100]];
    }
}
- (void)setName:(NSString *)name{
    nameField.text = [name notNullString];
}
- (void)setSex:(NSString *)sex{
    if ([sex isEqualToString:@"1"]) {
        [boyBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
        
        boyBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
        
        [girlBtn setTitleColor:RGB(108, 108, 108) forState:UIControlStateNormal];
        
        girlBtn.layer.borderColor = RGB(108, 108, 108).CGColor;
    }else{
        [girlBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
        
        girlBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
        
        [boyBtn setTitleColor:RGB(108, 108, 108) forState:UIControlStateNormal];
        
        boyBtn.layer.borderColor = RGB(108, 108, 108).CGColor;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_right).offset(10);
        make.centerY.equalTo(titleLabel.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-120);
        make.height.equalTo(weakSelf.frameHeight);
    }];
    
    [girlBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(30);
    }];
    
    [boyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(girlBtn.mas_left).offset(-20);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(30);
    }];
}
@end
