//
//  YLCRecordCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCRecordCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *time;
@property (nonatomic ,copy)NSString *price;
@property (nonatomic ,assign)BOOL isPrice;
@end
