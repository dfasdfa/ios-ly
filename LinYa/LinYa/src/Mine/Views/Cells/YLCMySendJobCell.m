//
//  YLCMySendJobCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMySendJobCell.h"
#import "YLCInviteInfoViewController.h"
@implementation YLCMySendJobCell
{
    UIView *backView;
    UILabel *titleLabel;
    UILabel *subLabel;
    UIButton *deleteBtn;
    UIButton *editBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backView = [[UIView alloc] initWithFrame:CGRectMake(10, 7, self.frameWidth-20, 63)];
    
    backView.backgroundColor = RGBA(255, 246, 237, 0.64);
    
    [self.contentView addSubview:backView];
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(61, 63, 64)];
    
    titleLabel.text = @"高级牙齿护理师";
    
    [backView addSubview:titleLabel];
    
    subLabel = [YLCFactory createLabelWithFont:12 color:RGB(157, 160, 161)];
    
    subLabel.text = @"2017-12-21 发布 | 122浏览 | 12 投递";
    
    [backView addSubview:subLabel];
    
    deleteBtn = [self commonBtnWithTitle:@"删除职位" color:[UIColor whiteColor] titleColor:RGB(183, 183, 183)];
    
    deleteBtn.layer.borderColor = RGB(183, 183, 183).CGColor;
    
    [self.contentView addSubview:deleteBtn];
    
    editBtn = [self commonBtnWithTitle:@"编辑" color:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [editBtn addTarget:self action:@selector(gotoEdit) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:editBtn];
}
- (void)gotoEdit{
    YLCInviteInfoViewController *con = [[YLCInviteInfoViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(14);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(16);
    }];
    
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.top.equalTo(backView.mas_bottom).offset(10);
        make.width.equalTo(70);
        make.height.equalTo(27);
    }];
    
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(editBtn.mas_left).offset(-14);
        make.centerY.equalTo(editBtn.mas_centerY);
        make.width.equalTo(70);
        make.height.equalTo(27);
    }];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    
    return button;
}
@end
