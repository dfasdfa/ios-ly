//
//  YLCOrderGoodsMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCOrderGoodsMessageCell.h"

@implementation YLCOrderGoodsMessageCell
{
    UIView *goodsBackgroundView;
    UIImageView *goodsImageView;
    UILabel *goodsTitleLabel;
    UILabel *priceLabel;
    UILabel *toPersonTitleLabel;
    UILabel *toPersonLabel;
    UILabel *toAdressTitleLabel;
    UILabel *toAdressLabel;
    UILabel *orderNumberTitleLabel;
    UILabel *orderNumberLabel;
    UILabel *orderTimeTitleLabel;
    UILabel *orderTimeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    goodsBackgroundView = [[UIView alloc] init];
    
    goodsBackgroundView.backgroundColor = RGBA(255, 246, 237, 0.64);

    [self.contentView addSubview:goodsBackgroundView];
    
    goodsImageView = [[UIImageView alloc] init];
    
    goodsImageView.image = [UIImage imageNamed:@"image_placeholder"];
    
    [goodsBackgroundView addSubview:goodsImageView];
    
    goodsTitleLabel = [YLCFactory createLabelWithFont:17 color:RGB(51, 51, 51)];
    
    goodsTitleLabel.numberOfLines = 0;
    
    goodsTitleLabel.text = @"超实惠医患沟通手册超实用最实惠";
    
    [goodsBackgroundView addSubview:goodsTitleLabel];
    
    priceLabel = [YLCFactory createLabelWithFont:16];
    
    NSString *price = @"￥400.00（含运费￥：0.00）";
    
    NSAttributedString *attr = [YLCFactory getThreeCountAttributedStringWithString:price FirstCount:1 secondCount:6 thirdCount:price.length-7 firstColor:YLC_THEME_COLOR secondColor:YLC_THEME_COLOR thirdColor:RGB(144, 144, 144) firstFont:12 secondFont:14 thirdFont:12];

    priceLabel.attributedText = attr;
    
    [goodsBackgroundView addSubview:priceLabel];
    
    toPersonTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    toPersonTitleLabel.text = @"收货人";
    
    [self.contentView addSubview:toPersonTitleLabel];
    
    toPersonLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    toPersonLabel.text = @"张国华 13122134567";
    
    [self.contentView addSubview:toPersonLabel];
    
    toAdressTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    toAdressTitleLabel.text = @"收货地址";
    
    [self.contentView addSubview:toAdressTitleLabel];
    
    toAdressLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    toAdressLabel.text = @"上海市闵行区吴中路66号";
    
    [self.contentView addSubview:toAdressLabel];
    
    orderNumberTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    orderNumberTitleLabel.text = @"订单编号";
    
    [self.contentView addSubview:orderNumberTitleLabel];
    
    orderNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    orderNumberLabel.text = @"GF1312217";
    
    [self.contentView addSubview:orderNumberLabel];
    
    orderTimeTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    orderTimeTitleLabel.text = @"交易时间";
    
    [self.contentView addSubview:orderTimeTitleLabel];
    
    orderTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(102, 102, 102)];
    
    orderTimeLabel.text = @"2018/01/12 9:30";
    
    [self.contentView addSubview:orderTimeLabel];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [goodsBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top).offset(7);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(105);
    }];
    
    [goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsBackgroundView.mas_left).offset(15);
        make.top.equalTo(goodsBackgroundView.mas_top).offset(8);
        make.width.equalTo(117);
        make.height.equalTo(86);
    }];
    
    [goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsImageView.mas_right).offset(22);
        make.top.equalTo(goodsImageView.mas_top).offset(11);
        make.width.equalTo(weakSelf.frameWidth-117-44-15);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsTitleLabel.mas_left);
        make.bottom.equalTo(goodsImageView.mas_bottom).offset(-11);
    }];
    
    [toPersonTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsBackgroundView.mas_left).offset(10);
        make.top.equalTo(goodsBackgroundView.mas_bottom).offset(10);
    }];
    
    [toPersonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(goodsBackgroundView.mas_right).offset(-10);
        make.top.equalTo(toPersonTitleLabel.mas_top);
    }];
    
    [toAdressTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(toPersonTitleLabel.mas_left);
        make.top.equalTo(toPersonTitleLabel.mas_bottom).offset(10);
    }];
    
    [toAdressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(toPersonLabel.mas_right);
        make.top.equalTo(toPersonLabel.mas_bottom).offset(10);
    }];
    
    [orderNumberTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(toAdressTitleLabel.mas_left);
        make.top.equalTo(toAdressTitleLabel.mas_bottom).offset(10);
    }];
    
    [orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(toAdressLabel.mas_right);
        make.top.equalTo(orderNumberTitleLabel.mas_top);
    }];
    
    [orderTimeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(orderNumberTitleLabel.mas_left);
        make.top.equalTo(orderNumberTitleLabel.mas_bottom).offset(10);
    }];
    
    [orderTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(orderNumberLabel.mas_right);
        make.top.equalTo(orderTimeTitleLabel.mas_top);
    }];
}
@end
