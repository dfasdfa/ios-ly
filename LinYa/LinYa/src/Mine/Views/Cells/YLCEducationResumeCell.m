//
//  YLCEducationResumeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEducationResumeCell.h"
#import "YLCWordExperienceViewController.h"
#import "YLCEduExpViewController.h"
#import "YLCWorkExpCell.h"
@interface YLCEducationResumeCell()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCEducationResumeCell
{
    UILabel *titleLabel;
    UIImageView *arrowView;
    UIImageView *lineView;
    UITableView *messageTable;
    UIButton *increateBtn;
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(142, 148, 150)];
        
        titleLabel.text = @"";
        
        [self.contentView addSubview:titleLabel];
        
        arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
        
        [self.contentView addSubview:arrowView];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = RGB(235, 235, 235);
        
        [self.contentView addSubview:lineView];
        
        messageTable = [[UITableView alloc] initWithFrame:CGRectZero];
        
        messageTable.backgroundColor = [UIColor whiteColor];
        
        messageTable.delegate = self;
        
        messageTable.dataSource = self;
        
        messageTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self.contentView addSubview:messageTable];
        
        increateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [increateBtn setTitle:@"添加" forState:UIControlStateNormal];
        
        [increateBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
        
        increateBtn.layer.borderWidth = 0.5;
        
        increateBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
        
        [increateBtn addTarget:self action:@selector(increateResume) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:increateBtn];
        
        UIView *footView = [[UIView alloc] init];
        
        footView.frame = CGRectMake(0, 0, self.frameWidth, 50);
        
        messageTable.tableFooterView = footView;
        
        [footView addSubview:increateBtn];
        
        [increateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(footView.mas_centerX);
            make.centerY.equalTo(footView.mas_centerY);
            make.width.equalTo(60);
            make.height.equalTo(30);
        }];
    }
    return self;
}
- (void)setIsEdit:(BOOL)isEdit{
    increateBtn.hidden = !isEdit;
}
- (void)setIndex:(NSInteger)index{
    _index = index;
    
    if (_index==1) {
        titleLabel.text = @"工作经历";
    }else{
        titleLabel.text = @"教育经历";
    }
}
- (void)increateResume{
    if (_index==1) {
        YLCEduExpViewController *con = [[YLCEduExpViewController alloc] init];
        
        [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    }else{
        YLCWordExperienceViewController *con = [[YLCWordExperienceViewController alloc] init];
        
        [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    }
}
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    
    [messageTable reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (JK_IS_ARRAY_NIL(_dataList)) {
        return 0;
    }
    return _dataList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cell_id = @"resume_id";
    
    YLCWorkExpCell *cell  = [tableView dequeueReusableCellWithIdentifier:cell_id];
    
    
    
    if (!cell) {
        cell = [[YLCWorkExpCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_id];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (_index==1) {
        NSDictionary *dict = _dataList[indexPath.row];
        cell.title1 = [NSString stringWithFormat:@"%@ ~ %@",[YLCTool normalChangedWithTimeString:dict[@"start_time"]],[YLCTool normalChangedWithTimeString:dict[@"end_time"]]];
        
        cell.title2 = dict[@"company_name"];
        
        cell.sub2 = dict[@"postion"];
    }else{
        NSDictionary *dict = _dataList[indexPath.row];
        
        cell.title1 = [NSString stringWithFormat:@"%@毕业",[YLCTool normalChangedWithTimeString:dict[@"end_time"]]];
        
        cell.sub1 = [NSString stringWithFormat:@"%@",dict[@"profession"]];
        
        cell.title2 = dict[@"school_name"];
        
        BOOL isFormal = [dict[@"is_formal"] isEqualToString:@"1"];
        
        cell.sub2 = [NSString stringWithFormat:@"%@ %@",isFormal?@"统招":@"非统招",dict[@"diploma_txt"]];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_isEdit) {
        return 50;
    }
    return 0;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(5);
    }];
    
    [arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.centerY.equalTo(titleLabel.mas_centerY);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.height.equalTo(0.5);
    }];
    
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(30, 0, 10, 0));
    }];
}
@end
