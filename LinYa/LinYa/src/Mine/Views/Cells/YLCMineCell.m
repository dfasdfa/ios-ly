//
//  YLCMineCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMineCell.h"
#import "YLCEasyCommunicateView.h"
#import "YLCTitleAndIconModel.h"
#import "YLCTrainRegistrationViewController.h"
#import "YLCDentistDynamicViewController.h"
#import "YLCResumeViewController.h"
#import "YLCReceiveResumeListViewController.h"
#import "YLCInviteViewController.h"
#import "YLCWebViewViewController.h"
#import "YLCMyApplyBaseViewController.h"
#import "YLCMyCollectionBaseViewController.h"
@implementation YLCMineCell
{
    UILabel *titleLabel;
    YLCEasyCommunicateView *collectionView;
    NSArray *dataList;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDataSource];
        [self createCustomView];
    }
    return self;
}
- (void)initDataSource{
    
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    NSArray *titleArr = @[@"我的报名",@"我的简历",@"收到的简历",@"投递的职位",@"我的收藏",@"使用帮助"];
    
    for (int i = 0; i<titleArr.count; i++) {
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = titleArr[i];
        
        model.iconName = [NSString stringWithFormat:@"mine_%d",i+1];
        
        [container addObject:model];
    }
    
    dataList = container.copy;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(119,121,122)];
    
    titleLabel.text = @"我的服务";
    
    [self.contentView addSubview:titleLabel];
    
    collectionView = [[YLCEasyCommunicateView alloc] initWithFrame:CGRectZero];
    
    collectionView.listArr = dataList;
    
    collectionView.imageSize = CGSizeMake(30, 30);
    
    collectionView.cellSize = CGSizeMake((self.frameWidth-1.5)/4, 85);
    
    [collectionView.touchSignal subscribeNext:^(id x) {
        NSIndexPath *indexPath = x;
        
        if (indexPath.row==0) {
            [self gotoTrain];
        }
        if (indexPath.row==1) {
            [self gotoMyResume];
        }
        if (indexPath.row==2) {
            [self gotoReceiveResumeList];
        }
        if (indexPath.row==4) {
            [self gotoDynamic];
        }
        if (indexPath.row==3) {
            [self gotoMyJoinJob];
        }
        if (indexPath.row==5) {
            [self gotoHelper];
        }
    }];
    
    [self.contentView addSubview:collectionView];
}
- (void)gotoHelper{
    [YLCWebViewViewController showWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.linya920.com/wx/user/help"]] navTitle:@"使用帮助" isModel:NO from:_target];
}
- (void)gotoMyJoinJob{
    YLCInviteViewController *con = [[YLCInviteViewController alloc] init];
    
    con.type = 1;
    
    con.isMine = YES;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)gotoReceiveResumeList{
    YLCReceiveResumeListViewController *con = [[YLCReceiveResumeListViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoMyResume{
    
    YLCResumeViewController *con = [[YLCResumeViewController alloc] init];
    
    con.isExit = YES;
    
    con.resumeType = 0;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)gotoTrain{
//    YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
//

    YLCMyApplyBaseViewController *con = [[YLCMyApplyBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoDynamic{
//    YLCDentistDynamicViewController *con = [[YLCDentistDynamicViewController alloc] init];
    YLCMyCollectionBaseViewController *con = [[YLCMyCollectionBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.top.equalTo(titleLabel.mas_bottom).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
}
@end
