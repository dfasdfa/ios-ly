//
//  YLCWorkExpCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCWorkExpCell : UITableViewCell
@property (nonatomic ,copy)NSString *title1;
@property (nonatomic ,copy)NSString *sub1;
@property (nonatomic ,copy)NSString *title2;
@property (nonatomic ,copy)NSString *sub2;
@end
