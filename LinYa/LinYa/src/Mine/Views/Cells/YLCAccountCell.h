//
//  YLCAccountCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/5.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCAccountCell : UICollectionViewCell

@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *middle;
@property (nonatomic ,copy)NSString *last;
@property (nonatomic ,assign)NSInteger index;

@end
