//
//  YLCSubSelectCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSubSelectCell.h"

@implementation YLCSubSelectCell
{
    UILabel *titleLabel;
    UILabel *subLabel;
    UIImageView *subArrow;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(108, 108, 108)];
    
    [self.contentView addSubview:titleLabel];
    
    subLabel = [YLCFactory createLabelWithFont:15 color:RGB(183, 183, 183)];
    
    [self.contentView addSubview:subLabel];
    
    subArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
    
    [self.contentView addSubview:subArrow];
}
- (void)setText:(NSString *)text{
    subLabel.text = [YLCFactory isStringOk:text]?text:_placeholder;
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_right).offset(10);
        make.centerY.equalTo(titleLabel.mas_centerY);
    }];
    
    [subArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(12);
    }];
}
@end
