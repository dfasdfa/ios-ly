//
//  YLCResumeExpListCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCResumeExpListCell.h"
@interface YLCResumeExpListCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel1;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel2;
@property (weak, nonatomic) IBOutlet UILabel *subLabel1;
@property (weak, nonatomic) IBOutlet UILabel *subLabel2;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;



@end
@implementation YLCResumeExpListCell
- (void)setTitle1:(NSString *)title1{
    _titleLabel1.text = title1;
}
- (void)setTitle2:(NSString *)title2{
    _titleLabel2.text = title2;
}
- (void)setSub1:(NSString *)sub1{
    _subLabel1.text = sub1;
}
- (void)setSub2:(NSString *)sub2{
    _subLabel2.text = sub2;
}

- (IBAction)deleteResume:(id)sender {
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, deleteOrEditWithIsDelete:index:)) {
        [self.delegate deleteOrEditWithIsDelete:YES index:_index];
    }
}

- (IBAction)editResume:(id)sender {
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, deleteOrEditWithIsDelete:index:)) {
        [self.delegate deleteOrEditWithIsDelete:NO index:_index];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _deleteBtn.layer.borderColor = YLC_GRAY_COLOR.CGColor;
    _deleteBtn.layer.borderWidth = 0.5;
}

@end
