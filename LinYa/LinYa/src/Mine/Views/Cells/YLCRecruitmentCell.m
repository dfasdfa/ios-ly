//
//  YLCRecruitmentCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRecruitmentCell.h"

@implementation YLCRecruitmentCell
{
    UILabel *titleLabel;
    UIButton *button1;
    UIButton *button2;
    UILabel *buttonTitle1;
    UILabel *buttonTitle2;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(119, 123, 124)];
    
    titleLabel.text = @"统招";
    
    [self.contentView addSubview:titleLabel];
    
    buttonTitle2 = [YLCFactory createLabelWithFont:14 color:RGB(110, 110, 110)];
    
    buttonTitle2.text = @"否";
    
    [self.contentView addSubview:buttonTitle2];
    
    button2 = [YLCFactory createBtnWithImage:@"edu_normal"];
    
    [button2 setImage:[UIImage imageNamed:@"edu_select"] forState:UIControlStateSelected];
    
    button2.tag = 910;
    
    [button2 addTarget:self action:@selector(selectTypeWithButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:button2];
    
    buttonTitle1 = [YLCFactory createLabelWithFont:14 color:RGB(110, 110, 110)];
    
    buttonTitle1.text = @"是";
    
    [self.contentView addSubview:buttonTitle1];
    
    button1 = [YLCFactory createBtnWithImage:@"edu_normal"];
    
    button1.selected = YES;
    
    [button1 setImage:[UIImage imageNamed:@"edu_select"] forState:UIControlStateSelected];
    
    button1.tag = 911;
    
    [button1 addTarget:self action:@selector(selectTypeWithButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:button1];
}
- (void)setModel:(YLCProgramSelectModel *)model{
    titleLabel.text = model.title;
}
- (void)selectTypeWithButton:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.tag==910) {
        button1.selected = !sender.selected;
    }else{
        button2.selected = !sender.selected;
    }
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectAtIndex:)) {
        [self.delegate didSelectAtIndex:sender.tag-910];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [buttonTitle2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [button2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(buttonTitle2.mas_left).offset(-10);
        make.centerY.equalTo(buttonTitle2.mas_centerY);
        make.width.height.equalTo(20);
    }];
    
    [buttonTitle1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(button2.mas_left).offset(-20);
        make.centerY.equalTo(button2.mas_centerY);
    }];
    
    [button1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(buttonTitle1.mas_left).offset(-10);
        make.centerY.equalTo(buttonTitle1.mas_centerY);
    }];
}
@end
