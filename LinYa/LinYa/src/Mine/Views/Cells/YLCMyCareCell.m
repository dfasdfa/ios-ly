//
//  YLCMyCareCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyCareCell.h"

@implementation YLCMyCareCell
{
    UIImageView *personHeadIconView;
    UILabel *nameLabel;
    UIImageView *sexTypeView;
    UILabel *fansNumberLabel;
    UILabel *attentionNumberLabel;
    UIButton *careBtn;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    personHeadIconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:personHeadIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:RGB(62, 66, 67)];
    
    [self.contentView addSubview:nameLabel];
    
    sexTypeView = [[UIImageView alloc] init];
    
    sexTypeView.image = [UIImage imageNamed:@"sex_man"];
    
    [self.contentView addSubview:sexTypeView];
    
    attentionNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
//    attentionNumberLabel.text = @"关注：15";
    
    [self.contentView addSubview:attentionNumberLabel];
    
    fansNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(142, 147, 149)];
    
//    fansNumberLabel.text = @"粉丝：15";
    
    [self.contentView addSubview:fansNumberLabel];
    
    careBtn = [self commonBtnWithTitle:@"关注" color:[UIColor whiteColor] titleColor:YLC_THEME_COLOR];
    
    [careBtn setTitle:@"取消关注" forState:UIControlStateSelected];
    
    careBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    [careBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateSelected];
    
    careBtn.layer.borderColor = YLC_THEME_COLOR.CGColor;
    
    [careBtn addTarget:self action:@selector(careOrCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:careBtn];

    lineView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:lineView];
}
- (void)setModel:(YLCCareModel *)model{
    
    _model = model;
    
    nameLabel.text = [model.nick_name notNullString];
    
    [personHeadIconView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)setIsCare:(BOOL)isCare{
    if (isCare) {
        careBtn.selected = isCare;
    }else{
        if ([_model.is_both isEqualToString:@"1"]) {
            careBtn.selected = YES;
        }else{
            careBtn.selected = NO;
        }
    }
    [self updateUI];
}
- (void)careOrCancel:(UIButton *)sender{
    sender.selected = !sender.selected;
    [self updateUI];
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, careOrCancelWithIsCare:index:)) {
        [self.delegate careOrCancelWithIsCare:sender.selected index:_index];
    }

}
- (void)updateUI{
    if (careBtn.selected) {
        careBtn.layer.borderWidth = 0.5;
        careBtn.backgroundColor = [UIColor whiteColor];
        [careBtn setTitleColor:YLC_THEME_COLOR forState:UIControlStateNormal];
    }else{
        careBtn.layer.borderWidth = 0;
        careBtn.backgroundColor = YLC_THEME_COLOR;
        [careBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [personHeadIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(18);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.width.equalTo(52);
        make.height.equalTo(53);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(personHeadIconView.mas_top).offset(4);
        make.left.equalTo(personHeadIconView.mas_right).offset(8);
    }];
    
    [sexTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.left.equalTo(nameLabel.mas_right).offset(7);
        make.width.equalTo(11);
        make.height.equalTo(12);
    }];
    
    [attentionNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.bottom.equalTo(personHeadIconView.mas_bottom).offset(-4);
    }];
    
    [fansNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(attentionNumberLabel.mas_top);
        make.left.equalTo(attentionNumberLabel.mas_right).offset(8);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    [careBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(70);
        make.height.equalTo(35);
    }];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    return button;
}

@end
