//
//  YLCEducationResumeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCEducationResumeCell : UICollectionViewCell
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,strong)NSArray *dataList;
@property (nonatomic ,assign)BOOL isEdit;
@end
