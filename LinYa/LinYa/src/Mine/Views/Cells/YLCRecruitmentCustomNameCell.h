//
//  YLCRecruitmentCustomNameCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCRecruitmentCustomNameCell;
@protocol YLCRecruitmentCustomNameCellDelegate <NSObject>
- (void)customNameMessageDidChangedWithName:(NSString *)name;
- (void)sexDidChangedWithSex:(NSString *)sex;
@end
@interface YLCRecruitmentCustomNameCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *name;
@property (nonatomic ,copy)NSString *sex;
@property (nonatomic ,assign)id<YLCRecruitmentCustomNameCellDelegate>delegate;
@end
