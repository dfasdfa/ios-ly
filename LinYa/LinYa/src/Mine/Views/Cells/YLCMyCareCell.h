//
//  YLCMyCareCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCCareModel.h"
@class YLCMyCareCell;
@protocol YLCMyCareCellDelegate <NSObject>
- (void)careOrCancelWithIsCare:(BOOL)isCare
                         index:(NSInteger)index;
@end
@interface YLCMyCareCell : UICollectionViewCell
@property (nonatomic ,strong)YLCCareModel *model;
@property (nonatomic ,weak)id<YLCMyCareCellDelegate>delegate;
@property (nonatomic ,assign)BOOL isCare;
@property (nonatomic ,assign)NSInteger index;
@end
