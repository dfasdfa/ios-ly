//
//  YLCEasyReBuyCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyReBuyCell.h"

@implementation YLCEasyReBuyCell
{
    UIView *backView;
    UIImageView *iconView;
    UILabel *rebuyLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        backView = [[UIView alloc] init];
        
        backView.backgroundColor = YLC_THEME_COLOR;
        
        [self.contentView addSubview:backView];
        
        iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        
        [backView addSubview:iconView];
        
        rebuyLabel = [YLCFactory createLabelWithFont:17 color:[UIColor whiteColor]];
        
        rebuyLabel.text = @"续费";
        
        [backView addSubview:rebuyLabel];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf);
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth-59*2);
        make.height.equalTo(41);
    }];
    
    [rebuyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backView.mas_centerX);
        make.centerY.equalTo(backView.mas_centerY);
    }];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(rebuyLabel.mas_left).offset(-10);
        make.centerY.equalTo(rebuyLabel.mas_centerY);
    }];
    
}
@end
