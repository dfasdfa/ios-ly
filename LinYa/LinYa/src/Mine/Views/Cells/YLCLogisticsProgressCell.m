//
//  YLCLogisticsProgressCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLogisticsProgressCell.h"
#import "YLCLogisticsinfoCell.h"
#define LOGISTIC_INFO  @"YLCLogisticsinfoCell"
@interface YLCLogisticsProgressCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCLogisticsProgressCell
{
    UICollectionView *progressTable;
    NSArray *progressArr;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initDataSource];
        [self createCustomView];
    }
    return self;
}
- (void)initDataSource{
    progressArr = @[@"已拍下",@"已付款",@"已发货",@"交易完成"];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    flow.minimumInteritemSpacing = 0;
    
    progressTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    progressTable.backgroundColor = [UIColor whiteColor];
    
    [progressTable registerClass:[YLCLogisticsinfoCell class] forCellWithReuseIdentifier:LOGISTIC_INFO];
    
    progressTable.delegate = self;
    
    progressTable.dataSource = self;
    
    [self.contentView addSubview:progressTable];
    WS(weakSelf)
    [progressTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.height.equalTo(41);
    }];
}
- (void)setProgress:(NSInteger)progress{
    _progress = progress;
    
    [progressTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCLogisticsinfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LOGISTIC_INFO forIndexPath:indexPath];
    
    cell.normalImageName = [NSString stringWithFormat:@"progress_nm_%ld",indexPath.row+1];
    
    cell.selectImageName = [NSString stringWithFormat:@"progress_selected_%ld",indexPath.row+1];
    
    if (indexPath.row<progressArr.count) {
        cell.title = progressArr[indexPath.row];
    }
    
    if (indexPath.row==_progress) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    cell.index = indexPath.row;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth/4, 41);
}
@end
