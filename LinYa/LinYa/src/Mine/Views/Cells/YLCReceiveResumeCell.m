//
//  YLCReceiveResumeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCReceiveResumeCell.h"
@interface YLCReceiveResumeCell()
@property (weak, nonatomic) IBOutlet UIImageView *userIconView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userAgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *eduLabel;
@property (weak, nonatomic) IBOutlet UILabel *workExpLabel;

@property (weak, nonatomic) IBOutlet UILabel *posterLabel;




@end
@implementation YLCReceiveResumeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor whiteColor];
    // Initialization code
}
- (void)setModel:(YLCReceiveResumeModel *)model{
    [_userIconView sd_setImageWithURL:[NSURL URLWithString:model.user[@"imgsrc"]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    _userAgeLabel.text = model.user[@""];
    
    _sendTimeLabel.text = model.add_time;
    
    _phoneNumberLabel.text = model.user[@"account"];
    
    _userNameLabel.text = model.resume[@"real_name"];
    
    _eduLabel.text = model.resume[@"education"];
    
    _workExpLabel.text = [NSString stringWithFormat:@"%@年",model.resume[@"work_year"]];
    
    _posterLabel.text = model.resume[@"position"];
}
@end
