//
//  YLCRechargeMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRechargeMessageCell.h"
#import "YLCPayTimeCell.h"
#import "YLCPayModel.h"
#define PAY_CELL @"YLCPayTimeCell"
@interface YLCRechargeMessageCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCRechargeMessageCell
{
    UICollectionView *timeTable;
    UILabel *numberTitleLabel;
    UILabel *payNumberLabel;
    UILabel *timeTitleLabel;
    UIImageView *lineView;
    NSArray *timeList;
    NSInteger selectIndex;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        selectIndex = 0;
        
        timeList = @[@"6",@"30",@"68",@"128",@"328",@"648"];
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
//    numberTitleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
//
//    numberTitleLabel.text = @"充值账户：";
//
//    [self.contentView addSubview:numberTitleLabel];
//
//    payNumberLabel = [YLCFactory createLabelWithFont:13 color:RGB(163, 180, 190)];
//
//    payNumberLabel.text = @"1234567890";
//
//    [self.contentView addSubview:payNumberLabel];
    
//    lineView = [[UIImageView alloc] init];
//
//    lineView.backgroundColor = RGB(219, 230, 234);
//
//    [self.contentView addSubview:lineView];
    
    timeTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(62, 65, 66)];
    
    [self.contentView addSubview:timeTitleLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumInteritemSpacing = 5;
    
    flow.minimumLineSpacing = 5;
    
    timeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    timeTable.backgroundColor = [UIColor clearColor];
    
    [timeTable registerClass:[YLCPayTimeCell class] forCellWithReuseIdentifier:PAY_CELL];
    
    timeTable.delegate = self;
    
    timeTable.dataSource = self;
    
    [self.contentView addSubview:timeTable];
}
- (void)setRechargeType:(NSInteger)rechargeType{
    if (rechargeType==1) {
        timeTitleLabel.text = @"充值金额";
        
    }else{
        timeTitleLabel.text = @"购买服务时间";
    }
}
- (void)setPayArr:(NSArray *)payArr{
    _payArr = payArr;
    
    [timeTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _payArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPayTimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PAY_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_payArr.count) {
        YLCPayModel *model = _payArr[indexPath.row];
        cell.string = model.title;
    }
    if (indexPath.row==selectIndex) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.frameWidth-34-15)/4, 44);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectIndex = indexPath.row;
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectPayWithSelectIndex:)) {
        [self.delegate didSelectPayWithSelectIndex:selectIndex];
    }
    [timeTable reloadData];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
//    [numberTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(weakSelf.mas_left).offset(17);
//        make.top.equalTo(weakSelf.mas_top).offset(12);
//    }];
//
//    [payNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-11);
//        make.centerY.equalTo(numberTitleLabel.mas_centerY);
//    }];
//
//    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(numberTitleLabel.mas_left);
//        make.top.equalTo(numberTitleLabel.mas_bottom).offset(12);
//        make.right.equalTo(weakSelf.mas_right);
//        make.height.equalTo(0.5);
//    }];
    
    [timeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(17);
        make.top.equalTo(weakSelf.mas_top).offset(12);
    }];
    
    [timeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeTitleLabel.mas_left);
        make.top.equalTo(timeTitleLabel.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-34);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
}

@end
