//
//  YLCRecordCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRecordCell.h"

@implementation YLCRecordCell
{
    UILabel *buyTimeLabel;
    UILabel *priceLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        buyTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
        
        [self.contentView addSubview:buyTimeLabel];
        
        priceLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
        
        priceLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:priceLabel];
    }
    return self;
}
- (void)setIsPrice:(BOOL)isPrice{
    _isPrice = isPrice;
    if (isPrice) {
        priceLabel.textColor = YLC_THEME_COLOR;
        buyTimeLabel.textAlignment = NSTextAlignmentLeft;
    }else{
        priceLabel.textColor = RGB(103, 108, 109);
        buyTimeLabel.textAlignment = NSTextAlignmentCenter;
    }
}
- (void)setTime:(NSString *)time{
    buyTimeLabel.text = time;
}
- (void)setPrice:(NSString *)price{
    if (_isPrice) {
        NSAttributedString *attr = [YLCFactory getAttributedStringWithString:price FirstCount:1 secondCount:price.length-1 firstFont:12 secondFont:14 firstColor:YLC_THEME_COLOR secondColor:YLC_THEME_COLOR];
        
        priceLabel.attributedText = attr;
    }else{
        priceLabel.text = price;
    }

    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [buyTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(5);
        make.right.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.height.equalTo(weakSelf.frameHeight);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_centerX);
        make.right.equalTo(weakSelf.mas_right);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.height.equalTo(weakSelf.mas_height);
    }];
}
@end
