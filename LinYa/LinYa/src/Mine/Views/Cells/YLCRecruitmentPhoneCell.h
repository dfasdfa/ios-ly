//
//  YLCRecruitmentPhoneCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCRecruitmentPhoneCell;
@protocol YLCRecruitmentPhoneCellDelegate <NSObject>
- (void)phoneMessageDidChangedWithPhone:(NSString *)phoneNum;
@end
@interface YLCRecruitmentPhoneCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *message;
@property (nonatomic ,assign)id<YLCRecruitmentPhoneCellDelegate>delegate;
@end
