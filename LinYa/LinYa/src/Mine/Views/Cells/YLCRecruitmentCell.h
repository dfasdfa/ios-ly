//
//  YLCRecruitmentCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCProgramSelectModel.h"
@class YLCRecruitmentCell;
@protocol YLCRecruitmentCellDelegate <NSObject>
- (void)didSelectAtIndex:(NSInteger)index;
@end
@interface YLCRecruitmentCell : UICollectionViewCell
@property (nonatomic ,strong)YLCProgramSelectModel *model;
@property (nonatomic ,weak)id<YLCRecruitmentCellDelegate>delegate;
@end
