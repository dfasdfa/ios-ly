//
//  YLCResumeExpListCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/20.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCResumeExpListCell;
@protocol YLCResumeExpListCellDelegate <NSObject>
- (void)deleteOrEditWithIsDelete:(BOOL)isDelete
                           index:(NSInteger)index;
@end
@interface YLCResumeExpListCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title1;
@property (nonatomic ,copy)NSString *sub1;
@property (nonatomic ,copy)NSString *title2;
@property (nonatomic ,copy)NSString *sub2;
@property (nonatomic ,weak)id<YLCResumeExpListCellDelegate>delegate;
@property (nonatomic ,assign)NSInteger index;
@end
