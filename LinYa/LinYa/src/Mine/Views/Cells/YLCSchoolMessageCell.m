//
//  YLCSchoolMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSchoolMessageCell.h"

@implementation YLCSchoolMessageCell
{
    UIImageView *imageView1;
    UIImageView *imageView2;
    UIImageView *imageLineView;
    UIImageView *lineView;
    UILabel *beganTitleLabel;
    UILabel *endTitleLabel;
    UILabel *beganTimeLabel;
    UILabel *endTimeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    imageView1 = [[UIImageView alloc] init];
    
    imageView1.backgroundColor = RGB(255, 102, 68);
    
    [self.contentView addSubview:imageView1];
    
    imageView2 = [[UIImageView alloc] init];
    
    imageView2.backgroundColor = RGB(255, 102, 68);
    
    [self.contentView addSubview:imageView2];
    
    imageLineView = [[UIImageView alloc] init];
    
    imageLineView.backgroundColor = RGB(235, 235, 235);
    
    [self.contentView addSubview:imageLineView];

    beganTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(60,63,64)];
    
    beganTitleLabel.text = @"开始时间";
    
    [self.contentView addSubview:beganTitleLabel];
    
    endTitleLabel = [YLCFactory createLabelWithFont:14 color:RGB(60,63,64)];
    
    endTitleLabel.text = @"结束时间";
    
    [self.contentView addSubview:endTitleLabel];
    
    beganTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(213, 213, 213)];
    
    beganTimeLabel.textAlignment = NSTextAlignmentRight;
    
    beganTimeLabel.text = @"请选择";
    
    beganTimeLabel.userInteractionEnabled = YES;
    
    [self.contentView addSubview:beganTimeLabel];
    
    lineView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:lineView];
    
    endTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(213, 213, 213)];
    
    endTimeLabel.userInteractionEnabled = YES;
    
    endTimeLabel.textAlignment = NSTextAlignmentRight;
    
    endTimeLabel.text = @"请选择";
    
    [self.contentView addSubview:endTimeLabel];
    
    UITapGestureRecognizer *beganGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beganTouch)];
    
    [beganTimeLabel addGestureRecognizer:beganGes];
    
    UITapGestureRecognizer *endGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endTouch)];
    
    [endTimeLabel addGestureRecognizer:endGes];
}
- (void)setBeganTime:(NSString *)beganTime{
    _beganTime = beganTime;
    beganTimeLabel.text = [YLCFactory isStringOk:beganTime]?beganTime:@"请选择";
}
- (void)setEndTime:(NSString *)endTime{
    _endTime = endTime;
    
    endTimeLabel.text = [YLCFactory isStringOk:endTime]?endTime:@"请选择";
}
- (void)beganTouch{
    RACSubject *beginSubject = [RACSubject subject];
    [YLCHUDShow showTimePickWithSignal:beginSubject];
    [beginSubject subscribeNext:^(id x) {
        _beganTime = x;
        beganTimeLabel.text = x;
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, sendTimeWithBeganTime:endTime:)) {
            [self.delegate sendTimeWithBeganTime:_beganTime endTime:_endTime];
        }
    }];
}

- (void)endTouch{
    RACSubject *endSubject = [RACSubject subject];
    [YLCHUDShow showTimePickWithSignal:endSubject];
    [endSubject subscribeNext:^(id x) {
        _endTime = x;
        endTimeLabel.text = x;
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, sendTimeWithBeganTime:endTime:)) {
            [self.delegate sendTimeWithBeganTime:_beganTime endTime:_endTime];
        }
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.height.equalTo(10);
    }];
    
    [imageLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(imageView1.mas_centerX);
        make.top.equalTo(imageView1.mas_bottom);
        make.width.equalTo(0.5);
        make.height.equalTo(50);
    }];
    
    [imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView1.mas_left);
        make.top.equalTo(imageLineView.mas_bottom);
        make.width.height.equalTo(10);
    }];
    
    [beganTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView1.mas_right).offset(40);
        make.centerY.equalTo(imageView1.mas_centerY);
    }];
    
    [endTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(beganTitleLabel.mas_left);
        make.centerY.equalTo(imageView2.mas_centerY);
    }];
    
    [beganTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.centerY.equalTo(beganTitleLabel.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(weakSelf.frameHeight);
    }];
    
    [endTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(beganTimeLabel.mas_right);
        make.centerY.equalTo(imageView2.mas_centerY);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(weakSelf.frameHeight);
    }];
}
@end
