//
//  YLCMyOrderCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyOrderCell.h"

@implementation YLCMyOrderCell
{
    UILabel *orderNumberLabel;
    UIImageView *goodsImageView;
    UILabel *goodsTitleLabel;
    UILabel *priceLabel;
    UIButton *refundBtn;
    UIButton *quickBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    orderNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(123, 135, 142)];
    
    [self.contentView addSubview:orderNumberLabel];
    
    goodsImageView = [[UIImageView alloc] init];
    
    goodsImageView.image = [UIImage imageNamed:@"image_placeholder"];
    
    [self.contentView addSubview:goodsImageView];
    
    goodsTitleLabel = [YLCFactory createLabelWithFont:18 color:RGB(51, 51, 51)];
    
    goodsTitleLabel.numberOfLines = 0;
    
    [self.contentView addSubview:goodsTitleLabel];
    
    priceLabel = [YLCFactory createLabelWithFont:14 color:[UIColor blackColor]];
    
    [self.contentView addSubview:priceLabel];
    
    refundBtn = [self commonBtnWithTitle:@"" color:[UIColor whiteColor] titleColor:RGB(105, 116, 120)];
    
    refundBtn.layer.borderColor = RGB(105, 116, 120).CGColor;
    
    [refundBtn addTarget:self action:@selector(goPublish) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:refundBtn];
    
    quickBtn = [self commonBtnWithTitle:@"" color:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    
    [self.contentView addSubview:quickBtn];
    
    quickBtn.hidden = YES;
}
- (void)setOrderTitle:(NSString *)orderTitle{
    orderNumberLabel.text = orderTitle;
}
- (void)setGoodsTitle:(NSString *)goodsTitle{
    goodsTitleLabel.text = goodsTitle;
}
- (void)setRefundTitle:(NSString *)refundTitle{
    [refundBtn setTitle:refundTitle forState:UIControlStateNormal];
}
- (void)setQuickTitle:(NSString *)quickTitle{
    [quickBtn setTitle:quickTitle forState:UIControlStateNormal];
    
}
- (void)setPriceAttr:(NSAttributedString *)priceAttr{
    priceLabel.attributedText = priceAttr;
}
- (void)setIconPath:(NSString *)iconPath{
    [goodsImageView sd_setImageWithURL:[NSURL URLWithString:iconPath] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)goPublish{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];

    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"goodsId":_caseId,@"isPublish":_publish} method:@"POST" urlPath:@"app/goods/under" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSString *tipString = [_isPublish isEqualToString:@"1"]?@"下架成功":@"上架成功";
        
//        [SVProgressHUD showSuccessWithStatus:tipString];
        [YLCHUDShow showAlertViewWithMessage:tipString title:@"提示"];
        
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeSuccess)) {
            [self.delegate didChangeSuccess];
        }
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(16);
        make.top.equalTo(weakSelf.mas_top).offset(12);
    }];
    
    [goodsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(orderNumberLabel.mas_left);
        make.top.equalTo(orderNumberLabel.mas_bottom).offset(20);
        make.width.equalTo(110);
        make.height.equalTo(92);
    }];
    
    [goodsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsImageView.mas_right).offset(18);
        make.top.equalTo(goodsImageView.mas_top).offset(9);
        make.right.equalTo(weakSelf.mas_right).offset(-23);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(goodsTitleLabel.mas_left);
        make.bottom.equalTo(goodsImageView.mas_bottom);
    }];
    
//    [quickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-22);
//        make.bottom.equalTo(weakSelf.mas_bottom).offset(-17);
//        make.width.equalTo(80);
//        make.height.equalTo(32);
//    }];
    
    [refundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-22);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-17);
        make.width.equalTo(80);
        make.height.equalTo(32);
    }];
}
- (UIButton *)commonBtnWithTitle:(NSString *)title
                           color:(UIColor *)color
                      titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button.layer.borderColor = color.CGColor;
    
    button.layer.borderWidth = 0.5;
    
    button.layer.cornerRadius = 3;
    
    button.layer.masksToBounds = YES;
    
    button.backgroundColor = color;
    
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    
    return button;
}
@end
