//
//  YLCProgramListView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCProgramListView : UIView
@property (nonatomic ,strong)NSArray *programList;
@property (nonatomic ,strong)RACSubject *selectSignal;
@property (nonatomic ,copy)NSString *classId;
@end
