//
//  YLCUnusedCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCUnusedCell.h"
#import "YLCGoodsModel.h"
#import "YLCAreaModel.h"
@implementation YLCUnusedCell
{
    UIImageView *itemImageView;
    UILabel *titleLabel;
    UILabel *payLabel;
    UILabel *areaLabel;
    UILabel *seeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    itemImageView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:itemImageView];
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(41, 41, 41)];
    
    [self.contentView addSubview:titleLabel];
    
    payLabel = [YLCFactory createLabelWithFont:17 color:YLC_THEME_COLOR];
    
    [self.contentView addSubview:payLabel];
    
    areaLabel = [YLCFactory createLabelWithFont:14 color:RGB(153, 153, 153)];
    
    [self.contentView addSubview:areaLabel];
    
    seeLabel = [YLCFactory createLabelWithFont:14 color:RGB(153, 153, 153)];
    
    [self.contentView addSubview:seeLabel];
}
- (void)setModel:(YLCGoodsModel *)model{
    [itemImageView sd_setImageWithURL:[NSURL URLWithString:model.main_img] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    titleLabel.text = [YLCFactory isStringOk:model.title]?model.title:@"";
    
    payLabel.text = [NSString stringWithFormat:@"￥%@",[YLCFactory isStringOk:model.price]?model.price:@""];
    
//    YLCAreaModel *areaModel = [[YLCAccountManager shareManager] getAreaModelWithAreaId:model.location_id];
    
//    YLCAreaModel *parentModel = [[YLCAccountManager shareManager] getAreaModelWithAreaId:areaModel.parentid];
    
//    areaLabel.text = [NSString stringWithFormat:@"%@ %@",[YLCFactory isStringOk:parentModel.areaname]?parentModel.areaname:@"",[YLCFactory isStringOk:areaModel.areaname]?areaModel.areaname:@""];
    areaLabel.text = [NSString stringWithFormat:@"%@",model.area];
    seeLabel.text = [NSString stringWithFormat:@"%@人评论",model.comment_num];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(130);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(itemImageView.mas_left).offset(5);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(itemImageView.mas_bottom).offset(5);
        make.height.equalTo(15);
    }];
    
    [payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(payLabel.mas_left);
        make.top.equalTo(payLabel.mas_bottom).offset(5);
    }];
    
    [seeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-5);
        make.top.equalTo(areaLabel.mas_top);
    }];
    
    
}
@end
