//
//  YLCTrainRegisInfoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegisInfoCell.h"

@implementation YLCTrainRegisInfoCell
{
    UILabel *titleLabel;
    UIImageView *lineView;
    UILabel *infoLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
        
        titleLabel.text = @"活动介绍";
        
        [self.contentView addSubview:titleLabel];
        
        lineView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:lineView];
        
        infoLabel = [YLCFactory createLabelWithFont:15 color:RGB(121, 130, 135)];
        
        infoLabel.numberOfLines = 0;
        
        [self.contentView addSubview:infoLabel];
    }
    return self;
}
- (void)setModel:(YLCActivityModel *)model{
    if (model.hasLoad) {
        infoLabel.attributedText = model.htmlAttr;
    }
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(0.5);
    }];
    
    [infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.greaterThanOrEqualTo(@10);
    }];
}
@end
