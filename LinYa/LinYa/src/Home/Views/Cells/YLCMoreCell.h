//
//  YLCMoreCell.h
//  LinYa
//
//  Created by 初程程 on 2018/6/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCMoreCell;
@protocol YLCMoreCellDelegate<NSObject>
- (void)didMoreSelectAtIndexPath:(NSIndexPath *)indexPath
                         section:(NSInteger)section;
@end
@interface YLCMoreCell : UICollectionViewCell
@property (nonatomic ,weak)id<YLCMoreCellDelegate>delegate;
@property (nonatomic ,strong)NSArray *moreList;
@property (nonatomic ,assign)NSInteger section;
@end
