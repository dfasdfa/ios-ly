//
//  YLCRegisPersonImageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRegisPersonImageCell.h"

@implementation YLCRegisPersonImageCell
{
    YLCUserHeadView *headView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        headView = [[YLCUserHeadView alloc] init];
        
        [self.contentView addSubview:headView];
    }
    return self;
}
- (void)setHeadIconUrl:(NSString *)headIconUrl{
//    [headView sd_setImageWithURL:[NSURL URLWithString:headIconUrl] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    [headView configHeadViewWithImageUrl:headIconUrl placeholder:@"image_placeholder" isGoCenter:YES userId:_userId];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
@end
