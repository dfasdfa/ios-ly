//
//  YLCEasyImageAddCell.m
//  LinYa
//
//  Created by 初程程 on 2018/5/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyImageAddCell.h"
@interface YLCEasyImageAddCell()
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
@implementation YLCEasyImageAddCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_selectBtn setImage:[UIImage imageNamed:@"knob_normal"] forState:UIControlStateNormal];
    
    [_selectBtn setImage:[UIImage imageNamed:@"buy_selected"] forState:UIControlStateSelected];
}

- (IBAction)didSelect:(id)sender {
//    UIButton *button = sender;
    _model.isSelect = !_model.isSelect;
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectEditWithModel:index:)) {
        [self.delegate didSelectEditWithModel:_model index:_index];
    }
}

- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        [_selectBtn setImage:[UIImage imageNamed:@"buy_selected"] forState:UIControlStateNormal];
    }else{
        [_selectBtn setImage:[UIImage imageNamed:@"knob_normal"] forState:UIControlStateNormal];
    }
}
- (void)setModel:(YLCEasyImageListModel *)model{
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)configCell{
    if (![_model.is_sys isEqualToString:@"1"]&&_isEdit) {
        _selectBtn.hidden = NO;
    }else{
        _selectBtn.hidden = YES;
    }
}
- (UIImageView *)getImageView{
    return _imageView;
}
@end
