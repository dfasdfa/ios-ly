//
//  YLCInviteCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteCell.h"

@implementation YLCInviteCell
{
    UILabel *inviteTitleLabel;
    UILabel *requireLabel;
    UILabel *payLabel;
    UIImageView *companyIconView;
    UILabel *companyNameLabel;
    UILabel *companyInfoLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    inviteTitleLabel = [YLCFactory createLabelWithFont:17 color:RGB(0, 0, 0)];
    
    inviteTitleLabel.numberOfLines = 0;
    
    [self.contentView addSubview:inviteTitleLabel];
    
    payLabel = [YLCFactory createLabelWithFont:16 color:YLC_THEME_COLOR];
    
    [self.contentView addSubview:payLabel];
    
    requireLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    
    
    [self.contentView addSubview:requireLabel];
    
    companyIconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:companyIconView];
    
    companyNameLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    
    
    [self.contentView addSubview:companyNameLabel];
    
    companyInfoLabel = [YLCFactory createLabelWithFont:12 color:RGB(215, 215, 215)];
    
    
    
    [self.contentView addSubview:companyInfoLabel];
}
- (void)setModel:(YLCInviteModel *)model{
    _model = model;
    
    inviteTitleLabel.text = [model.position notNullString];
    
    payLabel.text = [model.salary_name notNullString];
    
    requireLabel.text =[NSString stringWithFormat:@"%@ %@ %@",[model.location_name notNullString],[model.education_name notNullString],[model.workyear_name notNullString]];
    
    companyNameLabel.text = [model.company_name notNullString];
    
    [companyIconView sd_setImageWithURL:[NSURL URLWithString:model.company_logo] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
//    companyInfoLabel.text = [NSString stringWithFormat:@"%@ | %@",[model.company_size notNullString],[model.type notNullString]];
//    @"B轮 100-300人 | 医疗";
//    @"上海 本科 2年";
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [inviteTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(weakSelf.frameWidth-150);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [requireLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(inviteTitleLabel.mas_left);
        make.top.equalTo(inviteTitleLabel.mas_bottom).offset(5);
    }];
    
    [payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.top.equalTo(inviteTitleLabel.mas_top);
    }];
    
    [companyIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        make.left.equalTo(inviteTitleLabel.mas_left);
        make.width.equalTo(40);
        make.height.equalTo(40);
    }];
    
    [companyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(companyIconView.mas_right).offset(5);
        make.top.equalTo(companyIconView.mas_top);
    }];
    
    [companyInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(companyIconView.mas_bottom);
        make.left.equalTo(companyNameLabel.mas_left);
    }];
}
@end
