//
//  YLCTrainRegisInfoFootView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCActivityModel.h"
@interface YLCTrainRegisInfoFootView : UICollectionReusableView
@property (nonatomic ,strong)YLCActivityModel *model;
@property (nonatomic ,assign)NSInteger type;//0是培训报名列表 1是活动列表
@end
