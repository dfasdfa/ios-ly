//
//  YLCHomeRecommendCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeRecommendCell : UICollectionViewCell
@property (nonatomic ,strong)NSArray *subViews;
@property (nonatomic ,strong)NSArray *newsList;
@property (nonatomic ,strong)NSArray *topList;
@end
