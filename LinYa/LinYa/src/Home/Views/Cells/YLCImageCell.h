//
//  YLCImageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCCompanyRecruitModel;
@interface YLCImageCell : UICollectionViewCell
@property (nonatomic ,strong)YLCCompanyRecruitModel *model;
@property (nonatomic ,assign)BOOL needCorner;
@end
