//
//  YLCNewTrainCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewTrainCell.h"
#import "YLCHomeNewsTopView.h"
#import "YLCImageCell.h"
#import "YLCHomeVideoCell.h"
#import "YLCTrainbBaseViewController.h"
#import "YLCVideoLiftInfoViewController.h"
#define VIDEO_CELL @"YLCHomeVideoCell"
@interface YLCNewTrainCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCNewTrainCell
{
    YLCHomeNewsTopView *topView;
    UICollectionView *videoTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 5;
        
        self.layer.masksToBounds = YES;
        
        topView = [[YLCHomeNewsTopView alloc] initWithFrame:CGRectZero];
        
        topView.iconName = @"home_newTrain";
        
        [topView.moreSignal subscribeNext:^(id x) {
            [self gotoVideoTrain];
        }];
        
        [self.contentView addSubview:topView];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 10;
        
        videoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        videoTable.backgroundColor = [UIColor whiteColor];
        
        [videoTable registerClass:[YLCHomeVideoCell class] forCellWithReuseIdentifier:VIDEO_CELL];
        
        videoTable.delegate = self;
        
        videoTable.dataSource = self;
        
        videoTable.alwaysBounceVertical = NO;
        
        [self.contentView addSubview:videoTable];
    }
    return self;
}
- (void)gotoVideoTrain{
    YLCTrainbBaseViewController *con = [[YLCTrainbBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)setVideoList:(NSArray *)videoList{
    
    _videoList = videoList;
    
    [videoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _videoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCHomeVideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_videoList.count) {
        cell.model = _videoList[indexPath.row];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self gotoVideoInfoWithIndex:indexPath.row];
}
- (void)gotoVideoInfoWithIndex:(NSInteger)index{
    YLCHomeVideoModel *model = _videoList[index];
    
    YLCVideoLiftInfoViewController *con = [[YLCVideoLiftInfoViewController alloc] init];
    
    con.video_id = model.id;
    
    con.type = 1;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];

}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth-20, 175);
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(9);
        make.top.equalTo(weakSelf.mas_top).offset(17);
        make.width.equalTo(weakSelf.frameWidth-18);
        make.height.equalTo(26);
    }];
    
    [videoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(topView.mas_bottom).offset(22);
        make.width.equalTo(weakSelf.frameWidth);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
}
@end
