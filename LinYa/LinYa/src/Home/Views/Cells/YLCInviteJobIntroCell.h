//
//  YLCInviteJobIntroCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCInviteModel.h"
@interface YLCInviteJobIntroCell : UICollectionViewCell
@property (nonatomic ,strong)YLCInviteModel *model;
@end
