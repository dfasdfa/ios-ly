//
//  YLCProgramListCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramListCell.h"
#import "YLCStarView.h"
#import "YLCProgramIntroduceView.h"
@interface YLCProgramListCell()<UITableViewDelegate,UITableViewDataSource>
@end
@implementation YLCProgramListCell
{
    YLCProgramIntroduceView *introduceView;
    UILabel *programPriceLabel;
    UILabel *programPricelabel2;
    UITableView *bottomView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    introduceView = [[YLCProgramIntroduceView alloc] initWithFrame:CGRectZero];
    
    introduceView.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:introduceView];
    
//    programPriceLabel = [YLCFactory createLabelWithFont:16 color:[UIColor whiteColor]];
//
//    [self.contentView addSubview:programPriceLabel];
//
//    programPricelabel2 = [YLCFactory createLabelWithFont:14];
//
//    [self.contentView addSubview:programPricelabel2];
    
    bottomView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    bottomView.delegate = self;
    
    bottomView.dataSource = self;
    
    bottomView.backgroundColor = [UIColor clearColor];
    
    bottomView.tableFooterView = [[UIView alloc] init];
    
    bottomView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.contentView addSubview:bottomView];
}
- (void)setModel:(YLCEasyProgramListModel *)model{
    
    _model = model;
    introduceView.title = model.name;
    introduceView.score = model.star;
    introduceView.safe = model.period;
    introduceView.subsidy = [NSString stringWithFormat:@"%@",model.subsidy];
    introduceView.feature = model.feature;
    introduceView.personNum = model.people_num;
    [bottomView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _model.options.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"easy_table_cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"easy_table_cell"];
        
        cell.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (indexPath.row<_model.options.count) {
        NSDictionary *optionDict = _model.options[indexPath.row];
        NSString *optionName = optionDict[@"goods_option"];
        NSString *optionPrice = optionDict[@"money"];
        NSString *priceString = [NSString stringWithFormat:@"%@:￥%@",optionName,optionPrice];
        NSMutableAttributedString *attr = [YLCFactory getThreeCountAttributedStringWithString:priceString FirstCount:optionName.length+1 secondCount:1 thirdCount:optionPrice.length firstColor:RGB(140, 140, 140) secondColor:YLC_THEME_COLOR thirdColor:YLC_THEME_COLOR firstFont:15 secondFont:15 thirdFont:19];
        cell.textLabel.attributedText = attr;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didTouchTableSelectAtIndex:)) {
        [self.delegate didTouchTableSelectAtIndex:_index];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    CGSize size = [YLCFactory getStringSizeWithString:_model.feature width:self.frameWidth-40 font:13.7];
    
    [introduceView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 40, 0));
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(150+size.height-60);
    }];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(introduceView.mas_left);
        make.top.equalTo(introduceView.mas_bottom).offset(20);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
    
    
    
//    [programPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(weakSelf.mas_left).offset(21);
//        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
//    }];
//
//    [programPricelabel2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-21);
//        make.centerY.equalTo(programPriceLabel.mas_centerY);
//    }];
}
@end
