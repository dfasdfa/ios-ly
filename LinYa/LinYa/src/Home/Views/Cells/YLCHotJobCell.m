//
//  YLCHotJobCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHotJobCell.h"
#import "YLCHomeNewsTopView.h"
#import "YLCImageCell.h"
#import "YLCInviteViewController.h"
#import "YLCInviteInfoViewController.h"
#import "YLCCompanyRecruitModel.h"
#define IMAGE_CELL @"YLCImageCell"
@interface YLCHotJobCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCHotJobCell
{
    YLCHomeNewsTopView *topView;
    UICollectionView *companyListTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 5;
        
        self.layer.masksToBounds = YES;
        
        topView = [[YLCHomeNewsTopView alloc] initWithFrame:CGRectZero];
        
        topView.iconName = @"home_hotjob";
//        @weakify(self);
        [topView.moreSignal subscribeNext:^(id x) {
//            @strongify(self);
            [self gotoHotJobList];
        }];
        
        [self.contentView addSubview:topView];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 2;
        
        flow.minimumInteritemSpacing = 2;
        
        companyListTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        companyListTable.backgroundColor = [UIColor whiteColor];
        
        [companyListTable registerClass:[YLCImageCell class] forCellWithReuseIdentifier:IMAGE_CELL];
        
        companyListTable.delegate = self;
        
        companyListTable.dataSource = self;
        
        [self.contentView addSubview:companyListTable];

    }
    return self;
}
- (void)gotoHotJobList{
    YLCInviteViewController *con = [[YLCInviteViewController alloc] init];
    
//    YLCNavigationViewController *nav = ;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)setCompanyList:(NSArray *)companyList{
    
    _companyList = companyList;
    
    [companyListTable reloadData];
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _companyList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IMAGE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_companyList.count) {
        cell.model = _companyList[indexPath.row];
    }
    cell.needCorner = YES;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCCompanyRecruitModel *model = _companyList[indexPath.row];
    
    YLCInviteInfoViewController *con = [[YLCInviteInfoViewController alloc] init];
    
    con.inviteId = model.id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoInviteInfo{
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(71, 71);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(9);
        make.top.equalTo(weakSelf.mas_top).offset(17);
        make.width.equalTo(weakSelf.frameWidth-18);
        make.height.equalTo(26);
    }];
    
    [companyListTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(topView.mas_bottom).offset(12);
        make.width.equalTo(weakSelf.frameWidth-30);
        make.height.equalTo(71);
    }];
}
@end
