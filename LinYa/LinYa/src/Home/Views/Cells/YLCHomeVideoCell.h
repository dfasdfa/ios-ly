//
//  YLCHomeVideoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/13.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCHomeVideoModel.h"
@interface YLCHomeVideoCell : UICollectionViewCell
@property (nonatomic ,strong)YLCHomeVideoModel *model;
@end
