//
//  YLCVideoTrainCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoTrainCell.h"
@implementation YLCVideoTrainCell
{
    
    UIImageView *userIconView;
    UILabel *nameLabel;
    UILabel *seeNumberLabel;
    UIButton *moreBtn;
    UIButton *shareBtn;
    UILabel *titleLabel;
    UIButton *playBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}

- (void)setModel:(YLCVideoTrainModel *)model{
    _model = model;
    if (JK_IS_STR_NIL(model.played_num)) {
        seeNumberLabel.text = @"";
    }else{
        seeNumberLabel.text = [NSString stringWithFormat:@"%@次播放",model.played_num];
    }

    
    titleLabel.text = model.title;
    
    nameLabel.text = [YLCFactory isStringOk:model.author]?model.author:@"邻牙共享平台";
    
    [_posterView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)createCustomView{
    _posterView = [[UIImageView alloc] init];
    
    _posterView.backgroundColor = RGB(225, 225, 225);
    
    _posterView.tag = 1111;
    
    _posterView.userInteractionEnabled = YES;
    
    [self.contentView addSubview:_posterView];
    
    _posterView.frame = CGRectMake(0, 0, self.frameWidth, self.frameHeight-50);

    playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [playBtn setImage:[UIImage imageNamed:@"video_list_cell_big_icon"] forState:UIControlStateNormal];
    
    [playBtn addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];

    playBtn.frame = CGRectMake(_posterView.frameWidth/2-25, _posterView.frameHeight/2-25, 50, 50);
    
    [_posterView addSubview:playBtn];
    
    [playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_posterView);
        make.width.height.equalTo(50);
    }];
    WS(weakSelf)
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(255, 255, 255)];
    
    titleLabel.numberOfLines = 0;
    
    [_posterView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(17);
        make.width.equalTo(weakSelf.frameWidth-34);
        make.top.equalTo(weakSelf.mas_top).offset(13);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    userIconView = [[UIImageView alloc] init];

    userIconView.contentMode = UIViewContentModeScaleToFill;
    
    [self.contentView addSubview:userIconView];
    
    userIconView.frame = CGRectMake(13, self.frameHeight-50+13, 27, 27);
    
    nameLabel = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:nameLabel];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconView).offset(9);
        make.centerY.equalTo(userIconView.mas_centerY);
    }];
    
//    shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
//
//    [self.contentView addSubview:shareBtn];
//
//    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-14);
//        make.top.equalTo(userIconView.mas_top).offset(12);
//        make.width.height.equalTo(21);
//    }];
    
//    moreBtn  = [YLCFactory createBtnWithImage:@""];
//
//    [self.contentView addSubview:moreBtn];
//
//    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(shareBtn.mas_left).offset(-20);
//        make.centerY.equalTo(shareBtn.mas_centerY);
//        make.width.height.equalTo(20);
//    }];
    
    seeNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(147, 152, 154)];
    
    [self.contentView addSubview:seeNumberLabel];
    
    [seeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-14);
        make.centerY.equalTo(userIconView  .mas_centerY);
    }];
}
- (void)setDict:(NSDictionary *)dict{
    nameLabel.hidden = YES;
    seeNumberLabel.hidden = YES;
    userIconView.hidden = YES;
    titleLabel.text = dict[@"title"];
    [_posterView sd_setImageWithURL:[NSURL URLWithString:dict[@"cover"]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    _posterView.frame = CGRectMake(0, 0, self.frameWidth, self.frameHeight);
}
- (void)play:(UIButton *)sender {
    
    if (self.playBlock) {
        self.playBlock(sender);
    }
}
@end
