//
//  YLCInputInfoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInputInfoCell.h"

@implementation YLCInputInfoCell
{
    UITextView *inputView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    inputView = [[UITextView alloc] initWithFrame:self.bounds];
    
    [inputView.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didInfoChangedWithText:)) {
            [self.delegate didInfoChangedWithText:x];
        }
    }];
    
    inputView.placeholderAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor colorWithRed:160.0f/255.0f green:160.0f/255.0f blue:160.0f/255.0f alpha:1]};
    inputView.edgeInsetLeft = 11;
    
    [self.contentView addSubview:inputView];
}
- (void)setPlaceholder:(NSString *)placeholder{
    inputView.placeholder = placeholder;
}
- (void)setText:(NSString *)text{
    inputView.text = [text notNullString];
}
@end
