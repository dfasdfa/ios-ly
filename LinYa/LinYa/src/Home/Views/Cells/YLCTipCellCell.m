//
//  YLCTipCellCell.m
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTipCellCell.h"
@interface YLCTipCellCell()
@end
@implementation YLCTipCellCell
{
    UIImageView *iconView;
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    iconView = [[UIImageView alloc] init];
    
    iconView.contentMode = UIViewContentModeCenter;
    
    [self.contentView addSubview:iconView];
    
    titleLabel = [YLCFactory createLabelWithFont:14];
    
    [self.contentView addSubview:titleLabel];
}
- (void)setIconName:(NSString *)iconName{
    iconView.image = [UIImage imageNamed:iconName];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.height.and.width.equalTo(weakSelf.frameHeight-25-20);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(iconView.mas_centerX);
        make.top.equalTo(iconView.mas_bottom).offset(5);
    }];
}
@end
