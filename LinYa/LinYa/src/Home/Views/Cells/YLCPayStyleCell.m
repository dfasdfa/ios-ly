//
//  YLCPayStyleCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayStyleCell.h"

@implementation YLCPayStyleCell
{
    UIImageView *iconView;
    UILabel *titleLabel;
    UILabel *tipLabel;
    UIImageView *subIconView;
    UIImageView *lineView;
    UITextField *buyField;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:iconView];
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self.contentView addSubview:titleLabel];
    
    tipLabel = [YLCFactory createLabelWithFont:13 color:RGB(153, 153, 153)];
    
    [self.contentView addSubview:tipLabel];
    
    subIconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:subIconView];
    
    lineView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:lineView];
    
    buyField = [YLCFactory createFieldWithPlaceholder:@"请输入您的充值卡密码"];
    
    buyField.layer.cornerRadius = 5;
    
    buyField.layer.masksToBounds = YES;
    
    buyField.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [buyField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, cardFieldDidChangedWithText:)) {
            [self.delegate cardFieldDidChangedWithText:x];
        }
    }];
    
    [self.contentView addSubview:buyField];
}
- (void)setIconName:(NSString *)iconName{
    iconView.image = [UIImage imageNamed:iconName];
}
- (void)setTipString:(NSString *)tipString{
    tipLabel.text = [tipString notNullString];
}
- (void)setTitleString:(NSString *)titleString{
    titleLabel.text = [titleString notNullString];
}
- (void)setIndex:(NSInteger)index{
    _index = index;
    buyField.hidden = !(index==2);
}
- (void)setIsSelect:(BOOL)isSelect{
    NSString *subName = isSelect?@"buy_selected":@"buy_noSelect";
    
    subIconView.image = [UIImage imageNamed:subName];
}
- (void)setPayWay:(NSString *)payWay{
    if ([payWay isEqualToString:@"3"]&&_index==2) {
        buyField.hidden = NO;
    }else{
        buyField.hidden = YES;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(16);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(46);
        make.height.equalTo(46);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconView.mas_top).offset(6);
        make.left.equalTo(iconView.mas_right).offset(16);
    }];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.bottom.equalTo(iconView.mas_bottom).offset(-6);
    }];
    
    [subIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-26);
        make.centerY.equalTo(iconView.mas_centerY);
        make.width.equalTo(19);
        make.height.equalTo(19);
    }];
    
    [buyField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_left).offset(5);
        make.top.equalTo(iconView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-42);
        make.height.equalTo(40);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
}
@end
