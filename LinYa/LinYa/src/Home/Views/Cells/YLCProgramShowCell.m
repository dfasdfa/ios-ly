//
//  YLCProgramShowCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/29.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramShowCell.h"
#import "YLCLateralImageShowView.h"
#import "YLCPhotoModel.h"
@implementation YLCProgramShowCell
{
    UILabel *titleLabel;
    UIImageView *lineView;
    YLCLateralImageShowView *imageShowView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:13.65 color:RGB(51, 51, 51)];
    
    titleLabel.text = @"案例展示";
    
    [self.contentView addSubview:titleLabel];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = YLC_THEME_COLOR;
    
    [self.contentView addSubview:lineView];
    
    imageShowView = [[YLCLateralImageShowView alloc] initWithFrame:CGRectZero];
    
    imageShowView.cellSize = CGSizeMake(83, 85);
    
    [self.contentView addSubview:imageShowView];
}
- (void)setCaseList:(NSArray *)caseList{
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    
    if (JK_IS_ARRAY_NIL(caseList)) {
        return;
    }
    
    for (NSDictionary *imgDict in caseList) {
        YLCPhotoModel *model = [[YLCPhotoModel alloc] init];
        
        model.placeholder = @"image_placeholder";
        
        model.imagePath = imgDict[@"case_img"];
        
        model.isWeb = YES;
        
        [container addObject:model];
    }
    
    imageShowView.photoList = container.copy;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(12);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.height.equalTo(0.5);
    }];
    
    [imageShowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-12);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
}
@end
