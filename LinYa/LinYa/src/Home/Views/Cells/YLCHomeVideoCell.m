//
//  YLCHomeVideoCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/13.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeVideoCell.h"

@implementation YLCHomeVideoCell
{
    UIImageView *backImageView;
    UILabel *titleLabel;
    UIButton *actionBtn;
    UILabel *timeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        backImageView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:backImageView];
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:[UIColor whiteColor]];
        
        [backImageView addSubview:titleLabel];
        
        actionBtn = [YLCFactory createBtnWithImage:@"video_action"];
        
        [backImageView addSubview:actionBtn];
    }
    return self;
}
- (void)setModel:(YLCHomeVideoModel *)model{
    [backImageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    titleLabel.text = model.title;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backImageView.mas_left).offset(10);
        make.top.equalTo(backImageView.mas_top).offset(10);
    }];
    
    [actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backImageView.mas_centerX);
        make.centerY.equalTo(backImageView.mas_centerY);
        make.width.equalTo(37);
        make.height.equalTo(37);
    }];
}
@end
