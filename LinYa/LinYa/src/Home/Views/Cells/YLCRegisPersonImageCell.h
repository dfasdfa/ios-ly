//
//  YLCRegisPersonImageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCRegisPersonImageCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *userId;
@property (nonatomic ,copy)NSString *headIconUrl;
@end
