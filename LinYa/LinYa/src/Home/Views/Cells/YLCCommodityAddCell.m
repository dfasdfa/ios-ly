//
//  YLCCommodityAddCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCommodityAddCell.h"

@implementation YLCCommodityAddCell
{
    UILabel *titleLabel;
    UITextField *nameField;
    UITextField *priceField;
    UIButton *deleteBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:13.65 color:RGB(119, 123, 124)];
    
    titleLabel.text = @"商品价格1";
    
    [self.contentView addSubview:titleLabel];
    
    nameField = [YLCFactory createFieldWithPlaceholder:@"名称" backgroudColor:YLC_COMMON_BACKCOLOR];
    
    nameField.font = [UIFont systemFontOfSize:12];
    
    [nameField.rac_textSignal subscribeNext:^(id x) {
        [self changeText];
    }];
    
    [self.contentView addSubview:nameField];
    
    priceField = [YLCFactory createFieldWithPlaceholder:@"价格/单位" backgroudColor:YLC_COMMON_BACKCOLOR];
    
    priceField.keyboardType = UIKeyboardTypeNumberPad;
    
    [priceField.rac_textSignal subscribeNext:^(id x) {
        [self changeText];
    }];
    
    priceField.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:priceField];
    
    deleteBtn = [YLCFactory createBtnWithImage:@"commodity_delete"];
    
    [deleteBtn addTarget:self action:@selector(deleteProgram) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:deleteBtn];
}
- (void)setModel:(YLCProgramTraitFillModel *)model{
    _model = model;
    if ([YLCFactory isStringOk:model.name]) {
        nameField.text = model.name;
    }else{
        nameField.text = @"";
    }
    if ([YLCFactory isStringOk:model.price]) {
        priceField.text = model.price;
    }else{
        priceField.text = @"";
    }
}
- (void)changeText{
    _model.name = nameField.text;
    _model.price = priceField.text;
    if (!_index) {
        _index=0;
    }
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeTextWithModel:index:)) {
        [self.delegate didChangeTextWithModel:_model index:_index];
    }
}
- (void)deleteProgram{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didDeleteCommodityWithIndex:)) {
        [self.delegate didDeleteCommodityWithIndex:_index];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-19);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(25);
        make.height.equalTo(25);
    }];
    
    [priceField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(deleteBtn.mas_left).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(60);
        make.height.equalTo(24);
    }];
    
    [nameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(priceField.mas_left).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(60);
        make.height.equalTo(24);
    }];
}
@end
