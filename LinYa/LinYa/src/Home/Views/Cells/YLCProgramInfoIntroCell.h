//
//  YLCProgramInfoIntroCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCEasyProgramListModel.h"
@interface YLCProgramInfoIntroCell : UICollectionViewCell
@property (nonatomic ,strong)YLCEasyProgramListModel *model;
@end
