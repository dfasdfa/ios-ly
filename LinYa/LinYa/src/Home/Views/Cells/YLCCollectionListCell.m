//
//  YLCCollectionListCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCollectionListCell.h"

@implementation YLCCollectionListCell
{
    UIImageView *iconView;
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        iconView = [[UIImageView alloc] init];
        
        iconView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self.contentView addSubview:iconView];
        
        titleLabel = [YLCFactory createLabelWithFont:12 color:RGB(76, 73, 72)];
        
        [self.contentView addSubview:titleLabel];
    }
    return self;
}
- (void)setIconPath:(NSString *)iconPath{
    [iconView sd_setImageWithURL:[NSURL URLWithString:iconPath] placeholderImage:[UIImage imageNamed:_iconName]];
}
- (void)setIconName:(NSString *)iconName{
    _iconName = iconName;
    
    iconView.image = [UIImage imageNamed:iconName];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(_imageSize.width);
        make.height.equalTo(_imageSize.height);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(iconView.mas_centerX);
        make.top.equalTo(iconView.mas_bottom).offset(14);
    }];
}
@end
