//
//  YLCSelectCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCSelectCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *subString;
@property (nonatomic ,strong)UIColor *titleColor;
@property (nonatomic ,strong)UIColor *subColor;
@property (nonatomic ,assign)BOOL needHidden;
@end
