//
//  YLCEasyImageAddCell.h
//  LinYa
//
//  Created by 初程程 on 2018/5/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCEasyImageListModel.h"
@class YLCEasyImageAddCell;
@protocol YLCEasyImageAddCellDelegate <NSObject>
- (void)didSelectEditWithModel:(YLCEasyImageListModel *)model
                         index:(NSInteger)index;
@end
@interface YLCEasyImageAddCell : UICollectionViewCell
@property (nonatomic ,strong)YLCEasyImageListModel *model;
@property (nonatomic ,assign)BOOL isEdit;
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,weak)id<YLCEasyImageAddCellDelegate>delegate;
- (void)configCell;
- (UIImageView *)getImageView;
@end
