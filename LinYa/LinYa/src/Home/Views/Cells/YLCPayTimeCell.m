//
//  YLCPayTimeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayTimeCell.h"

@implementation YLCPayTimeCell
{
    UILabel *timeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        timeLabel = [[UILabel alloc] init];
        
        [self.contentView addSubview:timeLabel];
        
        timeLabel.layer.borderWidth = 0.5;
        
        timeLabel.layer.cornerRadius = 5;
        
        timeLabel.layer.masksToBounds = YES;
        
        timeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}
- (void)setString:(NSString *)string{
    timeLabel.text = [string notNullString];
}
- (void)setIsSelect:(BOOL)isSelect{
    if (isSelect) {
        timeLabel.textColor = YLC_THEME_COLOR;
        timeLabel.layer.borderColor = YLC_THEME_COLOR.CGColor;
    }else{
        timeLabel.textColor = RGB(108, 113, 116);
        timeLabel.layer.borderColor = RGB(108, 113, 116).CGColor;
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
@end
