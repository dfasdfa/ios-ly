//
//  YLCTrainRegistrationCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegistrationCell.h"

@implementation YLCTrainRegistrationCell
{
    UIImageView *leftView;
    YYLabel *titleLabel;
    UILabel *areaLabel;
    UILabel *timeLabel;
    UILabel *priceLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    leftView = [[UIImageView alloc] init];
    
    leftView.backgroundColor = YLC_THEME_COLOR;
    
    [self.contentView addSubview:leftView];
    
    titleLabel = [YYLabel new];
    
    titleLabel.font = [UIFont systemFontOfSize:17.5];
    
    titleLabel.numberOfLines = 0;
    
    [self.contentView addSubview:titleLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:12.9 color:RGB(145, 148, 150)];
    
    timeLabel.numberOfLines = 0;
    
    [self.contentView addSubview:timeLabel];
    
    priceLabel = [YLCFactory createLabelWithFont:14 color:YLC_THEME_COLOR];
    
    [self.contentView addSubview:priceLabel];
    
    areaLabel = [YLCFactory createLabelWithFont:12.9 color:RGB(11, 178, 214)];
    
    areaLabel.layer.cornerRadius = 5;
    
    areaLabel.layer.masksToBounds = YES;
    
    areaLabel.layer.borderColor = RGB(11, 178, 214).CGColor;
    
    areaLabel.layer.borderWidth = 0.5;
    
    areaLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView addSubview:areaLabel];
}
- (void)setModel:(YLCActivityModel *)model{
    _model = model;
    
    [leftView sd_setImageWithURL:[NSURL URLWithString:model.poster] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:model.title];
    
    attri.yy_font = [UIFont systemFontOfSize:17.5];
    
    NSMutableAttributedString *attachment = nil;
    
    UIImage *image = [UIImage imageNamed:@"train_new"];
    
    attachment = [NSMutableAttributedString yy_attachmentStringWithContent:image contentMode:UIViewContentModeCenter attachmentSize:image.size alignToFont:[UIFont systemFontOfSize:17.5] alignment:YYTextVerticalAlignmentCenter];
    
    if ([model.is_recommand isEqualToString:@"1"]) {
        [attri appendAttributedString: attachment];
    }
    
    titleLabel.attributedText = attri;
    
    timeLabel.text = [NSString stringWithFormat:@"%@开始",[YLCFactory isStringOk:model.begin_time]?model.begin_time:@""];
    if ([YLCFactory isStringOk:model.fee]) {
        priceLabel.text = [NSString stringWithFormat:@"￥%@",model.fee];
    }else{
        priceLabel.text = @"免费";
    }
    
    areaLabel.text = [YLCFactory isStringOk:model.area]?model.area:@"";
    
    CGSize size = [YLCFactory getStringWithSizeWithString:model.area height:10 font:13];
    
    [areaLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(size.width+20);
    }];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.top.equalTo(weakSelf.mas_top).offset(13);
        make.width.equalTo(170);
        make.height.equalTo(112);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftView.mas_right).offset(3);
        make.top.equalTo(leftView.mas_top).offset(1);
        make.width.equalTo(weakSelf.frameWidth-170-11-11-3);
        make.height.equalTo(40);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-12);
        make.bottom.equalTo(leftView.mas_bottom).offset(-1);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.bottom.equalTo(priceLabel.mas_bottom);
        make.width.equalTo(150);
        make.height.equalTo(40);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeLabel.mas_left);
        make.bottom.equalTo(timeLabel.mas_top).offset(-3);
        make.height.equalTo(19);
    }];
}
@end
