//
//  YLCInformationNewsCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInformationNewsCell.h"
#import "YLCNewsCell.h"
#import "YLCHomeNewsTopView.h"
#import "YLCTouTiaoListViewController.h"
#import "YLCMatterBaseViewController.h"
#import "YLCWebViewViewController.h"
#import "YLCToplineModel.h"
#define NEW_CELL @"YLCNewsCell"
@interface YLCInformationNewsCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCInformationNewsCell
{
    YLCHomeNewsTopView *topView;
    UICollectionView *newsTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 5;
        
        self.layer.masksToBounds = YES;
        
        topView = [[YLCHomeNewsTopView alloc] initWithFrame:CGRectZero];
        
        topView.iconName = @"home_information";
        
        [topView.moreSignal subscribeNext:^(id x) {
            [self gotoTouTiaoList];
        }];
        
        [self.contentView addSubview:topView];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 0;
        
        newsTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        [newsTable registerClass:[YLCNewsCell class] forCellWithReuseIdentifier:NEW_CELL];
        
        newsTable.backgroundColor = [UIColor whiteColor];
        
        newsTable.delegate  = self;
        
        newsTable.dataSource = self;
        
        [self.contentView addSubview:newsTable];
    }
    return self;
}
//- (void)setNewsList:(NSArray *)newsList{
//    _newsList = newsList;
//    [newsTable reloadData];
//}
- (void)setTop_lineArr:(NSArray *)top_lineArr{
    _top_lineArr = top_lineArr;
    
    [newsTable reloadData];
}
- (void)setNewsList:(NSArray *)newsList{
    _newsList = newsList;
    
    [newsTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _newsList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCNewsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NEW_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_newsList.count) {
        cell.model = _newsList[indexPath.row];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 104);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCToplineModel *model = _newsList[indexPath.row];
    UINavigationController *nav = (UINavigationController *)[YLCFactory currentNav];
    
    [YLCWebViewViewController showWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.url]] navTitle:@"" isModel:YES from:nav.viewControllers[nav.viewControllers.count-1]];
}
- (void)gotoTouTiaoList{
//    YLCTouTiaoListViewController *con = [[YLCTouTiaoListViewController alloc] init];
//
//    con.topLineTopArr = _newsList;
//
//    con.topLineBottomArr = _top_lineArr;
    YLCMatterBaseViewController *con = [[YLCMatterBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(9);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.width.equalTo(weakSelf.frameWidth-18);
        make.height.equalTo(26);
    }];
    
    [newsTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(5);
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
}
@end
