//
//  YLCProgramListCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCEasyProgramListModel.h"
@class YLCProgramListCell;
@protocol YLCProgramListCellDelegate<NSObject>
- (void)didTouchTableSelectAtIndex:(NSInteger)index;
@end
@interface YLCProgramListCell : UICollectionViewCell
@property (nonatomic ,weak)id<YLCProgramListCellDelegate>delegate;
@property (nonatomic ,strong)YLCEasyProgramListModel *model;
@property (nonatomic ,assign)NSInteger index;
@end
