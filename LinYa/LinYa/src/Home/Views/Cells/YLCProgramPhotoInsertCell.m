//
//  YLCProgramPhotoInsertCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramPhotoInsertCell.h"
@interface YLCProgramPhotoInsertCell()<HXPhotoViewDelegate>
@property (strong, nonatomic) HXPhotoManager *manager;
@end
@implementation YLCProgramPhotoInsertCell
{
    HXPhotoView *photoView;
}
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.photoMaxNum = 6;
        
    }
    return _manager;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        photoView = [HXPhotoView photoManager:self.manager];
        photoView.delegate = self;
        photoView.spacing = 10;
        photoView.lineCount = 4;
        photoView.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:photoView];

    }
    return self;
}
- (void)setInsertList:(NSArray *)insertList{
    [self.manager addNetworkingImageToAlbum:insertList selected:YES];
    [photoView refreshView];
}
// 当view更新高度时调用
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, changeFrameWithHeight:)) {
        [self.delegate changeFrameWithHeight:frame.size.height];
    }
}
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, insertPhotoWithPhotos:)) {
        [self.delegate insertPhotoWithPhotos:photos];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
    }];
}
@end
