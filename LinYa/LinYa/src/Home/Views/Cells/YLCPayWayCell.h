//
//  YLCPayWayCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCPayWayCell : UICollectionViewCell
@property (nonatomic ,strong)RACSubject *waySignal;
@property (nonatomic ,strong)RACSubject *cardSignal;
@property (nonatomic ,copy)NSString *payWay;
@property (nonatomic ,copy)NSString *subString;
@property (nonatomic ,assign)NSInteger rechargeType;
@end
