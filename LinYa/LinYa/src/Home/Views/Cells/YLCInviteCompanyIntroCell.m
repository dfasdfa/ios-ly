//
//  YLCInviteCompanyIntroCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteCompanyIntroCell.h"

@implementation YLCInviteCompanyIntroCell
{
    UILabel *companyNameLabel;
    UILabel *introLabel;
    UILabel *areaLabel;
//    UIImageView *arrowView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    companyNameLabel = [YLCFactory createLabelWithFont:16 color:RGB(30, 30, 30)];
    
    
    
    [self.contentView addSubview:companyNameLabel];
    
    introLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    
    
    [self.contentView addSubview:introLabel];
    
    areaLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    
    
    [self.contentView addSubview:areaLabel];
    
//    arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
//    
//    [self.contentView addSubview:arrowView];
}
- (void)setModel:(YLCInviteModel *)model{
    companyNameLabel.text = [model.company_name notNullString];
    
//    introLabel.text = [NSString stringWithFormat:@"%@ | %@",[model.company_size notNullString],model.type];
    
    areaLabel.text = [model.place notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [companyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(companyNameLabel.mas_left);
        make.top.equalTo(companyNameLabel.mas_bottom).offset(5);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(introLabel.mas_left);
        make.top.equalTo(introLabel.mas_bottom).offset(5);
    }];
    
//    [arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-10);
//        make.centerY.equalTo(weakSelf.mas_centerY);
//        make.width.equalTo(7);
//        make.height.equalTo(12);
//    }];
}
@end
