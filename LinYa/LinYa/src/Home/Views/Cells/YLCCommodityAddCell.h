//
//  YLCCommodityAddCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCProgramTraitFillModel.h"
@class YLCCommodityAddCell;
@protocol YLCCommodityAddCellDelegate <NSObject>
- (void)didDeleteCommodityWithIndex:(NSInteger)index;
- (void)didChangeTextWithModel:(YLCProgramTraitFillModel *)model
                         index:(NSInteger)index;
@end
@interface YLCCommodityAddCell : UICollectionViewCell
@property (nonatomic ,strong)YLCProgramTraitFillModel *model;
@property (nonatomic ,weak)id<YLCCommodityAddCellDelegate>delegate;
@property (nonatomic ,assign)NSInteger index;
@end
