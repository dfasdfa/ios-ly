//
//  YLCVideoLiftCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoLiftCell.h"
#import "YLCLikeButton.h"
@implementation YLCVideoLiftCell
{
    UIImageView *breviaryView;
    UIImageView *areaIconView;
    UILabel *areaLabel;
    YLCUserHeadView *headIconView;
    UILabel *nameLabel;
    YLCLikeButton *likeBtn;
    NSInteger number;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        number = 232;
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    breviaryView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:breviaryView];
    
//    areaIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"area_white"]];
//
//    [breviaryView addSubview:areaIconView];
//
//    areaLabel = [YLCFactory createLabelWithFont:14 color:RGB(53, 53, 53)];
//
//    areaLabel.textColor = [UIColor whiteColor];
//
//    [breviaryView addSubview:areaLabel];
    
    headIconView = [[YLCUserHeadView alloc] init];
    
    [self.contentView addSubview:headIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:16 color:[UIColor blackColor]];
    
    [self.contentView addSubview:nameLabel];
    
    likeBtn = [[YLCLikeButton alloc] initWithFrame:CGRectZero normalImage:@"like_normal" selectImage:@"like_selected" normalColor:[UIColor blackColor] selectedColor:[UIColor blackColor]];
    
    likeBtn.noTouch = YES;
    
    [likeBtn configureLikeStatus:NO count:0 animated:YES];
    
    [self.contentView addSubview:likeBtn];
    likeBtn.backgroundColor = [UIColor whiteColor];
    
//    [likeBtn addTarget:self action:@selector(likeAnimation) forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:likeBtn];
}
- (void)setModel:(YLCVideoLifeModel *)model{
    [breviaryView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    areaLabel.text = [NSString stringWithFormat:@"%@",model.location];
    
    [headIconView configHeadViewWithImageUrl:model.avatar_img placeholder:@"image_placeholder" isGoCenter:YES userId:model.user_id];
    
    likeBtn.count = model.top_num;
    
    nameLabel.text = model.nick_name;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [breviaryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(120);
    }];
    
//    [areaIconView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(breviaryView.mas_left).offset(10);
//        make.bottom.equalTo(breviaryView.mas_bottom).offset(-10);
//        make.width.equalTo(8);
//        make.height.equalTo(11);
//    }];
//    
//    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(areaIconView.mas_centerY);
//        make.left.equalTo(areaIconView.mas_right).offset(5);
//    }];
    
    [headIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(5);
        make.top.equalTo(breviaryView.mas_bottom).offset(10);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headIconView.mas_right).offset(5);
        make.centerY.equalTo(headIconView.mas_centerY);
    }];
    
    [likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-5);
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
}
@end
