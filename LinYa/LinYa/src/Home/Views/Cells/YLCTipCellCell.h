//
//  YLCTipCellCell.h
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTipCellCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *iconName;
@end
