//
//  YLCSelectCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCSelectCell.h"

@implementation YLCSelectCell
{
    UILabel *titleLabel;
    UILabel *subLabel;
    UIImageView *subArrow;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:13.65 color:RGB(119, 123, 124)];
    
    titleLabel.text = @"选择地址";
    
    [self.contentView addSubview:titleLabel];
    
    subArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
    
    [self.contentView addSubview:subArrow];
    
    subLabel = [YLCFactory createLabelWithFont:14 color:RGB(0, 0, 0)];
    
    subLabel.text = @"未选择";
    
    [self.contentView addSubview:subLabel];
}
- (void)setNeedHidden:(BOOL)needHidden{
    self.contentView.hidden = needHidden;
}
- (void)setTitle:(NSString *)title{
    
    titleLabel.text = [title notNullString];
    
}
- (void)setSubString:(NSString *)subString{
    
    subLabel.text = [subString notNullString];
    
}
- (void)setTitleColor:(UIColor *)titleColor{
    titleLabel.textColor = titleColor;
}
- (void)setSubColor:(UIColor *)subColor{
    subLabel.textColor = subColor;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [subArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-13);
        make.height.equalTo(10);
        make.width.equalTo(6);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(subArrow.mas_left).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
}
@end
