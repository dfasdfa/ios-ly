//
//  YLCProgramCorrelationCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramCorrelationCell.h"

@implementation YLCProgramCorrelationCell
{
    UILabel *titleLabel;
    UIImageView *lineView;
    UILabel *infoLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
        
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:13.6 color:RGB(51, 51, 51)];
    
    titleLabel.text = @"项目介绍";
    
    [self addSubview:titleLabel];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [self addSubview:lineView];
    
    infoLabel = [YLCFactory createLabelWithFont:13 color:RGB(123, 130, 135)];

    infoLabel.numberOfLines = 0;
    
    [self addSubview:infoLabel];
}
- (void)setInfoString:(NSString *)infoString{
    infoLabel.text = [infoString notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(12);
        make.top.equalTo(weakSelf.mas_top).offset(12);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.top.equalTo(titleLabel.mas_bottom).offset(12);
        make.height.equalTo(0.5);
    }];
    
    [infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-24);
        make.height.greaterThanOrEqualTo(@10);
    }];
}
@end
