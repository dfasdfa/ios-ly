//
//  YLCPayMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCPayMessageCell;
@protocol YLCPayMessageCellDelegate <NSObject>
- (void)didSelectPriceAtIndex:(NSInteger)index
                     payPrice:(NSString *)payPrice;
@end
@interface YLCPayMessageCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *payString;
@property (nonatomic ,weak)id<YLCPayMessageCellDelegate>delegate;
@end
