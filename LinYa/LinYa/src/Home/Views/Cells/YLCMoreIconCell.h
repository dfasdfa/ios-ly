//
//  YLCMoreIconCell.h
//  LinYa
//
//  Created by 初程程 on 2018/6/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCMoreModel.h"
@interface YLCMoreIconCell : UICollectionViewCell
@property (nonatomic ,strong)YLCMoreModel *model;
@end
