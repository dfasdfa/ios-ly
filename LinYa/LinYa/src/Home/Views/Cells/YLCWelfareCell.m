//
//  YLCWelfareCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWelfareCell.h"
#import "YLCBorderLabelCollectionView.h"
@implementation YLCWelfareCell
{
    UILabel *titleLabel;
    YLCBorderLabelCollectionView *labelView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(0, 0, 0)];
    
    titleLabel.text = @"福利：";
    
    [self.contentView addSubview:titleLabel];
    
    labelView = [[YLCBorderLabelCollectionView alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview:labelView];
}
- (void)setLabelList:(NSArray *)labelList{
    _labelList = labelList;
    labelView.labelList = labelList;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    CGFloat width = 0;
    for (NSString *string in _labelList) {
        CGSize size = [YLCFactory getStringWithSizeWithString:string height:10 font:14];
        
        width+=size.width;
        width+=10;
        width+=5;
    }
    [labelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_right).offset(5);
        make.centerY.equalTo(titleLabel.mas_centerY);
        make.width.equalTo(width);
        make.height.equalTo(30);
    }];
}
@end
