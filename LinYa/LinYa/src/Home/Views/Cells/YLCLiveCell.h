//
//  YLCLiveCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCLiveModel.h"
@interface YLCLiveCell : UICollectionViewCell
@property (nonatomic ,strong)YLCLiveModel *model;
@end
