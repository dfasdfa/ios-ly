//
//  YLCMoreIconCell.m
//  LinYa
//
//  Created by 初程程 on 2018/6/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMoreIconCell.h"

@implementation YLCMoreIconCell
{
    UIImageView *iconView;
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        iconView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        [self.contentView addSubview:iconView];
        
        titleLabel = [YLCFactory createLabelWithFont:14 color:RGB(50, 50, 50)];
        
        [self.contentView addSubview:titleLabel];
    }
    return self;
}
- (void)setModel:(YLCMoreModel *)model{
    if ([YLCFactory isStringOk:model.category_name]) {
        titleLabel.text = model.category_name;
    }else{
        titleLabel.text = @"";
    }
    if (model.isImage) {
        iconView.image = [UIImage imageNamed:model.category_icon];
    }else{
        [iconView sd_setImageWithURL:[NSURL URLWithString:model.category_icon] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.top.equalTo(weakSelf.mas_top).offset(5);
        make.width.height.equalTo(40);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconView.mas_bottom).offset(5);
        make.centerX.equalTo(iconView.mas_centerX);
    }];
}
@end
