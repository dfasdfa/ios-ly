//
//  YLCProgramInfoIntroCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramInfoIntroCell.h"
#import "YLCProgramIntroduceView.h"
@implementation YLCProgramInfoIntroCell
{
    YLCProgramIntroduceView *introduceView;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    introduceView = [[YLCProgramIntroduceView alloc] initWithFrame:CGRectZero];
    
    [self.contentView addSubview:introduceView];
    
    lineView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line"]];
    
    [self.contentView addSubview:lineView];
}
- (void)setModel:(YLCEasyProgramListModel *)model{

    introduceView.title = model.name;
    
    introduceView.score = model.star;
    
    introduceView.safe = model.period;
    
    introduceView.subsidy = model.subsidy;
    
    introduceView.feature = model.feature;
}
- (void)layoutSubviews{
    [introduceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 2, 0));
    }];
    WS(weakSelf)
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(2);
    }];
}
@end

