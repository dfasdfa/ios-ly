//
//  YLCTouTiaoBigCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTouTiaoBigCell.h"

@implementation YLCTouTiaoBigCell
{
    UILabel *titleLabel;
    UIImageView *contentImageView;
    UILabel *authorLabel;
    UILabel *discussLabel;
    UILabel *timeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(28, 29, 30)];
    
    [self.contentView addSubview:titleLabel];
    
    contentImageView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:contentImageView];
    
    authorLabel = [YLCFactory createLabelWithFont:12 color:RGB(162, 168, 171)];
    
    [self.contentView addSubview:authorLabel];
    
    discussLabel = [YLCFactory createLabelWithFont:12 color:RGB(104, 107, 109)];
    
    [self.contentView addSubview:discussLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:12 color:RGB(84, 87, 88)];
    
    [self.contentView addSubview:timeLabel];
}
- (void)setModel:(YLCToplineModel *)model{
    titleLabel.text = model.title;
    
    authorLabel.text = @"临牙共享平台";
    
    discussLabel.text = [NSString stringWithFormat:@"%@评论",model.comment_num];
    
    timeLabel.text = model.add_time;
    
    [contentImageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(14);
        make.top.equalTo(weakSelf.mas_top).offset(19);
    }];
    
    [contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(titleLabel.mas_bottom).offset(11);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.equalTo(219);
    }];
    
    [authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentImageView.mas_left);
        make.top.equalTo(contentImageView.mas_bottom).offset(11);
    }];
    
    [discussLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(authorLabel.mas_right).offset(13);
        make.centerY.equalTo(authorLabel.mas_centerY);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(discussLabel.mas_right).offset(10);
        make.centerY.equalTo(discussLabel.mas_centerY);
    }];
}
@end
