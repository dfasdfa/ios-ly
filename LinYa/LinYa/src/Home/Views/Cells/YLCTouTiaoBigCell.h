//
//  YLCTouTiaoBigCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCToplineModel.h"
@interface YLCTouTiaoBigCell : UICollectionViewCell
@property (nonatomic ,strong)YLCToplineModel *model;
@end
