//
//  YLCPayTimeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCPayTimeCell : UICollectionViewCell
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,copy)NSString *string;
@end
