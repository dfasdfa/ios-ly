//
//  YLCPayStyleCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCPayStyleCell;
@protocol YLCPayStyleCellDelegate <NSObject>
- (void)cardFieldDidChangedWithText:(NSString *)text;
@end
@interface YLCPayStyleCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *iconName;
@property (nonatomic ,copy)NSString *tipString;
@property (nonatomic ,copy)NSString *titleString;
@property (nonatomic ,assign)BOOL isSelect;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,copy)NSString *payWay;
@property (nonatomic ,weak)id<YLCPayStyleCellDelegate>delegate;
@end
