//
//  YLCCustomNoticceViewCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCustomNoticceViewCell.h"
#import "YLCSendGoodsViewController.h"
#import "YLCSendViewController.h"
#import "YLCSendQuestionViewController.h"
#import "YLCSendTrainViewController.h"
#import "HWVideoVC.h"
#import "YLCSendJobViewController.h"
@implementation YLCCustomNoticceViewCell
{
    UIImageView *iconView;
    UILabel *nameLabel;
    UILabel *messageLabel;
    UILabel *timeLabel;
    UIButton *sendBtn;
}
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] init];
    
    iconView.backgroundColor = YLC_THEME_COLOR;
    
    iconView.layer.cornerRadius = 15;
    
    iconView.layer.masksToBounds =  YES;
    
    [self.contentView addSubview:iconView];
    
    nameLabel = [YLCFactory createLabelWithFont:15 color:YLC_BLACK_TEXT_COLOR];
    
    nameLabel.text = @"浩*****0";
    
    [self.contentView addSubview:nameLabel];
    
    messageLabel = [YLCFactory createLabelWithFont:14 color:YLC_GRAY_TEXT_COLOR];
    
    messageLabel.text = @"卖了宝贝赚了800元";
    
    [self.contentView addSubview:messageLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:14];
    
    timeLabel.textColor = YLC_GRAY_TEXT_COLOR;
    
    timeLabel.text = @"刚刚";
    
    [self.contentView addSubview:timeLabel];
    
    sendBtn = [YLCFactory creatBtnWithCornerRadius:10 title:@"我要发布" backgroundColor:RGB(255, 86, 71) textColor:[UIColor whiteColor]];
    
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    
    [sendBtn addTarget:self action:@selector(gotoSend) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:sendBtn];
}
- (void)setModel:(YLCTrendModel *)model{
    _model = model;
    
    [iconView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    nameLabel.text = model.nick_name;
    
    timeLabel.text = model.time_txt;
    
    messageLabel.text = model.content;
}
- (void)gotoSend{
    NSInteger index = [_model.type integerValue];
    if (index==1) {
        [self gotoSendDetist];
    }
    if (index==2) {
        [self gotoSendQuestion];
    }
    if (index==3) {
        [self gotoGoods];
    }
    if (index==4) {
        [self sendJob];
    }
    if (index==5) {
        [self gotoSendTrainWithType:0];
    }
    if (index==6) {
        [self gotoSendTrainWithType:1];
    }
}
- (void)gotoSendDetist{
    YLCSendViewController *con  = [[YLCSendViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}

- (void)sendJob{
    YLCSendJobViewController *con = [[YLCSendJobViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)gotoSendQuestion{
    
    YLCSendQuestionViewController *con = [[YLCSendQuestionViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)gotoGoods{
    YLCSendGoodsViewController *con = [[YLCSendGoodsViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoSendTrainWithType:(NSInteger)type{
    YLCSendTrainViewController *con = [[YLCSendTrainViewController alloc] init];
    
    con.sendType = type;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(30);
        make.height.equalTo(30);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_right).offset(10);
        make.top.equalTo(iconView.mas_top);
    }];
    
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.top.equalTo(nameLabel.mas_bottom).offset(5);
        make.width.equalTo(weakSelf.frameWidth-50-20);
        make.height.equalTo(10);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        make.left.equalTo(messageLabel.mas_left);
    }];
    
    [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        make.width.equalTo(80);
        make.height.equalTo(20);
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
