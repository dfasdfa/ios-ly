//
//  YLCMatterCell.h
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCMatterModel.h"
@interface YLCMatterCell : UICollectionViewCell
@property (nonatomic ,strong)YLCMatterModel *model;
@end
