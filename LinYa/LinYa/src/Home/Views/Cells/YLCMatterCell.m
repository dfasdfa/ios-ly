//
//  YLCMatterCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMatterCell.h"
@interface YLCMatterCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;



@end
@implementation YLCMatterCell
- (void)setModel:(YLCMatterModel *)model{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    _titleLabel.text = model.title;
    
    _authorLabel.text = model.author;
    
    _timeLabel.text = model.add_time;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
