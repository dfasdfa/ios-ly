//
//  YLCPayWayCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayWayCell.h"
#import "YLCPayStyleCell.h"
#define STYLE_CELL  @"YLCPayStyleCell"
@interface YLCPayWayCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCPayStyleCellDelegate>
@end
@implementation YLCPayWayCell
{
    UILabel *titleLabel;
    UILabel *subLabel;
    UIImageView *lineView;
    UICollectionView *payTable;
    NSArray *wayArr;
    NSInteger selectRow;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        selectRow = 0;
        self.waySignal = [RACSubject subject];
        self.cardSignal = [RACSubject subject];
        wayArr = @[@{@"title":@"微信支付",@"iconName":@"wx_pay",@"tip":@"推荐已安装微信钱包的用户使用"},
                   @{@"title":@"账户余额支付",@"iconName":@"account_pay",@"tip":@"推荐账户余额的用户使用"},
                   @{@"title":@"充值卡支付",@"iconName":@"buy_pay",@"tip":@"推荐已有充值卡的用户使用"}];
        
        [self createCustomView];
    }
    return self;
}
- (void)setPayWay:(NSString *)payWay{
    _payWay = payWay;
 
    [payTable reloadData];
}
- (void)setRechargeType:(NSInteger)rechargeType{
    
    _rechargeType = rechargeType;
    
    [payTable reloadData];
}
- (void)setSubString:(NSString *)subString{
    NSString *attrString = [NSString stringWithFormat:@"所需金额:￥%@",subString];
    NSAttributedString *attr = [YLCFactory getThreeCountAttributedStringWithString:attrString FirstCount:5 secondCount:1 thirdCount:subString.length firstColor:RGB(51, 51, 51) secondColor:YLC_THEME_COLOR thirdColor:YLC_THEME_COLOR firstFont:14 secondFont:14 thirdFont:16];
    
    subLabel.attributedText = attr;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(153, 153, 153)];
    
    titleLabel.text = @"选择支付方式";
    
    [self.contentView addSubview:titleLabel];
    
    subLabel = [YLCFactory createLabelWithFont:15 color:RGB(153, 153, 153)];
    
    [self.contentView addSubview:subLabel];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(219, 230, 234);
    
    [self.contentView addSubview:lineView];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    payTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    payTable.backgroundColor = [UIColor whiteColor];
    
    [payTable registerClass:[YLCPayStyleCell class] forCellWithReuseIdentifier:STYLE_CELL];
    
    payTable.delegate = self;
    
    payTable.dataSource = self;
    
    [self addSubview:payTable];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_rechargeType==1) {
        return 1;
    }
    return wayArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCPayStyleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:STYLE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<wayArr.count) {
        NSDictionary *dic = wayArr[indexPath.row];
        cell.titleString = dic[@"title"];
        cell.iconName = dic[@"iconName"];
        cell.tipString = dic[@"tip"];
    }
    if (indexPath.row==selectRow) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    cell.index  =  indexPath.row;
    
    cell.payWay = _payWay;
    
    cell.delegate = self;
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==2) {
        if ([_payWay isEqualToString:@"3"]) {
            return CGSizeMake(self.frameWidth, 66+20+30);
        }else{
            return CGSizeMake(self.frameWidth, 66);
        }
        
    }
    return CGSizeMake(self.frameWidth, 66);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectRow = indexPath.row;
    
    [self.waySignal sendNext:indexPath];
    
    [payTable reloadData];
}
- (void)cardFieldDidChangedWithText:(NSString *)text{
    [self.cardSignal sendNext:text];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(16);
        make.left.equalTo(weakSelf.mas_left).offset(16);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(16);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(0.5);
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-16);
        make.centerY.equalTo(titleLabel.mas_centerY);
    }];
    
    [payTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(16);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
    }];
}
@end
