//
//  YLCImageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCImageCell.h"
#import "YLCCompanyRecruitModel.h"
@implementation YLCImageCell
{
    UIImageView *imageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [[UIImageView alloc] init];
        
        imageView.contentMode = UIViewContentModeScaleToFill;
        
        [self.contentView addSubview:imageView];
    }
    return self;
}
- (void)setNeedCorner:(BOOL)needCorner{
    if (needCorner) {
        imageView.layer.cornerRadius = 35;
        imageView.layer.masksToBounds = YES;
    }else{
        imageView.layer.cornerRadius = 0;
        imageView.layer.masksToBounds = NO;
    }
}
- (void)setModel:(YLCCompanyRecruitModel *)model{
    _model = model;
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.imgsrc]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.width.equalTo(71);
        make.height.equalTo(71);
    }];
}
@end
