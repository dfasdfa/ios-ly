//
//  YLCWorkNeedCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCWorkNeedCell.h"

@implementation YLCWorkNeedCell
{
    UILabel *titleLabel;
    UIImageView *titleView;
    UILabel *contentLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleView = [[UIImageView alloc] init];
    
    titleView.backgroundColor = YLC_THEME_COLOR;
    
    [self.contentView addSubview:titleView];
    
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
    
    titleLabel.text = @"职位描述:";
    
    [self.contentView addSubview:titleLabel];
    
    contentLabel = [YLCFactory createLabelWithFont:14 color:RGB(50, 50, 50)];
    
    [self.contentView addSubview:contentLabel];
    
    contentLabel.numberOfLines = 0;
}
- (void)setModel:(YLCInviteModel *)model{
    contentLabel.text = [model.content notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(5);
        make.height.equalTo(10);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleView.mas_centerY);
        make.left.equalTo(titleView.mas_right).offset(10);
    }];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-30);
        make.height.greaterThanOrEqualTo(@10);
    }];
}
@end
