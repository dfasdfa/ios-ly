//
//  YLCMoreCell.m
//  LinYa
//
//  Created by 初程程 on 2018/6/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMoreCell.h"
#import "YLCMoreIconCell.h"
#define MORE_ICON_CELL @"YLCMoreIconCell"
@interface YLCMoreCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCMoreCell
{
    UICollectionView *iconTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumLineSpacing = 5;
        
        flow.minimumInteritemSpacing = 10;
        
        iconTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        iconTable.backgroundColor = [UIColor whiteColor];
        
        [iconTable registerClass:[YLCMoreIconCell class] forCellWithReuseIdentifier:MORE_ICON_CELL];
        
        iconTable.delegate = self;
        
        iconTable.dataSource = self;
        
        [self.contentView addSubview:iconTable];
        
        [iconTable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];
    }
    return self;
}
- (void)setMoreList:(NSArray *)moreList{
    _moreList = moreList;
    [iconTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _moreList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMoreIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MORE_ICON_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_moreList.count) {
        cell.model = _moreList[indexPath.row];
    }

    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(70, 70);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didMoreSelectAtIndexPath:section:)) {
        [self.delegate didMoreSelectAtIndexPath:indexPath section:_section];
    }
}
@end
