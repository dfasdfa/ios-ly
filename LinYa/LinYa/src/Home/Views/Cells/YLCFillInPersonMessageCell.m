//
//  YLCFillInPersonMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFillInPersonMessageCell.h"

@implementation YLCFillInPersonMessageCell
{
    UILabel *titleLabel;
    UITextField *messageField;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        [self createCustomView];
        
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(119, 123, 124)];
    
    [self.contentView addSubview:titleLabel];
    
    messageField = [[UITextField alloc] init];
    
    [messageField.rac_textSignal subscribeNext:^(id x) {
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeFillInFieldWithText:index:)) {
            [self.delegate didChangeFillInFieldWithText:x index:_index];
        }
    }];
    
    [self.contentView addSubview:messageField];
    
}
- (void)setIsRight:(BOOL)isRight{
    if (isRight) {
        messageField.textAlignment = NSTextAlignmentRight;
    }else{
        messageField.textAlignment = NSTextAlignmentLeft;
    }
}
- (void)setIsNumber:(BOOL)isNumber{
    if (isNumber) {
        messageField.keyboardType = UIKeyboardTypeNumberPad;
    }else{
        messageField.keyboardType = UIKeyboardTypeDefault;
    }
    
}
- (void)setText:(NSString *)text{
    messageField.text = [text notNullString];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setPlaceholder:(NSString *)placeholder{
    messageField.placeholder = placeholder;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(13);;
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [messageField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(130);
        make.height.equalTo(weakSelf.frameHeight);
    }];
}
@end
