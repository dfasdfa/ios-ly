//
//  YLCProgramPhotoInsertCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCProgramPhotoInsertCell;
@protocol YLCProgramPhotoInsertCellDelegate <NSObject>
- (void)insertPhotoWithPhotos:(NSArray *)photos;
- (void)changeFrameWithHeight:(CGFloat)height;
@end
@interface YLCProgramPhotoInsertCell : UICollectionViewCell
@property (nonatomic ,weak)id<YLCProgramPhotoInsertCellDelegate>delegate;
@property (nonatomic ,strong)NSArray *insertList;
@end
