//
//  YLCTrainRegisInfoFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegisInfoFootView.h"
#import "YLCRegisPersonImageCell.h"
#import "YLCRegisPersonViewController.h"
#define HeadImageCell  @"YLCRegisPersonImageCell"
@interface YLCTrainRegisInfoFootView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCTrainRegisInfoFootView
{
    UILabel *regisNumberLabel;
    UILabel *numberLabel;
    UIImageView *lineView;
    UIImageView *arrowView;
    UICollectionView *imageTable;
    UIView *backView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frameWidth, 50)];
        
        [self addSubview:backView];
        
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPersonList)];
        
        [backView addGestureRecognizer:ges];
        
        self.backgroundColor = [UIColor whiteColor];
        
        regisNumberLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
        
        regisNumberLabel.text = @"报名人数";
        
        [self addSubview:regisNumberLabel];
        
        numberLabel = [YLCFactory createLabelWithFont:14 color:RGB(183,183 , 183)];
        
        [self addSubview:numberLabel];
        
        arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resume_arrow"]];
        
        [self addSubview:arrowView];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = RGB(213, 213, 213);
        
        [self addSubview:lineView];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        flow.minimumInteritemSpacing = 10;
        
        flow.minimumLineSpacing = 10;
        
        imageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        imageTable.backgroundColor = [UIColor whiteColor];
        
        [imageTable registerClass:[YLCRegisPersonImageCell class] forCellWithReuseIdentifier:HeadImageCell];
        
        imageTable.delegate = self;
        
        imageTable.dataSource = self;
        
        [self addSubview:imageTable];
    }
    return self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (JK_IS_ARRAY_NIL(_model.enrollList)) {
        return 0;
    }
    return _model.enrollList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCRegisPersonImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:HeadImageCell forIndexPath:indexPath];
    
    if (indexPath.row<_model.enrollList.count) {
        NSDictionary *dict = _model.enrollList[indexPath.row];
        cell.userId = dict[@"enroll_user_id"];
        cell.headIconUrl = dict[@"enroll_user_img"];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"进入人物详情");
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(50, 50);
}
- (void)gotoPersonList{
//    YLCRegisPersonViewController *con = [[YLCRegisPersonViewController alloc] init];
//    
//    con.activityId = _model.id;
//    
//    con.type = _type;
//    
//    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)setModel:(YLCActivityModel *)model{
    _model = model;
    if (JK_IS_ARRAY_NIL(model.enrollList)) {
        numberLabel.text = [NSString stringWithFormat:@"0人"];
    }else{
            numberLabel.text = [NSString stringWithFormat:@"%ld人",model.enrollList.count];
    }

    [imageTable reloadData];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [regisNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [arrowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-9);
        make.centerY.equalTo(regisNumberLabel.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(12);
    }];
    
    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(regisNumberLabel.mas_centerY);
        make.right.equalTo(arrowView.mas_right).offset(-10);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(regisNumberLabel.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(0.5);
    }];
    
    [imageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(lineView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.equalTo(50);
    }];
}
@end
