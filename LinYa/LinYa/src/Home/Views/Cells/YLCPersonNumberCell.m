//
//  YLCPersonNumberCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPersonNumberCell.h"

@implementation YLCPersonNumberCell
{
    UIImageView *iconView;
    UILabel *nameLabel;
    UIImageView *sexTypeView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        iconView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:iconView];
        
        nameLabel = [YLCFactory createLabelWithFont:17 color:RGB(36, 35, 35)];
        
        nameLabel.text = @"ccc";
        
        [self.contentView addSubview:nameLabel];
        
        sexTypeView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:sexTypeView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(51);
        make.height.equalTo(51);
    }];

    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [sexTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_right).offset(10);
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.width.equalTo(8);
        make.height.equalTo(8);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
