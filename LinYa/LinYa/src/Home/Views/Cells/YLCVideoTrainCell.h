//
//  YLCVideoTrainCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCVideoTrainModel.h"
@interface YLCVideoTrainCell : UICollectionViewCell
@property (nonatomic ,strong)YLCVideoTrainModel *model;
@property (nonatomic ,strong)UIImageView *posterView;
@property (nonatomic, copy  ) void(^playBlock)(UIButton *);
@property (nonatomic ,strong)NSDictionary *dict;
@end
