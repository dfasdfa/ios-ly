//
//  YLCProgramMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramMessageCell.h"
#import "YLCProgramePriceView.h"

@implementation YLCProgramMessageCell
{
    UILabel *titleLabel;
    UIImageView *lineView;
    UIView *backView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        titleLabel = [YLCFactory createLabelWithFont:14];
        
        titleLabel.frame = CGRectMake(10, 10, 100, 30);
        
        titleLabel.text = @"项目价格";
        
        [self.contentView addSubview:titleLabel];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = YLC_GRAY_COLOR;
        
        lineView.frame = CGRectMake(0, titleLabel.frameMaxY+10, self.frameWidth, 0.5);
        
        [self.contentView addSubview:lineView];
    }
    return self;
}
- (void)setOptionList:(NSArray *)optionList{
    WS(weakSelf)
    if (backView) {
        [backView removeFromSuperview];
        backView = nil;
    }
    
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, lineView.frameMaxY, self.frameWidth, self.frameHeight-lineView.frameMaxY)];
    
    [self.contentView addSubview:backView];
    
    if (JK_IS_ARRAY_NIL(optionList)) {
        return;
    }
    NSMutableArray *views = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i<optionList.count; i++) {
        YLCProgramePriceView *priceView = [[YLCProgramePriceView alloc] initWithFrame:CGRectMake(0, i*40, self.frameWidth, 40)];
        
        NSDictionary *dict = optionList[i];
        
        priceView.title = dict[@"goods_option"];
        
        priceView.price = dict[@"money"];
        
        [views addObject:priceView];
        
        [backView addSubview:priceView];
    }
    
//    if (views.count>1) {
//        [views mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
//
//        [views mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(weakSelf.mas_left);
//            make.top.equalTo(lineView.mas_bottom).offset(-30);
//            make.width.equalTo(weakSelf.frameWidth);
//            make.height.equalTo(40);
//        }];
//
//    }else{
//        YLCProgramePriceView *priceView = views[0];
//
//        [priceView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(weakSelf.mas_left);
//            make.top.equalTo(lineView.mas_bottom).offset(10);
//            make.width.equalTo(weakSelf.frameWidth);
//            make.height.equalTo(40);
//        }];
//    }
}
@end
