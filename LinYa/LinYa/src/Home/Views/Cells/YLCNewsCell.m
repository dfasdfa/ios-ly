//
//  YLCNewsCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewsCell.h"
#import "YLCToplineModel.h"
@implementation YLCNewsCell
{
    UIImageView *newImageView;
    UILabel *titleLabel;
    UILabel *seeNumberLabel;
    UILabel *timeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    newImageView = [[UIImageView alloc] init];
    
    newImageView.contentMode = UIViewContentModeScaleToFill;
    
    [self.contentView addSubview:newImageView];
    
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(28, 29, 30)];
    
    titleLabel.numberOfLines = 0;
    
    [self.contentView addSubview:titleLabel];
    
    seeNumberLabel = [YLCFactory createLabelWithFont:12 color:RGB(84, 87, 88)];
    
    [self.contentView addSubview:seeNumberLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:12 color:RGB(84, 87, 88)];
    
    [self.contentView addSubview:timeLabel];
}
//- (void)setImagePath:(NSString *)imagePath{
//    [iconView sd_setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage createImageWithColor:YLC_PLACEHOLDER_IMAGE_COLOR]];
//}
- (void)setModel:(YLCToplineModel *)model{
    _model = model;
    
    [newImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.imgsrc]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    timeLabel.text = [model.add_time notNullString];
    
    seeNumberLabel.text = [NSString stringWithFormat:@"%@人观看",[model.view_num notNullString]];
    
    titleLabel.text = [model.title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    
    [newImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(25);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(134);
        make.height.equalTo(88);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(newImageView.mas_right).offset(5);
        make.top.equalTo(newImageView.mas_top);
        make.width.equalTo(170);
        make.height.equalTo(40);
    }];
    
    [seeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.bottom.equalTo(newImageView.mas_bottom);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(seeNumberLabel.mas_right).offset(13);
        make.bottom.equalTo(seeNumberLabel.mas_bottom);
    }];
}
@end
