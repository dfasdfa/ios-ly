//
//  YLCTrainRegisInfoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCActivityModel.h"
@interface YLCTrainRegisInfoCell : UICollectionViewCell
@property (nonatomic ,strong)YLCActivityModel *model;
@end
