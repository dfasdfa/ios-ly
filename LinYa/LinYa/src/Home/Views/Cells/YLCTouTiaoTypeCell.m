//
//  YLCTouTiaoTypeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTouTiaoTypeCell.h"

@implementation YLCTouTiaoTypeCell
{
    UILabel *titleLabel;
//    UILabel *typeLabel;
    UILabel *authorLabel;
//    UILabel *discussLabel;
    UILabel *timeLabel;
    UIImageView *aboutView;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(28, 29, 30)];
        
        titleLabel.numberOfLines = 0;
        
        [self.contentView addSubview:titleLabel];
        
//        typeLabel = [YLCFactory createLabelWithFont:12];
//        
//        [self.contentView addSubview:typeLabel];
        
        authorLabel = [YLCFactory createLabelWithFont:12 color:RGB(162, 168, 171)];
        
        [self.contentView addSubview:authorLabel];
        
//        discussLabel = [YLCFactory createLabelWithFont:12 color:RGB(104, 107, 109)];
//
//        [self.contentView addSubview:discussLabel];
        
        timeLabel  = [YLCFactory createLabelWithFont:12 color:RGB(84, 87, 88)];
        
        [self.contentView addSubview:timeLabel];
        
        aboutView = [[UIImageView alloc] init];
        
        [self.contentView addSubview:aboutView];
        
        lineView = [[UIImageView alloc] init];
        
        lineView.backgroundColor = RGB(231, 236, 238);
        
        [self.contentView addSubview:lineView];
    }
    return self;
}
- (void)setModel:(YLCToplineModel *)model{
    titleLabel.text = model.title;
    
    authorLabel.text = @"临牙共享平台";
    
//    discussLabel.text = [NSString stringWithFormat:@"%@评论",model.comment_num];
    
    timeLabel.text = model.add_time;
    
    [aboutView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
//- (void)setTypeString:(NSString *)typeString{
//    _typeString = typeString;
//    if ([typeString integerValue]==0) {
//        typeLabel.hidden = YES;
//    }
//    if ([typeString integerValue]==1) {
//        typeLabel.hidden = NO;
//        typeLabel.text = @"置顶";
//        typeLabel.textColor = YLC_THEME_COLOR;
//        typeLabel.layer.borderColor = YLC_THEME_COLOR.CGColor;
//    }
//    if ([typeString integerValue]==2) {
//        typeLabel.hidden = NO;
//        typeLabel.text = @"热点";
//        typeLabel.textColor = RGB(244, 64, 34);
//        typeLabel.layer.borderColor = RGB(244, 64, 34).CGColor;
//    }
//    if ([typeString integerValue]==3) {
//        typeLabel.hidden = NO;
//        typeLabel.text = @"热点";
//        typeLabel.textColor = RGB(244, 64, 34);
//        typeLabel.layer.borderColor = RGB(23, 215, 238).CGColor;
//    }
//}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(23);
        make.width.equalTo(weakSelf.frameWidth-133-10-8-10);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
//    [typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(titleLabel.mas_left);
//        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
//        make.width.equalTo(29);
//        make.height.equalTo(16);
//    }];
    
    [authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
    }];
    
//    [discussLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(authorLabel.mas_right).offset(17);
//        make.centerY.equalTo(authorLabel.mas_centerY);
//    }];
    
    [aboutView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-8);
        make.top.equalTo(weakSelf.mas_top).offset(11);
        make.width.equalTo(133);
        make.height.equalTo(88);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(aboutView.mas_left).offset(-11);
        make.centerY.equalTo(authorLabel.mas_centerY);
    }];
    

    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.right.equalTo(aboutView.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
}
@end
