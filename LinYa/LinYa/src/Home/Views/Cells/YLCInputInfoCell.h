//
//  YLCInputInfoCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCInputInfoCell;
@protocol YLCInputInfoCellDelegate <NSObject>
- (void)didInfoChangedWithText:(NSString *)text;
@end
@interface YLCInputInfoCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *text;
@property (nonatomic ,copy)NSString *placeholder;
@property (nonatomic ,weak)id<YLCInputInfoCellDelegate>delegate;
@end
