//
//  YLCCommodityPriceSetCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCommodityPriceSetCell.h"
#import "YLCCommodityAddCell.h"
#import "YLCCommodityAddHeadView.h"
#define ADD_CELL  @"YLCCommodityAddCell"
#define HEAD_ADD  @"YLCCommodityAddHeadView"
@interface YLCCommodityPriceSetCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCCommodityAddCellDelegate>
@end
@implementation YLCCommodityPriceSetCell
{
    UICollectionView *commodityTable;
    BOOL isReload;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.deleteSignal = [RACSubject subject];
        self.insertSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    commodityTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    commodityTable.backgroundColor = [UIColor whiteColor];
    
    [commodityTable registerClass:[YLCCommodityAddCell class] forCellWithReuseIdentifier:ADD_CELL];
    
    [commodityTable registerClass:[YLCCommodityAddHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEAD_ADD];
    
    commodityTable.delegate = self;
    
    commodityTable.dataSource = self;
    
    [self.contentView addSubview:commodityTable];
}
- (void)setCommodityList:(NSArray *)commodityList{
    
    _commodityList = commodityList;
    
    [commodityTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _commodityList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCCommodityAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ADD_CELL forIndexPath:indexPath];
    
    cell.index = indexPath.row;
    
    cell.delegate = self;

    if (indexPath.row<_commodityList.count) {
        cell.model = _commodityList[indexPath.row];
    }
    
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        YLCCommodityAddHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HEAD_ADD forIndexPath:indexPath];
        WS(weakSelf)
        if (!isReload) {
            isReload = YES;
            [headView.insertSignal subscribeNext:^(id x) {
                
                [weakSelf.insertSignal sendNext:x];
            }];
        }
        return headView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 37);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth, 37);
}
- (void)didDeleteCommodityWithIndex:(NSInteger)index{
    [self.deleteSignal sendNext:[NSIndexPath indexPathForRow:index inSection:0]];
}
- (void)didChangeTextWithModel:(YLCProgramTraitFillModel *)model index:(NSInteger)index{
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeModelWithModel:index:cellType:)) {
        [self.delegate didChangeModelWithModel:model index:index cellType:_cellType];
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [commodityTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
@end
