//
//  YLCInviteJobIntroCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteJobIntroCell.h"

@implementation YLCInviteJobIntroCell
{
    UILabel *titleLabel;
    UILabel *seeNumberLabel;
    UILabel *payLabel;
    UIImageView *workTimeIconView;
    UILabel *workTimeLabel;
    UIImageView *areaIconView;
    UILabel *areaLabel;
    UIImageView *eduDegreeIconView;
    UILabel *eduDegreeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
    titleLabel = [YLCFactory createLabelWithFont:17 color:RGB(30, 30, 30)];
    
    [self.contentView addSubview:titleLabel];
    
    payLabel = [YLCFactory createLabelWithFont:15 color:[UIColor redColor]];
    
    [self.contentView addSubview:payLabel];
    
    seeNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    [self.contentView addSubview:seeNumberLabel];
    
    workTimeIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_bag"]];
    
    [self.contentView addSubview:workTimeIconView];
    
    workTimeLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    [self.contentView addSubview:workTimeLabel];
    
    areaIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_area"]];
    
    [self.contentView addSubview:areaIconView];
    
    areaLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    [self.contentView addSubview:areaLabel];
    
    eduDegreeIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invite_hat"]];
    
    [self.contentView addSubview:eduDegreeIconView];
    
    eduDegreeLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    eduDegreeLabel.text = @"大专";
    
    [self.contentView addSubview:eduDegreeLabel];
}
- (void)setModel:(YLCInviteModel *)model{
    titleLabel.text = [model.position notNullString];
    
    payLabel.text = [model.salary_name notNullString];
    
    seeNumberLabel.text = [NSString stringWithFormat:@"浏览：%@人",[model.visit_num notNullString]];
    
    workTimeLabel.text = [NSString stringWithFormat:@"%@年以上",[model.work_year notNullString]];
    
    areaLabel.text = [model.location_name notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    
    [seeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.top.equalTo(titleLabel.mas_top);
    }];
    
    [payLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [workTimeIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [workTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(workTimeIconView.mas_right).offset(5);
        make.centerY.equalTo(workTimeIconView.mas_centerY);
    }];
    
    [areaIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(workTimeLabel.mas_right).offset(30);
        make.centerY.equalTo(workTimeIconView.mas_centerY);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaIconView.mas_right).offset(5);
        make.centerY.equalTo(areaIconView.mas_centerY);
    }];
    
    [eduDegreeIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaLabel.mas_right).offset(30);
        make.centerY.equalTo(areaLabel.mas_centerY);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [eduDegreeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(eduDegreeIconView.mas_right).offset(5);
        make.centerY.equalTo(eduDegreeIconView.mas_centerY);
    }];
}
@end
