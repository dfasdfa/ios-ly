//
//  YLCFillInPersonMessageCell.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCFillInPersonMessageCell;
@protocol YLCFillInPersonMessageCellDelegate <NSObject>
- (void)didChangeFillInFieldWithText:(NSString *)text
                               index:(NSInteger)index;
@end
@interface YLCFillInPersonMessageCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *placeholder;
@property (nonatomic ,copy)NSString *text;
@property (nonatomic ,assign)NSInteger index;
@property (nonatomic ,assign)BOOL isRight;
@property (nonatomic ,assign)BOOL isNumber;
@property (nonatomic ,weak)id<YLCFillInPersonMessageCellDelegate>delegate;
@end
