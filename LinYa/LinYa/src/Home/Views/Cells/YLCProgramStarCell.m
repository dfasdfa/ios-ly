//
//  YLCProgramStarCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramStarCell.h"
#import "YLCStarView.h"
@implementation YLCProgramStarCell
{
    UILabel *titleLabel;
    YLCStarView *starView;
    UILabel *starNumberLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:13.65 color:RGB(119, 123, 124)];
    
    titleLabel.text = @"商品星级";
    
    [self.contentView addSubview:titleLabel];
    
    starNumberLabel = [YLCFactory createLabelWithFont:12 color:[UIColor blackColor]];
    
    starNumberLabel.text = @"";
    
    [self.contentView addSubview:starNumberLabel];
    
    starView = [[YLCStarView alloc] initWithFrame:CGRectMake(0, 0, 115, 20)];
    
    starView.grayName = @"star_gray";
    
    starView.yelloName = @"star_yellow";
    
    starView.canTouch = YES;
    
    [starView.starSignal subscribeNext:^(id x) {
        starView.score = [x floatValue];
        starNumberLabel.text = [NSString stringWithFormat:@"%@星",x];
        if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didChangeStarNumber:)) {
            [self.delegate didChangeStarNumber:x];
        }
    }];
    
    [self.contentView addSubview:starView];
}
- (void)setStarNum:(CGFloat)starNum{
    starView.score = starNum;
    
    starNumberLabel.text = [NSString stringWithFormat:@"%.0f星",starNum];
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [starNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-28);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(starNumberLabel.mas_left).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(115);
        make.height.equalTo(20);
    }];
}
@end
