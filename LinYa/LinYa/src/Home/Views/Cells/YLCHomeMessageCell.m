//
//  YLCHomeMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeMessageCell.h"
#import "GYRollingNoticeView.h"
#import "YLCCustomNoticceViewCell.h"
#import "YLCTrendModel.h"
#import "YLCDynamicInfoViewController.h"
#import "YLCTrainRegisInfoViewController.h"
#import "YLCActivityModel.h"
#import "YLCInviteInfoViewController.h"
@interface YLCHomeMessageCell()<GYRollingNoticeViewDelegate,GYRollingNoticeViewDataSource>
@end
@implementation YLCHomeMessageCell
{
    GYRollingNoticeView *noticeView;
}
- (void)invalidateTimer{
    [noticeView.timer invalidate];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        noticeView = [[GYRollingNoticeView alloc]initWithFrame:CGRectMake(0, 0, self.frameWidth, self.frameHeight)];
        [noticeView registerClass:[YLCCustomNoticceViewCell class] forCellReuseIdentifier:@"notice_cell"];
        noticeView.dataSource = self;
        noticeView.delegate = self;
        [self.contentView addSubview:noticeView];
//        [noticeView beginScroll];
    }
    return self;
}
- (void)setTrendArr:(NSArray *)trendArr{
//    if (JK_IS_ARRAY_NIL(trendArr)) {
//        noticeView.hidden = YES;
//    }else{
//        noticeView.hidden = NO;
    
        _trendArr = trendArr;
        
        [noticeView beginScroll];
//    }
    
}
- (NSInteger)numberOfRowsForRollingNoticeView:(GYRollingNoticeView *)rollingView{
    return _trendArr.count;
}
- (__kindof GYNoticeViewCell *)rollingNoticeView:(GYRollingNoticeView *)rollingView cellAtIndex:(NSUInteger)index
{
    YLCCustomNoticceViewCell *cell = [rollingView dequeueReusableCellWithIdentifier:@"notice_cell"];
    
    if (index<_trendArr.count) {
        cell.model = _trendArr[index];
    }
    
    return cell;
}
- (void)didClickRollingNoticeView:(GYRollingNoticeView *)rollingView forIndex:(NSUInteger)index{
    
    YLCTrendModel *model = _trendArr[index];
    
    switch ([model.type integerValue]) {
        case 1:
            [self gotoTieziWithModel:model];
            break;
        case 2:
            [self gotoQuestionWithModel:model];
            break;
        case 3:
            [self gotoGoodsWithModel:model];
            break;
        case 4:
            [self gotoInviteWithModel:model];
            break;
        case 5:
            [self gotoTrainWithModel:model];
            break;
        case 6:
            [self gotoActivityWithModel:model];
            break;
        default:
            break;
    }
}
- (void)gotoTieziWithModel:(YLCTrendModel *)model{
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.type = 5;
    
    con.caseId = model.publish_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoQuestionWithModel:(YLCTrendModel *)model{
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.type = 0;
    
    con.caseId = model.publish_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoGoodsWithModel:(YLCTrendModel *)model{
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.type = 4;
    
    con.caseId = model.publish_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoTrainWithModel:(YLCTrendModel *)model{
    YLCTrainRegisInfoViewController *con = [[YLCTrainRegisInfoViewController alloc] init];
    
    YLCActivityModel *activityModel = [[YLCActivityModel alloc] init];
    
    activityModel.id = model.publish_id;
    
    con.model = activityModel;
    
    con.type = 0;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoActivityWithModel:(YLCTrendModel *)model{
    YLCTrainRegisInfoViewController *con = [[YLCTrainRegisInfoViewController alloc] init];
    
    YLCActivityModel *activityModel = [[YLCActivityModel alloc] init];
    
    activityModel.id = model.publish_id;
    
    con.model = activityModel;
    
    con.type = 1;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoInviteWithModel:(YLCTrendModel *)model{
    YLCInviteInfoViewController *con = [[YLCInviteInfoViewController alloc] init];
    
    con.inviteId = model.publish_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
@end
