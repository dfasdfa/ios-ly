//
//  YLCProgramStarCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCProgramStarCell;
@protocol YLCProgramStarCellDelegate <NSObject>
- (void)didChangeStarNumber:(NSString *)starNumber;
@end
@interface YLCProgramStarCell : UICollectionViewCell
@property (nonatomic ,weak)id<YLCProgramStarCellDelegate>delegate;
@property (nonatomic ,assign)CGFloat starNum;
@end
