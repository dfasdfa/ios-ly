//
//  YLCPayMessageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayMessageCell.h"
#import "YLCPayTimeCell.h"
#define PAY_CELL @"YLCPayTimeCell"
@interface YLCPayMessageCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCPayMessageCell
{
    UICollectionView *timeTable;
    UILabel *numberTitleLabel;
    UILabel *payNumberLabel;
    UILabel *timeTitleLabel;
    UILabel *needPayLabel;
    UIImageView *lineView;
    NSArray *timeList;
    NSInteger selectIndex;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        selectIndex = 0;
        timeList = @[@"一年"];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    numberTitleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    numberTitleLabel.text = @"";
    
    [self.contentView addSubview:numberTitleLabel];
    
    payNumberLabel = [YLCFactory createLabelWithFont:13 color:RGB(163, 180, 190)];
    
    payNumberLabel.text = @"";
    
    [self.contentView addSubview:payNumberLabel];
    
    lineView = [[UIImageView alloc] init];
    
//    lineView.backgroundColor = RGB(219, 230, 234);
    
    lineView.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:lineView];
    
    timeTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(62, 65, 66)];
    
    timeTitleLabel.text = @"购买服务时间";
    
    [self.contentView addSubview:timeTitleLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumInteritemSpacing = 5;
    
    timeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    timeTable.backgroundColor = [UIColor clearColor];
    
    [timeTable registerClass:[YLCPayTimeCell class] forCellWithReuseIdentifier:PAY_CELL];
    
    timeTable.delegate = self;
    
    timeTable.dataSource = self;
    
    [self.contentView addSubview:timeTable];
    
    needPayLabel = [YLCFactory createLabelWithFont:17 color:[UIColor blackColor]];
    
    [self.contentView addSubview:needPayLabel];
    
    NSString *payString = @"所需金额:298元";
    
    NSMutableAttributedString *attr = [YLCFactory getAttributedStringWithString:payString FirstCount:5 secondCount:3 firstFont:15 secondFont:17 firstColor:RGB(45, 48, 49) secondColor:RGB(255, 84, 0)];
    
    needPayLabel.attributedText = attr;
}
- (void)setPayString:(NSString *)payString{
    
    NSString *pay = [NSString stringWithFormat:@"所需金额:%@元",payString];
    
    NSMutableAttributedString *attr = [YLCFactory getAttributedStringWithString:pay FirstCount:5 secondCount:3 firstFont:15 secondFont:17 firstColor:RGB(45, 48, 49) secondColor:RGB(255, 84, 0)];
    
    needPayLabel.attributedText = attr;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return timeList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPayTimeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PAY_CELL forIndexPath:indexPath];
    
    if (indexPath.row<timeList.count) {
        cell.string = timeList[indexPath.row];
    }
    if (indexPath.row==selectIndex) {
        cell.isSelect = YES;
    }else{
        cell.isSelect = NO;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(84, 44);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectIndex = indexPath.row;
    if (JK_IS_DELEGATE_RSP_SEL(self.delegate, didSelectPriceAtIndex:payPrice:)) {
        [self.delegate didSelectPriceAtIndex:indexPath.row payPrice:_payString];
    }
    [timeTable reloadData];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [numberTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(17);
        make.top.equalTo(weakSelf.mas_top).offset(12);
    }];
    
    [payNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-11);
        make.centerY.equalTo(numberTitleLabel.mas_centerY);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(numberTitleLabel.mas_left);
        make.top.equalTo(numberTitleLabel.mas_bottom).offset(12);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(0.5);
    }];
    
    [timeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(numberTitleLabel.mas_left);
        make.top.equalTo(lineView.mas_bottom).offset(8);
    }];
    
    [timeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeTitleLabel.mas_left);
        make.top.equalTo(timeTitleLabel.mas_bottom).offset(10);
        make.width.equalTo(89*4-5);
        make.height.equalTo(44);
    }];
    
    [needPayLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeTitleLabel.mas_left);
        make.top.equalTo(timeTable.mas_bottom).offset(10);
    }];
}
@end
