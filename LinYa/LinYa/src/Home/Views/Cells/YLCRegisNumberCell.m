//
//  YLCRegisNumberCell.m
//  LinYa
//
//  Created by 初程程 on 2018/4/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRegisNumberCell.h"

@implementation YLCRegisNumberCell
{
    UIImageView *iconView;
    UILabel *nameLabel;
    UIImageView *sexView;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:iconView];
    
    nameLabel = [YLCFactory createLabelWithFont:14 color:RGB(30, 30, 30)];
    
    [self.contentView addSubview:nameLabel];
    
    sexView = [[UIImageView alloc] init];
    
    [self.contentView addSubview:sexView];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(235, 235, 235);
    
    [self.contentView addSubview:lineView];
}
- (void)setPersonDict:(NSDictionary *)personDict{
    [iconView sd_setImageWithURL:[NSURL URLWithString:personDict[@"enroll_user_img"]] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    nameLabel.text = [YLCFactory isStringOk:personDict[@"enroll_user_name"]]?personDict[@"enroll_user_name"]:@"";
    
    NSString *imageName = [personDict[@"sex"] isEqualToString:@"1"]?@"sex_man":@"sex_girl";
    
    sexView.image = [UIImage imageNamed:imageName];
}
- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(50);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconView.mas_right).offset(10);
        make.centerY.equalTo(iconView.mas_centerY);
    }];
    
    [sexView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_right).offset(10);
        make.centerY.equalTo(nameLabel.mas_centerY);
        make.width.height.equalTo(20);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
}
@end
