//
//  YLCHomeRecommendCell.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeRecommendCell.h"
#import "YLCHomeRecommendView.h"
#import "YLCTitleAndIconModel.h"
#import "YLCUnusedViewController.h"
#import "YLCTrainRegistrationViewController.h"
#import "YLCVideoLifeViewController.h"
#import "YLCTouTiaoListViewController.h"
#import "YLCMatterBaseViewController.h"
@implementation YLCHomeRecommendCell
{
    UIView *backgroundView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)setSubViews:(NSArray *)subViews{
    _subViews = subViews;
    [self createCustomView];
}
- (void)createCustomView{
    if (backgroundView) {
        [backgroundView removeFromSuperview];
        backgroundView = nil;
    }
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frameWidth, self.frameHeight)];
    
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    [self.contentView addSubview:backgroundView];
    
    for (int i = 0; i<_subViews.count; i++) {
        YLCHomeRecommendView *view = [[YLCHomeRecommendView alloc] initWithFrame:CGRectMake((i%2)*self.frameWidth/2, (i/2)*(self.frameHeight/2), self.frameWidth/2, self.frameHeight/2)];
        
        YLCTitleAndIconModel *model = _subViews[i];
        
        view.title = model.title;
        
        view.imagePath = model.iconName;
        
        view.subText = model.subText;
        
        view.tag = 330+i;
        
        [backgroundView addSubview:view];
        
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectWithGes:)];
        
        [view addGestureRecognizer:ges];
    }
}
- (void)didSelectWithGes:(UITapGestureRecognizer *)ges{
    NSInteger index = ges.view.tag-330;
    if (index==0) {
        [self gotoUnused];
    }
    if (index==1) {
        [self gotoActivityList];
    }
    if (index==2) {
        [self gotoVideo];
    }
    if (index==3) {
        [self gotoTouTiao];
    }
}
- (void)gotoUnused{
    YLCUnusedViewController *con = [[YLCUnusedViewController alloc] init];
    
    con.showScreen = YES;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoActivityList{
    YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
    
    con.viewType = 1;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoVideo{
    
    YLCVideoLifeViewController *con = [[YLCVideoLifeViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoTouTiao{
//    YLCTouTiaoListViewController *con = [[YLCTouTiaoListViewController alloc] init];
//
//    con.topLineTopArr = _newsList;
//
//    con.topLineBottomArr = _topList;
//
//    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    YLCMatterBaseViewController *con = [[YLCMatterBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
@end
