//
//  YLCCommodityPriceSetCell.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YLCCommodityPriceSetCell,YLCProgramTraitFillModel;
@protocol YLCCommodityPriceSetCellDelegate <NSObject>
- (void)didChangeModelWithModel:(YLCProgramTraitFillModel *)model
                          index:(NSInteger)index
                       cellType:(NSInteger)cellType;
@end
@interface YLCCommodityPriceSetCell : UICollectionViewCell
@property (nonatomic ,strong)NSArray *commodityList;
@property (nonatomic ,strong)RACSubject *insertSignal;
@property (nonatomic ,strong)RACSubject *deleteSignal;
@property (nonatomic ,weak)id<YLCCommodityPriceSetCellDelegate>delegate;
@property (nonatomic ,assign)NSInteger cellType;
@end
