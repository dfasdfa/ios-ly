//
//  YLCProgramListView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramListView.h"
#import "YLCProgramListCell.h"
#import "YLCImageListViewController.h"
#import "YLCEasyVideoListViewController.h"
#define LIST_CELL   @"YLCProgramListCell"
@interface YLCProgramListView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCProgramListCellDelegate>
@end
@implementation YLCProgramListView
{
    UICollectionView *listTable;
    UIButton *imgBtn;
    UIButton *imageListBtn;
    UIButton *videoListBtn;
    NSArray *colorArr;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.selectSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    colorArr = @[@"#fbfced",@"#eefcf0",@"#ecf6fc"];
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    listTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    listTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [listTable registerClass:[YLCProgramListCell class] forCellWithReuseIdentifier:LIST_CELL];
    
    listTable.delegate =self;
    
    listTable.dataSource = self;
    
    [self addSubview:listTable];
    
    imgBtn = [YLCFactory createBtnWithImage:@""];
    
    imgBtn.backgroundColor = [UIColor redColor];
    
    [imgBtn addTarget:self action:@selector(gotoImageList) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:imgBtn];
    
    imageListBtn = [YLCFactory createBtnWithImage:@"easy_image_btn"];
    
    [imageListBtn addTarget:self action:@selector(gotoImageList) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:imageListBtn];
    
    videoListBtn = [YLCFactory createBtnWithImage:@"easy_video_btn"];
    
    videoListBtn.imageView.contentMode = UIViewContentModeScaleToFill;
    
    [videoListBtn addTarget:self action:@selector(gotoVideoList) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:videoListBtn];
}
- (void)gotoImageList{
    YLCImageListViewController *con = [[YLCImageListViewController alloc] init];
    
    con.classifyId = _classId;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoVideoList{
    YLCEasyVideoListViewController *con = [[YLCEasyVideoListViewController alloc] init];
    
    con.classifyId = _classId;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)setProgramList:(NSArray *)programList{
    
    _programList = programList;
    
    [listTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _programList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LIST_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_programList.count) {
        cell.model = _programList[indexPath.row];
    }
    cell.delegate = self;
    
    cell.index = indexPath.row;
    
    cell.backgroundColor = [UIColor colorWithHexString:colorArr[indexPath.row%3]];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCEasyProgramListModel *model = _programList[indexPath.row];
    
    CGSize size = [YLCFactory getStringSizeWithString:model.feature width:self.frameWidth-40 font:13.7];
    
    CGFloat tableHeight = model.options.count*30;
    
    return CGSizeMake(self.frameWidth, 150+size.height-20+tableHeight);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.selectSignal sendNext:indexPath];
}
- (void)didTouchTableSelectAtIndex:(NSInteger)index{
    [self.selectSignal sendNext:[NSIndexPath indexPathForRow:index inSection:0]];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [listTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [imageListBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-50);
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.width.height.equalTo(47);
    }];
    
    [videoListBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-50);
        make.right.equalTo(imageListBtn.mas_left).offset(-20);
        make.width.height.equalTo(47);
    }];
//    WS(weakSelf)
//    [imgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-60);
//        make.bottom.equalTo(weakSelf.mas_bottom).offset(-60);
//        make.width.height.equalTo(30);
//    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
