//
//  YLCHomeCompanyView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeCompanyView.h"
#import "YLCImageCell.h"
#define IMAGE_CELL @"YLCImageCell"
@interface YLCHomeCompanyView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCHomeCompanyView
{
    UICollectionView *imageTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.selectSignal = [RACSubject subject];
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        imageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        
        imageTable.backgroundColor = [UIColor whiteColor];
        
        imageTable.delegate = self;
        
        imageTable.dataSource = self;
        
        [imageTable registerClass:[YLCImageCell class] forCellWithReuseIdentifier:IMAGE_CELL];
        
        [self addSubview:imageTable];
    }
    return self;
}
- (void)setCompanyList:(NSArray *)companyList{
    _companyList = companyList;
    
    [imageTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _companyList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IMAGE_CELL forIndexPath:indexPath];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth/4, self.frameHeight);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [_selectSignal sendNext:indexPath];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [imageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
