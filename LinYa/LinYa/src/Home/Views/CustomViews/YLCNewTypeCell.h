//
//  YLCNewTypeCell.h
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCNewTypeCell : UICollectionViewCell
@property (nonatomic ,copy)NSString *iconName;
@property (nonatomic ,copy)NSString *title;
@end
