//
//  YLCInviteInfoView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteInfoView.h"
#import "YLCInviteJobIntroCell.h"
#import "YLCWelfareCell.h"
#import "YLCInviteCompanyIntroCell.h"
#import "YLCWorkNeedCell.h"
#import "YLCBottomBtnView.h"
#import "YLCChatBaseViewController.h"
#import "YLCResumeViewController.h"
#define INVITE_CELL   @"YLCInviteJobIntroCell"
#define WELF_CELL     @"YLCWelfareCell"
#define COMPANY_CELL  @"YLCInviteCompanyIntroCell"
#define WORK_NEED     @"YLCWorkNeedCell"
@interface YLCInviteInfoView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCInviteInfoView
{
    UICollectionView *infoTable;
    YLCBottomBtnView *bottomView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)initDataSource{
    
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    infoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    infoTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [infoTable registerClass:[YLCInviteJobIntroCell class] forCellWithReuseIdentifier:INVITE_CELL];
    
    [infoTable registerClass:[YLCWelfareCell class] forCellWithReuseIdentifier:WELF_CELL];
    
    [infoTable registerClass:[YLCInviteCompanyIntroCell class] forCellWithReuseIdentifier:COMPANY_CELL];
    
    [infoTable registerClass:[YLCWorkNeedCell class] forCellWithReuseIdentifier:WORK_NEED];
    
    infoTable.delegate = self;
    
    infoTable.dataSource = self;
    
    [self addSubview:infoTable];
    
    bottomView = [[YLCBottomBtnView alloc] initWithFrame:CGRectZero];
    
    [bottomView.applyBtn addTarget:self action:@selector(gotoInvite) forControlEvents:UIControlEventTouchUpInside];
    
    [bottomView.talkBtn addTarget:self action:@selector(gotoTalk) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:bottomView];
}
- (void)gotoTalk{
    if (![YLCLoginManager checkLoginStateWithTarget:self]) {
        return;
    }
    YLCChatBaseViewController *con = [[YLCChatBaseViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:_model.user_id];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)gotoInvite{

    [YLCHUDShow showSelectAlertWithTarget:self Message:@"是否投递简历" leftTitle:@"取消" rightTitle:@"确定" selector:^(NSInteger index) {
        if (index==1) {
            YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
            
            [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"recruitId":_model.id} method:@"POST" urlPath:@"app/resume/deliver" delegate:self response:^(id responseObject, NSError *error) {
                if (error) {
                    if (error.code==1001) {
                        
                        RACSubject *subject = [RACSubject subject];
                        
                        [YLCHUDShow showResumeHUDWithSignal:subject];
                        
                        [subject subscribeNext:^(id x) {
                            YLCResumeViewController *con = [[YLCResumeViewController alloc] init];
                            
                            con.isExit = YES;
                            
                            con.resumeType = 0;
                            
                            con.uid = userModel.user_id;
                            
                            [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
                        }];
                    }
                    return ;
                }
                [SVProgressHUD showSuccessWithStatus:@"投递成功！"];
            }];
        }
    }];
    

}
- (void)setModel:(YLCInviteModel *)model{
    _model = model;
    [infoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCInviteJobIntroCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INVITE_CELL forIndexPath:indexPath];
        
        cell.model = _model;
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCWelfareCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WELF_CELL forIndexPath:indexPath];
        
        cell.labelList = _model.welfare_arr;
        
        return cell;
    }
    if (indexPath.row==2) {
        YLCInviteCompanyIntroCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:COMPANY_CELL forIndexPath:indexPath];
        
        cell.model = _model;
        
        return cell;
    }
    if (indexPath.row==3) {
        YLCWorkNeedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WORK_NEED forIndexPath:indexPath];
        
        cell.model = _model;
        
        return cell;
    }
    
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.frameWidth, 110);
    }
    if (indexPath.row==1) {
        return CGSizeMake(self.frameWidth, 50);
    }
    if (indexPath.row==2) {
        return CGSizeMake(self.frameWidth, 80);
    }
    if (indexPath.row==3) {
        
        CGSize size = [YLCFactory getStringSizeWithString:_model.content width:self.frameWidth-30 font:14];
        
        return CGSizeMake(self.frameWidth, 40+size.height);
    }
    return CGSizeZero;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [infoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    WS(weakSelf)
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.left.equalTo(weakSelf.mas_left);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(60);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
