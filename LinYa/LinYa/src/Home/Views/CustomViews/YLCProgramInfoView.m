//
//  YLCProgramInfoView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramInfoView.h"
#import "YLCProgramShowCell.h"
#import "YLCProgramInfoIntroCell.h"
#import "YLCProgramMessageCell.h"
#import "YLCProgramCorrelationCell.h"
#import "YLCProgramAddViewController.h"
#define SHOW_CELL  @"YLCProgramShowCell"
#define INTRO_CELL @"YLCProgramInfoIntroCell"
#define MESSAGE_CELL   @"YLCProgramMessageCell"
#define CORRELA_CELL   @"YLCProgramCorrelationCell"
@interface YLCProgramInfoView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCProgramInfoView
{
    UICollectionView *infoTable;
    UIButton *editBtn;
    UIButton *deleteBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    infoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    infoTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [infoTable registerClass:[YLCProgramShowCell class] forCellWithReuseIdentifier:SHOW_CELL];
    
    [infoTable registerClass:[YLCProgramInfoIntroCell class] forCellWithReuseIdentifier:INTRO_CELL];
    
    [infoTable registerClass:[YLCProgramMessageCell class] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    [infoTable registerClass:[YLCProgramCorrelationCell class] forCellWithReuseIdentifier:CORRELA_CELL];
    
    infoTable.delegate = self;
    
    infoTable.dataSource = self;
    
    [self addSubview:infoTable];
    
    editBtn = [YLCFactory createBtnWithImage:@"easy_edit"];
    
    [editBtn addTarget:self action:@selector(editInfo) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:editBtn];
    
    deleteBtn = [YLCFactory createBtnWithImage:@"easy_delete"];
    
    [deleteBtn addTarget:self action:@selector(deleteInfo) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:deleteBtn];
}
- (void)deleteInfo{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    [YLCHUDShow showSelectAlertWithTarget:self Message:@"是否确认删除" leftTitle:@"确认" rightTitle:@"取消" selector:^(NSInteger index) {
        if (index==0) {
            [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"classifyId":_infoModel.classify_id,@"goodsId":_infoModel.product_id} method:@"POST" urlPath:@"app/sygt/delGoods" delegate:self response:^(id responseObject, NSError *error) {
                if (error) {
                    return ;
                }
                
                [SVProgressHUD showSuccessWithStatus:@"删除成功"];
                
                [(UINavigationController *)[YLCFactory currentNav] popViewControllerAnimated:YES];
            }];
        }
    }];
}
- (void)editInfo{
    YLCProgramAddViewController *con = [[YLCProgramAddViewController alloc] init];
    
    con.programModel = _infoModel;
    
    con.classifyId = _infoModel.classify_id;
    
    con.goodsId = _infoModel.product_id;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)setInfoModel:(YLCEasyProgramListModel *)infoModel{
    _infoModel = infoModel;
    
    [infoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCProgramShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SHOW_CELL forIndexPath:indexPath];
        
        cell.caseList = _infoModel.caseList;
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCProgramInfoIntroCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INTRO_CELL forIndexPath:indexPath];
        
        cell.model = _infoModel;
        
        return cell;
    }
    
    if (indexPath.row==2) {
        YLCProgramMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MESSAGE_CELL forIndexPath:indexPath];
        
        cell.optionList = _infoModel.options;
        
        return cell;
    }
    
    if (indexPath.row==3) {
        YLCProgramCorrelationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CORRELA_CELL forIndexPath:indexPath];
        
        cell.infoString = _infoModel.goodDescription;
        
        return cell;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        NSInteger horNumber = (self.frameWidth-24+10)/93;
        if (JK_IS_ARRAY_NIL(_infoModel.caseList)) {
            return CGSizeMake(self.frameWidth, 55);
        }
        NSInteger verNumber = _infoModel.caseList.count/horNumber;
        
        if (_infoModel.caseList.count%horNumber>0) {
            verNumber+=1;
        }
        
        return CGSizeMake(self.frameWidth, 55 + 95*verNumber);
    }
    if (indexPath.row==1) {
        CGSize size = [YLCFactory getStringSizeWithString:_infoModel.feature width:self.frameWidth-40 font:13.7];
        
        return CGSizeMake(self.frameWidth, 110+size.height);
    }
    if (indexPath.row==2) {
        if (JK_IS_ARRAY_NIL(_infoModel.options)) {
            return CGSizeMake(self.frameWidth, 60.5);
        }else{
            return CGSizeMake(self.frameWidth, 60.5+40*_infoModel.options.count);
        }
        
    }
    if (indexPath.row==3) {
        CGSize size = [YLCFactory getStringSizeWithString:_infoModel.goodDescription width:self.frameWidth-24 font:13];
        
        return CGSizeMake(self.frameWidth, 50+size.height);
    }
    return CGSizeZero;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [infoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-50);
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.width.height.equalTo(47);
    }];
    
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-50);
        make.right.equalTo(editBtn.mas_left).offset(-20);
        make.width.height.equalTo(47);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
