//
//  YLCTrainVideoView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFPlayer.h"
#import "YLCRefrshTableView.h"
@interface YLCTrainVideoView : YLCRefrshTableView
@property (nonatomic, strong) ZFPlayerView        *playerView;
@property (nonatomic ,strong)NSArray *videoList;
@end
