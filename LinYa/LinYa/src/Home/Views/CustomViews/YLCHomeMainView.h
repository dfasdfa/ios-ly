//
//  YLCHomeMainView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeMainView : UIView
@property (nonatomic ,strong)NSArray *tipArr;
@property (nonatomic ,strong)NSArray *recommendList;
@property (nonatomic ,strong)NSArray *hospitalList;
@property (nonatomic ,strong)NSArray *newsList;
@property (nonatomic ,strong)NSArray *bannerList;
@property (nonatomic ,strong)NSArray *videoList;
@property (nonatomic ,strong)NSArray *top_lineArr;
@property (nonatomic ,strong)NSArray *trendArr;
@property (nonatomic ,strong)RACSubject *chooseSignal;
@property (nonatomic ,strong)id target;
@property (nonatomic ,strong)RACSubject *reloadSubject;
- (void)endReload;
- (void)beganRefresh;
- (void)viewDisappear;
@end
