//
//  YLCHomeVideoView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoInfoHeadView.h"
@implementation YLCVideoInfoHeadView
{
    UILabel *titleLabel;
    UILabel *seeNumberLabel;
    UILabel *typeLabel;
    UIButton *likeBtn;
    UIButton *noLikeBtn;
    UIImageView *lineView;
    YLCUserHeadView *userIconView;
    UILabel *nameLabel;
    UILabel *timeLabel;
    UIButton *careBtn;
    UIView *playerBackView;
    ZFPlayerModel *playModel;
    UIImageView *likeImageView;
    UILabel *likeNumberLabel;
    UIButton *shareBtn;
}
- (void)videoStop{
    [self.playerView pause];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        playModel = [[ZFPlayerModel alloc] init];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    playerBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frameWidth, 240)];
    
    [self addSubview:playerBackView];
    
    playModel.fatherView = playerBackView;
    
    titleLabel = [YLCFactory createLabelWithFont:17 color:RGB(56, 57, 58)];
    
    [self addSubview:titleLabel];
    
    seeNumberLabel = [YLCFactory createLabelWithFont:15 color:RGB(147, 152, 154)];
    
    [self addSubview:seeNumberLabel];
    
    likeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"like_normal"]];
    
    [self addSubview:likeImageView];
    
    likeNumberLabel = [YLCFactory createLabelWithFont:14 color:RGB(215, 215, 215)];
    
    [self addSubview:likeNumberLabel];
    
    typeLabel = [YLCFactory createLabelWithFont:11 color:RGB(76, 186, 210)];
    
    [self addSubview:typeLabel];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(234, 235, 235);
    
    [self addSubview:lineView];
    
    userIconView = [[YLCUserHeadView alloc] init];
    
    [self addSubview:userIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:15 color:RGB(51, 51, 51)];
    
    [self addSubview:nameLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:12 color:RGB(162, 171, 174)];
    
    [self addSubview:timeLabel];
    
    shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
    
    [shareBtn addTarget:self action:@selector(shareLink) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:shareBtn];
    
//    careBtn = [YLCFactory createCommondBtnWithTitle:@"关注" backgroundColor:RGB(25, 183, 248) titleColor:[UIColor whiteColor]];
//
//    [self addSubview:careBtn];
}
- (void)shareLink{
    [YLCHUDShow showShareHUDWithContent:_infoModel.share_title shareLink:_infoModel.share_link];
}
- (void)setInfoModel:(YLCVideoLiftInfoModel *)infoModel{
    _infoModel = infoModel;
//    [userIconView sd_setImageWithURL:[NSURL URLWithString:infoModel.avatar_img] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    [userIconView configHeadViewWithImageUrl:infoModel.avatar_img placeholder:@"image_placeholder" isGoCenter:YES userId:infoModel.user_id];
    
    nameLabel.text = infoModel.nick_name;
    
    timeLabel.text = [infoModel.add_time notNullString];
    
    if (JK_IS_STR_NIL(infoModel.played_num)) {
        seeNumberLabel.text = @"0次播放";
    }else{
        seeNumberLabel.text = [NSString stringWithFormat:@"%@次播放",infoModel.played_num];
    }

    if (JK_IS_STR_NIL(infoModel.top_num)) {
        likeNumberLabel.text = @"0";
    }else{
        likeNumberLabel.text = [NSString stringWithFormat:@"%@",infoModel.top_num];
    }
    titleLabel.text = [infoModel.title notNullString];
    

//    playController.assetUrl = [NSURL URLWithString:@"http://yigoutong.shweijue.com/upload/videolife/video/201803/15222996427707.mp4"];
    
    playModel.title = [infoModel.title notNullString];
    
    playModel.videoURL = [NSURL URLWithString:infoModel.site];
    
    [self.playerView resetToPlayNewVideo:playModel];
}
- (ZFPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[ZFPlayerView alloc] init];
        
        /*****************************************************************************************
         *   // 指定控制层(可自定义)
         *   // ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
         *   // 设置控制层和播放模型
         *   // 控制层传nil，默认使用ZFPlayerControlView(如自定义可传自定义的控制层)
         *   // 等效于 [_playerView playerModel:self.playerModel];
         ******************************************************************************************/
//        [_playerView playerControlView:nil playerModel:nil];
        
        // 设置代理
//        _playerView.delegate = self;
        
        //（可选设置）可以设置视频的填充模式，内部设置默认（ZFPlayerLayerGravityResizeAspect：等比例填充，直到一个维度到达区域边界）
        // _playerView.playerLayerGravity = ZFPlayerLayerGravityResize;
        
        // 打开下载功能（默认没有这个功能）
        
        
        // 打开预览图
        _playerView.hasPreviewView = YES;
        
        
        [self.playerView playerControlView:nil playerModel:playModel];
        //        _playerView.forcePortrait = YES;
        /// 默认全屏播放
        //        _playerView.fullScreenPlay = YES;
        
    }
    return _playerView;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [userIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-18);
        make.width.equalTo(40);
        make.height.equalTo(40);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconView.mas_right).offset(6);
        make.top.equalTo(userIconView.mas_top).offset(7);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_left);
        make.top.equalTo(nameLabel.mas_bottom).offset(5);
    }];
    
//    [careBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(weakSelf.mas_right).offset(-17);
//        make.centerY.equalTo(userIconView.mas_centerY);
//        make.width.equalTo(67);
//        make.height.equalTo(30);
//    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(userIconView.mas_top).offset(-10);
        make.height.equalTo(0.5);
    }];
    
    [seeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(lineView.mas_top).offset(-20);
        make.left.equalTo(userIconView.mas_left);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userIconView.mas_left);
        make.bottom.equalTo(seeNumberLabel.mas_top).offset(-10);
        make.width.equalTo(weakSelf.frameWidth-30);
        make.height.greaterThanOrEqualTo(@10);
    }];
    [likeNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(seeNumberLabel.mas_centerY);
    }];
    [likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(likeNumberLabel.mas_left).offset(-5);
        make.centerY.equalTo(seeNumberLabel.mas_centerY);
    }];
    
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(likeNumberLabel.mas_right);
        make.centerY.equalTo(timeLabel.mas_centerY);
        make.width.height.equalTo(40);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
