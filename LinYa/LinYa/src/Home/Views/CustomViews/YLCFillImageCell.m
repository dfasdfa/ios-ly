//
//  YLCFillImageCell.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFillImageCell.h"

@implementation YLCFillImageCell
{
    UIImageView *imageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"feed_back_top"]];
        
        [self.contentView addSubview:imageView];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
@end
