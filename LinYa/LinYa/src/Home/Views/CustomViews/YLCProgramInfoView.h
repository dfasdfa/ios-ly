//
//  YLCProgramInfoView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCEasyProgramListModel.h"
@interface YLCProgramInfoView : UIView
@property (nonatomic ,strong)YLCEasyProgramListModel *infoModel;
@end
