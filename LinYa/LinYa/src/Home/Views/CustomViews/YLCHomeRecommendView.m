//
//  YLCHomeRecommendView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeRecommendView.h"

@implementation YLCHomeRecommendView
{
    UILabel *titleLabel;
    UILabel *subLabel;
    UIImageView *subImageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        subImageView = [[UIImageView alloc] init];
        
        [self addSubview:subImageView];
        
        titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(73, 75, 76)];
        
        [self addSubview:titleLabel];
        
        subLabel = [YLCFactory createLabelWithFont:13 color:RGB(153, 153, 153)];
        
        [self addSubview:subLabel];
    }
    return self;
}
- (void)setImagePath:(NSString *)imagePath{
    subImageView.image = [UIImage imageNamed:imagePath];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)setSubText:(NSString *)subText{
    subLabel.text = [subText notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [subImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-5);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(50);
        make.height.equalTo(50);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(subImageView.mas_centerY).offset(-2);
        
        make.left.equalTo(weakSelf.mas_left).offset(30);
        
    }];
    
    [subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(titleLabel.mas_left);
        
        make.top.equalTo(titleLabel.mas_bottom).offset(3);
        
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
