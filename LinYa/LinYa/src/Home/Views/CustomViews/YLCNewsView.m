//
//  YLCNewsView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewsView.h"
#import "YLCNewsCell.h"
#define NEW_CELL  @"YLCNewsCell"
@interface YLCNewsView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCNewsView
{
    UICollectionView *newTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    newTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    newTable.backgroundColor = [UIColor whiteColor];
    
    [newTable registerClass:[YLCNewsCell class] forCellWithReuseIdentifier:NEW_CELL];
    
    newTable.delegate = self;
    
    newTable.dataSource = self;
    
    [self addSubview:newTable];
}
- (void)setDataList:(NSArray *)dataList{
    _dataList = dataList;
    
    [newTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCNewsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NEW_CELL forIndexPath:indexPath];
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 45+88+10);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [newTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
