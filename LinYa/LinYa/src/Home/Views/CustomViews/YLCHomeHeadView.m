//
//  YLCHomeHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeHeadView.h"
#import "KIImagePager.h"
#import "YLCTipChooseView.h"
#import "YLCBannerModel.h"
#import "GYRollingNoticeView.h"
#define GY_CELL @"GYRollingNoticeCell"
#import "YLCToplineModel.h"
#import "YLCWebViewViewController.h"
@interface YLCHomeHeadView()<KIImagePagerDelegate,KIImagePagerDataSource,GYRollingNoticeViewDelegate,GYRollingNoticeViewDataSource>
@end
@implementation YLCHomeHeadView
{
    KIImagePager *bannerView;
    YLCTipChooseView *chooseView;
    UIView *bottomView;
    UIImageView *leftView;
    UILabel *titleLabel;
    UIButton *signBtn;
    UIButton *messageBtn;
    GYRollingNoticeView *noticeView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.chooseSignal = [RACSubject subject];
        bannerView = [[KIImagePager alloc] initWithFrame:CGRectMake(0, 0, self.frameWidth, 190)];
        
        bannerView.imageCounterDisabled = YES;
        
        bannerView.slideshowTimeInterval = 3.0;
        
        bannerView.backgroundColor = RGB(235, 235, 235);
        
        bannerView.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
        
        bannerView.pageControl.pageIndicatorTintColor = [UIColor blackColor];
        
        bannerView.slideshowShouldCallScrollToDelegate = YES;
        
        bannerView.delegate = self;
        
        bannerView.dataSource = self;
        
        [self addSubview:bannerView];
        
        bottomView = [[UIView alloc] initWithFrame:CGRectMake(7, bannerView.frameMaxY-20, self.frameWidth-14, 144)];
        
        bottomView.backgroundColor = [UIColor whiteColor];
        
        bottomView.layer.cornerRadius = 5;
        
        bottomView.layer.masksToBounds = YES;
        
        [self addSubview:bottomView];
        
        chooseView = [[YLCTipChooseView alloc] initWithFrame:CGRectMake(10, 10, bottomView.frameWidth-20, 77)];
        @weakify(self);
        [chooseView.chooseSignal subscribeNext:^(id x) {
            @strongify(self);
            [self.chooseSignal sendNext:x];
        }];
        
        [bottomView addSubview:chooseView];
        
        UIImage *leftImage = [UIImage imageNamed:@"home_top_line"];
        
        leftView = [[UIImageView alloc] initWithImage:leftImage];
        
        [bottomView addSubview:leftView];
        
        noticeView = [[GYRollingNoticeView alloc]initWithFrame:CGRectMake(0, 0, self.frameWidth, self.frameHeight)];
        [noticeView registerClass:[GYNoticeViewCell class] forCellReuseIdentifier:@"home_notice_cell"];
        noticeView.dataSource = self;
        noticeView.delegate = self;
        
        [bottomView addSubview:noticeView];
        
        [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(bottomView.mas_bottom).offset(-10);
            make.left.equalTo(chooseView.mas_left).offset(10);
            make.width.equalTo(leftImage.size.width);
            make.height.equalTo(leftImage.size.height);
        }];
        
        [noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(leftView.mas_right).offset(12);
            make.centerY.equalTo(leftView.mas_centerY);
            make.width.equalTo(256);
            make.height.equalTo(30);
        }];
        
        
    }
    return self;
}
- (void)setTopLineArr:(NSArray *)topLineArr{
    _topLineArr = topLineArr;
    
    [noticeView beginScroll];
}
- (NSInteger)numberOfRowsForRollingNoticeView:(GYRollingNoticeView *)rollingView{
    return _topLineArr.count;
}
- (__kindof GYNoticeViewCell *)rollingNoticeView:(GYRollingNoticeView *)rollingView cellAtIndex:(NSUInteger)index
{
    GYNoticeViewCell *cell = [rollingView dequeueReusableCellWithIdentifier:@"home_notice_cell"];
    
    if (index<_topLineArr.count) {
        YLCToplineModel *model = _topLineArr[index];
        
        cell.textLabel.text = model.title;
    }
    
    return cell;
}
- (void)setBannerDataArr:(NSArray *)bannerDataArr{
    _bannerDataArr = bannerDataArr;
    [bannerView reloadData];
}
- (void)setTipArr:(NSArray *)tipArr{
    chooseView.tipArray = tipArr;
}
- (void)didClickRollingNoticeView:(GYRollingNoticeView *)rollingView forIndex:(NSUInteger)index{
    UINavigationController *nav = (UINavigationController *)[YLCFactory currentNav];
    
    YLCToplineModel *model = _topLineArr[index];
    [YLCWebViewViewController showWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.url]] navTitle:model.title isModel:YES from:nav.viewControllers[nav.viewControllers.count-1]];
}
- (void)imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    YLCBannerModel *model = _bannerDataArr[index];
    
    if (JK_IS_STR_NIL(model.link)) {
        return;
    }
    [YLCWebViewViewController showWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.link]] navTitle:@"" isModel:YES from:_target];
    
}
- (NSArray *) arrayWithImages{
    if (!JK_IS_ARRAY_NIL(_bannerDataArr)) {
        NSMutableArray *bannerImageURLs = [NSMutableArray arrayWithCapacity:1];
        for (YLCBannerModel *model in _bannerDataArr) {
            
            if ([YLCFactory isStringOk:model.imgsrc]) {
                [bannerImageURLs addObject:[NSString stringWithFormat:@"%@",model.imgsrc]];
            }
        }
        return bannerImageURLs;
    }else{
        return nil;
    }
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image{
    return UIViewContentModeScaleToFill;
}
- (UIImage *)placeHolderImageForImagePager{
    
    return [UIImage imageNamed:@"image_placeholder"];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
