//
//  YLCPayFootView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayFootView.h"

@implementation YLCPayFootView
{
    
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _buyBtn = [YLCFactory createCommondBtnWithTitle:@"确认购买" backgroundColor:YLC_THEME_COLOR titleColor:[UIColor whiteColor]];
    

        
        [self addSubview:_buyBtn];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    [_buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(14, 22, 14, 22));
    }];
}
@end
