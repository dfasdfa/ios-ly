//
//  YLCNewTypeHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewTypeHeadView.h"

@implementation YLCNewTypeHeadView
{
    UIImageView *leftView;
    UILabel *titleLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    leftView = [[UIImageView alloc] init];
    
    leftView.backgroundColor = YLC_THEME_COLOR;
    
    leftView.layer.cornerRadius = 2;
    
    leftView.layer.masksToBounds = YES;
    
    [self addSubview:leftView];
    
    titleLabel = [YLCFactory createLabelWithFont:15 color:RGB(183, 183, 183)];
    
    [self addSubview:titleLabel];
}
- (void)setTitle:(NSString *)title{
    if ([YLCFactory isStringOk:title]) {
        titleLabel.text = title;
    }else{
        titleLabel.text = @"";
    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(4);
        make.height.equalTo(12);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftView.mas_right).offset(5);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
}
@end
