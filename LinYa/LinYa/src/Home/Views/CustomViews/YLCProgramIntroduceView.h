//
//  YLCProgramIntroduceView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCProgramIntroduceView : UIView
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *score;
@property (nonatomic ,copy)NSString *safe;
@property (nonatomic ,copy)NSString *subsidy;
@property (nonatomic ,copy)NSString *feature;
@property (nonatomic ,copy)NSString *personNum;
@end
