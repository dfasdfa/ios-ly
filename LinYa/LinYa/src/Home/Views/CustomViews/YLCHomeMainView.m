//
//  YLCHomeMainView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeMainView.h"
#import "YLCHomeHeadView.h"
#import "YLCHomeRecommendCell.h"
#import "YLCHomeMessageCell.h"
#import "YLCNewTrainCell.h"
#import "YLCHotJobCell.h"
#import "YLCInformationNewsCell.h"
#import "YLCTrainbBaseViewController.h"
#define HOME_HEAD      @"YLCHomeHeadView"
#define RECOMMEND_CELL @"YLCHomeRecommendCell"
#define MESSAGE_CELL   @"YLCHomeMessageCell"
#define NEW_TRAIN      @"YLCNewTrainCell"
#define HOT_JOB        @"YLCHotJobCell"
#define INFOR_NEW      @"YLCInformationNewsCell"
@interface YLCHomeMainView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCHomeMainView
{
    UICollectionView *homeTable;
    YLCHomeHeadView *headView;
    YLCHomeMessageCell *messageCell;
    BOOL isReload;
}
- (void)viewDisappear{
    [messageCell invalidateTimer];
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.chooseSignal = [RACSubject subject];
        self.reloadSubject = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    homeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    homeTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [homeTable registerClass:[YLCHomeMessageCell class] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    [homeTable registerClass:[YLCHomeRecommendCell class] forCellWithReuseIdentifier:RECOMMEND_CELL];
    
    [homeTable registerClass:[YLCNewTrainCell class] forCellWithReuseIdentifier:NEW_TRAIN];
    
    [homeTable registerClass:[YLCHotJobCell class] forCellWithReuseIdentifier:HOT_JOB];
    
    [homeTable registerClass:[YLCInformationNewsCell class] forCellWithReuseIdentifier:INFOR_NEW];
    
    [homeTable registerClass:[YLCHomeHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HOME_HEAD];
    
    homeTable.alwaysBounceVertical = YES;
    
    homeTable.delegate = self;
    
    homeTable.dataSource = self;
    
    [homeTable setupOnlyHeadRefrshWithTarget:self headAction:@selector(headReload)];
    
    [self addSubview:homeTable];
}
- (void)beganRefresh{
    [homeTable.mj_header beginRefreshing];
}
- (void)headReload{
    [self.reloadSubject sendNext:@""];
}
- (void)endReload{
    [homeTable.mj_header endRefreshing];
}
- (void)setBannerList:(NSArray *)bannerList{
    _bannerList = bannerList;
    
    headView.bannerDataArr = bannerList;
}
- (void)setNewsList:(NSArray *)newsList{
    _newsList = newsList;
    
    [homeTable reloadData];
}
- (void)setTipArr:(NSArray *)tipArr{
    _tipArr = tipArr;
    
    [homeTable reloadData];
}
- (void)setHospitalList:(NSArray *)hospitalList{
    
    _hospitalList = hospitalList;
    
    [homeTable reloadData];
}
- (void)setVideoList:(NSArray *)videoList{
    _videoList = videoList;
    
    [homeTable reloadData];
}
- (void)setTrendArr:(NSArray *)trendArr{
    _trendArr = trendArr;
    
    [homeTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 5;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCHomeRecommendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:RECOMMEND_CELL forIndexPath:indexPath];
        
        cell.subViews = _recommendList;
        
        cell.topList = _top_lineArr;
        
        cell.newsList = _newsList;
        
        return cell;
    }
    if (indexPath.row==1) {
        messageCell = [collectionView dequeueReusableCellWithReuseIdentifier:MESSAGE_CELL forIndexPath:indexPath];
        
        messageCell.trendArr = _trendArr;
        
        return messageCell;
    }
    if (indexPath.row==2) {
        YLCNewTrainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NEW_TRAIN forIndexPath:indexPath];
        
        cell.videoList = _videoList;
        
        return cell;
    }
    if (indexPath.row==3) {
        YLCHotJobCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:HOT_JOB forIndexPath:indexPath];
        
        cell.companyList = _hospitalList;
        
        return cell;
    }
    if (indexPath.row==4) {
        YLCInformationNewsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INFOR_NEW forIndexPath:indexPath];
        cell.top_lineArr = _top_lineArr;
        cell.newsList = _newsList;
        
        return cell;
    }
    return [[UICollectionViewCell alloc] init];
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:HOME_HEAD forIndexPath:indexPath];
        
        headView.tipArr = _tipArr;
        headView.topLineArr = _newsList;
        headView.target = _target;
        if (!isReload) {
            isReload = YES;
            @weakify(self);
            [headView.chooseSignal subscribeNext:^(id x) {
                @strongify(self);
                [self.chooseSignal sendNext:x];
            }];
        }
        return headView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.frameWidth-20, 185);
    }
    if (indexPath.row==1) {
        if (JK_IS_ARRAY_NIL(_trendArr)) {
            return CGSizeZero;
        }
        return CGSizeMake(self.frameWidth-20, 81);
    }
    if (indexPath.row==2) {
        return CGSizeMake(self.frameWidth-20, 22+10+ 175+26+17);
    }
    if (indexPath.row==3) {
        return CGSizeMake(self.frameWidth-20, 145);
    }
    if (indexPath.row==4) {
        return CGSizeMake(self.frameWidth-20, 15+26+5+10+104*_newsList.count);
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth, 144+218-22-30);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==2) {
        [self gotoVideoTrain];
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 0, 10, 0);
}
- (void)gotoVideoTrain{
    YLCTrainbBaseViewController *con = [[YLCTrainbBaseViewController alloc] init];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [homeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
