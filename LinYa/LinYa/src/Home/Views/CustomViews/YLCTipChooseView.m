//
//  YLCTipChooseView.m
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTipChooseView.h"
#import "YLCTipCellCell.h"
#import "YLCTitleAndIconModel.h"
#define TIP_CELL @"YLCTipCellCell"
@interface YLCTipChooseView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCTipChooseView
{
    UICollectionView *tipTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
        self.chooseSignal = [RACSubject subject];
        [self createCustomViews];
    }
    return self;
}
- (void)createCustomViews{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumInteritemSpacing = 0.5;
    
    tipTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    tipTable.backgroundColor = [UIColor whiteColor];
    
    [tipTable registerClass:[YLCTipCellCell class] forCellWithReuseIdentifier:TIP_CELL];
    
    tipTable.delegate = self;
    
    tipTable.dataSource = self;
    
    [self addSubview:tipTable];
}
- (void)setBackgroundColor:(UIColor *)backgroundColor{
    tipTable.backgroundColor = backgroundColor;
}
- (void)setNeedCorner:(BOOL)needCorner{
    if (needCorner) {
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
    }else{
        self.layer.cornerRadius = 0;
        self.layer.masksToBounds = NO;
    }
}
- (void)setTipArray:(NSArray *)tipArray{
    _tipArray = tipArray;
    [tipTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _tipArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCTipCellCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TIP_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_tipArray.count) {
        YLCTitleAndIconModel *model = _tipArray[indexPath.row];
        
        cell.title = model.title;
        
        cell.iconName = model.iconName;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(self.frameWidth/_tipArray.count-_tipArray.count*0.5+0.5, self.frameHeight);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [_chooseSignal sendNext:indexPath];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [tipTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
