//
//  YLCHomeNewsTopView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeNewsTopView : UIView
@property (nonatomic ,copy)NSString *iconName;
@property (nonatomic ,strong)RACSubject *moreSignal;
@end
