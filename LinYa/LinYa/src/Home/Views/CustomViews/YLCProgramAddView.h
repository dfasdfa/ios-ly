//
//  YLCProgramAddView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCProgramAddView : UIView
@property (nonatomic ,strong)NSArray *cellArr;
@property (nonatomic ,strong)RACSubject *commodityInsertSignal;
@property (nonatomic ,strong)RACSubject *commodityDeleteSignal;
@property (nonatomic ,strong)RACSubject *programFieldChangeSignal;
@property (nonatomic ,strong)RACSubject *changeReloadCellSignal;
@property (nonatomic ,strong)RACSubject *photoSignal;
@property (nonatomic ,assign)BOOL isEdit;
@property (nonatomic ,strong)NSArray *insertPhotoList;
@end
