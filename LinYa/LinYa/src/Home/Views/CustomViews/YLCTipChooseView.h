//
//  YLCTipChooseView.h
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTipChooseView : UIView
@property (nonatomic ,strong)NSArray *tipArray;
@property (nonatomic ,assign)BOOL needCorner;
@property (nonatomic ,strong)UIColor *backgroundColor;
@property (nonatomic ,strong)RACSubject *chooseSignal;
@end
