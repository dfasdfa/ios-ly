//
//  YLCProgramAddView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramAddView.h"
#import "YLCCommodityPriceSetCell.h"
#import "YLCSelectCell.h"
#import "YLCFillInPersonMessageCell.h"
#import "YLCProgramAddCellModel.h"
#import "YLCFillMessageModel.h"
#import "YLCInputInfoCell.h"
#import "YLCProgramStarCell.h"
#import "YLCProgramPhotoInsertCell.h"
#import "YLCProgramSelectModel.h"
#import "YLCPickModel.h"
#define SET_CELL @"YLCCommodityPriceSetCell"
#define SELECT_CELL @"YLCSelectCell"
#define FILL_CELL   @"YLCFillInPersonMessageCell"
#define INPUT_CELL  @"YLCInputInfoCell"
#define STAR_CELL   @"YLCProgramStarCell"
#define PHOTO_CELL  @"YLCProgramPhotoInsertCell"
@interface YLCProgramAddView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCCommodityPriceSetCellDelegate,YLCProgramStarCellDelegate,YLCInputInfoCellDelegate,YLCFillInPersonMessageCellDelegate,YLCProgramPhotoInsertCellDelegate>
@end
@implementation YLCProgramAddView
{
    UICollectionView *insertTable;
    CGFloat photoHeight;
    BOOL isReload;
    BOOL isPhotoLoad;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.commodityDeleteSignal = [RACSubject subject];
        self.commodityInsertSignal = [RACSubject subject];
        self.programFieldChangeSignal = [RACSubject subject];
        self.changeReloadCellSignal = [RACSubject subject];
        self.photoSignal = [RACSubject subject];
        
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    insertTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    insertTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [insertTable registerClass:[YLCCommodityPriceSetCell class] forCellWithReuseIdentifier:SET_CELL];
    
    [insertTable registerClass:[YLCSelectCell class] forCellWithReuseIdentifier:SELECT_CELL];
    
    [insertTable registerClass:[YLCFillInPersonMessageCell class] forCellWithReuseIdentifier:FILL_CELL];
    
    [insertTable registerClass:[YLCInputInfoCell class] forCellWithReuseIdentifier:INPUT_CELL];
    
    [insertTable registerClass:[YLCProgramStarCell class]  forCellWithReuseIdentifier:STAR_CELL];
    
    [insertTable registerClass:[YLCProgramPhotoInsertCell class] forCellWithReuseIdentifier:PHOTO_CELL];
    
    insertTable.delegate = self;
    
    insertTable.dataSource = self;
    
    [self addSubview:insertTable];
}
- (void)setCellArr:(NSArray *)cellArr{
    _cellArr = cellArr;
    
    [insertTable reloadData];
}
//- (void)setInsertPhotoList:(NSArray *)insertPhotoList{
//    
//}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _cellArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCProgramAddCellModel *model = _cellArr[indexPath.row];
    if (model.cellType==YLCTextViewCellType) {
        YLCInputInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INPUT_CELL forIndexPath:indexPath];
        
        cell.delegate = self;
        
        cell.text = model.cellModel;
        
        cell.placeholder = @"输入您的项目介绍内容...";
        
        return cell;
    }
    
    if (model.cellType==YLCStarCellType) {
        YLCProgramStarCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:STAR_CELL forIndexPath:indexPath];
        
        cell.delegate = self;
        
        cell.starNum = [model.cellModel floatValue];
        
        return cell;
    }
    if (model.cellType==YLCFiedCellType) {
        
        YLCFillInPersonMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FILL_CELL forIndexPath:indexPath];
        
        YLCFillMessageModel *fillModel = model.cellModel;
        
        cell.title = fillModel.title;
        
        cell.index = indexPath.row;
        
        cell.text = fillModel.message;
        
        cell.placeholder = fillModel.placeholder;
        
        cell.delegate = self;
        
        cell.isRight = fillModel.isRight;
        
        cell.isNumber = fillModel.isNumber;
        
        return cell;
    }
    
    if (model.cellType==YLCSelectCellType) {
        
        YLCSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SELECT_CELL forIndexPath:indexPath];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        cell.titleColor = selectModel.titleColor;
        
        cell.title = selectModel.title;
        
        cell.subColor = selectModel.subColor;
        
        cell.subString = JK_IS_STR_NIL(selectModel.subString)?selectModel.placeholder:selectModel.subString;
        
        return cell;
    }
    if (model.cellType==YLCCommondityCellType) {
        
        YLCCommodityPriceSetCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SET_CELL forIndexPath:indexPath];
        
        cell.delegate = self;
        
        cell.cellType = YLCCommondityCellType;
        
        cell.commodityList = model.cellModel;
        if (!isReload) {
            isReload = YES;
            @weakify(self);
            [cell.insertSignal subscribeNext:^(id x) {
                @strongify(self);
                [self.commodityInsertSignal sendNext:x];
            }];
            
            [cell.deleteSignal subscribeNext:^(id x) {
                @strongify(self);
                [self.commodityDeleteSignal sendNext:x];
            }];
        }
        
        return cell;
        
    }
    if (model.cellType==YLCPhotoAddCellType) {
        YLCProgramPhotoInsertCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];
        
        cell.delegate = self;
        if (!JK_IS_ARRAY_NIL(_insertPhotoList)) {
            if (!isPhotoLoad) {
                isPhotoLoad = YES;
                cell.insertList = _insertPhotoList;
            }
        }

    
        return cell;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramAddCellModel *model = _cellArr[indexPath.row];
    if (model.cellType==YLCCommondityCellType) {
        NSArray *commodityList = model.cellModel;
        return CGSizeMake(self.frameWidth, 1+37+37*commodityList.count);
    }
    if (model.cellType==YLCFiedCellType) {
        return CGSizeMake(self.frameWidth, 50);
    }
    if (model.cellType==YLCSelectCellType) {
        return CGSizeMake(self.frameWidth, 50);
    }
    if (model.cellType==YLCTextViewCellType) {
        return CGSizeMake(self.frameWidth, 200);
    }
    if (model.cellType==YLCStarCellType) {
        return CGSizeMake(self.frameWidth, 50);
    }
    if (model.cellType==YLCPhotoAddCellType) {
        return CGSizeMake(self.frameWidth, photoHeight+20);
    }
    return CGSizeZero;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCProgramAddCellModel *model = _cellArr[indexPath.row];
    if (model.cellType==YLCSelectCellType) {
        YLCProgramSelectModel *selectModel = model.cellModel;
        
        RACSubject *selectSubject = [RACSubject subject];
        
        YLCPickModel *theModel = [[YLCPickModel alloc] init];
        
        theModel.selectIndex = 0;
        
        theModel.rowArr = selectModel.selectList;
        
        [YLCHUDShow showPickerListWithList:@[theModel] signal:selectSubject];
        
        [selectSubject subscribeNext:^(id x) {
            YLCPickModel *pickModel = x[0];
            
            selectModel.subString = pickModel.rowArr[pickModel.selectIndex];
            
            model.cellModel = selectModel;
            
            NSMutableArray *container = _cellArr.mutableCopy;
            
            [container setObject:model atIndexedSubscript:indexPath.row];
            
            [self.changeReloadCellSignal sendNext:container.copy];
        }];
    }
}
#pragma mark - CellDelegate
- (void)didChangeModelWithModel:(YLCProgramTraitFillModel *)model index:(NSInteger)index cellType:(NSInteger)cellType{
    NSMutableArray *cellContainer = _cellArr.mutableCopy;
    for (YLCProgramAddCellModel *addModel in _cellArr) {
        if (addModel.cellType==cellType) {
            NSArray *commodityList = addModel.cellModel;
            
            NSMutableArray *container = [commodityList mutableCopy];
            if (!index) {
                
                
                [container setObject:model atIndexedSubscript:0];
            }else{
                [container setObject:model atIndexedSubscript:index];
            }

            
            addModel.cellModel = container.copy;
            
            [cellContainer setObject:addModel atIndexedSubscript:1];
            
            [self.programFieldChangeSignal sendNext:cellContainer.copy];
        }
        
    }
}
- (void)didChangeStarNumber:(NSString *)starNumber{
    YLCProgramAddCellModel *model = _cellArr[6];
    model.cellModel = starNumber;
    NSMutableArray *container = _cellArr.mutableCopy;
    [container setObject:model atIndexedSubscript:6];
    [self.changeReloadCellSignal sendNext:container.copy];
}
- (void)didInfoChangedWithText:(NSString *)text{
    
    YLCProgramAddCellModel *model = _cellArr[7];
    
    model.cellModel = text;

    NSMutableArray *container = _cellArr.mutableCopy;
    
    [container setObject:model atIndexedSubscript:7];
    
    [self.programFieldChangeSignal sendNext:container.copy];
}
- (void)didChangeFillInFieldWithText:(NSString *)text index:(NSInteger)index{
    YLCProgramAddCellModel *model = _cellArr[index];
    
    YLCFillMessageModel *fillModel = model.cellModel;
    
    fillModel.message = text;
    
    model.cellModel = fillModel;
    
    NSMutableArray *container = _cellArr.mutableCopy;
    
    [container setObject:model atIndexedSubscript:index];
    
    [self.programFieldChangeSignal sendNext:container.copy];
}

- (void)changeFrameWithHeight:(CGFloat)height{
    photoHeight = height;
    [insertTable reloadData];
}
- (void)insertPhotoWithPhotos:(NSArray *)photos{
    [self.photoSignal sendNext:photos];
}
//cellArr = @[nameModel,priceModel,traitModel,starModel,inputmodel,photoAddModel];
- (void)layoutSubviews{
    [super layoutSubviews];
    [insertTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
