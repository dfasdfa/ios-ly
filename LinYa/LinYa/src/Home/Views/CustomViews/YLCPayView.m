//
//  YLCPayView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayView.h"
#import "YLCPayMessageCell.h"
#import "YLCPayWayCell.h"
#import "YLCPayFootView.h"
#import "WXApi.h"
#define MESSAGE_CELL   @"YLCPayMessageCell"
#define WAY_CELL       @"YLCPayWayCell"
#define PAY_FOOT       @"YLCPayFootView"
@interface YLCPayView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WXApiDelegate,YLCPayMessageCellDelegate>
@end
@implementation YLCPayView
{
    UICollectionView *payTable;
    NSString *payWay;
    NSString *price;
    NSString *cardText;
    BOOL isReload;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        payWay = @"2";
        price = @"298";
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    payTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    payTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [payTable registerClass:[YLCPayMessageCell class] forCellWithReuseIdentifier:MESSAGE_CELL];
    
    [payTable registerClass:[YLCPayWayCell class] forCellWithReuseIdentifier:WAY_CELL];
    
    [payTable registerClass:[YLCPayFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:PAY_FOOT];
    
    payTable.delegate = self;
    
    payTable.dataSource = self;
    
    [self addSubview:payTable];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCPayMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MESSAGE_CELL forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }
    if (indexPath.row==1) {
        YLCPayWayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:WAY_CELL forIndexPath:indexPath];
        
        cell.payWay = payWay;
        
        if (!isReload) {
            isReload = YES;
            [cell.waySignal subscribeNext:^(id x) {
                NSIndexPath *indexPath = x;
                [self fetchPayWayWithIndex:indexPath.row];
            }];
            
            [cell.cardSignal subscribeNext:^(id x) {
                cardText = x;
            }];
        }
        return cell;
    }
    return nil;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionFooter) {
        YLCPayFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:PAY_FOOT forIndexPath:indexPath];
        
        [footView.buyBtn addTarget:self action:@selector(gotoPay) forControlEvents:UIControlEventTouchUpInside];
        
        return footView;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.frameWidth, 172);
    }
    if (indexPath.row==1) {
        if ([payWay isEqualToString:@"3"]) {
            return CGSizeMake(self.frameWidth, 331);
        }
        return CGSizeMake(self.frameWidth, 281);
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth, 28+40);
}
- (void)gotoPay{
//    if ([payWay isEqualToString:@"2"]) {
//        [self wxPay];
//        return;
//    }
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    NSString *cardNumber = @"";
    if ([payWay isEqualToString:@"3"]) {
        if (JK_IS_STR_NIL(cardText)) {
            [SVProgressHUD showSuccessWithStatus:@"请输入充值卡密码"];
            return;
        }
        cardNumber = cardText;
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":model.user_id,@"token":model.token,@"type":payWay,@"price":@"298",@"month":@"12",@"card":cardNumber} method:@"POST" urlPath:@"app/sygt/purchase" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        if ([payWay isEqualToString:@"2"]) {
            [[YLCWxPayManager shareManager] wxPayWithParam:responseObject];
            
            [[YLCWxPayManager shareManager].paySignal subscribeNext:^(id x) {
                
            }];
        }else{
            [YLCHUDShow showBuySuccessHUDWithTip:@"恭喜您，购买成功"];
            
            [(UINavigationController *)[YLCFactory currentNav] popViewControllerAnimated:YES];
        }

    }];
}
- (void)onReq:(BaseReq *)req{
    
    if ([req isKindOfClass:[PayReq class]]) {
        
    }
}
- (void)fetchPayWayWithIndex:(NSInteger)index{
    if (index==0) {
        payWay = @"2";
    }
    if (index==1) {
        payWay = @"1";
    }
    if (index==2) {
        payWay = @"3";
    }
    
    [payTable reloadData];
}
- (void)didSelectPriceAtIndex:(NSInteger)index payPrice:(NSString *)payPrice{
    price = payPrice;
    [payTable reloadData];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [payTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
