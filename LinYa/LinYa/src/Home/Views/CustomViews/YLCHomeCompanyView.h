//
//  YLCHomeCompanyView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeCompanyView : UIView
@property (nonatomic ,strong)NSArray *companyList;
@property (nonatomic ,strong)RACSubject *selectSignal;
@end
