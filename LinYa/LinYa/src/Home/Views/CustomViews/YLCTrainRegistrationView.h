//
//  YLCTrainRegistrationView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTrainRegistrationView : UIView
@property (nonatomic ,strong)NSArray *activityList;
@property (nonatomic ,assign)NSInteger type;//0是培训报名列表 1是活动列表
@end
