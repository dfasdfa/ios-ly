//
//  YLCTouTiaoListView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTouTiaoListView.h"
#import "YLCTouTiaoTypeCell.h"
#import "YLCTouTiaoBigCell.h"
#import "YLCWebViewViewController.h"
#define TYPE_CELL @"YLCTouTiaoTypeCell"
#define BIG_CELL  @"YLCTouTiaoBigCell"
@interface YLCTouTiaoListView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCTouTiaoListView
{
    UICollectionView *newTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    newTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    newTable.backgroundColor = [UIColor whiteColor];
    
    [newTable registerClass:[YLCTouTiaoBigCell class] forCellWithReuseIdentifier:BIG_CELL];
    
    [newTable registerClass:[YLCTouTiaoTypeCell class] forCellWithReuseIdentifier:TYPE_CELL];
    
    newTable.delegate = self;
    
    newTable.dataSource = self;
    
    newTable.alwaysBounceVertical = YES;
    
    [self addSubview:newTable];
}
- (void)setTopLineTopArr:(NSArray *)topLineTopArr{
    _topLineTopArr = topLineTopArr;
    
    [newTable reloadData];
}
- (void)setTopLineBottomArr:(NSArray *)topLineBottomArr{
    _topLineBottomArr = topLineBottomArr;
    
    [newTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _topLineTopArr.count+_topLineBottomArr.count;;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<_topLineTopArr.count) {
        YLCTouTiaoTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TYPE_CELL forIndexPath:indexPath];
        
        if (indexPath.row<_topLineTopArr.count) {
            cell.model = _topLineTopArr[indexPath.row];
        }
        
        return cell;
    }
    YLCTouTiaoBigCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BIG_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_topLineTopArr.count+_topLineBottomArr.count) {
        cell.model = _topLineBottomArr[indexPath.row-_topLineTopArr.count];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<_topLineTopArr.count) {
        return CGSizeMake(self.frameWidth, 120);
    }
    return CGSizeMake(self.frameWidth, 264);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<_topLineTopArr.count) {
        YLCToplineModel *model = _topLineTopArr[indexPath.row];
        
        [YLCNetWorking loadNetServiceWithParam:@{@"topLineId":model.id} method:@"POST" urlPath:@"app/topLine/detail" delegate:self response:^(id responseObject, NSError *error) {
            if (error) {
                
                return ;
            }
            
            YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
            NSString *urlString =[NSString stringWithFormat:@"%@/fromapp_user_id/%@",model.url,userModel.user_id];
            [YLCWebViewViewController showShareWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] navTitle:@"头条详情" shareUrl:responseObject[@"share_link"] shareContent:responseObject[@"share_title"] shareImageUrl:@"" isModel:YES from:_target];
        }];

    }
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [newTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
