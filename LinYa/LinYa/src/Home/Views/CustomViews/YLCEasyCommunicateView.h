//
//  YLCEasyCommunicateView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCEasyCommunicateView : UIView
@property (nonatomic ,strong)RACSubject *touchSignal;
@property (nonatomic ,strong)NSArray *listArr;
@property (nonatomic ,assign)CGSize imageSize;
@property (nonatomic ,assign)CGSize cellSize;
@property (nonatomic ,assign)BOOL alwaysBounceVertical;
@property (nonatomic ,assign)BOOL isMine;
@end
