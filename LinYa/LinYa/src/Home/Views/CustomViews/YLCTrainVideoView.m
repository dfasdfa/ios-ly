//
//  YLCTrainVideoView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainVideoView.h"
#import "YLCVideoTrainCell.h"
#import "YLCVideoTrainModel.h"
#import "YLCVideoLiftInfoViewController.h"
#define VIDEO_CELL   @"YLCVideoTrainCell"
@interface YLCTrainVideoView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ZFPlayerDelegate>

@property (nonatomic, strong) ZFPlayerControlView *controlView;
@end
@implementation YLCTrainVideoView
{
    UICollectionView *videoTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (UIScrollView *)customScrollView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    videoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    videoTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [videoTable registerClass:[YLCVideoTrainCell class] forCellWithReuseIdentifier:VIDEO_CELL];
    
    videoTable.delegate = self;
    
    videoTable.dataSource = self;

    return videoTable;
}

- (void)setVideoList:(NSArray *)videoList{
    _videoList = videoList;
    self.dataArr = videoList;
    [videoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _videoList.count+1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCVideoTrainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_CELL forIndexPath:indexPath];
    if (indexPath.row<_videoList.count) {
        cell.model = _videoList[indexPath.row];
        WS(weakSelf)
        __block UICollectionView *weakCollectionView = collectionView;
        __block YLCVideoTrainModel *trainModel = _videoList[indexPath.row];
        __block NSIndexPath *weakIndexPath = indexPath;
        __block YLCVideoTrainCell *weakCell     = cell;
        cell.playBlock = ^(UIButton *btn){
            
            
            NSMutableDictionary *dic = @{@"高清":trainModel.site}.mutableCopy;
            //        for (ZFVideoResolution * resolution in model.playInfo) {
            //            [dic setValue:resolution.url forKey:resolution.name];
            //        }
            // 取出字典中的第一视频URL
            NSURL *videoURL = [NSURL URLWithString:trainModel.site];
            ZFPlayerModel *playerModel = [[ZFPlayerModel alloc] init];
            playerModel.title            = trainModel.title;
            playerModel.videoURL         = videoURL;
            //        playerModel.placeholderImageURLString = model.coverForFeed;
            playerModel.scrollView       = weakCollectionView;
            playerModel.indexPath        = weakIndexPath;
            // 赋值分辨率字典
            playerModel.resolutionDic    = dic;
            // player的父视图tag
            playerModel.fatherViewTag    = weakCell.posterView.tag;
            
            // 设置播放控制层和model
            [weakSelf.playerView playerControlView:nil playerModel:playerModel];
            // 下载功能
            weakSelf.playerView.hasDownload = NO;
            // 自动播放
            [weakSelf.playerView autoPlayTheVideo];
        };

    }
   
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 262);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCVideoTrainModel *model = _videoList[indexPath.row];
    
    YLCVideoLiftInfoViewController *con = [[YLCVideoLiftInfoViewController alloc] init];
    
    con.video_id = model.id;
    
    con.type = 1;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [videoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (ZFPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [ZFPlayerView sharedPlayerView];
        _playerView.delegate = self;
        
        _playerView.cellPlayerOnCenter = NO;
        
        _playerView.forcePortrait = NO;
        
        _playerView.stopPlayWhileCellNotVisable = YES;
        
        ZFPlayerShared.isLockScreen = YES;
        ZFPlayerShared.isStatusBarHidden = NO;
    }
    return _playerView;
}

- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [[ZFPlayerControlView alloc] init];
    }
    return _controlView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
