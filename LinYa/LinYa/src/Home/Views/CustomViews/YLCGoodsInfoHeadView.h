//
//  YLCGoodsInfoHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCGoodsModel.h"
@interface YLCGoodsInfoHeadView : UICollectionReusableView
@property (nonatomic ,strong)YLCGoodsModel *model;
@end
