//
//  YLCInviteView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCInviteView : UIView
@property (nonatomic ,assign)BOOL isMine;
@property (nonatomic ,strong)NSArray *salaryList;
@property (nonatomic ,strong)NSArray *inviteList;
@property (nonatomic ,strong)NSArray *screenList;
@property (nonatomic ,strong)RACSubject *reloadSignal;
@property (nonatomic ,strong)RACSubject *conditionReloadSignal;
- (void)beganRefresh;
- (void)resetNoMore;
- (void)loadedAllData;
@end
