//
//  YLCVideoLiftView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoLiftView.h"
#import "YLCVideoLiftCell.h"
#import "YLCVideoLifeModel.h"
#import "YLCVideoLiftInfoViewController.h"
#define VIDEO_LIFT  @"YLCVideoLiftCell"
@interface YLCVideoLiftView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCVideoLiftView
{
    UICollectionView *videoTable;
}
- (UIScrollView *)customScrollView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 10;
    
    videoTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    videoTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [videoTable registerClass:[YLCVideoLiftCell class] forCellWithReuseIdentifier:VIDEO_LIFT];
    
    videoTable.delegate = self;
    
    videoTable.dataSource = self;
    
    return videoTable;

}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    
}
- (void)setVideoList:(NSArray *)videoList{
    _videoList = videoList;
    self.dataArr = videoList;
    [videoTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _videoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCVideoLiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:VIDEO_LIFT forIndexPath:indexPath];
    
    if (indexPath.row<_videoList.count) {
        cell.model = _videoList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.frameWidth-30)/2, 170);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCVideoLifeModel *model = _videoList[indexPath.row];
    
    YLCVideoLiftInfoViewController *con = [[YLCVideoLiftInfoViewController alloc] init];
    
    con.video_id = model.id;
    
    con.type = 0;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [videoTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
