//
//  YLCCommodityAddHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCCommodityAddHeadView.h"

@implementation YLCCommodityAddHeadView
{
    UILabel *titleLabel;
    UIButton *addBtn;
    UIImageView *lineView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.insertSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:13.65 color:RGB(119, 123, 124)];
    
    titleLabel.text = @"商品价格选项";
    
    [self addSubview:titleLabel];
    
    addBtn = [YLCFactory createBtnWithImage:@"easy_buy_add"];
    
    [addBtn setEnlargeEdgeWithTop:10 right:10 bottom:10 left:10];
    
    [addBtn addTarget:self action:@selector(addProgram) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:addBtn];
    
    lineView = [[UIImageView alloc] init];
    
    lineView.backgroundColor = RGB(230, 230, 230);
    
    [self addSubview:lineView];
}
- (void)addProgram{
    [self.insertSignal sendNext:nil];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(11);
        make.centerY.equalTo(weakSelf.mas_centerY);
        
    }];
    
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-19);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.and.height.equalTo(25);
    }];
    
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(5);
        make.right.equalTo(weakSelf.mas_right).offset(-5);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.equalTo(0.5);
    }];
    
    
}
@end
