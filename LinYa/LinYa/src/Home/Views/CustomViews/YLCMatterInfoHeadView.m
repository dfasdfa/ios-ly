//
//  YLCMatterInfoHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/3.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMatterInfoHeadView.h"

@implementation YLCMatterInfoHeadView
{
    UILabel *titleLabel;
    UIImageView *iconView;
    UILabel *userNameLabel;
    UILabel *timeLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    titleLabel.numberOfLines = 0;
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:titleLabel];
    
    iconView = [[UIImageView alloc] init];
    
    iconView.layer.cornerRadius = 25;
    
    iconView.layer.masksToBounds = YES;
    
    [self addSubview:iconView];
    
    userNameLabel = [YLCFactory createLabelWithFont:14 color:RGB(51, 51, 51)];
    
    [self addSubview:userNameLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:13 color:RGB(183, 183, 183)];
    
    [self addSubview:timeLabel];
}
- (void)setModel:(YLCMatterModel *)model{
    titleLabel.text = model.title;
    [iconView sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]];
    timeLabel.text = [NSString stringWithFormat:@"%@发布",model.add_time];
    userNameLabel.text = model.user_name;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.width.equalTo(weakSelf.frameWidth-20);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(10);
        make.height.width.equalTo(50);
    }];
    
    [userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconView.mas_top).offset(2);
        make.left.equalTo(iconView.mas_right).offset(5);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(userNameLabel.mas_left);
        make.bottom.equalTo(iconView.mas_bottom).offset(-3);
    }];
}
@end
