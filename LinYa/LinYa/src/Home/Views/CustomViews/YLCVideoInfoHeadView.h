//
//  YLCHomeVideoView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCVideoLiftInfoModel.h"
#import "ZFPlayer.h"
@interface YLCVideoInfoHeadView : UICollectionReusableView <ZFPlayerDelegate>
@property (nonatomic ,strong)YLCVideoLiftInfoModel *infoModel;
@property (nonatomic ,strong)ZFPlayerView *playerView;
- (void)videoStop;
@end
