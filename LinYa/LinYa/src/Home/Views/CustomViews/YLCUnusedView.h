//
//  YLCUnusedView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLCRefrshTableView.h"
@interface YLCUnusedView : YLCRefrshTableView
@property (nonatomic ,strong)RACSubject *classReloadSignal;
@property (nonatomic ,strong)NSArray *goodsList;
@property (nonatomic ,assign)BOOL showScreen;
@property (nonatomic ,strong)NSArray *typeList;
@end
