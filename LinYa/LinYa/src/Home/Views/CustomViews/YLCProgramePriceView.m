//
//  YLCProgramePriceView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramePriceView.h"

@implementation YLCProgramePriceView
{
    UIImageView *pointView;
    UILabel *titleLabel;
    UILabel *priceLabel;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    pointView = [[UIImageView alloc] init];
    
    pointView.backgroundColor = YLC_THEME_COLOR;
    
    [self addSubview:pointView];
    
    titleLabel = [YLCFactory createLabelWithFont:13 color:RGB(105, 112, 116)];
    
    [self addSubview:titleLabel];
    
    priceLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:priceLabel];
}
- (void)setPrice:(NSString *)price{
    NSString *string = [NSString stringWithFormat:@"￥%@",price];
    
    NSAttributedString *attr = [YLCFactory getAttributedStringWithString:string FirstCount:1 secondCount:price.length firstFont:11 secondFont:19 firstColor:YLC_THEME_COLOR secondColor:YLC_THEME_COLOR];
    
    priceLabel.attributedText = attr;
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = [title notNullString];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.height.equalTo(5);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(pointView.mas_right).offset(10);
        make.centerY.equalTo(pointView.mas_centerY);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
