//
//  YLCCommodityAddHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCCommodityAddHeadView : UICollectionReusableView
@property (nonatomic ,strong)RACSubject *insertSignal;
@end
