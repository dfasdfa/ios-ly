//
//  YLCHomeTipView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeTipView.h"

@implementation YLCHomeTipView
{
    UIImageView *iconView;
    UIButton *morBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        iconView = [[UIImageView alloc] init];
        
        [self addSubview:iconView];
        
        morBtn = [YLCFactory creatCornerAndBorderWithCornerRadius:12 title:@"更多" backgroundColor:[UIColor whiteColor] textColor:RGB(118, 124, 126) borderColor:RGB(209, 216, 218) borderWidth:0.5];
        
        [self addSubview:morBtn];
    }
    return self;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(16);
        make.top.equalTo(weakSelf.mas_top).offset(17);
        make.width.equalTo(69);
        make.height.equalTo(69);
    }];
    
    [morBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-27);
        make.centerY.equalTo(iconView.mas_centerY);
        make.width.equalTo(50);
        make.height.equalTo(26);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
