//
//  YLCHomeRecommendView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeRecommendView : UIView
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *imagePath;
@property (nonatomic ,copy)NSString *subText;
@end
