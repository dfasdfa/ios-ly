//
//  YLCUnusedView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCUnusedView.h"
#import "YLCUnusedCell.h"
#import "YLCDynamicInfoViewController.h"
#import "YLCGoodsModel.h"
#import "YLCScreenView.h"
#import "YLCScreenModel.h"
#define UNUSED_CELL @"YLCUnusedCell"
@interface YLCUnusedView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCScreenViewDelegate>
@end
@implementation YLCUnusedView
{
    UICollectionView *itemTable;
    YLCScreenView *screenView;
}
- (UIScrollView *)customScrollView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 10;
    
    itemTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    itemTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [itemTable registerClass:[YLCUnusedCell class] forCellWithReuseIdentifier:UNUSED_CELL];
    
    itemTable.delegate = self;
    
    itemTable.dataSource = self;
    
    return itemTable;

}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.classReloadSignal = [RACSubject subject];
        
        screenView = [[YLCScreenView alloc] initWithFrame:CGRectMake(0, 10, self.frameWidth, 42)];
        
        YLCScreenModel *sexModel = [[YLCScreenModel alloc] init];
        
        sexModel.title = @"区域";
        
        sexModel.showType = 1;
        
        sexModel.value = @"";
        
        sexModel.screenList = @[];
        
        YLCScreenModel *workModel = [[YLCScreenModel alloc] init];
        
        workModel.title = @"类别";
        
        workModel.screenList = @[];
        
        screenView.screenArr = @[sexModel,workModel];
        
        workModel.value = @"";
        
        screenView.delegate = self;
        
        [self addSubview:screenView];
        WS(weakSelf)
        
        [screenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left);
            make.right.equalTo(weakSelf.mas_right);
            make.top.equalTo(weakSelf.mas_top);
            make.height.equalTo(52);
        }];
        
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(62, 0, 0, 0));
        }];
    }
    return self;
}
- (void)setShowScreen:(BOOL)showScreen{
    screenView.hidden = !showScreen;
    [itemTable mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(showScreen?62:0, 0, 0, 0));
    }];
}
- (void)setTypeList:(NSArray *)typeList{
    _typeList = typeList;
    YLCScreenModel *workModel = screenView.screenArr[1];
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *screenContainer = [screenView.screenArr mutableCopy];
    for (NSDictionary *dict in typeList) {
        [container addObject:dict[@"classify_name"]];
    }
    workModel.screenList = container.copy;
    [screenContainer setObject:workModel atIndexedSubscript:1];
    screenView.screenArr = screenContainer.copy;
}
- (void)setGoodsList:(NSArray *)goodsList{
    _goodsList = goodsList;
    self.dataArr = goodsList;
    if (JK_IS_ARRAY_NIL(goodsList)) {
        UILabel *normalLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frameWidth/2-50, self.frameHeight/2-100, 100, 30)];
        
        normalLabel.text = @"无数据";
        
        normalLabel.textAlignment = NSTextAlignmentCenter;
        
        [itemTable addSubview:normalLabel];
    }
    [itemTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _goodsList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCUnusedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:UNUSED_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_goodsList.count) {
        cell.model = _goodsList[indexPath.row];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.frameWidth-30)/2, 200);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCGoodsModel *model = _goodsList[indexPath.row];
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];
    
    con.caseId = model.goodsId;
    
    con.type = 4;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)screenMessageDidChangeWithDataList:(NSArray *)dataList{
    NSMutableArray *container = dataList.mutableCopy;

    YLCScreenModel *typeScreenModel = dataList[1];
    if (typeScreenModel.isChanged) {
        NSDictionary *dict = _typeList[typeScreenModel.selectIndex];
        typeScreenModel.value = dict[@"id"];
        
        [container setObject:typeScreenModel atIndexedSubscript:1];
    }

    [self.classReloadSignal sendNext:container.copy];
}
- (void)layoutSubviews{
    [super layoutSubviews];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
