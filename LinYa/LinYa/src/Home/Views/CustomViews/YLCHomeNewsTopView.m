//
//  YLCHomeNewsTopView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeNewsTopView.h"

@implementation YLCHomeNewsTopView
{
    UIImageView *iconView;
    UIButton *moreBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.moreSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    iconView = [[UIImageView alloc] init];
    
    [self addSubview:iconView];
    
    moreBtn = [YLCFactory createLabelBtnWithTitle:@"更多" titleColor:RGB(118, 124, 126)];
    
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    moreBtn.layer.borderColor = RGB(209, 216, 218).CGColor;
    
    moreBtn.layer.borderWidth = 0.5;
    
    moreBtn.layer.cornerRadius = 12;
    
    moreBtn.layer.masksToBounds = YES;
    
    [moreBtn addTarget:self action:@selector(touchMore) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:moreBtn];
}
- (void)touchMore{
    [self.moreSignal sendNext:nil];
}
- (void)setIconName:(NSString *)iconName{
    iconView.image = [UIImage imageNamed:iconName];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    WS(weakSelf)
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(69);
        make.height.equalTo(14);
    }];
    
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.top.equalTo(iconView.mas_top);
        make.width.equalTo(50);
        make.height.equalTo(26);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
