//
//  YLCTrainRegistrationView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegistrationView.h"
#import "YLCTrainRegistrationCell.h"
#import "YLCTrainRegisInfoViewController.h"
#import "YLCActivityModel.h"
#define TRAIN_CELL    @"YLCTrainRegistrationCell"
@interface YLCTrainRegistrationView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCTrainRegistrationView
{
    UICollectionView *trationTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)setActivityList:(NSArray *)activityList{
    _activityList = activityList;
    
    [trationTable reloadData];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    trationTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    trationTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [trationTable registerClass:[YLCTrainRegistrationCell class] forCellWithReuseIdentifier:TRAIN_CELL];
    
    trationTable.delegate = self;
    
    trationTable.dataSource = self;
    
    [self addSubview:trationTable];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _activityList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCTrainRegistrationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TRAIN_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_activityList.count) {
        cell.model = _activityList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 130);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YLCTrainRegisInfoViewController *con = [[YLCTrainRegisInfoViewController alloc] init];
    
    con.model = _activityList[indexPath.row];
    
    con.type = _type;
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [trationTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
