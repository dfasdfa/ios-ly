//
//  YLCNewTypeView.m
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewTypeView.h"
#import "YLCNewTypeCell.h"
#import "YLCNewTypeHeadView.h"
#import "YLCNewTypeFootView.h"
#define TYPE_CELL  @"YLCNewTypeCell"
#define TYPE_HEAD  @"YLCNewTypeHeadView"
#define TYPE_FOOT  @"YLCNewTypeFootView"
@interface YLCNewTypeView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end
@implementation YLCNewTypeView
{
    UICollectionView *typeTable;
    UIView *view;
    NSArray *typeList;
    UIButton *cancelBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDataSource];
        [self createCustomView];
    }
    return self;
}
- (void)initDataSource{
    self.reloadSignal = [RACSubject subject];
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/material/dataCategory" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        typeList = responseObject;
        
        [typeTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    flow.minimumInteritemSpacing = 30;
    
    typeTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    typeTable.backgroundColor = [UIColor clearColor];
    
    [typeTable registerNib:[UINib nibWithNibName:@"YLCNewTypeCell" bundle:nil] forCellWithReuseIdentifier:TYPE_CELL];
    
    [typeTable registerClass:[YLCNewTypeHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:TYPE_HEAD];
    
    [typeTable registerClass:[YLCNewTypeFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:TYPE_FOOT];
    
    typeTable.delegate = self;
    
    typeTable.dataSource = self;
    
    [self addSubview:typeTable];
    WS(weakSelf)
    [typeTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(60);
        make.width.equalTo(weakSelf.frameWidth-30);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-80);
    }];
    
    cancelBtn = [YLCFactory createBtnWithImage:@"type_cancel"];
    
    [cancelBtn addTarget:self action:@selector(gotoCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:cancelBtn];
    
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-40);
        make.width.height.equalTo(30);
    }];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return typeList.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSDictionary *dict = typeList[section];
    NSArray *list = dict[@"list"];
    return list.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCNewTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TYPE_CELL forIndexPath:indexPath];
    if (indexPath.section<typeList.count) {
        NSDictionary *dict = typeList[indexPath.section];
        NSArray *list = dict[@"list"];
        if (indexPath.row<list.count) {
            NSDictionary *typeDict = list[indexPath.row];
            cell.iconName = typeDict[@"category_icon"];
            cell.title = typeDict[@"category_name"];
        }
    }
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionHeader) {
        YLCNewTypeHeadView *headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:TYPE_HEAD forIndexPath:indexPath];
        
        if (indexPath.section<typeList.count) {
            NSDictionary *dict = typeList[indexPath.section];
            headView.title = dict[@"category_name"];
        }
        
        return headView;
    }
    if (kind==UICollectionElementKindSectionFooter) {
        YLCNewTypeFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:TYPE_FOOT forIndexPath:indexPath];
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth-30, 20);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.frameWidth-30, 40);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(62, 60);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self dissMiss];
    if (indexPath.section<typeList.count) {
        NSDictionary *dict = typeList[indexPath.section];
        NSArray *list = dict[@"list"];
        if (indexPath.row<list.count) {
            NSDictionary *typeDict = list[indexPath.row];
            [self.reloadSignal sendNext:typeDict[@"id"]];
        }
    }
}
- (void)gotoCancel{
    [self dissMiss];
}
- (void)show{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    view = [[UIView alloc] initWithFrame:keyWindow.bounds];
    view.alpha = 0.8;
    view.backgroundColor = [UIColor blackColor];
    [keyWindow addSubview:view];
    [keyWindow addSubview:self];
    [keyWindow bringSubviewToFront:self];
}
- (void)dissMiss{
    [self removeFromSuperview];
    [view removeFromSuperview];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
