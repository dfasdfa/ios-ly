
//
//  YLCGoodsInfoHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCGoodsInfoHeadView.h"
#import "YLCPhotoCell.h"
#import "YLCPhotoModel.h"
#define PHOTO_CELL  @"YLCPhotoCell"
@interface YLCGoodsInfoHeadView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCGoodsInfoHeadView
{
    UIView *backgroundView;
    UIImageView *lineView;
    YLCUserHeadView *customIconView;
    UILabel *nameLabel;
    UILabel *timeLabel;
    UILabel *oldPriceLabel;
    UILabel *newPriceLabel;
    UILabel *contentLabel;
    UIImageView *oldLineView;
    UICollectionView *imageTable;
    NSArray *imgList;
    UIButton *shareBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    backgroundView = [[UIView alloc] init];
    
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:backgroundView];
    
    customIconView = [[YLCUserHeadView alloc] initWithFrame:CGRectZero];
//    [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"image_placeholder"]];
//
    customIconView.contentMode = UIViewContentModeScaleToFill;
    
    [self addSubview:customIconView];
    
    nameLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:nameLabel];
    
    timeLabel = [YLCFactory createLabelWithFont:14];
    
    [self addSubview:timeLabel];
    
    oldPriceLabel = [YLCFactory createLabelWithFont:14 color:RGB(183, 183, 183)];
    
    [self addSubview:oldPriceLabel];
    
    newPriceLabel  =[YLCFactory createLabelWithFont:14 color:YLC_THEME_COLOR];
    
    [self addSubview:newPriceLabel];
    
    oldLineView = [[UIImageView alloc] init];
    
    oldLineView.backgroundColor = RGB(183, 183, 183);
    
    [self addSubview:oldLineView];
    
    contentLabel = [YLCFactory createLabelWithFont:14];
    
    contentLabel.numberOfLines = 0;
    
    [self addSubview:contentLabel];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 5;
    
    flow.minimumInteritemSpacing = 5;
    
    imageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    [imageTable registerClass:[YLCPhotoCell class] forCellWithReuseIdentifier:PHOTO_CELL];
    
    imageTable.delegate = self;
    
    imageTable.dataSource = self;
    
    imageTable.bounces = NO;
    
    imageTable.alwaysBounceVertical = NO;
    
    imageTable.alwaysBounceHorizontal = NO;
    
    imageTable.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:imageTable];
    
    shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
    
    [shareBtn addTarget:self action:@selector(shareLink) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:shareBtn];
}
- (void)shareLink{
    [YLCHUDShow showShareHUDWithContent:_model.share_title shareLink:_model.share_link];
}
- (void)setModel:(YLCGoodsModel *)model{
    
    _model = model;
    
//    [customIconView sd_setImageWithURL:[NSURL URLWithString:model.user_img] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    [customIconView configHeadViewWithImageUrl:model.user_img placeholder:@"image_placeholder" isGoCenter:YES userId:model.user_id];
    nameLabel.text = [YLCFactory isStringOk:model.nick_name]?model.nick_name:@"";
    
    timeLabel.text = model.add_time;
    
    oldPriceLabel.text = [NSString stringWithFormat:@"￥%@",[YLCFactory isStringOk:model.old_price]?model.old_price:@""];
    
    newPriceLabel.text = [NSString stringWithFormat:@"￥%@",[YLCFactory isStringOk:model.price]?model.price:@""];
    
    contentLabel.text = model.goodsDescription;
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    for (NSString *url in model.img_src) {
        
        YLCPhotoModel *photoModel = [[YLCPhotoModel alloc] init];
        
        photoModel.imagePath = url;
        
        photoModel.isWeb = YES;
        
        [container addObject:photoModel];
    }
    imgList = container.copy;
    
    [imageTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return imgList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];
    
    if (indexPath.row<imgList.count) {
        cell.model = imgList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(self.frameWidth-20, 200);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 10, 0));
    }];
    
    [customIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backgroundView.mas_left).offset(18);
        make.top.equalTo(backgroundView.mas_top).offset(11);
        make.width.equalTo(45);
        make.height.equalTo(45);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customIconView.mas_right).offset(10);
        make.top.equalTo(customIconView.mas_top).offset(2);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customIconView.mas_right).offset(10);
        make.top.equalTo(nameLabel.mas_bottom).offset(5);
//        make.bottom.equalTo(customIconView.mas_bottom);
    }];
    
    [oldPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(backgroundView.mas_right).offset(-18);
        make.top.equalTo(nameLabel.mas_top);
    }];
    
    [oldLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(oldPriceLabel.mas_left).offset(-3);
        make.right.equalTo(oldPriceLabel.mas_right).offset(3);
        make.centerY.equalTo(oldPriceLabel.mas_centerY);
        make.height.equalTo(1);
    }];
    
    [newPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(oldPriceLabel.mas_left);
        make.top.equalTo(oldPriceLabel.mas_bottom).offset(5);
    }];
    
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customIconView.mas_left);
        make.right.equalTo(backgroundView.mas_right).offset(-18);
        make.top.equalTo(customIconView.mas_bottom).offset(10);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [imageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentLabel.mas_left);
        make.right.equalTo(contentLabel.mas_right);
        make.top.equalTo(contentLabel.mas_bottom).offset(10);
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-10);
    }];
    
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageTable.mas_right);
        make.bottom.equalTo(backgroundView.mas_bottom).offset(-10);
        make.width.height.equalTo(40);
    }];
}
@end
