//
//  YLCFillProgramMessageView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFillProgramMessageView.h"
#import "YLCFillInPersonMessageCell.h"
#import "YLCFillMessageModel.h"
#define FILL_CELL @"YLCFillInPersonMessageCell"
@interface YLCFillProgramMessageView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCFillProgramMessageView
{
    UICollectionView *messageTable;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [messageTable registerClass:[YLCFillInPersonMessageCell class] forCellWithReuseIdentifier:FILL_CELL];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    [self addSubview:messageTable];
}
- (void)setFillArr:(NSArray *)fillArr{
    _fillArr = fillArr;
    
    [messageTable reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _fillArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCFillInPersonMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FILL_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_fillArr.count) {
        YLCFillMessageModel *model = _fillArr[indexPath.row];
        
        cell.title = model.title;
        
        cell.text = model.message;
        
        cell.placeholder = model.placeholder;
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth, 50);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [messageTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
