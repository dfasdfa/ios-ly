//
//  YLCProgramIntroduceView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramIntroduceView.h"
#import "YLCStarView.h"
@implementation YLCProgramIntroduceView
{
    UILabel *titleLabel;
    YLCStarView *starView;
    UILabel *starNumberLabel;
    UIImageView *safeIconView;
    UILabel *safeLabel;
    UIImageView *priceIconView;
    UILabel *priceLabel;
    UILabel *specialtyLabel;
    UIImageView *markView;
    UILabel *chooseNumberLabel;
    UIImageView *iconImageView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    titleLabel = [YLCFactory createLabelWithFont:17.5 color:RGB(68, 68, 68)];
    
    [self addSubview:titleLabel];
    
    starView = [[YLCStarView alloc] initWithFrame:CGRectMake(0, 0, 115, 20)];
    
    starView.grayName = @"star_gray";
    
    starView.yelloName = @"star_yellow";
    
    [self addSubview:starView];
    
    starNumberLabel = [YLCFactory createLabelWithFont:11.7 color:RGB(81, 86, 88)];
    
    [self addSubview:starNumberLabel];
    
    safeIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"safe_icon"]];
    
    [self addSubview:safeIconView];
    
    safeLabel = [YLCFactory createLabelWithFont:11.7 color:RGB(130, 139, 141)];
    
    [self addSubview:safeLabel];
    
    priceIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tie_icon"]];
    
    [self addSubview:priceIconView];
    
    priceLabel = [YLCFactory createLabelWithFont:11.7 color:RGB(130, 139, 141)];
    
    [self addSubview:priceLabel];
    
    specialtyLabel = [YLCFactory createLabelWithFont:13.7 color:RGB(104, 106, 106)];
    
    specialtyLabel.numberOfLines = 0;
    
    [self addSubview:specialtyLabel];
    
    markView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"easy_info_icon"]];
    
    [self addSubview:markView];
    
    chooseNumberLabel = [YLCFactory createLabelWithFont:12 color:[UIColor whiteColor]];
    
    chooseNumberLabel.text = @"0人选择";
    
    [markView addSubview:chooseNumberLabel];
    
    iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"easy_small_icon"]];
    
    [self addSubview:iconImageView];
}
- (void)setPersonNum:(NSString *)personNum{
    if ([personNum integerValue]>999) {
        personNum = @"999+";
    }
    chooseNumberLabel.text = [NSString stringWithFormat:@"%@人选择",personNum];
}
- (void)setScore:(NSString *)score{
    starView.score = [score integerValue];
    
    starNumberLabel.text = [NSString stringWithFormat:@"%@星",score];
}
- (void)setTitle:(NSString *)title{
    titleLabel.text = title;
}
- (void)setSafe:(NSString *)safe{
    safeLabel.text = [NSString stringWithFormat:@"%@年",safe];
}
- (void)setSubsidy:(NSString *)subsidy{
    if (JK_IS_STR_NIL(subsidy)) {
        priceLabel.text = @"0元";
    }else{
        priceLabel.text = [NSString stringWithFormat:@"%@元",subsidy];
    }
    
}
- (void)setFeature:(NSString *)feature{
    specialtyLabel.text = feature;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(20);
        make.top.equalTo(weakSelf.mas_top).offset(22);
    }];
    
    [starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel.mas_left);
        make.top.equalTo(titleLabel.mas_bottom).offset(15);
        make.width.equalTo(115);
        make.height.equalTo(20);
    }];
    
    [starNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(starView.mas_right).offset(5);
        make.centerY.equalTo(starView.mas_centerY);
    }];
    
    [safeIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(starNumberLabel.mas_right).offset(10);
        make.centerY.equalTo(starNumberLabel.mas_centerY);
        make.width.equalTo(13);
        make.height.equalTo(14);
    }];
    
    [safeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(safeIconView.mas_right).offset(5);
        make.centerY.equalTo(safeIconView.mas_centerY);
    }];
    
    [priceIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(safeLabel.mas_right).offset(10);
        make.centerY.equalTo(safeLabel.mas_centerY);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(priceIconView.mas_right).offset(5);
        make.centerY.equalTo(priceIconView.mas_centerY);
    }];
    
    [specialtyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(starView.mas_bottom).offset(10);
        make.left.equalTo(starView.mas_left);
        make.width.equalTo(weakSelf.frameWidth-40);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [markView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-22);
        make.top.equalTo(weakSelf.mas_top).offset(-2);
        make.width.equalTo(75);
        make.height.equalTo(30);
    }];
    
    [chooseNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(markView.mas_centerX);
        make.centerY.equalTo(markView.mas_centerY);
    }];
    
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-26);
        make.top.equalTo(markView.mas_bottom).offset(21);
        make.width.height.equalTo(50);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
