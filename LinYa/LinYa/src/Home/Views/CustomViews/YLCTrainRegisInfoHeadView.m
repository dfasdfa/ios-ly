//
//  YLCTrainRegisInfoHeadView.m
//  LinYa
//
//  Created by 初程程 on 2018/3/19.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegisInfoHeadView.h"

@implementation YLCTrainRegisInfoHeadView
{
    UIImageView *bannerView;
    UIImageView *areaView;
    UILabel *areaLabel;
    UIImageView *timeView;
    UILabel *timeLabel;
    UIImageView *customView;
    UILabel *customLabel;
    UIImageView *line1;
    UIImageView *line2;
    UIButton *shareBtn;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    self.backgroundColor = [UIColor whiteColor];
    
    bannerView = [[UIImageView alloc] init];
    
    [self addSubview:bannerView];

    areaView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"train_area"]];
    
    areaView.contentMode = UIViewContentModeCenter;
    
    [self addSubview:areaView];
    
    areaLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self addSubview:areaLabel];
    
    line1 = [[UIImageView alloc] init];
    
    line1.backgroundColor = RGB(213, 213, 213);
    
    [self addSubview:line1];
    
    timeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"train_time"]];
    
    timeView.contentMode = UIViewContentModeCenter;
    
    [self addSubview:timeView];
    
    timeLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self addSubview:timeLabel];
    
    line2 = [[UIImageView alloc] init];
    
    line2.backgroundColor = RGB(213, 213, 213);
    
    [self addSubview:line2];
    
    customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"train_custom"]];
    
    customView.contentMode = UIViewContentModeCenter;
    
    [self addSubview:customView];
    
    customLabel = [YLCFactory createLabelWithFont:16 color:RGB(51, 51, 51)];
    
    [self addSubview:customLabel];
    
    shareBtn = [YLCFactory createBtnWithImage:@"web_share"];
    
    [shareBtn addTarget:self action:@selector(shareLink) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:shareBtn];
}
- (void)shareLink{
    [YLCHUDShow showShareHUDWithContent:_model.share_title shareLink:_model.share_link];
}
- (void)setModel:(YLCActivityModel *)model{
    
    _model = model;
    
    [bannerView sd_setImageWithURL:[NSURL URLWithString:model.poster] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
    
    areaLabel.text = model.place;
    
    timeLabel.text = model.begin_time;
    if (JK_IS_STR_NIL(model.contact_name)) {
        model.contact_name = @"";
    }
    customLabel.text = [NSString stringWithFormat:@"%@  %@",model.contact_name,model.contact1];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    WS(weakSelf)
    [bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(weakSelf.mas_top);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(170);
    }];
    
    [areaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(bannerView.mas_bottom).offset(20);
        make.width.equalTo(14);
        make.height.equalTo(18);
    }];
    
    [areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaView.mas_right).offset(10);
        make.centerY.equalTo(areaView.mas_centerY);
    }];
    
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(areaView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(0.5);
    }];
    
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaView.mas_left);
        make.top.equalTo(line1.mas_bottom).offset(10);
        make.width.equalTo(14);
        make.height.equalTo(18);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(timeView.mas_right).offset(10);
        make.centerY.equalTo(timeView.mas_centerY);
    }];
    
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.top.equalTo(timeView.mas_bottom).offset(10);
        make.width.equalTo(weakSelf.frameWidth);
        make.height.equalTo(0.5);
    }];
    
    [customView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(areaView.mas_left);
        make.top.equalTo(line2.mas_bottom).offset(10);
        make.width.equalTo(14);
        make.height.equalTo(18);
    }];
    
    [customLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(customView.mas_right).offset(10);
        make.centerY.equalTo(customView.mas_centerY);
    }];
    
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(customLabel.mas_centerY);
        make.width.height.equalTo(30);
    }];
}
@end
