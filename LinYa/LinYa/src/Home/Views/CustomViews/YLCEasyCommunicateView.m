//
//  YLCEasyCommunicateView.m
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCEasyCommunicateView.h"
#import "YLCCollectionListCell.h"
#import "YLCTitleAndIconModel.h"
#define COLLECTION_CELL @"YLCCollectionListCell"
@interface YLCEasyCommunicateView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@end
@implementation YLCEasyCommunicateView
{
    UICollectionView *collectionView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.touchSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
//    flow.minimumLineSpacing = 0.5;
//    
//    flow.minimumInteritemSpacing = 0.5;
    
    collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    collectionView.backgroundColor = [UIColor clearColor];
    
    [collectionView registerClass:[YLCCollectionListCell class] forCellWithReuseIdentifier:COLLECTION_CELL];
    
    collectionView.delegate = self;
    
    collectionView.dataSource = self;
    
    collectionView.alwaysBounceVertical = YES;
    
    [self addSubview:collectionView];
}
- (void)setListArr:(NSArray *)listArr{
    
    _listArr = listArr;
    
    [collectionView reloadData];
}
- (void)setAlwaysBounceVertical:(BOOL)alwaysBounceVertical{
    collectionView.alwaysBounceVertical = alwaysBounceVertical;
}
- (void)setCellSize:(CGSize)cellSize{
    _cellSize = cellSize;
    
    [collectionView reloadData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _listArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCCollectionListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:COLLECTION_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_listArr.count) {
        YLCTitleAndIconModel *model = _listArr[indexPath.row];
        
        cell.iconName = model.iconName;
        
        cell.title = model.title;
        
        cell.iconPath = model.iconPath;
    }
    
    cell.imageSize = _imageSize;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return _cellSize;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (![YLCLoginManager checkLoginStateWithTarget:self]) {
        return;
    }
    [_touchSignal sendNext:indexPath];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
