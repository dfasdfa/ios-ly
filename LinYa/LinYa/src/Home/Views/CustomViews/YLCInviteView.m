//
//  YLCInviteView.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteView.h"
#import "YLCInviteCell.h"
#import "YLCInviteInfoViewController.h"
#import "YLCScreenView.h"
#import "YLCScreenModel.h"
#define INVITE_CELL @"YLCInviteCell"
@interface YLCInviteView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCScreenViewDelegate>
@end
@implementation YLCInviteView
{
    UICollectionView *inviteTable;
    YLCScreenView *screenView;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.reloadSignal = [RACSubject subject];
        self.conditionReloadSignal = [RACSubject subject];
        [self createCustomView];
    }
    return self;
}
- (void)setScreenList:(NSArray *)screenList{
    _screenList = screenList;
    
    screenView.screenArr = screenList;
}
- (void)createCustomView{
    
    screenView = [[YLCScreenView alloc] initWithFrame:CGRectMake(0, 10, self.frameWidth, 42)];
        
    screenView.delegate = self;
    
    [self addSubview:screenView];
    
    WS(weakSelf)
    
    [screenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.height.equalTo(52);
    }];

    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    inviteTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    inviteTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [inviteTable registerClass:[YLCInviteCell class] forCellWithReuseIdentifier:INVITE_CELL];
    
    inviteTable.delegate = self;
    
    inviteTable.dataSource = self;
    
    [inviteTable setupNormalRefreshHeadAndFootWithTarget:self headAction:@selector(headReload) footAction:@selector(didTriggerLoadNextPageData)];
    
    [self addSubview:inviteTable];
    
    [inviteTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(72, 0, 0, 0));
    }];
}
- (void)beganRefresh{
    [inviteTable.mj_header beginRefreshing];
}
- (void)setIsMine:(BOOL)isMine{
    if (isMine) {
        screenView.hidden = YES;
        [inviteTable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsZero);
        }];
    }
}
- (void)setSalaryList:(NSArray *)salaryList{
    _salaryList = salaryList;
}
- (void)setInviteList:(NSArray *)inviteList{
    _inviteList = inviteList;
    
    [inviteTable reloadData];
    
    [inviteTable.mj_header endRefreshing];
}
- (void)resetNoMore{
    [inviteTable.mj_footer endRefreshing];
}
- (void)loadedAllData{
    [inviteTable.mj_footer endRefreshingWithNoMoreData];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _inviteList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCInviteCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INVITE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_inviteList.count) {
        cell.model = _inviteList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.frameWidth-20, 145);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    YLCInviteInfoViewController *con = [[YLCInviteInfoViewController alloc] init];
    
    con.model = _inviteList[indexPath.row];
    
    [(UINavigationController *)[YLCFactory currentNav] pushViewController:con animated:YES];
}
- (void)headReload{
    [self.reloadSignal sendNext:@"0"];
}
- (void)didTriggerLoadNextPageData{
    NSInteger page = _inviteList.count/[PAGE_LIMIT integerValue];
    NSString *pageString = [NSString stringWithFormat:@"%ld",page];
    [self.reloadSignal sendNext:pageString];
}
- (void)screenMessageDidChangeWithDataList:(NSArray *)dataList{
    YLCScreenModel *timeModel = dataList[2];
    if (timeModel.isChanged) {
        timeModel.value = [NSString stringWithFormat:@"%ld",timeModel.selectIndex];
    }
    NSMutableArray *container = dataList.mutableCopy;
    
    [container setObject:timeModel atIndexedSubscript:2];
    
    YLCScreenModel *salaryModel = dataList[1];
    
    if (salaryModel.isChanged) {
        NSDictionary *dict = _salaryList[salaryModel.selectIndex];
        
        salaryModel.value = dict[@"id"];
    }
    
    [container setObject:salaryModel atIndexedSubscript:1];
    
    [self.conditionReloadSignal sendNext:container.copy];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
