//
//  YLCTouTiaoListView.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCTouTiaoListView : UIView
@property (nonatomic ,strong)NSArray *topLineTopArr;
@property (nonatomic ,strong)NSArray *topLineBottomArr;
@property (nonatomic ,strong)id target;
@end
