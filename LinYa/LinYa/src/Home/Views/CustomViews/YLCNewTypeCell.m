//
//  YLCNewTypeCell.m
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCNewTypeCell.h"
@interface YLCNewTypeCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
@implementation YLCNewTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _iconView.layer.cornerRadius = 15;
    
    _iconView.layer.masksToBounds = YES;
}
- (void)setTitle:(NSString *)title{
    if ([YLCFactory isStringOk:title]) {
        _titleLabel.text = title;
    }else{
        _titleLabel.text = @"";
    }
}
- (void)setIconName:(NSString *)iconName{
    [_iconView sd_setImageWithURL:[NSURL URLWithString:iconName] placeholderImage:[UIImage imageNamed:@"image_placeholder"]];
}
@end
