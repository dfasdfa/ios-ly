//
//  YLCHomeHeadView.h
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YLCHomeHeadView : UICollectionReusableView
@property (nonatomic ,strong)NSArray *bannerDataArr;
@property (nonatomic ,strong)NSArray *tipArr;
@property (nonatomic ,strong)NSArray *topLineArr;
@property (nonatomic ,strong)RACSubject *chooseSignal;
@property (nonatomic ,strong)id target;
@end
