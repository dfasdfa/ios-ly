//
//  YLCEasyImageListModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/16.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCEasyImageListModel : NSObject
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_imgsrc;
@property (nonatomic ,copy)NSString *classify_name;
@property (nonatomic ,copy)NSString *is_sys;
@property (nonatomic ,assign)BOOL isSelect;
@end
