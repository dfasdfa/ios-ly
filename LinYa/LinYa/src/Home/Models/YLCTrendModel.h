//
//  YLCTrendModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/28.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCTrendModel : NSObject
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *publish_id;
@property (nonatomic ,copy)NSString *time_txt;
//发布类型 ：1贴子,2问答,3物品,4招聘,5培训,6活动,7小视频, 8 h5网页
@property (nonatomic ,copy)NSString *type;
@property (nonatomic ,copy)NSString *user_id;
@end
