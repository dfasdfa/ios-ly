//
//  YLCAdvertiseModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCRecruitModel : NSObject
/**
 发布者ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 企业名
 */
@property (nonatomic ,copy)NSString *company_name;
/**
 主图片对应的ID
 */
@property (nonatomic ,copy)NSString *img_id;
@end
