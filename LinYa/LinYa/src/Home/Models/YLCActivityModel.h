//
//  YLCActivityModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCActivityModel : NSObject
@property (nonatomic ,assign)BOOL hasLoad;
@property (nonatomic ,strong)NSAttributedString *htmlAttr;
@property (nonatomic ,copy)NSString *id;
/**
 发布者ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 发布者昵称
 */
@property (nonatomic ,copy)NSString *user_name;
/**
 活动海报
 */
@property (nonatomic ,copy)NSString *poster;
/**
 活动标题
 */
@property (nonatomic ,copy)NSString *title;
/**
 活动介绍
 */
@property (nonatomic ,copy)NSString *content;
/**
 添加时间
 */
@property (nonatomic ,copy)NSString *add_time;
/**
 开始时间
 */
@property (nonatomic ,copy)NSString *begin_time;
/**
 结束时间
 */
@property (nonatomic ,copy)NSString *end_time;
/**
 活动地区编码
 */
@property (nonatomic ,copy)NSString *location_id;
/**
 地区
 */
@property (nonatomic ,copy)NSString *place;
@property (nonatomic ,copy)NSString *area;
/**
 费用
 */
@property (nonatomic ,copy)NSString *fee;
/**
 活动联系人联系方式
 */
@property (nonatomic ,copy)NSString *contact1;
/**
 活动联系人
 */
@property (nonatomic ,copy)NSString *contact_name;
/**
 是否推荐
 */
@property (nonatomic ,copy)NSString *is_recommand;
/**
 报名人数
 */
@property (nonatomic ,copy)NSString *count;

/**
 报名人数列表
 */
@property (nonatomic ,strong)NSArray *enrollList;
/**
 是否收藏
 */
@property (nonatomic ,copy)NSString *is_collect;
/**
 是否报名
 */
@property (nonatomic ,copy)NSString *is_enroll;
/**
 是否是访问者发布
 */
@property (nonatomic ,copy)NSString *is_self;
/**
 分享内容
 */
@property (nonatomic ,copy)NSString *share_content;
/**
 分享图片
 */
@property (nonatomic ,copy)NSString *share_img;
/**
 分享链接
 */
@property (nonatomic ,copy)NSString *share_link;
/**
 分享标题
 */
@property (nonatomic ,copy)NSString *share_title;
- (void)downLoadHTMLAttrWithComplete:(void (^)(void))complete;
@end
