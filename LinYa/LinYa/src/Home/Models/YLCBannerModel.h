//
//  YLCBannerModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCBannerModel : NSObject
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *device;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *link;
@property (nonatomic ,copy)NSString *seque;
@property (nonatomic ,copy)NSString *site;
@property (nonatomic ,copy)NSString *title;
@end
