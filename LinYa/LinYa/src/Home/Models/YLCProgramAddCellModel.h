//
//  YLCProgramAddCellModel.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger ,YLCCellType){
    YLCFiedCellType,
    YLCSelectCellType,
    YLCCommondityCellType,
    YLCStarCellType,
    YLCTextViewCellType,
    YLCPhotoAddCellType,
    YLCSubFieldCellType,
    YLCTimeCellType,
    YLCDoubleSelectType
};
@interface YLCProgramAddCellModel : NSObject
@property (nonatomic ,assign)YLCCellType cellType;
@property (nonatomic ,strong)id cellModel;
@property (nonatomic ,assign)NSInteger cellIndex;
@end
