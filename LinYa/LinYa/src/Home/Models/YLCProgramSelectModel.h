//
//  YLCProgramSelectModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCProgramSelectModel : NSObject
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *subString;
@property (nonatomic ,copy)NSString *placeholder;
@property (nonatomic ,strong)UIColor *titleColor;
@property (nonatomic ,strong)UIColor *subColor;
@property (nonatomic ,strong)NSArray *selectList;
@property (nonatomic ,copy)NSString *key;
@property (nonatomic ,assign)NSInteger selectIndex;
@property (nonatomic ,strong)id subData;
@end
