//
//  YLCEasyProgramListModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/13.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCEasyProgramListModel : NSObject
/**
 产品ID
 */
@property (nonatomic ,copy)NSString *product_id;
/**
 产品名
 */
@property (nonatomic ,copy)NSString *name;
/**
 产品图片
 */
@property (nonatomic ,copy)NSString *imgsrc;
/**
 特点
 */
@property (nonatomic ,copy)NSString *feature;
/**
 保质期
 */
@property (nonatomic ,copy)NSString *period;
/**
 补贴
 */
@property (nonatomic ,copy)NSString *subsidy;
/**
 星级
 */
@property (nonatomic ,copy)NSString *star;
/**
 用户ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 简介
 */
@property (nonatomic ,copy)NSString *goodDescription;
/**
 分类ID
 */
@property (nonatomic ,copy)NSString *classify_id;
/**
 特点
 */
@property (nonatomic ,copy)NSString *seque;
/**
 单位
 */
@property (nonatomic ,copy)NSString *unit;
/**
 项目列表
 */
@property (nonatomic ,strong)NSArray *options;
/**
 案例列表
 */
@property (nonatomic ,strong)NSArray *caseList;
@property (nonatomic ,copy)NSString *people_num;
@end
