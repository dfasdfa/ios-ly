//
//  YLCInviteModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/9.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCInviteModel : NSObject
/**
 公司logo
 */
@property (nonatomic ,copy)NSString *company_logo;
/**
 公司名
 */
@property (nonatomic ,copy)NSString *company_name;
/**
 企业规模
 */
@property (nonatomic ,copy)NSString *company_size;
/**
 职位描述
 */
@property (nonatomic ,copy)NSString *content;
/**
 教育
 */
@property (nonatomic ,copy)NSString *education;
@property (nonatomic ,copy)NSString *education_name;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *img_id;
@property (nonatomic ,copy)NSString *img_num;
@property (nonatomic ,copy)NSString *imglist;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *location_id;
@property (nonatomic ,copy)NSString *location_name;
@property (nonatomic ,copy)NSString *main_img;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *place;
/**
 职位
 */
@property (nonatomic ,copy)NSString *position;
/**
 薪资区间
 */
@property (nonatomic ,copy)NSString *salary;
@property (nonatomic ,copy)NSString *salary_name;
@property (nonatomic ,copy)NSString *tags;
/**
 置顶时间
 */
@property (nonatomic ,copy)NSString *top_time;
/**
 职位类别
 */
@property (nonatomic ,copy)NSString *type;
/**
 发布者ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 访问次数
 */
@property (nonatomic ,copy)NSString *visit_num;
/**
 福利
 */
@property (nonatomic ,strong)NSArray *welfare;
/**
 福利列表
 */
@property (nonatomic ,strong)NSArray *welfare_arr;
/**
 工作年限
 */
@property (nonatomic ,copy)NSString *work_year;
@property (nonatomic ,copy)NSString *workyear_name;
@end
