//
//  YLCCompanyRecruitModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCCompanyRecruitModel : NSObject
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *company_name;
@property (nonatomic ,copy)NSString *company_logo;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *img_num;
@property (nonatomic ,strong)NSArray *imglist;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *location_id;
@property (nonatomic ,copy)NSString *location_name;
@property (nonatomic ,copy)NSString *main_img;
@property (nonatomic ,copy)NSString *nick_name;
@property (nonatomic ,copy)NSString *place;
@property (nonatomic ,copy)NSString *position;
@property (nonatomic ,copy)NSString *salary;
@property (nonatomic ,copy)NSString *salary_name;
@property (nonatomic ,copy)NSString *top_time;
@property (nonatomic ,copy)NSString *type;
@property (nonatomic ,copy)NSString *update_time;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *visit_num;
@property (nonatomic ,strong)NSArray *welfare;
@property (nonatomic ,strong)NSArray *welfare_arr;
@end
