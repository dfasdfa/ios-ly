//
//  YLCToplineModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCToplineModel : NSObject
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *banner_id;
@property (nonatomic ,copy)NSString *class_txt;
@property (nonatomic ,copy)NSString *comment_num;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_img;
@property (nonatomic ,copy)NSString *is_recommand;
@property (nonatomic ,copy)NSString *source;
@property (nonatomic ,copy)NSString *source_link;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *view_num;
@property (nonatomic ,copy)NSString *url;
@end
