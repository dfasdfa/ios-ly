//
//  YLCFillMessageModel.h
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCFillMessageModel : NSObject
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *placeholder;
@property (nonatomic ,copy)NSString *message;
@property (nonatomic ,assign)BOOL isRight;
@property (nonatomic ,assign)BOOL isNumber;
@end
