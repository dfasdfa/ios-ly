//
//  YLCVideoLifeModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCVideoLifeModel : NSObject
@property (nonatomic ,copy)NSString *add_time;
/**
 头像
 */
@property (nonatomic ,copy)NSString *avatar_img;
/**
 评论数
 */
@property (nonatomic ,copy)NSString *comment_num;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_img;
@property (nonatomic ,copy)NSString *location;
@property (nonatomic ,copy)NSString *played_num;
@property (nonatomic ,copy)NSString *site;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *top_num;
@property (nonatomic ,copy)NSString *type;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *verify_time;
@property (nonatomic ,copy)NSString *nick_name;
@end
