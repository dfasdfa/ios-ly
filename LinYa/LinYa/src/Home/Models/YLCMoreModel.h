//
//  YLCMoreModel.h
//  LinYa
//
//  Created by 初程程 on 2018/6/4.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCMoreModel : NSObject
@property (nonatomic ,copy)NSString *category_icon;
@property (nonatomic ,copy)NSString *category_name;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,assign)BOOL isImage;
@end
