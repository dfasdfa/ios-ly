//
//  YLCTitleAndIconModel.h
//  YouLe
//
//  Created by 初程程 on 2018/1/10.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCTitleAndIconModel : NSObject
@property (nonatomic ,copy)NSString *iconName;
@property (nonatomic ,copy)NSString *iconPath;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *subText;
@end
