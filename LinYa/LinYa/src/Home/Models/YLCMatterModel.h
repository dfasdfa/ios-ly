//
//  YLCMatterModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCMatterModel : NSObject
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *author;
@property (nonatomic ,copy)NSString *category_id;
@property (nonatomic ,copy)NSString *coin;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *down_num;
@property (nonatomic ,copy)NSString *down_site;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_recommand;
@property (nonatomic ,copy)NSString *publish_time;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *url;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *user_img;
@property (nonatomic ,copy)NSString *user_name;

@end
