//
//  YLCVideoModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCHomeVideoModel : NSObject
@property (nonatomic ,copy)NSString *add_time;
@property (nonatomic ,copy)NSString *author;
@property (nonatomic ,copy)NSString *begin;
@property (nonatomic ,copy)NSString *class_txt;
@property (nonatomic ,copy)NSString *coin;
@property (nonatomic ,copy)NSString *comment_num;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *is_img;
@property (nonatomic ,copy)NSString *live_site;
@property (nonatomic ,copy)NSString *played_num;
@property (nonatomic ,copy)NSString *site;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *top_num;
@property (nonatomic ,copy)NSString *id;
@end
