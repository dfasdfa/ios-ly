//
//  YLCActivityModel.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCActivityModel.h"

@implementation YLCActivityModel

- (void)downLoadHTMLAttrWithComplete:(void (^)(void))complete{
    __block NSMutableAttributedString *attr = [NSMutableAttributedString new];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *headString = @"<head><style>img{max-width:200px !important;}</style></head>";
        NSString *contentString = [NSString stringWithFormat:@"%@%@",headString,self.content];
        attr =  [[NSMutableAttributedString alloc] initWithData:[contentString dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.htmlAttr = attr;
            self.hasLoad = YES;
            complete();
        });
    });
}
@end
