//
//  YLCVideoTrainModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/13.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCVideoTrainModel : NSObject
/**
 视频ID
 */
@property (nonatomic ,copy)NSString *id;
/**
 视频标题
 */
@property (nonatomic ,copy)NSString *title;
/**
 作者
 */
@property (nonatomic ,copy)NSString *author;
/**
 视频封面
 */
@property (nonatomic ,copy)NSString *imgsrc;
/**
 视频介绍
 */
@property (nonatomic ,copy)NSString *content;
/**
 视频源
 */
@property (nonatomic ,copy)NSString *site;
/**
 分类（1 培训视频  5 直播培训）
 */
@property (nonatomic ,copy)NSString *video_class;
/**
 大于0收费
 */
@property (nonatomic ,copy)NSString *coin;
/**
 播放次数
 */
@property (nonatomic ,copy)NSString *played_num;
/**
 点赞数量
 */
@property (nonatomic ,copy)NSString *top_num;
/**
 视频封面图片是否有效
 */
@property (nonatomic ,copy)NSString *is_img;
@end
