//
//  YLCGoodsModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/12.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCGoodsModel : NSObject
/**
 时间
 */
@property (nonatomic ,copy)NSString *add_time;
/**
 物品ID
 */
@property (nonatomic ,copy)NSString *goodsId;
/**
 发布者ID
 */
@property (nonatomic ,copy)NSString *user_id;
/**
 发布者头像
 */
@property (nonatomic ,copy)NSString *imgsrc;
/**
 发布者昵称
 */
@property (nonatomic ,copy)NSString *nick_name;
/**
 物品名称
 */
@property (nonatomic ,copy)NSString *title;
/**
 主图
 */
@property (nonatomic ,copy)NSString *main_img;
/**
 价格
 */
@property (nonatomic ,copy)NSString *price;
/**
 原价
 */
@property (nonatomic ,copy)NSString *old_price;
/**
 进价
 */
@property (nonatomic ,copy)NSString *cost_price;
/**
 标签
 */
@property (nonatomic ,copy)NSString *tags;
/**
 介绍
 */
@property (nonatomic ,copy)NSString *goodsDescription;
/**
 地区编码
 */
@property (nonatomic ,copy)NSString *location_id;
/**
 分类
 */
@property (nonatomic ,copy)NSString *type;
/**
 评论数
 */
@property (nonatomic ,copy)NSString *comment_num;
/**
 是否上架
 */
@property (nonatomic ,copy)NSString *is_publish;
@property (nonatomic ,copy)NSString *is_collect;
@property (nonatomic ,copy)NSString *is_follow;
/**
 上架时间
 */
@property (nonatomic ,copy)NSString *publish_time;
@property (nonatomic ,copy)NSString *share_link;
@property (nonatomic ,copy)NSString *share_title;
/**
 物品配图列表
 */
@property (nonatomic ,strong)NSArray *img_src;
@property (nonatomic ,copy)NSString *area;
@property (nonatomic ,strong)NSArray *imglist;
@property (nonatomic ,copy)NSString *user_img;
@end
