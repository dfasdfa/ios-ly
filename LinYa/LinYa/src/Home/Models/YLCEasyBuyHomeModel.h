//
//  YLCEasyBuyHomeModel.h
//  LinYa
//
//  Created by 初程程 on 2018/3/13.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCEasyBuyHomeModel : NSObject
/**
 分类ID
 */
@property (nonatomic ,copy)NSString *classify_id;
/**
 分类名称
 */
@property (nonatomic ,copy)NSString *classify_name;
/**
 分类图标
 */
@property (nonatomic ,copy)NSString *classify_img;
/**
 标题图片
 */
@property (nonatomic ,copy)NSString *title_img;
/**
 父级分类
 */
@property (nonatomic ,copy)NSString *parent_id;
/**
 排序
 */
@property (nonatomic ,copy)NSString *seque;
/**
 是否有图标
 */
@property (nonatomic ,copy)NSString *is_classfiy_img;
/**
 是否有标题图标
 */
@property (nonatomic ,copy)NSString *is_title_img;
/**
 父级分类名称
 */
@property (nonatomic ,copy)NSString *parent_name;
@end
