//
//  YLCLiveModel.h
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YLCLiveModel : NSObject
@property (nonatomic ,copy)NSString *author;
@property (nonatomic ,copy)NSString *begin;
/**
 费用
 */
@property (nonatomic ,copy)NSString *coin;
@property (nonatomic ,copy)NSString *comment_num;
@property (nonatomic ,copy)NSString *content;
@property (nonatomic ,copy)NSString *id;
@property (nonatomic ,copy)NSString *imgsrc;
@property (nonatomic ,copy)NSString *live_site;
@property (nonatomic ,copy)NSString *played_num;
@property (nonatomic ,copy)NSString *title;
@property (nonatomic ,copy)NSString *user_id;
@property (nonatomic ,copy)NSString *top_num;
@end
