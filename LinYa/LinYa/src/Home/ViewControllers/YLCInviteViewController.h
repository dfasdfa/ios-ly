//
//  YLCInviteViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCInviteViewController : YLCBaseViewController
@property (nonatomic ,assign)BOOL isMine;
@property (nonatomic ,assign)NSInteger type; //0为发布的职业  1为投递的职业
@end
