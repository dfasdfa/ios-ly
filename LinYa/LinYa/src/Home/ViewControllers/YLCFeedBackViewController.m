//
//  YLCFeedBackViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFeedBackViewController.h"
#import "YLCFillImageCell.h"
#import "YLCInputInfoCell.h"
#import "YLCTitleFieldCell.h"
#import "YLCButtonFootView.h"
#import "YLCProgramPhotoInsertCell.h"
#import "HXPhotoTools.h"
#define IMAGE_CELL  @"YLCFillImageCell"
#define INPUT_CELL  @"YLCInputInfoCell"
#define FIELD_CELL  @"YLCTitleFieldCell"
#define BUTTON_FOOT @"YLCButtonFootView"
#define PHOTO_CELL  @"YLCProgramPhotoInsertCell"
@interface YLCFeedBackViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCProgramPhotoInsertCellDelegate,YLCInputInfoCellDelegate,YLCTitleFieldCellDelegate>

@end

@implementation YLCFeedBackViewController
{
    UICollectionView *feedTable;
    CGFloat photoHeight;
    NSString *feedBackString;
    NSString *phoneNum;
    NSArray *photoList;
    NSInteger photoNum;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    phoneNum = @"";
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    feedTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    feedTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [feedTable registerClass:[YLCFillImageCell class] forCellWithReuseIdentifier:IMAGE_CELL];
    
    [feedTable registerClass:[YLCInputInfoCell class] forCellWithReuseIdentifier:INPUT_CELL];
    
    [feedTable registerClass:[YLCProgramPhotoInsertCell class] forCellWithReuseIdentifier:PHOTO_CELL];
    
    [feedTable registerClass:[YLCTitleFieldCell class] forCellWithReuseIdentifier:FIELD_CELL];
    
    [feedTable registerClass:[YLCButtonFootView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT];
    
    feedTable.delegate = self;
    
    feedTable.dataSource = self;
    
    [self.view addSubview:feedTable];
    
    [feedTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        YLCFillImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:IMAGE_CELL forIndexPath:indexPath];
        
        return cell;
    }
    if (indexPath.row==1) {
        YLCInputInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:INPUT_CELL forIndexPath:indexPath];
        
        cell.placeholder = @"请提出您的宝贵意见，我们会在第一时间反馈";
        
        cell.delegate = self;
        
        cell.text = feedBackString;
        
        return cell;
    }
    if (indexPath.row==2) {
        YLCProgramPhotoInsertCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PHOTO_CELL forIndexPath:indexPath];
        
        cell.delegate = self;
        
        return cell;
    }
    YLCTitleFieldCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:FIELD_CELL forIndexPath:indexPath];
    
    cell.delegate = self;
    
    cell.titleField.placeholder = @"请留下您的手机号(选填)";
    
    cell.titleField.text = phoneNum;
    
    cell.titleField.leftViewMode = UITextFieldViewModeNever;
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.view.frameWidth, 123);
    }
    if (indexPath.row==1) {
        return CGSizeMake(self.view.frameWidth, 168);
    }
    if (indexPath.row==2) {
        return CGSizeMake(self.view.frameWidth,photoHeight+20);
    }
    return CGSizeMake(self.view.frameWidth, 44);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind==UICollectionElementKindSectionFooter) {
        YLCButtonFootView *footView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:BUTTON_FOOT forIndexPath:indexPath];
        
        [footView.footBtn addTarget:self action:@selector(postFeed) forControlEvents:UIControlEventTouchUpInside];
        
        return footView;
    }
    return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(self.view.frameWidth, 60);
}
- (void)postFeed{
    if (JK_IS_STR_NIL(feedBackString)) {
        [SVProgressHUD showErrorWithStatus:@"请输入意见内容"];
        return;
    }
    if (photoNum<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择照片"];
        return;
    }
    
    if (photoList.count<photoNum) {
        [SVProgressHUD showErrorWithStatus:@"请等待照片加载完成"];
        return;
    }
    
    NSMutableArray *photoBase = [NSMutableArray arrayWithCapacity:0];
    
    for (UIImage *photo in photoList) {
        
        NSString *photoString = [photo base64String];
        
        [photoBase addObject:[NSString stringWithFormat:@"data:image/jpeg;base64,%@",photoString]];
        
    }
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"contact":phoneNum,@"content":feedBackString,@"imgData":photoBase.copy} method:@"POST" urlPath:@"app/question/doSave" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"提交成功！"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - CellDelegate
- (void)changeFrameWithHeight:(CGFloat)height{
    photoHeight = height;
    [feedTable reloadData];
}
- (void)insertPhotoWithPhotos:(NSArray *)photos{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    photoNum = photos.count;
    [SVProgressHUD show];
    for (HXPhotoModel *model in photos) {
        [HXPhotoTools getHighQualityFormatPhoto:model.asset size:model.imageSize succeed:^(UIImage *image) {
            [array addObject:image];
            photoList = array.copy;
            [SVProgressHUD dismiss];
        } failed:^{
            [SVProgressHUD dismiss];
        }];
    }
}
//意见内容
- (void)didInfoChangedWithText:(NSString *)text{
    feedBackString = text;
}
//手机号（选填）
- (void)titleDidChangeWithTitle:(NSString *)title{
    phoneNum = title;
}
- (NSString *)title{
    return @"意见反馈";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
