//
//  YLCRegisPersonViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/4/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCRegisPersonViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *personList;
@property (nonatomic ,copy)NSString *activityId;
@property (nonatomic ,assign)NSInteger type;//0是培训报名列表 1是活动列表
@end
