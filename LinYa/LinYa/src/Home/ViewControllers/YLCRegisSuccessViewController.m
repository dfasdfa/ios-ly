//
//  YLCRegisSuccessViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRegisSuccessViewController.h"

@interface YLCRegisSuccessViewController ()

@end

@implementation YLCRegisSuccessViewController
{
    UIImageView *headImageView;
    UILabel *tipLabel;
    UIButton *finishBtn;
    UILabel *btnTitleLabel;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    headImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"regis_success"]];
    
    [self.view addSubview:headImageView];
    
    tipLabel = [YLCFactory createLabelWithFont:14];
    
    tipLabel.attributedText = _tipAttr;
    
    tipLabel.textAlignment = NSTextAlignmentCenter;
    
    tipLabel.numberOfLines = 0;
    
    [self.view addSubview:tipLabel];
    
    finishBtn = [YLCFactory createBtnWithImage:@"success_confirm_btn"];
    
    finishBtn.contentMode = UIViewContentModeScaleToFill;
    
    [finishBtn addTarget:self action:@selector(finishTouch) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:finishBtn];
    
    btnTitleLabel = [YLCFactory createLabelWithFont:15 color:RGB(215, 215, 215)];
    
    btnTitleLabel.text = @"知道了";
    
    [self.view addSubview:btnTitleLabel];
    
    WS(weakSelf)
    
    [headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.view.mas_centerX);
        make.top.equalTo(weakSelf.view.mas_top).offset(50);
        make.width.equalTo(190);
        make.height.equalTo(160);
    }];
    
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headImageView.mas_centerX);
        make.top.equalTo(headImageView.mas_bottom).offset(20);
        make.width.equalTo(210);
        make.height.greaterThanOrEqualTo(@10);
    }];
    
    [btnTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.view.mas_bottom).offset(-60);
        make.centerX.equalTo(headImageView.mas_centerX);
    }];
    
    [finishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(headImageView.mas_centerX);
        make.bottom.equalTo(btnTitleLabel.mas_top).offset(-10);
        make.width.height.equalTo(70);
    }];
}
- (void)finishTouch{
    [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-3] animated:YES];
}
- (NSString *)title{
    return @"报名成功";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
