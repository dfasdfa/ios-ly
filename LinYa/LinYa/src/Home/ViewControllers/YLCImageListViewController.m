//
//  YLCIimageListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/15.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCImageListViewController.h"
#import "KIImagePager.h"
#import "YLCEasyImageListModel.h"
#import "YLCEasyImageAddViewController.h"
@interface YLCImageListViewController ()<KIImagePagerDelegate,KIImagePagerDataSource>

@end

@implementation YLCImageListViewController
{
    KIImagePager *bannerView;
    NSArray *imgList;
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"添加图集" titleColor:[UIColor whiteColor] selector:@selector(gotoAddImageList) target:self];
}
- (void)gotoAddImageList{
    YLCEasyImageAddViewController *con = [[YLCEasyImageAddViewController alloc] init];
    
    con.imageList = imgList;
    
    con.classifyId = _classifyId;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    bannerView = [[KIImagePager alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, 220)];
    
    bannerView.scrollViewBanner.alwaysBounceVertical = NO;
    
    bannerView.scrollView.alwaysBounceVertical = NO;
    
    bannerView.imageCounterDisabled = YES;
    
    bannerView.backgroundColor = [UIColor whiteColor];
    
    bannerView.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    
    bannerView.pageControl.pageIndicatorTintColor = [UIColor blackColor];
    
    bannerView.slideshowShouldCallScrollToDelegate = YES;
    
    bannerView.delegate = self;
    
    bannerView.dataSource = self;
    
    bannerView.pageControl.hidden = YES;
    
    [self.view addSubview:bannerView];

    [bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    UIButton *nextBtn = [YLCFactory createBtnWithImage:@"resume_arrow"];
    
    [bannerView addSubview:nextBtn];
    
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bannerView.mas_right).offset(-30);
        make.centerY.equalTo(bannerView.mas_centerY);
        make.width.equalTo(7);
        make.height.equalTo(13);
    }];
    
    
    
//    [bannerView.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(bannerView.mas_centerX);
//        make.bottom.equalTo(bannerView.mas_bottom).offset(-50);
//        make.width.equalTo(200);
//        make.height.equalTo(30);
//    }];
}
- (void)initDataSource{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":model.user_id,@"token":model.token,@"classifyId":_classifyId} method:@"POST" urlPath:@"app/sygt/gallery" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in list) {
            YLCEasyImageListModel *model = [YLCEasyImageListModel modelWithDict:dict];
            
            [container addObject:model];
        }
        imgList = container.copy;
        
        [bannerView reloadData];
    }];
}
- (void)imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
    
}
- (NSArray *) arrayWithImages{
    if (!JK_IS_ARRAY_NIL(imgList)) {
        NSMutableArray *bannerImageURLs = [NSMutableArray arrayWithCapacity:1];
        for (YLCEasyImageListModel *model in imgList) {

            if ([YLCFactory isStringOk:model.imgsrc]) {
                [bannerImageURLs addObject:[NSString stringWithFormat:@"%@",model.imgsrc]];
            }
        }
        return bannerImageURLs;
    }else{
        return nil;
    }
}
- (UIViewContentMode) contentModeForImage:(NSUInteger)image{
    return UIViewContentModeCenter;
}
- (UIImage *)placeHolderImageForImagePager{
    
    return [UIImage imageNamed:@"image_placeholder"];
}
- (NSString *)title{
    return @"图集";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
