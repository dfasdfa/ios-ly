//
//  YLCLiveViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCLiveViewController.h"
#import "YLCLiveCell.h"
#import "YLCLiveModel.h"
#import "YLCWebViewViewController.h"
#define LIVE_CELL  @"YLCLiveCell"
@interface YLCLiveViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCLiveViewController
{
    UICollectionView *liveTable;
    NSArray *videoList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    videoList = @[];
    self.dataArr = videoList;
    [self fetchDataWithPage:0];
}
- (void)fetchDataWithPage:(NSInteger)page{
    NSString *pageString = [NSString stringWithFormat:@"%ld",page];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"page":pageString} method:@"POST" urlPath:@"app/train/live" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            [self loadedAllData];
            return ;
        }
        NSMutableArray *container = videoList.mutableCopy;
        
        if (page==0) {
            [container removeAllObjects];
        }
        NSArray *list = responseObject;
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self loadedAllData];
        }else{
            [self resetNoMore];
        }
        
        for (NSDictionary *dict in responseObject) {
            YLCLiveModel *model = [YLCLiveModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        videoList = container.copy;
        
        self.dataArr = videoList;
        
        [liveTable reloadData];
    }];
}
- (UIScrollView *)customScrollView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    liveTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    liveTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [liveTable registerClass:[YLCLiveCell class] forCellWithReuseIdentifier:LIVE_CELL];
    
    liveTable.delegate = self;
    
    liveTable.dataSource = self;
    
    return liveTable;
}
- (void)createCustomView{
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return videoList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCLiveCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LIVE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<videoList.count) {
        
        cell.model = videoList[indexPath.row];
        
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 262);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCLiveModel *model = videoList[indexPath.row];
    
    [YLCWebViewViewController showWebViewWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.live_site]] navTitle:model.title isModel:NO from:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
