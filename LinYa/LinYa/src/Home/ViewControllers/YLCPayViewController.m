//
//  YLCPayViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/24.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCPayViewController.h"
#import "YLCPayView.h"
@interface YLCPayViewController ()
@property (nonatomic ,strong)YLCPayView *payView;
@end

@implementation YLCPayViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self createCustomView];
    
}
- (void)createCustomView{
    
    self.payView = [[YLCPayView alloc] initWithFrame:self.view.bounds];
    
    [self.view addSubview:self.payView];
    
    [self.payView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSString *)title{
    return @"购买";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
