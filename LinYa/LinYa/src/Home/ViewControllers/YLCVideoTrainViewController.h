//
//  YLCVideoTrainViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCVideoTrainViewController : YLCBaseViewController
@property (nonatomic ,assign)NSInteger type;//0是培训  1是易购通视频 2.是素材视频
@property (nonatomic ,copy)NSString *classifyId;
@end
