//
//  YLCVideoLiftInfoViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoLiftInfoViewController.h"
#import "YLCVideoLiftInfoModel.h"
#import "YLCPostInfoMainView.h"
#import "IQKeyboardManager.h"
#import "YLCDiscussModel.h"
@interface YLCVideoLiftInfoViewController ()

@end

@implementation YLCVideoLiftInfoViewController
{
    YLCVideoLiftInfoModel *infoModel;
    YLCPostInfoMainView *infoView;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [infoView videoStop];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{

    
    if (_type==1) {
        [self loadTrainData];
        [self loadTrainComment];
    }else{
        [self loadLifeData];
        [self loadVideoLiftComment];
    }
}
- (void)loadTrainData{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"videoId":_video_id} method:@"POST" urlPath:@"app/train/videoDetail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        infoModel = [YLCVideoLiftInfoModel modelWithDict:responseObject];
        
        infoModel.isTrain = YES;
        
        infoView.videoModel = infoModel;
        
        infoView.comment_num = infoView.comment_num;
    }];
}
- (void)loadLifeData{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"videoId":_video_id} method:@"POST" urlPath:@"app/videoLife/detail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        
        infoModel = [YLCVideoLiftInfoModel modelWithDict:responseObject];
        
        infoModel.isTrain = NO;
        
        infoView.videoModel = infoModel;
        
        infoView.comment_num = infoView.comment_num;
    }];
}
- (void)loadTrainComment{
    
    [YLCNetWorking loadNetServiceWithParam:@{@"videoId":_video_id,@"page":@"0"} method:@"POST" urlPath:@"app/train/commentLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            YLCDiscussModel *model = [YLCDiscussModel modelWithDict:dict];
            
            [container addObject:model];
        }
        infoView.dataList = container.copy;
    }];
}
- (void)loadVideoLiftComment{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"videoId":_video_id,@"page":@"0"} method:@"POST" urlPath:@"app/videoLife/commentLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            YLCDiscussModel *model = [YLCDiscussModel modelWithDict:dict];
            
            [container addObject:model];
        }
        infoView.dataList = container.copy;
        
        
    }];
}
- (void)createCustomView{
    infoView = [[YLCPostInfoMainView alloc] initWithFrame:CGRectZero];
    
    
    
//    if (_type==0) {
        infoView.infoState = VideoLifeState;
//    }else{
//        infoView.infoState = TrainVideoState;
//    }
    
    [self.view addSubview:infoView];
    
    [infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    [infoView.reloadSignal subscribeNext:^(id x) {
        [self initDataSource];
    }];

}
- (NSString *)title{
    return @"视频详情";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
