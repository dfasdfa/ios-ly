//
//  YLCAddEasyImageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/5/6.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCAddEasyImageViewController.h"

@interface YLCAddEasyImageViewController ()<HXPhotoViewDelegate>
@property (strong, nonatomic) HXPhotoManager *manager;
@end

@implementation YLCAddEasyImageViewController
{
    UIView *backView;
    HXPhotoView *photoView;
    NSArray *photoList;
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"提交" titleColor:[UIColor whiteColor] selector:@selector(gotoUpload) target:self];
}
- (void)gotoUpload{
    NSMutableArray *photoContainer = [NSMutableArray arrayWithCapacity:0];
    for (UIImage *image in photoList) {
        [photoContainer addObject:[NSString stringWithFormat:@"data:image/jpeg;base64,%@",[image base64String]]];
    }
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"classifyId":_classifyId,@"token":userModel.token,@"imgdata":photoContainer.copy} method:@"POST" urlPath:@"app/sygt/uploadGallery" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        [SVProgressHUD showSuccessWithStatus:@"提交成功"];
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhotoAndVideo];
    }
    return _manager;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createCustomView];
}
- (void)createCustomView{
    backView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, self.view.frameWidth, 100)];
    
    backView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:backView];
    
    photoView = [HXPhotoView photoManager:self.manager];
    //    photoView.frame = CGRectMake(kPhotoViewMargin, lineView.bottom+kPhotoViewMargin, SCREENWIDTH - kPhotoViewMargin * 2, 0);
    photoView.delegate = self;
    photoView.backgroundColor = [UIColor whiteColor];
    
    photoView.frame = CGRectMake(10, 10, self.view.frameWidth-20, 60);
    
    [backView addSubview:photoView];
}
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame{
    NSLog(@"%f",frame.size.height);
    
    backView.frame = CGRectMake(0, 20, frame.size.width+20, frame.size.height+20);
}
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal{
    NSMutableArray *photoContainer = [NSMutableArray arrayWithCapacity:0];
    for (HXPhotoModel *model in photos) {
        [HXPhotoTools getHighQualityFormatPhoto:model.asset size:model.imageSize succeed:^(UIImage *image) {
            [photoContainer addObject:image];
            photoList = photoContainer.copy;
        } failed:^{
            
        }];
    }
}
- (NSString *)title{
    return @"图解";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
