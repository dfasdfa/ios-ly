//
//  YLCVideoTrainViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoTrainViewController.h"
#import "YLCTrainVideoView.h"
#import "YLCVideoTrainModel.h"
@interface YLCVideoTrainViewController ()
@property (nonatomic ,strong)YLCTrainVideoView *videoView;
@end

@implementation YLCVideoTrainViewController
{
    NSArray *videoList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [_videoView.playerView resetPlayer];
    
}
- (void)initDataSource{
    videoList = @[];
    [self reloadDataWithPage:0];
}
- (void)reloadDataWithPage:(NSInteger)pageNum{
    if (_type==0) {
        [self reloadTrainDataWithPage:pageNum];
    }else if (_type==2){
        [self reloadMaterDataWithPage:pageNum];
    }
    else{
        [self reloadEasyDataWithPage:pageNum];
    }
}
- (void)reloadMaterDataWithPage:(NSInteger)pageNum{
    NSString *pageString = [NSString stringWithFormat:@"%ld",pageNum];

    
    [YLCNetWorking loadNetServiceWithParam:@{@"page":pageString} method:@"POST" urlPath:@"app/material/videoLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            [self.videoView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = videoList.mutableCopy;
        
        if (pageNum==0) {
            [container removeAllObjects];
        }
        
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self.videoView loadedAllData];
        }else{
            [self.videoView resetNoMore];
        }
        
        for (NSDictionary *dict in list) {
            YLCVideoTrainModel *model = [YLCVideoTrainModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        videoList = container.copy;
        
        self.videoView.videoList = videoList;
    }];

}
- (void)reloadTrainDataWithPage:(NSInteger)pageNum{
    
    NSString *pageString = [NSString stringWithFormat:@"%ld",pageNum];
    
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token,@"page":pageString} method:@"POST" urlPath:@"app/train/videoLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            [self.videoView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = videoList.mutableCopy;
        
        if (pageNum==0) {
            [container removeAllObjects];
        }
        
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self.videoView loadedAllData];
        }else{
            [self.videoView resetNoMore];
        }
        
        for (NSDictionary *dict in list) {
            YLCVideoTrainModel *model = [YLCVideoTrainModel modelWithDict:dict];
            
            [container addObject:model];
        }
        
        videoList = container.copy;
        
        self.videoView.videoList = videoList;
    }];
}
- (void)reloadEasyDataWithPage:(NSInteger)pageNum{
    
    
}
- (void)createCustomView{
    self.videoView = [[YLCTrainVideoView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:self.videoView];
    
    [self.videoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    @weakify(self);
    [self.videoView.reloadSignal subscribeNext:^(id x) {
        @strongify(self);
        [self reloadDataWithPage:[x integerValue]];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
