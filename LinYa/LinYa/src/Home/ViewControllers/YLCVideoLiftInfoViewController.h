//
//  YLCVideoLiftInfoViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/3/14.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCVideoLiftInfoViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *video_id;
@property (nonatomic ,assign)NSInteger type; //0是视频生活详情 1是培训
@end
