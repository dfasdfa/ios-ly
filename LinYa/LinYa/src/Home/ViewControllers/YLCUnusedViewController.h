//
//  YLCUnusedViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCUnusedViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *userId;
@property (nonatomic ,assign)BOOL showScreen;
@property (nonatomic ,assign)NSInteger type; //0是普通列表  2是收藏列表
@end
