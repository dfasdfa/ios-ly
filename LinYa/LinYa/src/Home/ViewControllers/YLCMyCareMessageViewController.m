//
//  YLCMyCareMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMyCareMessageViewController.h"
#import "YLCQuestionModel.h"
#import "YLCDynamicCell.h"
#import "YLCQuestionCell.h"
#import "YLCDynamicInfoViewController.h"
#define QUESTION_CELL  @"YLCQuestionCell"
#define DYNAMIC_CELL   @"YLCDynamicCell"
@interface YLCMyCareMessageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCMyCareMessageViewController
{
    
    UICollectionView *messageTable;
    
    NSArray *dataList;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initDataSource];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(0, 0, 69, 0));
    }];
    
}
- (UIScrollView *)customScrollView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    messageTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    messageTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [messageTable registerClass:[YLCQuestionCell class] forCellWithReuseIdentifier:QUESTION_CELL];
    
    [messageTable registerClass:[YLCDynamicCell class] forCellWithReuseIdentifier:DYNAMIC_CELL];
    
    messageTable.delegate = self;
    
    messageTable.dataSource = self;
    
    return messageTable;
}
- (void)initDataSource{
    self.dataArr = @[];
    [self beganRefresh];
}
- (void)fetchDataWithPage:(NSInteger)page{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    NSString *pageString = [NSString stringWithFormat:@"%ld",page];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"token":userModel.token,@"userId":userModel.user_id,@"page":pageString} method:@"POST" urlPath:@"app/user/follow" delegate:self response:^(id responseObject, NSError *error) {
        if (error || JK_IS_ARRAY_NIL(responseObject)) {
            [self loadedAllData];
            return ;
        }
        
        NSMutableArray *container = self.dataArr.mutableCopy;
        
        NSArray *list = responseObject;
        
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self loadedAllData];
        }else{
            [self resetNoMore];
        }
        
        if (page==0) {
            [container removeAllObjects];
        }
        
        for (NSDictionary *dict in responseObject) {
            
            YLCQuestionModel *model = [YLCQuestionModel modelWithDict:dict];
            
            if ([model.type isEqualToString:@"forum"]) {
                [model getHeigthWithWidth:self.view.frameWidth];
            }
            
            [container addObject:model];
        }
        
        self.dataArr = container.copy;
        
        dataList = self.dataArr;
        
        [messageTable reloadData];
        
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return dataList.count;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCQuestionModel *model = dataList[indexPath.row];
    
    if ([model.type isEqualToString:@"forum"]) {
        
        YLCDynamicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DYNAMIC_CELL forIndexPath:indexPath];
        
        cell.model = model;
        
        cell.photoList = model.imglist;
        
        return cell;
        
    }else{
        
        YLCQuestionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:QUESTION_CELL forIndexPath:indexPath];
        
        cell.model = model;
        
        [cell configUI];
        
        return cell;
        
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCQuestionModel *model = dataList[indexPath.row];
 
    if ([model.type isEqualToString:@"forum"]){

        return CGSizeMake(self.view.frameWidth, model.height+60);
        
    }else{
        
        CGFloat heigth = 120;
        CGSize questionSize = [YLCFactory getStringSizeWithString:model.desc width:self.view.frameWidth-24 font:15];
        CGSize replySize = CGSizeZero;
        if (!JK_IS_DICT_NIL(model.solve)) {
            replySize = [YLCFactory getStringSizeWithString:model.solve[@"content"] width:self.view.frameWidth-24-28 font:15];
            
            replySize.height += 34;
        }
        
        return CGSizeMake(self.view.frameWidth, heigth+questionSize.height+replySize.height);
        
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YLCQuestionModel *model = dataList[indexPath.row];
    
    YLCDynamicInfoViewController *con = [[YLCDynamicInfoViewController alloc] init];

    con.caseId = model.id;
    
    [self.navigationController pushViewController:con animated:YES];
    if ([model.type isEqualToString:@"forum"]){
        
        con.type = 5;
        
    }else{
        
        con.type = 0;

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
