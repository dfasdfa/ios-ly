//
//  YLCHomeViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCHomeViewController.h"
#import "YLCHomeMainView.h"
#import "YLCLikeButton.h"
#import "YLCTitleAndIconModel.h"
#import "YLCEasyCommunicateListViewController.h"
#import "YLCTrainbBaseViewController.h"
#import "YLCLoginManager.h"
#import "YLCBannerModel.h"
#import "YLCCompanyRecruitModel.h"
#import "YLCToplineModel.h"
#import "YLCInviteViewController.h"
#import "YLCHomeVideoModel.h"
#import "YLCTouTiaoListViewController.h"
#import "YLCChatBaseViewController.h"
#import "YLCAddressManagerViewController.h"
#import "YLCMoreViewController.h"
#import "YLCRYConversationListViewController.h"
#import "YLCTrendModel.h"
#import <RongIMKit/RongIMKit.h>
@interface YLCHomeViewController ()
@property (nonatomic ,strong)YLCHomeMainView  *mainView;
@end

@implementation YLCHomeViewController
{
    YLCLikeButton *button;
    NSArray *tipArr;
    NSArray *recommendArr;
    NSArray *hospitalArr;
    NSArray *bannerDataList;
    NSArray *recruitDataList;
    NSArray *topLineTopDataArr;
    NSArray *topNewLineArr;
    NSArray *trend_arr;
}
//- (UIBarButtonItem *)leftBarButtonItem{
//
//    return [YLCFactory createTitleWitiIconBarButtonWithImageName:@"home_sign" title:@"签到" titleColor:[UIColor whiteColor] selector:@selector(signIn) target:self];
//}
- (UIBarButtonItem *)rightBarButtonItem{
//    return [YLCFactory createTitleWitiIconBarButtonWithImageName:@"home_sign" title:@"分享" titleColor:[UIColor whiteColor] selector:@selector(shareApp) target:self];
//;
    return [YLCFactory createImageBarButtonWithImageName:@"home_sign" selector:@selector(shareApp) target:self];
}
- (void)shareApp{
    
    [YLCHUDShow showShareHUDWithContent:@"邻牙 · 高效工作，轻松生活\n汇聚30000+牙医，沟通洽谈、招聘求职、培训学习......上邻牙，更轻松。" shareLink:@"http://www.linya920.com/Nwx/download#"];
}
- (void)signIn{
    
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];

    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id} method:@"POST" urlPath:@"app/index/sign" delegate:self response:^(id responseObject, NSError *error) {
//        [YLCHUDShow showAlertViewWithMessage: title:@"提示"];
        [YLCHUDShow showSignInHUDWithIntegral:responseObject];
    }];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:YLC_THEME_COLOR];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:16], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [self.navigationController.navigationBar setTitleTextAttributes:attributes];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
    [self bindViewModel];
}
- (void)initDataSource{
    
    NSMutableArray *containerArr = [NSMutableArray arrayWithCapacity:0];
    
    NSMutableArray *recommendContainer = [NSMutableArray arrayWithCapacity:0];
    
    NSArray *iconArr = @[@"home_easy",@"home_tran",@"home_find",@"home_more"];
    
    NSArray *titleArr = @[@"易沟通",@"培训",@"招聘",@"更多"];
    
    NSArray *recommendList = @[@"闲置好物品",@"精彩的活动",@"视频好生活",@"素材基地"];
    
    NSArray *recommendSubList = @[@"淘你所爱",@"好玩有趣",@"记录生活记录美",@"海量素材库"];
    
    for (int i = 0; i<4; i++) {
        
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = titleArr[i];
        
        model.iconName = iconArr[i];
        
        [containerArr addObject:model];
        
    }
    
    for (int i = 0; i<recommendList.count; i++) {
        
        YLCTitleAndIconModel *model = [[YLCTitleAndIconModel alloc] init];
        
        model.title = recommendList[i];
        
        model.iconName = [NSString stringWithFormat:@"recommend_%d",i+1];
        
        model.subText = recommendSubList[i];
        
        [recommendContainer addObject:model];
    }
    tipArr = containerArr.copy;
    
    recommendArr = recommendContainer.copy;
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    
    NSString *appCurVersionNum = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    [YLCNetWorking loadNetServiceWithParam:@{@"versionName":appCurVersionNum,@"agent":@"ios",@"brand":@"others",@"versionCode":@"4"} method:@"POST" urlPath:@"app/public/appVersion" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSString *have_update_app = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"have_update_app"]];
        
        if ([have_update_app isEqualToString:@"1"]) {
            NSString *is_force_update = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"is_force_update"]];
            
            if ([is_force_update isEqualToString:@"1"]) {
                [YLCHUDShow showOnlyOneSelectAlertWithTarget:self Message:@"有新的版本更新了，请前去更新" leftTitle:@"确定" selector:^(NSInteger index) {
                    NSString *str = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/id%@",kAPPId];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                }];
            }else{
                [YLCHUDShow showSelectAlertWithTarget:self Message:@"有新的更新版本是否更新" leftTitle:@"确定" rightTitle:@"取消" selector:^(NSInteger index) {
                    if (index==0) {
                        NSString *str = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/id%@",kAPPId];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                    }
                }];
            }
        }else{
            
        }
    }];
    
    [self reloadData];
}
- (void)reloadData{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id} method:@"POST" urlPath:@"app/index/index" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return;
        }
        
        NSArray *bannerList = [responseObject objectForKey:@"banner_arr"];
        
        NSMutableArray *bannerContainer = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in bannerList) {
            
            YLCBannerModel *model = [YLCBannerModel modelWithDict:dict] ;
            
            [bannerContainer addObject:model];
        }
        
        bannerDataList = bannerContainer.copy;
        
        self.mainView.bannerList = bannerDataList;
        
        NSArray *recruitList = [responseObject objectForKey:@"recruit_arr"];
        
        NSMutableArray *recruitContainer = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in recruitList) {
            YLCCompanyRecruitModel *model = [YLCCompanyRecruitModel modelWithDict:dict];
            
            [recruitContainer addObject:model];
        }
        
        recruitDataList = [recruitContainer copy];
        
        self.mainView.hospitalList = recruitDataList;
        
        NSArray *toplineArr = [responseObject objectForKey:@"topline_arr"];
        
        NSArray *toplineTop = [responseObject objectForKey:@"topline_top"];
        
        NSArray *videoArr = [responseObject objectForKey:@"video_arr"];
        
        NSArray *trendArr = [responseObject objectForKey:@"trend_arr"];
        
        NSMutableArray *trendContainer = [NSMutableArray arrayWithCapacity:0];
        
        NSMutableArray *topContainer = [NSMutableArray arrayWithCapacity:0];
        
        NSMutableArray *topLineContainer = [NSMutableArray arrayWithCapacity:0];
        
        NSMutableArray *videoContainer = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in toplineTop) {
            
            YLCToplineModel *model = [YLCToplineModel modelWithDict:dict];
            
            [topContainer addObject:model];
            
        }
        
        for (NSDictionary *dict in videoArr) {
            YLCHomeVideoModel *model = [YLCHomeVideoModel modelWithDict:dict];
            
            [videoContainer addObject:model];
        }
        
        for (NSDictionary *dict in toplineArr) {
            YLCToplineModel *model = [YLCToplineModel modelWithDict:dict];
            
            [topLineContainer addObject:model];
        }
        if (!JK_IS_ARRAY_NIL(trendArr)) {
            for (NSDictionary *dict in trendArr) {
                YLCTrendModel *model = [YLCTrendModel modelWithDict:dict];
                
                [trendContainer addObject:model];
            }
        }

        trend_arr = trendContainer.copy;
        
        topLineTopDataArr = topContainer.copy;
        
        topNewLineArr = topLineContainer.copy;
        
        self.mainView.top_lineArr = topNewLineArr;
        
        self.mainView.newsList = topLineTopDataArr;

        self.mainView.videoList = videoContainer.copy;
        
        self.mainView.trendArr = trend_arr;
        
        [self.mainView endReload];
    }];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
//    [self.mainView viewDisappear];
}
- (void)createCustomView{
    
    self.mainView = [[YLCHomeMainView alloc] initWithFrame:CGRectZero];
    
    self.mainView.recommendList = recommendArr;
    
    self.mainView.tipArr = tipArr;
    
    self.mainView.target = self;
    
    self.mainView.hospitalList = recruitDataList;
    
    self.mainView.newsList = @[];
    
    [self.view addSubview:self.mainView];
    
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (void)bindViewModel{
    @weakify(self);
    [self.mainView.chooseSignal subscribeNext:^(id x) {
        @strongify(self);
        NSIndexPath *indexPath = x;
        if (indexPath.row==0) {
            [self gotoEasyTalk];
        }
        if (indexPath.row==1) {
            [self gotoTrain];
        }
        if (indexPath.row==2) {
            [self gotoInvite];
        }
        if (indexPath.row==3) {
            [self gotoMore];
        }
    }];
    [self.mainView.reloadSubject subscribeNext:^(id x) {
        @strongify(self)
        [self reloadData];
    }];
}
//更多
- (void)gotoMore{
    YLCMoreViewController *con = [[YLCMoreViewController alloc] init];

    [self.navigationController pushViewController:con animated:YES];

//    RCConversationViewController *con = [[RCConversationViewController alloc] initWithConversationType:ConversationType_PRIVATE targetId:@"28"];
    
//    con.conversationType = ConversationType_PRIVATE;
//
//    con.targetId = @"28";
    
//    con.title = @"会话";
//
//    con.hidesBottomBarWhenPushed = YES;
//
//    [self.navigationController pushViewController:con animated:YES];
    

}
//易购通
- (void)gotoEasyTalk{
    YLCEasyCommunicateListViewController *con = [[YLCEasyCommunicateListViewController alloc] init];

    [self.navigationController pushViewController:con animated:YES];
    

}
//培训列表
- (void)gotoTrain{
    
    YLCTrainbBaseViewController *con = [[YLCTrainbBaseViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
//招聘列表
- (void)gotoInvite{
    YLCInviteViewController *con = [[YLCInviteViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    return @"邻牙";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
