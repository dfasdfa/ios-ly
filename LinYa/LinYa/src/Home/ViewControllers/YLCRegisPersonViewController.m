//
//  YLCRegisPersonViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/8.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCRegisPersonViewController.h"
#import "YLCRegisNumberCell.h"
#define NUMBER_CELL @"YLCRegisNumberCell"
@interface YLCRegisPersonViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation YLCRegisPersonViewController
{
    UICollectionView *personTable;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    NSString *path = @"";
    NSDictionary *param = @{};
    if (_type==0) {
        path = @"app/train/enrolLists";
        param = @{@"activityId":_activityId};
    }else{
        path = @"app/activity/enrollList";
        param = @{@"activityId":_activityId};
    }
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        _personList = responseObject;
        [personTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 0;
    
    personTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    personTable.backgroundColor = [UIColor whiteColor];
    
    [personTable registerClass:[YLCRegisNumberCell class] forCellWithReuseIdentifier:NUMBER_CELL];
    
    personTable.delegate = self;
    
    personTable.dataSource = self;
    
    [self.view addSubview:personTable];
    
    [personTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (JK_IS_ARRAY_NIL(_personList)) {
        return 0;
    }
    return _personList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCRegisNumberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NUMBER_CELL forIndexPath:indexPath];
    
    if (indexPath.row<_personList.count) {
        cell.personDict = _personList[indexPath.row];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frameWidth, 60);
}
- (NSString *)title{
    return @"报名人数";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
