//
//  YLCTouTiaoListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTouTiaoListViewController.h"
#import "YLCTouTiaoListView.h"
#import "YLCToplineModel.h"
@interface YLCTouTiaoListViewController ()
@property (nonatomic ,strong)YLCTouTiaoListView *listView;
@end

@implementation YLCTouTiaoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    if (!_needLoad) {
        return;
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"page":@"0"} method:@"POST" urlPath:@"app/topLine/lists" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in responseObject) {
            YLCToplineModel *model = [YLCToplineModel modelWithDict:dict];
            
            [container addObject:model];
        }
        _topLineTopArr = container.copy;
        
        self.listView.topLineTopArr = _topLineTopArr;
    }];

}
- (void)createCustomView{
    self.listView = [[YLCTouTiaoListView alloc] initWithFrame:[YLCFactory normalLayoutFrame]];
    self.listView.target = self;
    self.listView.topLineTopArr = _topLineTopArr;
    self.listView.topLineBottomArr = _topLineBottomArr;
    [self.view addSubview:self.listView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
