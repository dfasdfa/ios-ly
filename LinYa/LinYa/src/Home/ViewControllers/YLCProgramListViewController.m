//
//  YLCProgramListViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/27.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramListViewController.h"
#import "YLCProgramListView.h"
#import "YLCProgramInfoViewController.h"
#import "YLCProgramAddViewController.h"
#import "YLCEasyProgramListModel.h"
#import "YLCImageListViewController.h"
@interface YLCProgramListViewController ()
@property (nonatomic ,strong)YLCProgramListView *listView;
@end

@implementation YLCProgramListViewController
{
    NSArray *programList;
    UIButton *editBtn;
    UIButton *deleteBtn;
    
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"添加项目" titleColor:RGB(255, 255, 255) selector:@selector(gotoAddProgram) target:self];
}
- (void)gotoAddProgram{
    
    YLCProgramAddViewController *con = [[YLCProgramAddViewController alloc] init];
    
    con.classifyId = _classify_id;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCustomView];
    [self bindViewModel];
}
- (void)initDataSource{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];

    [YLCNetWorking loadNetServiceWithParam:@{@"token":model.token,@"userId":model.user_id,@"classifyId":_classify_id} method:@"POST" urlPath:@"app/sygt/goodsList" delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            return;
        }
        NSArray *list = responseObject;

        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];

        for (NSDictionary *dict in list) {
            YLCEasyProgramListModel *model = [YLCEasyProgramListModel modelWithDict:dict];
            
            model.product_id = dict[@"id"];

            [container addObject:model];
        }

        programList = [container copy];

        self.listView.programList = programList;
    }];
    
}
- (void)createCustomView{
    
    self.listView = [[YLCProgramListView alloc] initWithFrame:[YLCFactory normalLayoutFrame]];
    
    self.listView.classId = _classify_id;
    
    [self.view addSubview:self.listView];
    
}
- (void)bindViewModel{
    @weakify(self);
    [self.listView.selectSignal subscribeNext:^(id x) {
        @strongify(self);
        NSIndexPath *indexPath = x;
        [self gotoProgramInfoWithIndex:indexPath.row];
    }];
}
- (void)gotoProgramInfoWithIndex:(NSInteger)index{
    YLCEasyProgramListModel *model = programList[index];
    
    YLCProgramInfoViewController *con = [[YLCProgramInfoViewController alloc] init];
    
    con.classifyId = _classify_id;
    
    con.goodsId = model.product_id;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    return @"项目";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
