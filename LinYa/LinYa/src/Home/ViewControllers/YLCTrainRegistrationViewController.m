//
//  YLCTrainRegistrationViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainRegistrationViewController.h"
#import "YLCTrainRegistrationView.h"
#import "YLCActivityModel.h"
@interface YLCTrainRegistrationViewController ()
@property (nonatomic ,strong)YLCTrainRegistrationView *regisView;
@end

@implementation YLCTrainRegistrationViewController
//- (UIBarButtonItem *)rightBarButtonItem{
//    
//    return [YLCFactory createTitleBarButtonItemWithTitle:@"筛选" titleColor:[UIColor whiteColor] selector:@selector(gotoSelect) target:self];
//}
- (void)gotoSelect{
    RACSubject *subject = [RACSubject subject];
    
    [YLCHUDShow showScreenTrainWithTitleList:@[@"费用",@"时间"] selectList:@[@[@"不限",@"免费",@"收费"],@[@"不限",@"今天",@"周末",@"近一周",@"近一月"]] signal:subject];
    [subject subscribeNext:^(id x) {
        [self reloadDataWithPage:0 fee:x[0] time:x[1]];
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    if (_viewType == 0) {
        [self reloadTrainListWithType:@"" fee:@"" beginTime:@""];
    }
    if (_viewType == 1) {
        [self reloadDataWithPage:0 fee:@"0" time:@"0"];
    }
    
}
- (void)reloadTrainListWithType:(NSString *)type
                            fee:(NSString *)fee
                      beginTime:(NSString *)beginTime
{
    
    if (JK_IS_STR_NIL(_userId)) {
        
        _userId = @"";
        
    }
    
    NSString *path = @"";
    
    NSDictionary *param = @{};
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];

    if (_type==1) {
        path = @"app/collect/lists";
        
        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"type":@"3",@"page":@"0"};
    }
    if(_type==0){
        path = @"app/train/lists";
        param =@{@"page":@"0",@"type":type,@"fee":fee,@"beginTime":beginTime,@"userId":_userId};
    }
    if (_type==2) {
        path = @"app/user/train";
        param =@{@"token":userModel.token,@"userId":userModel.user_id,@"page":@"0"};
    }
    if (_type==3) {
        path = @"app/user/myTrain";
        param =@{@"token":userModel.token,@"userId":userModel.user_id,@"page":@"0"};
    }
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSArray *list = responseObject;
        
        if (JK_IS_ARRAY_NIL(list)) {
            return;
        }
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in list) {
            
            YLCActivityModel *model = [YLCActivityModel modelWithDict:dict];
            
            [container addObject:model];
        }
        self.regisView.activityList = container.copy;
    }];
}
- (void)reloadDataWithPage:(NSInteger)pageNum
                       fee:(NSString *)fee
                      time:(NSString *)time{
    NSString *path = @"";
    NSDictionary *param = @{};
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    if (_type==1) {
        path = @"app/collect/lists";

        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"type":@"1",@"page":@"0"};
    }
    if (_type==0) {
        path = @"app/activity/lists";
        param = @{@"page":@"0",@"time":time,@"fee":fee};
    }
    if (_type==2) {
        path = @"app/user/activity";
        param =@{@"token":userModel.token,@"userId":userModel.user_id,@"page":@"0"};
    }
    if (_type==3) {
        path = @"app/user/myActivity";
        param =@{@"token":userModel.token,@"userId":userModel.user_id,@"page":@"0"};
    }
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSArray *list = responseObject;
        
        if (JK_IS_ARRAY_NIL(list)) {
            return;
        }
        
        NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in list) {
            
            YLCActivityModel *model = [YLCActivityModel modelWithDict:dict];
            
            [container addObject:model];
        }
        self.regisView.activityList = container.copy;
    }];
}
- (void)createCustomView{
    self.regisView = [[YLCTrainRegistrationView alloc] initWithFrame:[YLCFactory layoutFrame]];
    
    self.regisView.type = _viewType;
    
    [self.view addSubview:self.regisView];
}
- (NSString *)title{
    return @"活动列表";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
