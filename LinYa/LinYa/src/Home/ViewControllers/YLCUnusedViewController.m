//
//  YLCUnusedViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCUnusedViewController.h"
#import "YLCUnusedView.h"
#import "YLCGoodsModel.h"
#import "YLCScreenModel.h"
@interface YLCUnusedViewController ()
@property (nonatomic ,strong)YLCUnusedView *unsedView;
@end

@implementation YLCUnusedViewController
{
    NSArray *goodList;
    NSArray *typeList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    [self loadTypeList];
    goodList = @[];
    [self loadGoodsWithPageNumber:@"0" locationId:@"" type:@""];
}
- (void)loadTypeList{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/goods/typeLists" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        if (!JK_IS_ARRAY_NIL(responseObject)) {
            typeList = responseObject;
            self.unsedView.typeList = responseObject;
        }
    }];
}
- (void)loadGoodsWithPageNumber:(NSString *)pageNumber
                     locationId:(NSString *)locationId
                           type:(NSString *)type{
    if (JK_IS_STR_NIL(_userId)) {
        _userId = @"";
    }
    NSString *path = @"";
    NSDictionary *param = @{};
    if (_type==2) {
        path = @"app/collect/lists";
        YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
        
        param = @{@"token":userModel.token,@"userId":userModel.user_id,@"type":@"2",@"page":@"0"};
    }else{
        path = @"app/goods/lists";
        param = @{@"page":pageNumber,@"userId":_userId,@"type":type,@"locationId":locationId};
    }
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:path delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            self.unsedView.goodsList = @[];
            [self.unsedView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        NSMutableArray *container = goodList.mutableCopy;
        
        if ([pageNumber integerValue]==0) {
            
            [container removeAllObjects];
        }
        
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self.unsedView loadedAllData];
        }else{
            [self.unsedView resetNoMore];
        }
        for (NSDictionary *dict in list) {
            
            YLCGoodsModel *model = [YLCGoodsModel modelWithDict:dict];
            
            model.goodsId = dict[@"id"];
            
            model.goodsDescription = dict[@"description"];
            
            [container addObject:model];
        }
        
        goodList = container.copy;
        self.unsedView.goodsList = goodList;
        
    }];
}
- (void)createCustomView{
    self.unsedView = [[YLCUnusedView alloc] initWithFrame:CGRectZero];
    
    self.unsedView.showScreen = _showScreen;
    
    [self.view addSubview:self.unsedView];
    @weakify(self)
    [self.unsedView.classReloadSignal subscribeNext:^(id x) {
        @strongify(self)
        NSArray *dataList = x;
        YLCScreenModel *areaScreenModel = dataList[0];
        YLCScreenModel *typeScreenModel = dataList[1];
        [self loadGoodsWithPageNumber:@"0" locationId:areaScreenModel.value type:typeScreenModel.value];
    }];
    
    
    [self.unsedView.reloadSignal subscribeNext:^(id x) {
        [self loadGoodsWithPageNumber:x locationId:@"" type:@""];
    }];
    
    [self.unsedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSString *)title{
    return @"闲置物品";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
