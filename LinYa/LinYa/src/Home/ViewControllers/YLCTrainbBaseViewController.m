//
//  YLCTrainViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCTrainbBaseViewController.h"
#import "DLCustomSlideView.h"
#import "DLLRUCache.h"
#import "DLScrollTabbarView.h"
#import "YLCTrainRegistrationViewController.h"
#import "YLCVideoTrainViewController.h"
#import "YLCLiveViewController.h"
@interface YLCTrainbBaseViewController ()<DLCustomSlideViewDelegate>
@property (nonatomic ,strong)DLCustomSlideView *slideView;
@end

@implementation YLCTrainbBaseViewController
{
    NSMutableArray *itemArray_;
}
- (DLCustomSlideView *)slideView{
    if (!_slideView) {
        _slideView = [[DLCustomSlideView alloc] initWithFrame:self.view.bounds];
    }
    return _slideView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *titleArr = @[@"视频培训",@"直播培训",@"培训报名"];
    DLLRUCache *cache = [[DLLRUCache alloc] initWithCount:titleArr.count];
    DLScrollTabbarView *tabbar = [[DLScrollTabbarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 42)];
    tabbar.backgroundColor = RGB(243, 243, 241);
    tabbar.tabItemNormalColor = RGB(110, 117, 119);
    tabbar.tabItemSelectedColor = YLC_THEME_COLOR;
    tabbar.tabItemNormalFontSize = 16.0f;
    tabbar.trackColor = [UIColor clearColor];
    itemArray_ = [NSMutableArray array];
    for (int i=0; i<titleArr.count
         ; i++) {
        DLScrollTabbarItem *item = [DLScrollTabbarItem itemWithTitle:titleArr[i] width:self.view.frameWidth/titleArr.count];
        
        [itemArray_ addObject:item];
    }
    tabbar.tabbarItems = itemArray_;
    self.slideView.tabbar = tabbar;
    self.slideView.cache = cache;
    self.slideView.tabbarBottomSpacing = 5;
    self.slideView.baseViewController = self;
    self.slideView.delegate = self;
    [self.slideView setup];
    self.slideView.selectedIndex = 0;
    [self.view addSubview:self.slideView];
}
- (NSInteger)numberOfTabsInDLCustomSlideView:(DLCustomSlideView *)sender{
    return itemArray_.count;
}
- (UIViewController *)DLCustomSlideView:(DLCustomSlideView *)sender controllerAt:(NSInteger)index{
    if (index==2) {
        YLCTrainRegistrationViewController *con = [[YLCTrainRegistrationViewController alloc] init];
        con.viewType = 0;
        return con;
    }else if (index==0){
        return [[YLCVideoTrainViewController alloc] init];
    }
    else{
        return [[YLCLiveViewController alloc] init];
    }
    
}

- (void)initDataSource{
    
}
- (void)createCustomView{
    
}
- (NSString *)title{
    return @"培训";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
