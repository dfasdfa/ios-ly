//
//  YLCInviteInfoViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteInfoViewController.h"
#import "YLCInviteInfoView.h"
@interface YLCInviteInfoViewController ()
@property (nonatomic ,strong)YLCInviteInfoView *infoView;
@end

@implementation YLCInviteInfoViewController
- (UIBarButtonItem *)rightBarButtonItem{
    return [YLCFactory createImageBarButtonWithImageName:@"web_share" selector:@selector(shareLink) target:self];
}
- (void)shareLink{
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    if (JK_IS_STR_NIL(_inviteId)) {
        return;
    }
    [YLCNetWorking loadNetServiceWithParam:@{@"recruitId":_inviteId} method:@"POST" urlPath:@"app/recruit/detail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        _model = [YLCInviteModel modelWithDict:responseObject];
        
        self.infoView.model = _model;
    }];
}
- (void)createCustomView{
    self.infoView = [[YLCInviteInfoView alloc] initWithFrame:[YLCFactory layoutFrame]];
    
    self.infoView.model = _model;
    
    [self.view addSubview:_infoView];
    
    [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSString *)title{
    return @"招聘详情";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
