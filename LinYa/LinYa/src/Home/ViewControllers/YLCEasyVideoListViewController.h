//
//  YLCEasyVideoListViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/5/7.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "ZFPlayer.h"
@interface YLCEasyVideoListViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *classifyId;
@property (nonatomic, strong) ZFPlayerView *playerView;
@end
