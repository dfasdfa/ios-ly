//
//  YLCTrainRegistrationViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/1/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCTrainRegistrationViewController : YLCBaseViewController
@property (nonatomic ,assign)NSInteger viewType; //0是培训报名列表 1是活动列表
@property (nonatomic ,copy)NSString *userId;

@property (nonatomic ,assign)NSInteger type;//0是普通列表 1是收藏列表 2是报名列表 3.是我发布的列表
@end
