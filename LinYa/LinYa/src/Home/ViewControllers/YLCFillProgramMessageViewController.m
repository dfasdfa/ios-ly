//
//  YLCFillProgramMessageViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCFillProgramMessageViewController.h"
#import "YLCFillProgramMessageView.h"
#import "YLCFillMessageModel.h"
@interface YLCFillProgramMessageViewController ()
@property (nonatomic ,strong)YLCFillProgramMessageView *messageView;
@end

@implementation YLCFillProgramMessageViewController
{
    NSArray *messageList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
    
}
- (void)initDataSource{
    NSMutableArray *container = [NSMutableArray arrayWithCapacity:0];
    NSArray *titleArr = @[@"姓名",@"手机号",@"你的单位",@"职务"];
    
    for (int i = 0; i<4; i++) {
        YLCFillMessageModel *model = [[YLCFillMessageModel alloc] init];
        model.title = titleArr[i];
        model.placeholder = [NSString stringWithFormat:@"请输入%@",titleArr[i]];
        [container addObject:model];
    }
    messageList = container.copy;
}
- (void)createCustomView{
    self.messageView = [[YLCFillProgramMessageView alloc] initWithFrame:[YLCFactory layoutFrame]];
    self.messageView.fillArr = messageList;
    [self.view addSubview:self.messageView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
