//
//  YLCInviteInfoViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/23.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCInviteModel.h"
@interface YLCInviteInfoViewController : YLCBaseViewController
@property (nonatomic ,strong)YLCInviteModel *model;
@property (nonatomic ,copy)NSString *inviteId;
@end
