//
//  YLCMatterBaseViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/4/22.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMatterBaseViewController.h"
#import "DLCustomSlideView.h"
#import "DLLRUCache.h"
#import "DLScrollTabbarView.h"
#import "YLCMatterViewController.h"
#import "YLCTouTiaoListViewController.h"
#import "YLCVideoTrainViewController.h"
@interface YLCMatterBaseViewController ()<DLCustomSlideViewDelegate>
@property (nonatomic ,strong)DLCustomSlideView *slideView;

@end

@implementation YLCMatterBaseViewController
{
    DLScrollTabbarView *tabbar;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (DLCustomSlideView *)slideView{
    if (!_slideView) {
        _slideView = [[DLCustomSlideView alloc] initWithFrame:CGRectMake(0, 0, self.view.frameWidth, self.view.frameHeight)];
        
            }
    return _slideView;
}
- (void)initDataSource{
//    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/material/dataCategory" delegate:self response:^(id responseObject, NSError *error) {
//        if (error) {
//            return ;
//        }
//        categoryList = responseObject;
//
//    }];
}
- (void)createCustomView{
    
    DLLRUCache *cache = [[DLLRUCache alloc] initWithCount:6];
    tabbar = [[DLScrollTabbarView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    tabbar.backgroundColor = [UIColor whiteColor];
    tabbar.tabItemNormalColor = [UIColor blackColor];
    tabbar.tabItemSelectedColor = YLC_THEME_COLOR;
    tabbar.tabItemNormalFontSize = 14.0f;
    tabbar.trackColor = YLC_THEME_COLOR;
    self.slideView.tabbar = tabbar;
    self.slideView.cache = cache;
    self.slideView.tabbarBottomSpacing = 5;
    self.slideView.baseViewController = self;
    self.slideView.delegate = self;
    [self.view addSubview:self.slideView];
    NSMutableArray *itemArray_ = [NSMutableArray array];
    NSArray *titleArr = @[@"头条",@"资讯",@"资料",@"视频"];
    for (int i=0; i<titleArr.count; i++) {
//        NSString *dict = titleArr[i];
//        CGSize size = [YLCFactory getStringWithSizeWithString:dict[@"category_name"] height:20 font:14];
        DLScrollTabbarItem *item = [DLScrollTabbarItem itemWithTitle:[NSString stringWithFormat:@"%@", titleArr[i]] width:60];
        [itemArray_ addObject:item];
    }
    tabbar.tabbarItems = itemArray_;
    [self.slideView setup];
    self.slideView.selectedIndex = 0;
    [self.slideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (NSInteger)numberOfTabsInDLCustomSlideView:(DLCustomSlideView *)sender{
    return 4;
}

- (UIViewController *)DLCustomSlideView:(DLCustomSlideView *)sender controllerAt:(NSInteger)index{
    if (index==0) {
        YLCTouTiaoListViewController *con = [[YLCTouTiaoListViewController alloc] init];
        
        con.needLoad = YES;
        
        return con;
    }
    if (index==3) {
        YLCVideoTrainViewController *con = [[YLCVideoTrainViewController alloc] init];
        
        con.type = 2;
        
        return con;
    }
    YLCMatterViewController *con = [[YLCMatterViewController alloc] init];
    con.type = index;
    return con;
}
- (NSString *)title{
    return @"素材基地";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
