//
//  YLCTouTiaoListViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCTouTiaoListViewController : YLCBaseViewController
@property (nonatomic ,strong)NSArray *topLineTopArr;
@property (nonatomic ,strong)NSArray *topLineBottomArr;
@property (nonatomic ,assign)BOOL needLoad;
@end
