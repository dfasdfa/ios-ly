//
//  YLCInviteViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/21.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCInviteViewController.h"
#import "YLCInviteView.h"
#import "YLCInviteModel.h"
#import "YLCSendJobViewController.h"
#import "YLCScreenModel.h"
@interface YLCInviteViewController ()
@property (nonatomic ,strong)YLCInviteView *inviteView;
@end

@implementation YLCInviteViewController
{
    NSArray *inviteList;
    NSArray *salaryList;
    NSArray *screenDataList;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initDataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    [self initDataSource];
    [self createCustomView];
}
- (YLCInviteView *)inviteView{
    if (!_inviteView) {
        _inviteView = [[YLCInviteView alloc] initWithFrame:CGRectZero];
    }
    return _inviteView;
}
- (void)initDataSource{
    YLCScreenModel *sexModel = [[YLCScreenModel alloc] init];
    
    sexModel.title = @"区域";
    
    sexModel.showType = 1;
    
    sexModel.value = @"";
    
    YLCScreenModel *salaryModel = [[YLCScreenModel alloc] init];
    
    salaryModel.title = @"月薪";
    
    salaryModel.screenList = @[];
    
    salaryModel.value = @"";
    
    YLCScreenModel *timeModel = [[YLCScreenModel alloc] init];
    
    timeModel.title = @"发布时间";
    
    timeModel.screenList = @[@"不限",@"一周",@"一个月",@"三个月"];
    
    timeModel.value = @"";
    
    screenDataList = @[sexModel,salaryModel,timeModel];

    inviteList = @[];
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/recruit/salary" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        salaryList = responseObject;
        _inviteView.salaryList = salaryList;
        NSMutableArray *salaryNameContainer = [NSMutableArray arrayWithCapacity:0];
        
        for (NSDictionary *dict in salaryList) {
            [salaryNameContainer addObject:dict[@"salary_name"]];
        }
        YLCScreenModel *salaryModel = screenDataList[1];
        
        salaryModel.screenList = salaryNameContainer.copy;
        
        NSMutableArray *screenContainer = screenDataList.mutableCopy;
        
        [screenContainer setObject:salaryModel atIndexedSubscript:1];
        
        screenDataList = screenContainer.copy;
        
        self.inviteView.screenList = screenDataList;
    }];
//    [self reloadDataWithPage:0 location:@"" salary:@"" time:@""];
    [self.inviteView beganRefresh];
}
- (UIBarButtonItem *)rightBarButtonItem{
    
    return [YLCFactory createTitleBarButtonItemWithTitle:@"发布" titleColor:[UIColor whiteColor] selector:@selector(gotoSend) target:self];
}
- (void)gotoSend{
    YLCSendJobViewController *con = [[YLCSendJobViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)reloadDataWithPage:(NSInteger)page
                  location:(NSString *)locationId
                    salary:(NSString *)salary
                      time:(NSString *)time
{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];

    NSString *url = _isMine?@"app/user/recruit":@"app/recruit/lists";
    
    if (_type==1) {
        url = @"app/user/deliverResume";
    }
    
    NSDictionary *param = _isMine?@{@"userId":userModel.user_id,@"token":userModel.token,@"page":pageString,@"locationId":locationId,@"salary":salary,@"time":time}:@{@"page":pageString,@"locationId":locationId,@"salary":salary,@"time":time};
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:url delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            self.inviteView.inviteList = @[];
            [self.inviteView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = inviteList.mutableCopy;
        
        if (page==0) {
            
            [container removeAllObjects];
            
        }
        for (NSDictionary *dict in responseObject) {
            YLCInviteModel *model = [YLCInviteModel modelWithDict:dict];
            if (_type==1) {
                model = [YLCInviteModel modelWithDict:dict[@"recruit"]];
            }
            [container addObject:model];
        }
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self.inviteView loadedAllData];
        }else{
            [self.inviteView resetNoMore];
        }
        inviteList = container.copy;
        
        self.inviteView.inviteList = inviteList;
    }];
}
- (void)createCustomView{
    
    
    self.inviteView.screenList = screenDataList;
    self.inviteView.isMine = _isMine;
    WS(weakSelf)
    [self.inviteView.reloadSignal subscribeNext:^(id x) {
        if ([x integerValue]==0) {
            [weakSelf reloadDataWithPage:[x integerValue] location:@"" salary:@"" time:@""];
        }else{
            
            YLCScreenModel *areaModel = screenDataList[0];
            YLCScreenModel *salaryModel = screenDataList[1];
            YLCScreenModel *timeModel = screenDataList[2];
            [weakSelf reloadDataWithPage:[x integerValue] location:areaModel.value salary:salaryModel.value time:timeModel.value];
        }

    }];
    
    [self.inviteView.conditionReloadSignal subscribeNext:^(id x) {
        NSArray *screenList = x;
        YLCScreenModel *areaModel = screenList[0];
        YLCScreenModel *salaryModel = screenList[1];
        YLCScreenModel *timeModel = screenList[2];
        screenDataList = x;
        [weakSelf reloadDataWithPage:0 location:areaModel.value salary:salaryModel.value time:timeModel.value];
    }];
    
    [self.view addSubview:self.inviteView];
    
    [self.inviteView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(UIEdgeInsetsZero);
        
    }];
    

}
- (NSString *)title{
    return @"招聘";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
