//
//  YLCVideoLifeViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"

@interface YLCVideoLifeViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *userId;
@property (nonatomic ,assign)BOOL isMine;
@end
