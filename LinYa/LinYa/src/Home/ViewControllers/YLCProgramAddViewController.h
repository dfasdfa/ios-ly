//
//  YLCProgramAddViewController.h
//  LinYa
//
//  Created by 初程程 on 2018/2/2.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCBaseViewController.h"
#import "YLCEasyProgramListModel.h"
@interface YLCProgramAddViewController : YLCBaseViewController
@property (nonatomic ,copy)NSString *classifyId;
@property (nonatomic ,copy)NSString *goodsId;
@property (nonatomic ,assign)BOOL isEdit;
@property (nonatomic ,assign)YLCEasyProgramListModel *programModel;

@end
