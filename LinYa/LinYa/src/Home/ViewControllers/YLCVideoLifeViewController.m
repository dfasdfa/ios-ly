//
//  YLCVideoLifeViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/25.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCVideoLifeViewController.h"
#import "YLCVideoLiftView.h"
#import "YLCVideoLifeModel.h"
@interface YLCVideoLifeViewController ()
@property (nonatomic ,strong)YLCVideoLiftView *videoView;
@end

@implementation YLCVideoLifeViewController
{
    NSArray *videoList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    videoList = @[];
    [self loadDataWithKey:@""];
}
- (void)loadDataWithKey:(NSString *)key{
    [self reloadWithPageNumber:@"0"];
}
- (void)reloadWithPageNumber:(NSString *)pageNumber{
    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    if (JK_IS_STR_NIL(_userId)) {
        _userId = @"";
    }
    NSString *url = @"";
    NSDictionary *param = @{};
    if (_isMine) {
        url = @"app/user/videoLife";
        param = @{@"page":pageNumber,@"userId":userModel.user_id,@"token":userModel.token,@"key":@"",@"userId":_userId};
    }else{
        url = @"app/videoLife/lists";
        param = @{@"page":pageNumber,@"key":@"",@"userId":_userId};
    }
    
    [YLCNetWorking loadNetServiceWithParam:param method:@"POST" urlPath:url delegate:self response:^(id responseObject, NSError *error) {
        if (error||JK_IS_ARRAY_NIL(responseObject)) {
            [self.videoView loadedAllData];
            return ;
        }
        NSArray *list = responseObject;
        
        NSMutableArray *container = videoList.mutableCopy;
        
        if ([pageNumber integerValue]==0) {
            [container removeAllObjects];
        }
        if (list.count<[PAGE_LIMIT integerValue]) {
            [self.videoView loadedAllData];
        }else{
            [self.videoView resetNoMore];
        }
        for (NSDictionary *dict in list) {
            YLCVideoLifeModel *model = [YLCVideoLifeModel modelWithDict:dict];
            
            [container addObject:model];
        }
        videoList = container.copy;
        
        self.videoView.videoList = videoList;
    }];
}
- (void)createCustomView{
    
    self.videoView = [[YLCVideoLiftView alloc] initWithFrame:self.view.bounds];
    
    [self.view addSubview:self.videoView];
    
    [self.videoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    [self.videoView.reloadSignal subscribeNext:^(id x) {
        [self reloadWithPageNumber:x];
    }];
}
- (NSString *)title{
    return @"视频生活";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
