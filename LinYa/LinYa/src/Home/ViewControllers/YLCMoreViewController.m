//
//  YLCMoreViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/3/30.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCMoreViewController.h"
#import "YLCFeedBackViewController.h"
#import "YLCMoreModel.h"
#import "YLCMoreCell.h"
#import "YLCMatterViewController.h"
#define MORE_CELL  @"YLCMoreCell"
@interface YLCMoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,YLCMoreCellDelegate>

@end

@implementation YLCMoreViewController
{
    UICollectionView *moreTable;
    NSArray *dataList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
//    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frameWidth, 80)];
//
//    backView.backgroundColor = [UIColor whiteColor];
//
//    [self.view addSubview:backView];
//
//    UIButton *feedBackBtn = [YLCFactory createBtnWithImage:@"home_more" title:@"意见反馈" titleColor:[UIColor blackColor] frame:CGRectMake(0, 0, 60, 60)];
//
//    [feedBackBtn addTarget:self action:@selector(gotoFeedBack) forControlEvents:UIControlEventTouchUpInside];
//
//    [backView addSubview:feedBackBtn];
//
//    [feedBackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.view.mas_left).offset(15);
//        make.centerY.equalTo(backView.mas_centerY);
//        make.width.height.equalTo(60);
//    }];
}
- (void)initDataSource{
    [YLCNetWorking loadNetServiceWithParam:@{} method:@"POST" urlPath:@"app/material/dataCategory" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        NSMutableArray *baseContainer = [NSMutableArray arrayWithCapacity:0];
        YLCMoreModel *feedModel = [[YLCMoreModel alloc] init];
        
        feedModel.category_icon = @"home_more";
        
        feedModel.category_name = @"意见反馈";
        
        feedModel.isImage = YES;
        
        [baseContainer addObject:@[feedModel]];
        
        for (NSDictionary *dict in responseObject) {
            NSMutableArray *listContainer = [NSMutableArray arrayWithCapacity:0];
            for (NSDictionary *dic in dict[@"list"]) {
                YLCMoreModel *model = [YLCMoreModel modelWithDict:dic];
                
                [listContainer addObject:model];
            }
            [baseContainer addObject:listContainer];
        }
        dataList = baseContainer.copy;
        
        [moreTable reloadData];
//        typeList = responseObject;
//
//        [typeTable reloadData];
    }];
}
- (void)createCustomView{
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    flow.minimumLineSpacing = 10;
    
    moreTable = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    
    moreTable.backgroundColor = YLC_COMMON_BACKCOLOR;
    
    [moreTable registerClass:[YLCMoreCell class] forCellWithReuseIdentifier:MORE_CELL];
    
    moreTable.delegate = self;
    
    moreTable.dataSource = self;
    
    [self.view addSubview:moreTable];
    
    [moreTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    YLCMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MORE_CELL forIndexPath:indexPath];
    
    if (indexPath.row<dataList.count) {
        cell.moreList = dataList[indexPath.row];
    }
    cell.section = indexPath.row;
    cell.delegate = self;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return CGSizeMake(self.view.frameWidth, 80);
    }
    return CGSizeMake(self.view.frameWidth, 220);
}

- (void)didMoreSelectAtIndexPath:(NSIndexPath *)indexPath
                         section:(NSInteger)section{
    if (section==0) {
        [self gotoFeedBack];
    }else{
        [self gotoMatterListWithIndexPath:indexPath section:section];
    }
}
- (void)gotoMatterListWithIndexPath:(NSIndexPath *)indexPath
                            section:(NSInteger)section{
    NSArray *list = dataList[section];
    
    YLCMoreModel *moreModel = list[indexPath.row];
    
    YLCMatterViewController *con = [[YLCMatterViewController alloc] init];
    
    con.type = 3;
    
    con.categoryId = moreModel.id;
    
    [self.navigationController pushViewController:con animated:YES];
}
- (void)gotoFeedBack{
    YLCFeedBackViewController *con = [[YLCFeedBackViewController alloc] init];
    
    [self.navigationController pushViewController:con animated:YES];
}
- (NSString *)title{
    return @"更多";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
