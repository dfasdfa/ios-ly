//
//  YLCProgramInfoViewController.m
//  LinYa
//
//  Created by 初程程 on 2018/2/1.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "YLCProgramInfoViewController.h"
#import "YLCProgramInfoView.h"
#import "YLCEasyProgramListModel.h"
@interface YLCProgramInfoViewController ()

@end

@implementation YLCProgramInfoViewController
{
    YLCEasyProgramListModel *infoModel;
    YLCProgramInfoView *infoView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initDataSource];
    [self createCustomView];
}
- (void)initDataSource{
    YLCAccountModel *model = [[YLCAccountManager shareManager] currentAccount];
    [YLCNetWorking loadNetServiceWithParam:@{@"userId":model.user_id,@"classifyId":_classifyId,@"token":model.token,@"goodsId":_goodsId} method:@"POST" urlPath:@"app/sygt/goodsDetail" delegate:self response:^(id responseObject, NSError *error) {
        if (error) {
            return ;
        }
        infoModel = [YLCEasyProgramListModel modelWithDict:responseObject];
        
        infoModel.caseList = responseObject[@"case"];
        
        infoModel.goodDescription = responseObject[@"description"];
        
        infoModel.product_id = responseObject[@"id"];
        
        infoView.infoModel = infoModel;
    }];
}
- (void)createCustomView{
    infoView = [[YLCProgramInfoView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:infoView];
    
    [infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsZero);
    }];
}
- (void)bindViewModel{
    
}
- (NSString *)title{
    return @"项目详情";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
