//
//  AppDelegate.m
//  LinYa
//
//  Created by 初程程 on 2018/1/18.
//  Copyright © 2018年 初程程. All rights reserved.
//

#import "AppDelegate.h"
#import "YLCTabBarViewController.h"
#import "YLCTabBarConfig.h"
#import "IQKeyboardManager.h"
#import "SVProgressHUD.h"
#import "YLCAccountManager.h"
#import <RongIMLib/RongIMLib.h>
#import <RongIMKit/RongIMKit.h>
#import "WXApi.h"
#import "RYMessageManager.h"
@interface AppDelegate ()<WXApiDelegate>

@end

@implementation AppDelegate
+ (AppDelegate *)appDelegate {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    return appDelegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    YLCTabBarViewController *tabBarController = [YLCTabBarConfig configController];
    
    self.window.rootViewController = tabBarController;
    
    [tabBarController hiddenRed];
    
    [self keyBoardSet];
    [self confirmSVP];
    [self loadArea];
    [self rongyunConfig];
    [self wxRegis];
    [self loginHX];
//    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
    

    return YES;
}
- (void)loginHX{
//    YLCAccountModel *userModel = [[YLCAccountManager shareManager] currentAccount];
//
//    if ([YLCFactory isStringOk:userModel.user_id]&&[YLCFactory isStringOk:userModel.token]) {
//        [YLCNetWorking loadNetServiceWithParam:@{@"userId":userModel.user_id,@"token":userModel.token} method:@"POST" urlPath:@"app/public/getRongToken" delegate:self response:^(id responseObject, NSError *error) {
//            if (error) {
//                return ;
//            }
            [[RYMessageManager shareManager] rongyunLogin];
//        }];
//
//    }
}
- (void)wxRegis{
    [WXApi registerApp:WXAppid];
}
- (void)loadArea{
    [[YLCAccountManager shareManager] loadLocationData];
}
- (void)rongyunConfig{
    
    [[RCIMClient sharedRCIMClient] initWithAppKey:RongYunKey];
    
    [[RYMessageManager shareManager] observerMessage];
    
    [RCIM sharedRCIM].enableMessageAttachUserInfo = YES;

}
- (void)keyBoardSet{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = YES;
}
- (void)confirmSVP{
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setMinimumDismissTimeInterval:2];
    [SVProgressHUD setMaximumDismissTimeInterval:2];
    [SVProgressHUD setMaxSupportedWindowLevel:UIWindowLevelStatusBar];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation{
    return [WXApi handleOpenURL:url delegate:[YLCWxPayManager shareManager]];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return [WXApi handleOpenURL:url delegate:[YLCWxPayManager shareManager]];
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""]
                        stringByReplacingOccurrencesOfString:@">"
                        withString:@""] stringByReplacingOccurrencesOfString:@" "
                       withString:@""];
    
    [[RCIMClient sharedRCIMClient] setDeviceToken:token];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
